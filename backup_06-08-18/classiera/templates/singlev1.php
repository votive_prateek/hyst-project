<!-- post title-->


<!-- FlexSlider -->
<script defer src="<?php echo get_template_directory_uri() ?>/js/flexslider/jquery.flexslider.js"></script>
<?php 
global $redux_demo;
$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
global $post;
$postID = '';
$current_user = wp_get_current_user();
$edit_post_page_id = $redux_demo['edit_post'];
$postID = $post->ID;
global $wp_rewrite;
if ($wp_rewrite->permalink_structure == ''){
	$edit_post = $edit_post_page_id."&post=".$postID;
}else{
	$edit_post = $edit_post_page_id."?post=".$postID;
}
/*PostMultiCurrencycheck*/
$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
if(!empty($post_currency_tag)){
	$classieraCurrencyTag = classiera_Display_currency_sign($post_currency_tag);
}else{
	global $redux_demo;
	$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
}
/*PostMultiCurrencycheck*/
?>
<div class="single-post-title">
	<div class="post-price hide">
		<?php $post_price = get_post_meta($post->ID, 'post_price', true);  ?>
		<h4>
			<?php 
			if(is_numeric($post_price)){
				echo classiera_post_price_display($post_currency_tag, $post_price);
			}else{ 
				echo $post_price; 
			}
			?>
		</h4>
	</div>
	<h4 class="text-uppercase">
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		<?php 
		if($post->post_author == $current_user->ID && get_post_status ( $post->ID ) == 'publish'){		
		
			
				$user = wp_get_current_user();
				$roles = $user->roles;
				if(in_array('inventory_user',$roles))
				{?>
					
					<a href="<?php echo site_url().'/edit-inventory/?post='.$postID; ?>" class="edit-post btn btn-sm btn-default">
						<i class="fa fa-pencil-square-o pull-left"></i>
						<span class="xs-hide"><?php esc_html_e( 'Edit INVENTORY', 'classiera' ); ?></span>
					</a><?php
				}
				else if(in_array('company_user',$roles))
				{?>
					
					<a href="<?php echo site_url().'/edit-inventory/?post='.$postID; ?>" class="edit-post btn btn-sm btn-default">
					<i class="fa fa-pencil-square-o pull-left"></i>
					<span class="xs-hide"><?php esc_html_e( 'Edit INVENTORY', 'classiera' ); ?></span>
				</a><?php
				}
				else
				{?>
					
					<a href="<?php echo $edit_post; ?>" class="edit-post btn btn-sm btn-default">
						<i class="fa fa-pencil-square-o pull-left"></i>
						<span class="xs-hide"><?php esc_html_e( 'Edit HYST', 'classiera' ); ?></span>
					</a><?php
				} ?>
			
			
			<?php
		}elseif( current_user_can('administrator')){
			?>
			<a href="<?php echo $edit_post; ?>" class="edit-post btn btn-sm btn-default">
				<i class="fa fa-pencil-square-o pull-left"></i>
				<span class="xs-hide"><?php esc_html_e( 'Edit HYST', 'classiera' ); ?></span>
			</a>
			<?php
		}
		?>		
		<!--Edit Ads Button-->
	</h4>
	<div class="col-sm-6 rtl">	
		<div class="post-category">
		<?php 
			$category = get_the_category();
		?>
			<p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-1.png'; ?>"/></p>
			
			<p><span>Category :  <?php classiera_Display_cat_level($post->ID); ?>
			</span></p>
		</div>
	</div>
	
	<div class="col-sm-6">	
		<div class="post-category">
		    <?php 
				//$locShownBy = $redux_demo['location-shown-by'];
				//$post_location = get_post_meta($post->ID, $locShownBy, true);
				$post_location = get_post_meta($post->ID, 'post_location', true);
			?>
			<p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-2.png'; ?>"/></p>
			
			<p><a href="#"><span>location : <?php echo $post_location; ?></span></a></p>
		</div>
	</div>

	<div class="col-sm-6 rtl">	
		<div class="post-category">
		    <?php 
			    $user_ID = $post->post_author;
				$authorName = get_the_author_meta('display_name', $user_ID );
				if(empty($authorName)){
					$authorName = get_the_author_meta('user_nicename', $user_ID );
				}
				if(empty($authorName)){
					$authorName = get_the_author_meta('user_login', $user_ID );
				}
			?>
			<?php if ( is_user_logged_in() ) { ?>
			<a href="<?php echo get_author_posts_url($user_ID); ?>"><p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-3.png'; ?>"/></p></a>
			<?php } 
			else { ?>
				<p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-3.png'; ?>"/></p></a>
			<?php } ?>
			<p>
			<?php
			if ( is_user_logged_in() ) {
			?><a href="<?php echo get_author_posts_url($user_ID); ?>"><span>User : <?php echo $authorName; ?></span></a><?php
			}
			else
			{
			?><span>User : <?php echo $authorName; ?></span><?php
			}?>
			</p>
		</div>
	</div>
	
	<div class="col-sm-6">	
		<div class="post-category">
			<p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-4.png'; ?>"/></p>
			
			<p><a href="#"><span>User Price : <b><?php 
				if(is_numeric($post_price)){
					echo classiera_post_price_display($post_currency_tag, $post_price);
				}else{ 
					echo $post_price; 
				}
			?></b></span></a></p>
		</div>
	</div>
	
	
	
	
</div>
<div class="clearfix"></div>
<div class="top-buffer2"></div>

<div class="single_description">
	<div class="single_title">
		 <h2>description</h2>
	</div>
	<div class="top-buffer2"></div>
    <div class="inner-addon flex">
    	<p class="form-control form-control-sm text" id="description"><?php echo $post->post_content;?></p>
	</div>	
</div>
<div class="top-buffer2"></div>

<div class="image_section">
	<div class="single_title">
		 <h2>images</h2>
	</div>




<!-- post title-->
<!-- single post carousel-->
<?php 
		$attachments = get_children(array('post_parent' => $post->ID,
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'order' => 'ASC',
			'orderby' => 'menu_order ID'
			)
		);
?>
<?php
	if ( has_post_thumbnail() || !empty($attachments)) { ?>
		<!-- Demo CSS -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/flexslider/flexslider.css" type="text/css" media="screen" />

		<div id="main" role="main" class="custom-fxslider">
			<section class="slider">
				<?php if(count($attachments)>1) {  
					$count = 1; ?>
					<div id="slider" class="flexslider">
						<ul class="slides">
							<?php foreach($attachments as $att_id => $attachment) {
								$full_img_url = wp_get_attachment_url($attachment->ID); ?>
								<li data-thumb="<?php echo $full_img_url; ?>">
									<img src="<?php echo $full_img_url; ?>" />
								</li>
							<?php
								$count++;
							} ?>
						</ul>
					</div>
					<div id="carousel" class="flexslider">
	          			<ul class="slides">
							<?php foreach($attachments as $att_id => $attachment) {
								$full_img_url = wp_get_attachment_url($attachment->ID); ?>
								<li>
									<img src="<?php echo $full_img_url; ?>" />
								</li>
							<?php
								$count++;
							} ?>
						</ul>
	        		</div>
				<?php } else {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
					?>
					<div class="single-slide-Item"> <!-- SLIDE ITEM -->
						<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">          
					</div>
				<?php }	?>
			</section>
		</div>

		<script type="text/javascript">
		jQuery(document).ready(function($) {
		    // The slider being synced must be initialized first
			jQuery('#carousel').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 247,
			itemMargin: 10,
			asNavFor: '#slider'
			});

			jQuery('#slider').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			sync: "#carousel"
			});
		});
		</script>
<?php } else { ?>
	<div id="main" role="main" class="custom-fxslider">
		<section class="slider">
			<div class="flexslider">
				<ul class="slides">
					<?php $image = get_template_directory_uri().'/images/nothumb.png'; ?>
					<div class="single-slide-Item"> <!-- SLIDE ITEM -->
						<img src="<?php echo $image; ?>" alt="nothumb">
					</div>
				</ul>
			</div>
		</section>
	</div>
<?php } ?>				
<!-- single post carousel-->