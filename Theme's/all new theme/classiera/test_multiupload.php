<?php
/**
 * Template name: Test Multiupload
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage classiera
 * @since classiera 1.0
 */
 
 get_header();
 //echo 'i am here';
 ?>
 <style>
 #jquery-script-menu {
position: fixed;
height: 90px;
width: 100%;
top: 0;
left: 0;
border-top: 5px solid #316594;
background: #fff;
-moz-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
-webkit-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
z-index: 999999;
padding: 10px 0;
-webkit-box-sizing:content-box;
-moz-box-sizing:content-box;
box-sizing:content-box;
}

.jquery-script-center {
width: 960px;
margin: 0 auto;
}
.jquery-script-center ul {
width: 212px;
float:left;
line-height:45px;
margin:0;
padding:0;
list-style:none;
}
.jquery-script-center a {
	text-decoration:none;
}
.jquery-script-ads {
width: 728px;
height:90px;
float:right;
}
.jquery-script-clear {
clear:both;
height:0;
}

 </style>
 <?php //echo 'line 60'?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/bootstrap.min.css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 <?php //echo 'line 63';?>
<link href="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/dist/bootstrap-FileUpload.css" rel="stylesheet" type="text/css">
<div class="container">
    <h1>jQuery Bootstrap FileUpload Basic Demo</h1>
    <div class="fileupload-wrapper"><div id="myUpload"></div></div>
  </div>
 
  <script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/jquery-1.12.4.min.js" ></script>
  <script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/dist/bootstrap-FileUpload.js"></script>
<script>
  jQuery("#myUpload").bootstrapFileUpload({
  url: "<?php echo site_url();?>/ajax_multiupload.php"
});
</script>
 <?php
 
  get_footer();
 
 ?>