<?php
/**
* Template name: Home Test page
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Classiera
 * @since Classiera 1.0
 */
get_header(); ?>
	
<?php
$wp_user_search = new WP_User_Query( array('role' => 'Subscriber'));
$admins = $wp_user_search->get_results();
$admin_ids = array();
foreach($admins as $admin) {
    $admin_ids[] = $admin->ID;
}
$args = implode(',', $admin_ids);
$wpquery = query_posts("author=$args&posts_per_page=-1");
?>
<?php
$wp_user_search2 = new WP_User_Query( array('role__in' => array('inventory_user','company_user')));
$admins2 = $wp_user_search2->get_results();
$admin_ids2 = array();
foreach($admins2 as $admin2) {
    $admin_ids2[] = $admin2->ID;
}
$args2 = implode(',', $admin_ids2);
$wpquery2 = query_posts("author=$args2&posts_per_page=-1");
//echo "<pre>";print_r($wpquery2);
?>
<?php 
	global $redux_demo;
	$classieraMAPStyle = $redux_demo['map-style'];	
	$classieraMAPPostType = $redux_demo['classiera_map_post_type'];	
	$classieraMAPPostCount = $redux_demo['classiera_map_post_count'];	
	$category_icon_code = "";
	$category_icon_color = "";
	$catIcon = "";
	$$iconPath = "";
	
	$classieraPriceRange = $redux_demo['classiera_pricerange_on_off'];
	$classieraPriceRangeStyle = $redux_demo['classiera_pricerange_style'];
	$postCurrency = $redux_demo['classierapostcurrency'];
	$classieraMultiCurrency = $redux_demo['classiera_multi_currency'];
		$classieraTagDefault = $redux_demo['classiera_multi_currency_default'];
	$classieraMaxPrice = $redux_demo['classiera_max_price_input'];
	$classieraLocationSearch = $redux_demo['classiera_search_location_on_off'];
	$locationsStateOn = $redux_demo['location_states_on'];
	$locationsCityOn= $redux_demo['location_city_on'];
	
	if($classieraMultiCurrency == 'multi'){
		$classieraPriceTagForSearch = classiera_Display_currency_sign($classieraTagDefault);
	}elseif(!empty($postCurrency) && $classieraMultiCurrency == 'single'){
		$classieraPriceTagForSearch = $postCurrency;
	}	
?>

<link rel="stylesheet" href="https://hyst2.temp.co.za/mymaps/leaflet.css" />
<!-- <script src="https://hyst2.temp.co.za/mymaps/jquery-1.11.0.min.js"></script> -->
<script src="https://hyst2.temp.co.za/mymaps/leaflet.js"></script>
<link href="https://hyst2.temp.co.za/mymaps/mapstyle.css" rel="stylesheet">
<style type="text/css">
	.div-visible-pos2 {
		visibility: hidden;
		opacity: 0;
	}
	.div-visible-pos2 {
		height:0px !important;
	}
	.div-position {
	   position: absolute !important;
	   top: 0px;
	}
</style>
<section id="classiera_map">
	<div id="log" style="display:none;"></div>
	<input id="latitude" type="hidden" value="">
	<input id="longitude" type="hidden" value="">
	<div class="toggle-comp"><button id="toggle_comp">Company/Inventor</button></div>
	<div id="classiera_main_map" class="div-visible-pos" style="width:100%; height:600px;">
		<script type="text/javascript">			
			jQuery(document).ready(function(){
				var addressPoints = [
					<?php 
					$wp_query= null;
					//
					$arags = array(
						'author' => $args,
						'posts_per_page' => $classieraMAPPostCount,
					);
					$wp_query = new WP_Query($arags);
					
					while ($wp_query->have_posts()) : $wp_query->the_post();
					$iconPath=array();
					$found_latitude=array();
					$found_latitude=array();
					$mycustcount=0;
				//	empty($iconPath);
				//	empty($found_latitude);
				//	empty($found_latitude);
						$category = get_the_category();
						$catID = $category[0]->cat_ID;
						if ($category[0]->category_parent == 0){
							$tag = $category[0]->cat_ID;
							$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
							if (isset($tag_extra_fields[$tag])) {
								$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
								$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
								$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
							}
						}elseif(isset($category[1]->category_parent) && $category[1]->category_parent == 0){
							$tag = $category[0]->category_parent;
							$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
							if (isset($tag_extra_fields[$tag])) {
								$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
								$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
								$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
							}
						}else{
							$tag = $category[0]->category_parent;
							$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
							if (isset($tag_extra_fields[$tag])) {
								$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
								$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
								$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
							}
						}
						if(!empty($category_icon_code)){
							$category_icon = stripslashes($category_icon_code);
						}						
						$post_latitude = get_post_meta($post->ID, 'post_latitude', true);
						$post_longitude = get_post_meta($post->ID, 'post_longitude', true);
						$post_price = get_post_meta($post->ID, 'post_price', true);
						$post_phone = get_post_meta($post->ID, 'post_phone', true);
						$theTitle = get_the_title();
						$postCatgory = get_the_category( $post->ID );
						$postCurCat = $postCatgory[0]->name;
						$categoryLink = get_category_link($catID);
						$classieraPostAuthor = $post->post_author;
						$classieraAuthorEmail = get_the_author_meta('user_email', $classieraPostAuthor);
						$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);
						$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
						if(is_numeric($post_price)){
							$classieraPostPrice =  classiera_post_price_display($post_currency_tag, $post_price);
						}else{ 
							$classieraPostPrice =  $post_price; 
						}
						if( has_post_thumbnail()){
							$classieraIMG = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'classiera-370');
							$classieraIMGURL = $classieraIMG[0];
						}else{
							$classieraIMGURL = get_template_directory_uri() . '/images/nothumb.png';
						}
						if(empty($classieraCatIcoIMG)){
							//$iconPath = get_template_directory_uri() .'/images/abc.png';
							
								$getfoundslocations=$wpdb->get_results("select * from wp_found_hysts where hyst_id='".$post->ID."' and is_url=0");
							//	print_r($getfoundslocations);exit;
								if(count($getfoundslocations)>0)
								{
									foreach($getfoundslocations as $key=>$val)
									{
										if($getfoundslocations[$key]->confirmed_by_owner==1 && $getfoundslocations[$key]->confirmed_by_user3==1 && $getfoundslocations[$key]->confirmed_by_user4==1)
										{
											$iconPath[$mycustcount]=get_template_directory_uri().'/../classiera-child/images/green.png';
										}
										else if($getfoundslocations[$key]->confirmed_by_owner==2 && $getfoundslocations[$key]->confirmed_by_user3==2 && $getfoundslocations[$key]->confirmed_by_user4==2)
										{
											$iconPath[$mycustcount]=get_template_directory_uri().'/../classiera-child/images/red.png';
										}
										else
										{
											$iconPath[$mycustcount]=get_template_directory_uri().'/../classiera-child/images/yellow.png';
										}
										$found_latitude[$mycustcount]=$getfoundslocations[$key]->found_latitude;
										$found_longitude[$mycustcount]=$getfoundslocations[$key]->found_longitude;
										$found_price[$mycustcount]=$getfoundslocations[$key]->found_price;
										$mycustcount++;
									}
									$foundhyst=1;
								}
								else
								{
									$foundhyst=0;
									//$iconPath[] = get_template_directory_uri().'/../classiera-child/images/looking_for_marker.png';
									//$iconPath_1= get_template_directory_uri().'/../classiera-child/images/looking_for_marker.png';
									
									//$found_latitude[]=$post_latitude;
										//$found_longitude[]=$post_longitude;
										//$found_price[]= $classieraPostPrice;
										$iconPath_1 = get_template_directory_uri().'/../classiera-child/images/looking_for_marker.png';
								}
							}else{
								//$iconPath = $classieraCatIcoIMG;
								$iconPath_1 = $classieraCatIcoIMG;
								$found_price[]=$classieraPostPrice;
								
								$found_latitude[]=$post_latitude;
										$found_longitude[]=$post_longitude;
							}
						/*else{
							$iconPath = $classieraCatIcoIMG;
						}*/						
						
					//if(!empty($post_latitude)){	
					if($foundhyst==1){	
					/*$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$classieraPostPrice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';
					?>
					
					[<?php echo $post_latitude; ?>, <?php echo $post_longitude; ?>, '<?php echo $content; ?>', "<?php echo $iconPath; ?>"],
					
					<?php */
					
					//for($lp=0;$lp<count($iconPath);$lp++)
					for($lp=0;$lp<count($mycustcount);$lp++)
						{
							if($found_price[$lp]=='')
							{
								$dispprice=$classieraPostPrice;
							}
							else
							{
								$dispprice=$found_price[$lp];
							}
							$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$dispprice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';?>
							<?php /*['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php*/
							if($found_latitude[$lp]!='' && $found_longitude[$lp]!='')
							{
							?>
							['<?php echo $found_latitude[$lp]; ?>', '<?php echo $found_longitude[$lp]; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php
							}
							else
							{?>
								['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php
							}
							/*else
							{?>
								['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php
							}*/
							
							
						}
					}
					else
					{
								$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$classieraPostPrice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';
					?>
					
					['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath_1; ?>"],<?php
					}
					endwhile;
					wp_reset_query();
					?>
				];
				var mapopts;
				if(window.matchMedia("(max-width: 1024px)").matches){
					var mapopts =  {
						dragging:false,
						tap:false,
					};					
				};
				var map = L.map('classiera_main_map', {zoomControl: false},mapopts).setView([0,0],1);
		
				map.dragging.disable;
				map.scrollWheelZoom.disable();
			
				L.control.zoom({
     			position:'bottomleft'
				}).addTo(map);
	
				var roadMutant = L.gridLayer.googleMutant({
				
				<?php if($classieraMAPStyle){?>styles: <?php echo $classieraMAPStyle; ?>,<?php }?>
					maxZoom: 13,
					type:'roadmap',
					zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.LEFT_CENTER
                }
				}).addTo(map);
				var markers = L.markerClusterGroup({
					spiderfyOnMaxZoom: true,
					showCoverageOnHover: true,
					zoomToBoundsOnClick: true,
					maxClusterRadius: 10
				});
				markers.on('clusterclick', function(e) {
					map.setView(e.latlng, 13);				
				});			
				/*my code start*/
				if ("geolocation" in navigator){
		
						navigator.geolocation.getCurrentPosition(function(position){
								infoWindow = new google.maps.InfoWindow({map: map});
								var pos = {lat: position.coords.latitude, lng: position.coords.longitude};
								
								
								
								/*infoWindow.setPosition(pos);
								infoWindow.setContent("Found your location <br />Lat : "+position.coords.latitude+" </br>Lang :"+ position.coords.longitude);
								map.panTo(pos);
								*/
								
							
							//var latitude = lat: position.coords.latitude;
							//var longitude = lng: position.coords.longitude;
							map.setView(pos,18);	
								
								
							});
					}else{
						console.log("Browser doesn't support geolocation!");
				}
				/*my code end*/
				var markerArray = [];
				for (var i = 0; i < addressPoints.length; i++){
					var a = addressPoints[i];
					var newicon = new L.Icon({iconUrl: a[3],
						iconSize: [36, 36], // size of the icon
						//iconAnchor: [20, 10], // point of the icon which will correspond to marker's location
						iconAnchor: [23, 36], // point of the icon which will correspond to marker's location
						popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor                                 
					});
					var title = a[2];
					var marker = L.marker(new L.LatLng(a[0], a[1]));
					marker.setIcon(newicon);
					marker.bindPopup(title, {minWidth:"400"});
					marker.title = title;
					//marker.on('click', function(e) {
						//map.setView(e.latlng, 13);
						
					//});				
					markers.addLayer(marker);
					markerArray.push(marker);
					if(i==addressPoints.length-1){//this is the case when all the markers would be added to array
						var group = L.featureGroup(markerArray); //add markers array to featureGroup
						map.fitBounds(group.getBounds());   
					}
				}
				var circle;
				map.addLayer(markers);
				function getLocation(){
					if(navigator.geolocation){
						navigator.geolocation.getCurrentPosition(showPosition);
					}else{
						x.innerHTML = "Geolocation is not supported by this browser.";
					}
				}
					
				
				function showPosition(position){	
						
					jQuery('#latitude').val(position.coords.latitude);
					jQuery('#longitude').val(position.coords.longitude);
					var latitude = jQuery('#latitude').val();
					var longitude = jQuery('#longitude').val();
					map.setView([latitude,longitude],13);
					circle = new L.circle([latitude, longitude], {radius: 2500}).addTo(map);
				}
				jQuery('#getLocation').on('click', function(e){
					e.preventDefault();
					getLocation();
				});
				
				
				//Search on MAP//
				var geocoder;
				function initialize(){
					geocoder = new google.maps.Geocoder();     
				}		
				
						
				jQuery("#classiera_map_address").autocomplete({
				
					  //This bit uses the geocoder to fetch address values					  
					source: function(request, response){
						geocoder = new google.maps.Geocoder();
						geocoder.geocode( {'address': request.term }, function(results, status) {
							response(jQuery.map(results, function(item) {
								return {
								  label:  item.formatted_address,
								  value: item.formatted_address,
								  latitude: item.geometry.location.lat(),
								  longitude: item.geometry.location.lng()
								}
							}));
						})
					},
					  //This bit is executed upon selection of an address
					select: function(event, ui) {
						jQuery("#latitude").val(ui.item.latitude);
						jQuery("#longitude").val(ui.item.longitude);
						var latitude = jQuery('#latitude').val();
						var longitude = jQuery('#longitude').val();
						map.setView([latitude,longitude],10);						
					}
				});
				//Search on MAP//		
				
				
				jQuery(".marker-cluster").click(function(){
				         console.log('hello');	
				});
				
			});
			
			
			
		</script>
	</div>
	<div id="classiera_main_map2" class="div-visible-pos2" style="width:100%; height:600px;">
		<script type="text/javascript">			
				jQuery(document).ready(function(){
					var addressPoints = [
						<?php 
						$wp_query= null;
						//
						$arags = array(
							'author' => $args2,
							'posts_per_page' => $classieraMAPPostCount,
						);
						$wp_query = new WP_Query($arags);
						
						while ($wp_query->have_posts()) : $wp_query->the_post();
						$iconPath=array();
						$found_latitude=array();
						$found_latitude=array();
						$mycustcount=0;
					//	empty($iconPath);
					//	empty($found_latitude);
					//	empty($found_latitude);
							$category = get_the_category();
							$catID = $category[0]->cat_ID;
							if ($category[0]->category_parent == 0){
								$tag = $category[0]->cat_ID;
								$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
								if (isset($tag_extra_fields[$tag])) {
									$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
									$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
									$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
								}
							}elseif(isset($category[1]->category_parent) && $category[1]->category_parent == 0){
								$tag = $category[0]->category_parent;
								$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
								if (isset($tag_extra_fields[$tag])) {
									$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
									$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
									$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
								}
							}else{
								$tag = $category[0]->category_parent;
								$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
								if (isset($tag_extra_fields[$tag])) {
									$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
									$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
									$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
								}
							}
							if(!empty($category_icon_code)){
								$category_icon = stripslashes($category_icon_code);
							}						
							$post_latitude = get_post_meta($post->ID, 'post_latitude', true);
							$post_longitude = get_post_meta($post->ID, 'post_longitude', true);
							$post_price = get_post_meta($post->ID, 'post_price', true);
							$post_phone = get_post_meta($post->ID, 'post_phone', true);
							$theTitle = get_the_title();
							$postCatgory = get_the_category( $post->ID );
							$postCurCat = $postCatgory[0]->name;
							$categoryLink = get_category_link($catID);
							$classieraPostAuthor = $post->post_author;
							$classieraAuthorEmail = get_the_author_meta('user_email', $classieraPostAuthor);
							$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);
							$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
							if(is_numeric($post_price)){
								$classieraPostPrice =  classiera_post_price_display($post_currency_tag, $post_price);
							}else{ 
								$classieraPostPrice =  $post_price; 
							}
							if( has_post_thumbnail()){
								$classieraIMG = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'classiera-370');
								$classieraIMGURL = $classieraIMG[0];
							}else{
								$classieraIMGURL = get_template_directory_uri() . '/images/nothumb.png';
							}
							if(empty($classieraCatIcoIMG)){
								//$iconPath = get_template_directory_uri() .'/images/abc.png';
								
									$getfoundslocations=$wpdb->get_results("select * from wp_found_hysts where hyst_id='".$post->ID."' and is_url=0");
								//	print_r($getfoundslocations);exit;
									if(count($getfoundslocations)>0)
									{
										foreach($getfoundslocations as $key=>$val)
										{
											if($getfoundslocations[$key]->confirmed_by_owner==1 && $getfoundslocations[$key]->confirmed_by_user3==1 && $getfoundslocations[$key]->confirmed_by_user4==1)
											{
												$iconPath[$mycustcount]=get_template_directory_uri().'/../classiera-child/images/green.png';
											}
											else if($getfoundslocations[$key]->confirmed_by_owner==2 && $getfoundslocations[$key]->confirmed_by_user3==2 && $getfoundslocations[$key]->confirmed_by_user4==2)
											{
												$iconPath[$mycustcount]=get_template_directory_uri().'/../classiera-child/images/red.png';
											}
											else
											{
												$iconPath[$mycustcount]=get_template_directory_uri().'/../classiera-child/images/yellow.png';
											}
											$found_latitude[$mycustcount]=$getfoundslocations[$key]->found_latitude;
											$found_longitude[$mycustcount]=$getfoundslocations[$key]->found_longitude;
											$found_price[$mycustcount]=$getfoundslocations[$key]->found_price;
											$mycustcount++;
										}
										$foundhyst=1;
									}
									else
									{
										$foundhyst=0;
										//$iconPath[] = get_template_directory_uri().'/../classiera-child/images/looking_for_marker.png';
										//$iconPath_1= get_template_directory_uri().'/../classiera-child/images/looking_for_marker.png';
										
										//$found_latitude[]=$post_latitude;
											//$found_longitude[]=$post_longitude;
											//$found_price[]= $classieraPostPrice;
											$iconPath_1 = get_template_directory_uri().'/../classiera-child/images/looking_for_marker.png';
									}
								}else{
									//$iconPath = $classieraCatIcoIMG;
									$iconPath_1 = $classieraCatIcoIMG;
									$found_price[]=$classieraPostPrice;
									
									$found_latitude[]=$post_latitude;
											$found_longitude[]=$post_longitude;
								}
							/*else{
								$iconPath = $classieraCatIcoIMG;
							}*/						
							
						//if(!empty($post_latitude)){	
						if($foundhyst==1){	
						/*$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$classieraPostPrice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';
						?>
						
						[<?php echo $post_latitude; ?>, <?php echo $post_longitude; ?>, '<?php echo $content; ?>', "<?php echo $iconPath; ?>"],
						
						<?php */
						
						//for($lp=0;$lp<count($iconPath);$lp++)
						for($lp=0;$lp<count($mycustcount);$lp++)
							{
								if($found_price[$lp]=='')
								{
									$dispprice=$classieraPostPrice;
								}
								else
								{
									$dispprice=$found_price[$lp];
								}
								$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$dispprice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';?>
								<?php /*['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php*/
								if($found_latitude[$lp]!='' && $found_longitude[$lp]!='')
								{
								?>
								['<?php echo $found_latitude[$lp]; ?>', '<?php echo $found_longitude[$lp]; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php
								}
								else
								{?>
									['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php
								}
								/*else
								{?>
									['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php
								}*/
								
								
							}
						}
						else
						{
									$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$classieraPostPrice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';
						?>
						
						['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath_1; ?>"],<?php
						}
						endwhile;
						wp_reset_query();
						?>
					];
					var mapopts;
					if(window.matchMedia("(max-width: 1024px)").matches){
						var mapopts =  {
							dragging:false,
							tap:false,
						};					
					};
					var map = L.map('classiera_main_map2', {zoomControl: false},mapopts).setView([0,0],1);
			
					map.dragging.disable;
					map.scrollWheelZoom.disable();
				
					L.control.zoom({
	     			position:'bottomleft'
					}).addTo(map);
		
					var roadMutant = L.gridLayer.googleMutant({
					
					<?php if($classieraMAPStyle){?>styles: <?php echo $classieraMAPStyle; ?>,<?php }?>
						maxZoom: 13,
						type:'roadmap',
						zoomControl: true,
	                zoomControlOptions: {
	                    style: google.maps.ZoomControlStyle.SMALL,
	                    position: google.maps.ControlPosition.LEFT_CENTER
	                }
					}).addTo(map);
					var markers = L.markerClusterGroup({
						spiderfyOnMaxZoom: true,
						showCoverageOnHover: true,
						zoomToBoundsOnClick: true,
						maxClusterRadius: 10
					});
					markers.on('clusterclick', function(e) {
						map.setView(e.latlng, 13);				
					});			
					/*my code start*/
					if ("geolocation" in navigator){
			
							navigator.geolocation.getCurrentPosition(function(position){
									infoWindow = new google.maps.InfoWindow({map: map});
									var pos = {lat: position.coords.latitude, lng: position.coords.longitude};
									
									
									
									/*infoWindow.setPosition(pos);
									infoWindow.setContent("Found your location <br />Lat : "+position.coords.latitude+" </br>Lang :"+ position.coords.longitude);
									map.panTo(pos);
									*/
									
								
								//var latitude = lat: position.coords.latitude;
								//var longitude = lng: position.coords.longitude;
								map.setView(pos,18);	
									
									
								});
						}else{
							console.log("Browser doesn't support geolocation!");
					}
					/*my code end*/
					var markerArray = [];
					for (var i = 0; i < addressPoints.length; i++){
						var a = addressPoints[i];
						var newicon = new L.Icon({iconUrl: a[3],
							iconSize: [36, 36], // size of the icon
							//iconAnchor: [20, 10], // point of the icon which will correspond to marker's location
							iconAnchor: [23, 36], // point of the icon which will correspond to marker's location
							popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor                                 
						});
						var title = a[2];
						var marker = L.marker(new L.LatLng(a[0], a[1]));
						marker.setIcon(newicon);
						marker.bindPopup(title, {minWidth:"400"});
						marker.title = title;
						//marker.on('click', function(e) {
							//map.setView(e.latlng, 13);
							
						//});				
						markers.addLayer(marker);
						markerArray.push(marker);
						if(i==addressPoints.length-1){//this is the case when all the markers would be added to array
							var group = L.featureGroup(markerArray); //add markers array to featureGroup
							map.fitBounds(group.getBounds());   
						}
					}
					var circle;
					map.addLayer(markers);
					function getLocation(){
						if(navigator.geolocation){
							navigator.geolocation.getCurrentPosition(showPosition);
						}else{
							x.innerHTML = "Geolocation is not supported by this browser.";
						}
					}
						
					
					function showPosition(position){	
							
						jQuery('#latitude').val(position.coords.latitude);
						jQuery('#longitude').val(position.coords.longitude);
						var latitude = jQuery('#latitude').val();
						var longitude = jQuery('#longitude').val();
						map.setView([latitude,longitude],13);
						circle = new L.circle([latitude, longitude], {radius: 2500}).addTo(map);
					}
					jQuery('#getLocation').on('click', function(e){
						e.preventDefault();
						getLocation();
					});
					
					
					//Search on MAP//
					var geocoder;
					function initialize(){
						geocoder = new google.maps.Geocoder();     
					}		
					
							
					jQuery("#classiera_map_address").autocomplete({
					
						  //This bit uses the geocoder to fetch address values					  
						source: function(request, response){
							geocoder = new google.maps.Geocoder();
							geocoder.geocode( {'address': request.term }, function(results, status) {
								response(jQuery.map(results, function(item) {
									return {
									  label:  item.formatted_address,
									  value: item.formatted_address,
									  latitude: item.geometry.location.lat(),
									  longitude: item.geometry.location.lng()
									}
								}));
							})
						},
						  //This bit is executed upon selection of an address
						select: function(event, ui) {
							jQuery("#latitude").val(ui.item.latitude);
							jQuery("#longitude").val(ui.item.longitude);
							var latitude = jQuery('#latitude').val();
							var longitude = jQuery('#longitude').val();
							map.setView([latitude,longitude],10);						
						}
					});
					//Search on MAP//		
					
					
					jQuery(".marker-cluster").click(function(){
					         console.log('hello');	
					});
					
				});
				
				jQuery("#toggle_comp").click(function(event) {
					if (jQuery('#classiera_main_map2').hasClass('div-visible-pos2')) {
				        jQuery('#classiera_main_map2').removeClass('div-visible-pos2');
				        jQuery('#classiera_main_map2').addClass('div-position');
				    } else {
				        jQuery('#classiera_main_map2').toggleClass('div-visible-pos2');
				        jQuery('#classiera_main_map2').removeClass('div-position');
				    }
				    if (jQuery('#classiera_main_map').hasClass('div-visible-pos')) {
				        jQuery('#classiera_main_map').removeClass('div-visible-pos');
				        jQuery('#classiera_main_map').css({
				        	visibility: 'hidden',
				        	opacity: 0
				        });
				    } else {
				        jQuery('#classiera_main_map').toggleClass('div-visible-pos');
				        jQuery('#classiera_main_map').css({
				        	visibility: 'visible',
				        	opacity: 1
				        });
				    }  
				});
				
				
			</script>
	</div>
			<div class="map_search">
		     <form method="get" action="" name="frm_header_search" id="frm_header_search">
			    <div class="search_icon">
				    <div class="search-image">
				       <img src="<?=get_template_directory_uri().'/../classiera-child/images/search.png'; ?>"/>
					</div>
				</div>
				<div class="search-form">
					<div id="innerSearch" class="collapse in classiera__inner">
					     
						<div class="map_title hide">
						     <h2>search</h2>
						</div>

						<!--Username-->
						<div class="inner-search-box">
							<div class="inner-addon right-addon">
								<?php /*<input type="search" name="s" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'description', 'classiera' ); ?>">*/?>
								<input type="text" name="username" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'username', 'classiera' ); ?>">
							</div>
						</div>
						<!--Username-->				
					   
					    <!--Keywords-->
						<div class="inner-search-box">
							<div class="inner-addon right-addon">
								<?php /*<input type="search" name="s" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'description', 'classiera' ); ?>">*/?>
								<input type="text" name="s" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'description', 'classiera' ); ?>">
							</div>
						</div>
						<!--Keywords-->
						
					
						<!--Price Range-->
						<?php if($classieraPriceRange == 1){?>
						<div class="inner-search-box">
								<?php 
								if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
									//echo $postCurrency;
									$currencySign = $postCurrency;
								}elseif($classieraMultiCurrency == 'multi'){
									//echo classiera_Display_currency_sign($classieraTagDefault);
									$currencySign = classiera_Display_currency_sign($classieraTagDefault);
								}else{
									//echo "&dollar;";
									$currencySign = "&dollar;";
								}
								?>							
							<?php if($classieraPriceRangeStyle == 'slider'){?>
								<?php 
								$startPrice = $classieraMaxPrice*10/100; 
								$secondPrice = $startPrice+$startPrice; 
								$thirdPrice = $startPrice+$secondPrice; 
								$fourthPrice = $startPrice+$thirdPrice; 
								$fivePrice = $startPrice+$fourthPrice; 
								$sixPrice = $startPrice+$fivePrice; 
								$sevenPrice = $startPrice+$sixPrice; 
								$eightPrice = $startPrice+$sevenPrice; 
								$ninePrice = $startPrice+$eightPrice; 
								$tenPrice = $startPrice+$ninePrice;
								?>

								<div class="classiera_price_slider">
									<p>
									  <!--<label for="amount" style="color:#fff;"><?php // esc_html_e( 'Price range', 'classiera' ); ?>:</label>  -->
									  <input data-cursign="<?php echo $currencySign; ?>" type="hidden" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;background:none;">
									</p>					 
									<div id="slider-range-home"></div>
									<input type="hidden" id="classieraMaxPrice" value="<?php echo $classieraMaxPrice; ?>">
									<input type="hidden" id="range-first-val" name="search_min_price" value="">
									<input type="hidden" id="range-second-val" name="search_max_price" value="">
								</div>	
												
								
							<?php }else{?>
								<!--Price Range input-->
								<div class="inner-addon right-addon">
									<input type="text" name="search_min_price" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Min price', 'classiera' ); ?>">
								</div>
								<div class="inner-addon right-addon">
									<input type="text" name="search_max_price" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Max price', 'classiera' ); ?>">
								</div>
								<!--Price Range input-->
							<?php } ?>
						</div>
						<?php } ?>
						<!--Price Range-->
						
						<!--km range -->
						<div class="inner-search-box visible-xs">
							<div class="classiera_km_slider">
								
								<p>
								  <!--<label for="amount" style="color:#fff;"><?php // esc_html_e( 'Price range', 'classiera' ); ?>:</label>  -->
								  <input data-cursign="<?php echo $currencySign; ?>" type="hidden" id="amount1" readonly style="border:0; color:#f6931f; font-weight:bold;background:none;">
								</p>					 
								<div id="slider-range-km"></div>
								<?php /*<input type="hidden" id="classieraMaxPrice1" value="<?php echo $classieraMaxPrice; ?>">
								<input type="hidden" id="range-first-val1" name="search_min_price" value="">
								<input type="hidden" id="range-second-val1" name="search_max_price" value="">*/?>
								
							</div>
						</div>
						<!--km range -->
						
						<!--Locations-->
						<?php if($classieraLocationSearch == 1){?>
						<div class="inner-search-box flex">
							<!--SelectCountry-->
							<?php
							$args = array(
								'post_type' => 'countries',
								'posts_per_page'   => -1,
								'orderby'          => 'title',
								'order'            => 'ASC',
								'post_status'      => 'publish',
								'suppress_filters' => false 
							);
							$country = get_posts($args);
							if(!empty($country)){
							?>
							<div class="inner-addon right-addon">
							<!--	<i class="right-addon form-icon fa fa-sort"></i>
								<select name="post_location" class="form-control form-control-sm" id="post_location">
									<option value="-1" selected disabled>
										<?php // esc_html_e('Select Country', 'classiera'); ?>
									</option>
									<?php // foreach( $country as $singleCountry ){?>
									<option value="<?php // echo $singleCountry->ID; ?>"><?php // echo $singleCountry->post_title; ?></option>
									<?php // } ?>
								</select> -->
								
								<input type="text" name="post_location" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'location', 'classiera' ); ?>">
								
							</div>
							<?php } ?>
							 <?php wp_reset_postdata(); ?>
							<!--SelectCountry-->
							<!--Select State-->
							<?php if($locationsStateOn == 1){?>
							<div class="inner-addon right-addon post_sub_loc">
							  <!--	<i class="right-addon form-icon fa fa-sort"></i>
								<select name="post_state" class="form-control form-control-sm" id="post_state">
									<option value=""><?php // esc_html_e('Select State', 'classiera'); ?></option>
								</select>  -->
								
								<input type="text" name="post_state" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'location', 'classiera' ); ?>">
								
								
							</div>
							<?php } ?>
							<!--Select State-->
							<!--Select City-->
							<?php if($locationsCityOn == 1){?>
							<div class="inner-addon right-addon post_sub_loc">
							<!--	<i class="right-addon form-icon fa fa-sort"></i>
								<select name="post_city" class="form-control form-control-sm" id="post_city">
									<option value=""><?php // esc_html_e('Select City', 'classiera'); ?></option>
								</select>  -->
								
								<input type="text" name="post_city" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'location', 'classiera' ); ?>">
								
							</div>
							<?php } ?>
							<!--Select City-->
						   <button type="submit" name="search" class="btn btn-primary sharp btn-sm btn-style-one btn-block" value="<?php esc_html_e( 'Search', 'classiera') ?>"><?php esc_html_e( 'Search', 'classiera') ?></button>
						</div>
						<?php } ?>
						<!--Locations-->
						<!--Categories-->
					</div><!--innerSearch-->
				</div><!--search-form-->
			</form>
		</div>
	<div class="map_discription" style="background:black;">
		<ul>
		     <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/looking_for.png'; ?>"/>
		     	<span>Looking For</span>
		     </li>
			 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/green_marker.png'; ?>"/>
			 	<span>Verified</span>
			 </li>
			 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/yellow_marker.png'; ?>"/>
			 	<span>Pending</span>
			 </li>
			 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/red_marker.png'; ?>"/>
			 	<span>unconfirmed</span>
			 </li>
		</ul>
	</div>
<div class="clearfix"></div>
</section>
<?php get_footer(); ?>