<?php
/*
Plugin Name: Approve Profile Pic

*/

// Register settings using the Settings API 
 
// Modify capability
/**
 * Register a custom menu page.
 */
function wpdocs_register_my_custom_menu_page1(){
    add_menu_page( 
        __( 'Approve Profile Pic', 'textdomain' ),
        'Approve Profile Pic',
        'manage_options',
        'appprofpic',
        'my_custom_menu_page1',
        '',
        6
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page1' );
 
/**
 * Display a custom menu page
 */
function my_custom_menu_page1(){
global $wpdb;
if((isset($_GET['action']) && $_GET['action']=='approve_pic') && (isset($_GET['app_uid']) && $_GET['app_uid']!=''))
{
	 update_user_meta($_GET['app_uid'],'profile_pic_approve',1);
	 $wpdb->query("insert into wp_hyst_likes(owner_user_id, profile_pic, like_time) values('".$_GET['app_uid']."', '1', '".date("Y-m-d h:i:s")."');");
}
if((isset($_GET['action']) && $_GET['action']=='unapprove_pic') && (isset($_GET['app_uid']) && $_GET['app_uid']!=''))
{
	 update_user_meta($_GET['app_uid'],'classify_author_avatar_url','');
	  $wpdb->query("insert into wp_hyst_likes(owner_user_id, profile_pic, like_time) values('".$_GET['app_uid']."', '0', '".date("Y-m-d h:i:s")."');");
}
$args = array(
	
	'meta_key'     => 'profile_pic_approve',
	'meta_value'   => '0',
	'meta_compare' => '=',
	
 ); 
$users=get_users( $args );
//print_r($users);
?><?php
if(count($users)>0)
{
?>
	 <link href="<?php echo plugins_url();?>/graphical_analysis/plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />

        <!-- App css -->
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/responsive.css" rel="stylesheet" type="text/css" />
		<h1>Approve Profile Photos</h1>
	 <div class="row" style="margin-top:20px;">
		<div class="col-lg-6">
			<div class="card-box">
				<h4 class="m-t-0 header-title"><b>LIST TO APPROVE</b></h4>
				<div class="table-responsive">
	<table class="table table-hover m-0 mails table-actions-bar">
		<tr><th width="50">Id</th><th width="200">User Email</th><th width="200">Pic</th><th>Action</th></tr>
		<?php
		foreach($users as $key=>$val)
		{
			$getpic=get_usermeta($users[$key]->ID, classify_author_avatar_url);
			//print_r($getpic);
			if($getpic!='')
			{?>
			<tr>
				<td><h5 class="m-b-0 m-t-0"><?php echo $users[$key]->ID;?></h5></td>
				<td><h5 class="m-b-0 m-t-0"><?php echo $users[$key]->user_email ;?></h5></td>
				<td><?php
					$profileIMGID = get_user_meta($users[$key]->ID, "classify_author_avatar_url", true);
					$profileIMG = classiera_get_profile_img($profileIMGID);
					$authorName = get_the_author_meta('display_name', $users[$key]->ID);
					$author_verified = get_the_author_meta('author_verified', $users[$key]->ID);
					if(empty($profileIMG)){
						$profileIMG = classiera_get_avatar_url ( get_the_author_meta('user_email', $users[$key]->ID), $size = '130' );
						?>
					<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>">	
					<?php
					}else{ ?>
					<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>">	
					<?php } ?>
				</td>
				<td><a href="admin.php?page=appprofpic&action=approve_pic&app_uid=<?php echo $users[$key]->ID;?>"><h5 class="m-b-0 m-t-0">Approve</h5></a>&nbsp;<a href="admin.php?page=appprofpic&action=unapprove_pic&app_uid=<?php echo $users[$key]->ID;?>"><h5 class="m-b-0 m-t-0">Unapprove</h5></a></td>
			</tr>
			<?php
			}
		}
		?>
	</table>
	</div>
	</div>
	</div>
	</div>
<?php
}
else
{
	echo '<span style="font-size:16px; color:red;">No Pics available to approve</span>';
}

}
?>