<?php
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php wp_head(); ?>
	<style type="text/css">
	</style>
</head>
<body <?php body_class(); ?> style="background-color: #FFF">
	<?php if (have_posts()): ?>
	<?php while (have_posts()): the_post(); ?>
	<section class="inner-page-content single-post-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<input type="button" class="btn btn-primary" onclick="window.print();" value="<?php esc_attr_e('Print listing', 'classiera'); ?>" />
				</div>
				<div class="col-sm-2">
					<input type="button" class="btn btn-primary" onclick="window.close();" value="<?php esc_attr_e('Close window', 'classiera'); ?>" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="single-post">
						<?php 
						global $post;
						global $redux_demo;
						$post_price = get_post_meta($post->ID, 'post_price', true); 
						$post_old_price = get_post_meta($post->ID, 'post_old_price', true);
						$postVideo = get_post_meta($post->ID, 'post_video', true);
						$dateFormat = get_option( 'date_format' );
						$postDate = get_the_date($dateFormat, $post->ID);
						$itemCondition = get_post_meta($post->ID, 'item-condition', true); 
						$post_location = get_post_meta($post->ID, 'post_location', true);
						$post_state = get_post_meta($post->ID, 'post_state', true);
						$post_city = get_post_meta($post->ID, 'post_city', true);
						$post_phone = get_post_meta($post->ID, 'post_phone', true);
						$post_latitude = get_post_meta($post->ID, 'post_latitude', true);
						$post_longitude = get_post_meta($post->ID, 'post_longitude', true);
						$post_address = get_post_meta($post->ID, 'post_address', true);
						$classieraCustomFields = get_post_meta($post->ID, 'custom_field', true);					
						$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
						$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);
						$attachments = get_children(array('post_parent' => $post->ID,
							'post_status' => 'inherit',
							'post_type' => 'attachment',
							'post_mime_type' => 'image',
							'order' => 'ASC',
							'orderby' => 'menu_order ID'
							)
						);
						if(empty($attachments)){
							if ( has_post_thumbnail()){
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
								?>
								<img class="img-responsive" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
								<?php 
							}
						}else{
							foreach($attachments as $att_id => $attachment){
								$full_img_url = wp_get_attachment_url($attachment->ID);
								?>
								<img class="img-responsive" src="<?php echo $full_img_url; ?>" alt="<?php the_title(); ?>">
								<?php
							}
						}
						?>
						<!-- ad details -->
						<div class="border-section border details">
							<h4 class="border-section-heading text-uppercase"><i class="fa fa-file-text-o"></i><?php esc_html_e('Ad Details', 'classiera') ?></h4>
							<div class="post-details">
								<ul class="list-unstyled clearfix">
									<li>
										<p><?php esc_html_e( 'Ad ID', 'classiera' ); ?>: 
										<span class="pull-right flip">
											<i class="fa fa-hashtag IDIcon"></i>
											<?php echo $post->ID; ?>
										</span>
										</p>
									</li><!--PostDate-->
									<li>
										<p><?php esc_html_e( 'Added', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $postDate; ?></span>
										</p>
									</li><!--PostDate-->								
									<!--Price Section -->
									<?php 
									$classieraPriceSection = $redux_demo['classiera_sale_price_off'];
									if($classieraPriceSection == 1){
									?>
									<?php if(!empty($post_price)){?>
									<li>
										<p><?php esc_html_e( 'Sale Price', 'classiera' ); ?>: 
										<span class="pull-right flip">
											<?php 
											if(is_numeric($post_price)){
												$classieraPostPrice =  classiera_post_price_display($post_currency_tag, $post_price);
											}else{ 
												$classieraPostPrice =  $post_price; 
											}
											echo $classieraPostPrice;
											?>
										</span>
										</p>
									</li><!--Sale Price-->
									<?php } ?>
									<?php if(!empty($post_old_price)){?>
									<li>
										<p><?php esc_html_e( 'Regular Price', 'classiera' ); ?>: 
										<span class="pull-right flip">										
											<?php 
											if(is_numeric($post_old_price)){
												echo classiera_post_price_display($post_currency_tag, $post_old_price);
											}else{ 
												echo $post_old_price; 
											}
											?>
										</span>
										</p>
									</li><!--Regular Price-->
									<?php } ?>
									<!--Price Section -->
									<?php } ?>
									<?php if(!empty($itemCondition)){?>
									<li>
										<p><?php esc_html_e( 'Condition', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $itemCondition; ?></span>
										</p>
									</li><!--Condition-->
									<?php } ?>
									<?php if(!empty($post_location)){?>
									<li>
										<p><?php esc_html_e( 'Location', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $post_location; ?></span>
										</p>
									</li><!--Location-->
									<?php } ?>
									<?php if(!empty($post_state)){?>
									<li>
										<p><?php esc_html_e( 'State', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $post_state; ?></span>
										</p>
									</li><!--state-->
									<?php } ?>
									<?php if(!empty($post_city)){?>
									<li>
										<p><?php esc_html_e( 'City', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $post_city; ?></span>
										</p>
									</li><!--City-->
									<?php } ?>
									<?php if(!empty($post_phone)){?>
									<li>
										<p><?php esc_html_e( 'Phone', 'classiera' ); ?>: 
										<span class="pull-right flip">
											<a href="tel:<?php echo $post_phone; ?>"><?php echo $post_phone; ?></a>
										</span>
										</p>
									</li><!--Phone-->
									<?php } ?>
									<li>
										<p><?php esc_html_e( 'Views', 'classiera' ); ?>: 
										<span class="pull-right flip">
											<?php echo classiera_get_post_views(get_the_ID()); ?>
										</span>
										</p>
									</li><!--Views-->
									<?php 
									if(!empty($classieraCustomFields)) {
										for ($i = 0; $i < count($classieraCustomFields); $i++){
											if($classieraCustomFields[$i][2] != 'dropdown' && $classieraCustomFields[$i][2] != 'checkbox'){
												if(!empty($classieraCustomFields[$i][1]) && !empty($classieraCustomFields[$i][0]) ) {
													?>
												<li>
													<p><?php echo $classieraCustomFields[$i][0]; ?>: 
													<span class="pull-right flip">
														<?php echo $classieraCustomFields[$i][1]; ?>
													</span>
													</p>
												</li><!--test-->	
													<?php
												}
											}
										}
										for ($i = 0; $i < count($classieraCustomFields); $i++){
											if($classieraCustomFields[$i][2] == 'dropdown'){
												if(!empty($classieraCustomFields[$i][1]) && !empty($classieraCustomFields[$i][0]) ){
												?>
												<li>
													<p><?php echo $classieraCustomFields[$i][0]; ?>: 
													<span class="pull-right flip">
														<?php echo $classieraCustomFields[$i][1]; ?>
													</span>
													</p>
												</li><!--dropdown-->
												<?php
												}
											}
										}
										for ($i = 0; $i < count($classieraCustomFields); $i++){
											if($classieraCustomFields[$i][2] == 'checkbox'){
												if(!empty($classieraCustomFields[$i][1]) && !empty($classieraCustomFields[$i][0]) ){
												?>
												<li>
													<p><?php echo $classieraCustomFields[$i][0]; ?>: 
													<span class="pull-right flip">
														<?php esc_html_e( 'Yes', 'classiera' ); ?>
													</span>
													</p>
												</li><!--dropdown-->
												<?php	
												}
											}
										}
									}
									?>
								</ul>
							</div><!--post-details-->
						</div>
						<!-- ad details -->
						<!-- post description -->
						<div class="border-section border description">
							<h4 class="border-section-heading text-uppercase">
							<?php esc_html_e( 'Description', 'classiera' ); ?>
							</h4>
							<?php echo the_content(); ?>
							<div class="tags">
								<span>
									<i class="fa fa-tags"></i>
									<?php esc_html_e( 'Tags', 'classiera' ); ?> :
								</span>
								<?php the_tags('','',''); ?>
							</div>
						</div>
						<!-- post description -->
						<!--Author Details-->
						<section class="print_author_details">
							<?php 
							$user_ID = $post->post_author;
							$authorName = get_the_author_meta('display_name', $user_ID );
							if(empty($authorName)){
								$authorName = get_the_author_meta('user_nicename', $user_ID );
							}
							if(empty($authorName)){
								$authorName = get_the_author_meta('user_login', $user_ID );
							}
							$author_avatar_url = get_user_meta($user_ID, "classify_author_avatar_url", true);
							$author_avatar_url = classiera_get_profile_img($author_avatar_url);
							$authorEmail = get_the_author_meta('user_email', $user_ID);
							$authorURL = get_the_author_meta('user_url', $user_ID);
							$authorPhone = get_the_author_meta('phone', $user_ID);
							if(empty($author_avatar_url)){										
								$author_avatar_url = classiera_get_avatar_url ($authorEmail, $size = '150' );
							}
							$UserRegistered = get_the_author_meta( 'user_registered', $user_ID );
							$dateFormat = get_option( 'date_format' );
							$classieraRegDate = date_i18n($dateFormat,  strtotime($UserRegistered));
							?>
							<div class="media">
								<div class="media-left">
									<img class="media-object" src="<?php echo $author_avatar_url; ?>" alt="<?php echo $authorName; ?>">
								</div><!--media-left-->
								<div class="media-body">
									<h5 class="media-heading text-uppercase">
										<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo $authorName; ?></a>
										<?php echo classiera_author_verified($user_ID);?>
									</h5>
									<p>
										<?php esc_html_e('Member Since', 'classiera') ?>&nbsp;<?php echo $classieraRegDate;?>
									</p>
								</div>
							</div>
							<h5 class="text-uppercase"><?php esc_html_e('Contact Details', 'classiera') ?> :</h5>
							<ul class="list-unstyled fa-ul c-detail">
								<?php if(!empty($authorPhone)){?>
								<li><i class="fa fa-li fa-phone-square"></i>&nbsp;
									<span class="phNum" data-replace="<?php echo $authorPhone;?>"><?php echo $authorPhone;?></span>
									<button type="button" id="showNum"><?php esc_html_e('Reveal', 'classiera') ?></button>
								</li>
								<?php } ?>
								<?php if(!empty($authorURL)){?>
								<li><i class="fa fa-li fa-globe"></i> 
									<a href="<?php echo $authorURL; ?>"><?php echo $authorURL; ?></a>
								</li>
								<?php } ?>
								<?php if(!empty($authorEmail)){?>
								<li><i class="fa fa-li fa-envelope"></i> 
									<a href="mailto:<?php echo $authorEmail; ?>"><?php echo $authorEmail; ?></a>
								</li>
								<?php } ?>
							</ul>
						</section>
						<!--Author Details-->
					</div><!--single-post-->
				</div><!--col-md-12-->
			</div><!--row-->
		</div><!--container-->
	</section><!--single-post-page-->
	<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_footer(); ?>
</body>
</html>