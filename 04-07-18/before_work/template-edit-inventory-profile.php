<?php
/**
 * Template name: Inventory Profile Settings
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage classiera
 * @since classiera 1.0
 */
//echo 'im here';exit;
if ( !is_user_logged_in() ) { 

	global $redux_demo; 
	$login = $redux_demo['login'];
	wp_redirect( $login ); exit;

}

if(!current_user_can('inventory_user'))
{
	if(!current_user_can('company_user'))
	{
		get_template_part('error');
		exit;
	}
}

global $user_ID, $user_identity, $user_level;
global $redux_demo;
$caticoncolor="";
$category_icon_code ="";
$category_icon="";
$category_icon_color="";
$profile = $redux_demo['profile'];

if ($user_ID){
	
	if($_POST){
		//print_r($_POST);
		$message =  esc_html__( 'Your profile updated successfully.', 'classiera' );

		//$first = esc_sql($_POST['first_name']);

		//$last = esc_sql($_POST['last_name']);
        // $job = esc_sql($_POST['job']);
		 //$location = esc_sql($_POST['location']);
		 //$interests = esc_sql($_POST['interests']);
		// $interests = esc_sql($_POST['my_interest']);
		 $interests = esc_sql($_POST['my_interest1']);
		$email = esc_sql($_POST['email']);
		$current_email = esc_sql($_POST['current_email']);

		$user_url = esc_sql($_POST['website']);

		$user_phone = esc_sql($_POST['phone']);
		
		$user_phone2 = esc_sql($_POST['phone2']);
		$facebook = esc_sql($_POST['facebook']);
		$twitter = esc_sql($_POST['twitter']);
		$googleplus = esc_sql($_POST['google-plus']);
		$linkedin = esc_sql($_POST['linkedin']);
		$pinterest = esc_sql($_POST['pinterest']);
		$instagram = esc_sql($_POST['instagram']);
		$youtube = esc_sql($_POST['youtube']);
		$vimeo = esc_sql($_POST['vimeo']);
		
		//$country = esc_sql($_POST['country']);
		//$state = esc_sql($_POST['state']);
		//$city = esc_sql($_POST['city']);
		//$post_code = esc_sql($_POST['post_code']);
		$user_address = esc_sql($_POST['address']);

		//$description = esc_sql($_POST['desc']);
		$description = $_POST['desc'];

		$password = esc_sql($_POST['pwd']);

		$confirm_password = esc_sql($_POST['confirm']);

		
		$your_image_url = esc_sql($_POST['your_author_image_url']);

		//update_user_meta( $user_ID, 'first_name', $first );

		//update_user_meta( $user_ID, 'last_name', $last );
        //update_user_meta( $user_ID, 'job', $job );
		$user = wp_get_current_user();
	    $role = ( array ) $user->roles;
		global $wpdb;
		if(in_array("inventory_user",$role))
		{
			update_user_meta( $user_ID, 'company_name', $_POST['company_name']);
			if(isset($_POST['hdn_total_branch']) && $_POST['hdn_total_branch']!='')
			{
				update_user_meta( $user_ID, 'branch_count', $_POST['hdn_total_branch']);
				for($lp=1;$lp<=$_POST['hdn_total_branch'];$lp++)
				{
					if(isset($_POST['branch_location'.$lp]) && $_POST['branch_location'.$lp]!='')
					{
						update_user_meta( $user_ID, 'branch_location'.$lp, $_POST['branch_location'.$lp]);
					}
				}
			}
			
			/*if(isset($_POST['branch_location1']) && $_POST['branch_location1']!='')
			{
				update_user_meta( $user_ID, 'branch_location1', $_POST['branch_location1']);
			}
			if(isset($_POST['branch_location2']) && $_POST['branch_location2']!='')
			{
				update_user_meta( $user_ID, 'branch_location2', $_POST['branch_location2']);
			}
			if(isset($_POST['branch_location3']) && $_POST['branch_location3']!='')
			{
				update_user_meta( $user_ID, 'branch_location3', $_POST['branch_location3']);
			}*/
			//wp_update_user( array ('ID' => $user_ID, 'user_url' => $_POST['website']) );
		//echo $user_url;exit;
			update_user_meta( $user_ID, 'company_website', $_POST['website']);
			//$wpdb->update($wpdb->users, array('user_url' => $_POST['website']), array('ID' => $user_ID));
		}
		else
		{
			$companyid=get_user_meta($user_ID, 'company_id');
			//print_r($_POST);exit;
			/*if(isset($_POST['branch_location1']) && $_POST['branch_location1']!='')
			{
				update_user_meta( $companyid[0], 'branch_location1', $_POST['branch_location1']);
			}
			if(isset($_POST['branch_location2']) && $_POST['branch_location2']!='')
			{
				update_user_meta( $companyid[0], 'branch_location2', $_POST['branch_location2']);
			}
			if(isset($_POST['branch_location3']) && $_POST['branch_location3']!='')
			{
				update_user_meta( $companyid[0], 'branch_location3',$_POST['branch_location3']);
			}*/
			
			if(isset($_POST['hdn_total_branch']) && $_POST['hdn_total_branch']!='')
			{
				update_user_meta( $user_ID, 'branch_count', $_POST['hdn_total_branch']);
				for($lp=1;$lp<=$_POST['hdn_total_branch'];$lp++)
				{
					if(isset($_POST['branch_location'.$lp]) && $_POST['branch_location'.$lp]!='')
					{
						update_user_meta( $user_ID, 'branch_location'.$lp, $_POST['branch_location'.$lp]);
					}
				}
			}
			update_user_meta( $companyid[0], 'company_name', $_POST['company_name']);
			//update_user_meta( $companyid[0], 'branch_location1', $location);
			//$wpdb->update($wpdb->users, array('user_url' => $_POST['website']), array('ID' => $companyid[0]));
			update_user_meta( $companyid[0], 'company_website', $_POST['website']);
			
			//wp_update_user( array ('ID' => $companyid[0], 'user_url' => $_POST['website']) );
		}
		
		//update_user_meta( $user_ID, 'location', $location );
		update_user_meta( $user_ID, 'interests', $interests );
		update_user_meta( $user_ID, 'phone', $user_phone );
		update_user_meta( $user_ID, 'phone2', $user_phone2 );
		
		update_user_meta( $user_ID, 'facebook', $facebook );
		update_user_meta( $user_ID, 'twitter', $twitter );
		update_user_meta( $user_ID, 'googleplus', $googleplus );
		update_user_meta( $user_ID, 'linkedin', $linkedin );
		update_user_meta( $user_ID, 'pinterest', $pinterest );
		update_user_meta( $user_ID, 'instagram', $instagram );
		update_user_meta( $user_ID, 'youtube', $youtube );
		update_user_meta( $user_ID, 'vimeo', $vimeo );

		update_user_meta( $user_ID, 'country', $country );
		update_user_meta( $user_ID, 'state', $state );
		update_user_meta( $user_ID, 'city', $city );
		update_user_meta( $user_ID, 'postcode', $post_code );
		update_user_meta( $user_ID, 'address', $user_address );

		update_user_meta( $user_ID, 'description', $description );

				
		
		if($email != $current_email){
			wp_update_user( array ('ID' => $user_ID, 'user_email' => $email) ) ;
			update_user_meta( $user_ID, 'author_verified', 'unverified');
			$verify_code = '';
			update_user_meta( $user_ID, 'author_vcode', $verify_code );
		}
		if(isset($_POST['username']) && $_POST['username']!='')
		{
			//echo 'im in if';exit;
			//wp_update_user( array ('ID' => $user_ID, 'user_login' => $_POST['username']) ) ;	
			$wpdb->update($wpdb->users, array('user_login' => $_POST['username']), array('ID' => $user_ID));
		}
		
		if($password){

			if (strlen($password) < 5 || strlen($password) > 25) {
				$message =  esc_html__( 'Password must be 5 to 25 characters in length.', 'classiera' );
				}

			//elseif( $password == $confirm_password ) {
			$confirmPWD = $_POST['confirm'];
			$confirmPWD2 = $_POST['confirm2'];
			if(isset($confirmPWD) && $confirmPWD != $confirmPWD2) {

				$message =  esc_html__( 'Password Mismatch', 'classiera' );

			} elseif ( isset($confirmPWD) && !empty($password) ) {

				$update = wp_set_password( $confirmPWD, $user_ID );				
				$message =  esc_html__( 'Your profile updated successfully.', 'classiera' );

			}
		}
	}
	/*ImageUploading*/
	if ( isset($_FILES['upload_attachment']) ) {
		$count = '0';
		$files = $_FILES['upload_attachment'];
		foreach ($files['name'] as $key => $value) {				
			if ($files['name'][$key]) {
				$file = array(
					'name'     => $files['name'][$key],
					'type'     => $files['type'][$key],
					'tmp_name' => $files['tmp_name'][$key],
					'error'    => $files['error'][$key],
					'size'     => $files['size'][$key]
				);
				$_FILES = array("upload_attachment" => $file);				
				foreach ($_FILES as $file => $array) {					
					$newupload = classiera_insert_userIMG($file);
					$count++;
					$profileImage = $newupload;
					if(!empty($profileImage )){
					 update_user_meta( $user_ID, 'profile_pic_approve', '0' );
						update_user_meta( $user_ID, 'classify_author_avatar_url', $profileImage );
					}
				}
			}
		}/*Foreach*/
	}
	//header('Location: '.site_url().'/profile-settings-2/'); 
	//exit;
}

get_header();

$profile = $redux_demo['profile'];
$user_info = get_userdata($user_ID);
 ?>
<?php 
	$page = get_page($post->ID);
	$current_page_id = $page->ID;
	
?>	
		<?php /*<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>*/?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<!-- user pages -->
<section class="user-pages section-gray-bg edit_pages all_usertemplate">
    <div class="container-fluid">
        <div class="row">
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
				<?php if($_POST){?>
				<div class="alert alert-success" role="alert">
					<?php echo $message; ?>
				</div>
				<?php } ?>
				<div class="user-detail-section section-bg-white">
					<div class="user-ads user-profile-settings">
						<h4 class="user-detail-section-heading text-uppercase hide">
							<?php esc_html_e("Profile Settings", 'classiera') ?>
						</h4>						
						<form data-toggle="validator" role="form" method="POST" id="primaryPostForm" action="" enctype="multipart/form-data">
							<!-- upload avatar -->
                            <div class="media margin0">
							    <div class="top-buffer2"></div>
                                <div class="uploadImage">
									 <div class="upload_images">		
											<?php 
											$profileIMGID = get_user_meta($user_ID, "classify_author_avatar_url", true);
											$profileIMG = classiera_get_profile_img($profileIMGID);
											$authorName = get_the_author_meta('display_name', $user_ID);
											$author_verified = get_the_author_meta('author_verified', $user_ID);
											if(empty($profileIMG)){
												$profileIMG = classiera_get_avatar_url ( get_the_author_meta('user_email', $user_ID), $size = '130' );
												?>
											<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>">	
											<?php
											}else{ ?>
											<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>">	
											<?php } ?>
											<input class="criteria-image-url" id="your_image_url" type="text" size="36" name="your_author_image_url" style="display: none;" value="" />
									</div> 
									<div class="top-buffer1"></div>		
									<p><?php esc_html_e( 'Update your Profile Photo', 'classiera' ); ?></p>
									<div class="top-buffer1"></div>	
									<div class="choose-image text-center block-display">
                                        <input type="file" id="file-1" name="upload_attachment[]" class="inputfile inputfile-1 hide author-UP" data-multiple-caption="{count} files selected" multiple />
                                        <label for="file-1" class="upload-author-image hide"><i class="fa fa-camera"></i>
											<span><?php esc_html_e( 'Upload photo', 'classiera' ); ?></span>
										</label>
										<!-- verify profile btn-->
										<?php if($author_verified != 'verified'){ ?>										
                                        <button class="classiera_verify_btn" data-toggle="modal" data-target="#verifyModal" type="button"> <i class="fa fa-check"></i> <?php esc_html_e( 'Verify your account', 'classiera' ); ?></button>
										<?php } ?>
										<div class="top-buffer2"></div>
										<!-- verify profile btn-->
                                    </div>	
							   </div>
								
                                <div class="media-body pad0">
								    <hr class="line1">
									<h2 class="user_name"><?php esc_html_e( 'edit your Profile', 'classiera' ); ?></h2>
									<hr class="line1">
                                    <p class="hide"><?php esc_html_e( 'Update your avatar manually,If the not set the default Gravatar will be the same as your login email/user account.', 'classiera' ); ?></p>                                    								
                                </div>
								
                            </div><!-- /.upload avatar -->
							<!-- user basic information -->
							
							
							<div class="form-inline row form-inline-margin">
								<?php /*<div class="form-group col-sm-5">
                                    <label for="first-name"><?php esc_html_e( 'First Name', 'classiera' ); ?></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="first_name" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Update first Name..', 'classiera' ); ?>" value="<?php echo $user_info->first_name; ?>">
                                    </div>
                                </div><!--Firstname-->
								<div class="form-group col-sm-5">
									<label for="last-name"><?php esc_html_e( 'Last Name', 'classiera' ); ?></label>
									<div class="inner-addon">
										<input type="text" id="last-name" name="last_name" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'update last Name', 'classiera' ); ?>" value="<?php echo $user_info->last_name; ?>">
									</div>
								</div><!--Last name-->*/
								$user = wp_get_current_user();
								$role = ( array ) $user->roles;
								if(in_array("inventory_user",$role))
								{
									$company_name=get_user_meta($user->ID, "company_name");
									//print_r($company_name);
									$company_location=get_user_meta($user->ID, "location");
									$branch_location1=get_user_meta($user->ID, "branch_location1");
									//print_r($branch_location1);
									$branch_location2=get_user_meta($user->ID, "branch_location2");
									$branch_location3=get_user_meta($user->ID, "branch_location3");
									$user_url=get_user_meta($user->ID, "company_website");
									//print_r($user_url);
									$custuid=$user->ID;
								}
								else
								{

									$company_id=get_user_meta($user->ID, "company_id");
									
									$company_name=get_user_meta($company_id[0], 'company_name');
									$company_location=get_user_meta($company_id[0], 'location');
									$branch_location1=get_user_meta($company_id[0], "branch_location1");
									$branch_location2=get_user_meta($company_id[0], "branch_location2");
									$branch_location3=get_user_meta($company_id[0], "branch_location3");
									$user_url=get_user_meta($company_id[0], "company_website");
									//print_r($user_url);
									$custuid=$company_id[0];
								}
								?>
								
							<div class="first_section">	
									<p class="user-detail-section-heading poppins-lite pad0">
										 <img src="<?=get_template_directory_uri().'/../classiera-child/images/quate.png' ?>" width="50" height="50"/><br/>
										<?php esc_html_e( 'Basic Info', 'classiera' ); ?>
									</p>	
									<div class="col-md-10 col-md-offset-1">	
										<div class="form-group col-sm-6">
											<label for="companycompany_namename" class="hide"><?php esc_html_e( 'company Name', 'classiera' ); ?></label>
											<div class="inner-addon">
												<input type="text" id="company_name" name="company_name" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'update Company Name', 'classiera' ); ?>" value="<?php if (!empty($company_name)){ echo $company_name[0];} ?>">
											</div>
										</div>
										<div class="form-group col-sm-6">
											<label for="company_website" class="hide"><?php esc_html_e( 'company website:', 'classiera' ); ?></label>
											<div class="inner-addon">
												<input type="text" id="website" name="website" class="form-control form-control-sm" placeholder="<?php if($user_url[0]==''){ esc_html_e( 'Website:', 'classiera' );}else{ echo $user_url[0]; } ?>" value="<?php echo $user_url[0]; ?>">
											</div>
										</div>
										<div class="form-group col-sm-6">
											<label for="bio" class="hide"><?php esc_html_e( 'About us', 'classiera' );?></label>
											<div class="inner-addon">
												<textarea name="desc" id="bio" placeholder="<?php esc_html_e( 'enter your short info.', 'classiera' ); ?>"><?php echo $user_info->description; ?></textarea>
											</div>
										</div>
									
							
							       
									
									
									
							
						
								
								
			<?php /*?><div class="input_fields_wrap col-sm-12">
			   
			<label for="interests"><?php esc_html_e( 'Interests', 'classiera' );?></label>
				<button class="add_field_button">Add More</button>
			 <div><input type="text" name="mytext[]"id="Interests" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'interests:', 'classiera' ); ?>" value="<?php echo $user_info->interests; ?>">
			 </div>
			</div><?php */?>
			
			 <?php /* <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/normalize.min.css">*/?>
			 <style>
			 
hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}

img {
    vertical-align: middle;
}

fieldset {
    border: 0;
    margin: 0;
    padding: 0;
}

textarea {
    resize: vertical;
}

.chromeframe {
    margin: 0.2em 0;
    background: #ccc;
    color: #000;
    padding: 0.2em 0;
}


/* ==========================================================================
   Author's custom styles
   ========================================================================== */


.wrapper {
    margin: 20px auto;
    width: 400px;
}
#myAutocomplete {
    width: 100%;
}

.ui-autocomplete-multiselect.ui-state-default {
    display: block;
    background: #fff;
    border: 1px solid #ccc;
    padding: 3px 3px;
    padding-bottom: 0px;
    overflow: hidden;
    cursor: text;
}

.ui-autocomplete-multiselect .ui-autocomplete-multiselect-item .ui-icon {
    float: right;
    cursor: pointer;
}

.ui-autocomplete-multiselect .ui-autocomplete-multiselect-item {
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    padding: 1px 3px;
    margin-right: 2px;
    margin-bottom: 3px;
    color: #333;
    background-color: #f6f6f6;
}

.ui-autocomplete-multiselect input {
    display: inline-block;
    border: none;
    outline: none;
    height: auto;
    margin: 2px;
    overflow: visible;
    margin-bottom: 5px;
    text-align: left;
}

.ui-autocomplete-multiselect.ui-state-active {
    outline: none;
    border: 1px solid #7ea4c7;
    -moz-box-shadow: 0 0 5px rgba(50,150,255,0.5);
    -webkit-box-shadow: 0 0 5px rgba(50,150,255,0.5);
    -khtml-box-shadow: 0 0 5px rgba(50,150,255,0.5);
    box-shadow: 0 0 5px rgba(50,150,255,0.5);
}

.ui-autocomplete {
    border-top: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}












/* ==========================================================================
   Media Queries
   ========================================================================== */

@media only screen and (min-width: 35em) {

}

@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
       only screen and (min-resolution: 144dpi) {

}

/* ==========================================================================
   Helper classes
   ========================================================================== */

.ir {
    background-color: transparent;
    border: 0;
    overflow: hidden;
    *text-indent: -9999px;
}

.ir:before {
    content: "";
    display: block;
    width: 0;
    height: 100%;
}

.hidden {
    display: none !important;
    visibility: hidden;
}

.visuallyhidden {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}

.visuallyhidden.focusable:active,
.visuallyhidden.focusable:focus {
    clip: auto;
    height: auto;
    margin: 0;
    overflow: visible;
    position: static;
    width: auto;
}

.invisible {
    visibility: hidden;
}

.clearfix:before,
.clearfix:after {
    content: " ";
    display: table;
}

.clearfix:after {
    clear: both;
}

.clearfix {
    *zoom: 1;
}
a, a:hover{
    text-decoration: none;
}

/* ==========================================================================
   Print styles
   ========================================================================== */

@media print {
    * {
        background: transparent !important;
        color: #000 !important; /* Black prints faster: h5bp.com/s */
        box-shadow: none !important;
        text-shadow: none !important;
    }

    a,
    a:visited {
        text-decoration: underline;
    }

    a[href]:after {
        content: " (" attr(href) ")";
    }

    abbr[title]:after {
        content: " (" attr(title) ")";
    }

    /*
     * Don't show links for images, or javascript/internal links
     */

    .ir a:after,
    a[href^="javascript:"]:after,
    a[href^="#"]:after {
        content: "";
    }

    pre,
    blockquote {
        border: 1px solid #999;
        page-break-inside: avoid;
    }

    thead {
        display: table-header-group; /* h5bp.com/t */
    }

    tr,
    img {
        page-break-inside: avoid;
    }

    img {
        max-width: 100% !important;
    }

    @page {
        margin: 0.5cm;
    }

    p,
    h2,
    h3 {
        orphans: 3;
        widows: 3;
    }

    h2,
    h3 {
        page-break-after: avoid;}
		}
   
			 </style>
							<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"/>
							<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/main.css">
							<script>
							var $=jQuery.noConflict();
							</script>
							
							<script src="<?php echo get_template_directory_uri();?>/js/multiselectautocomplete/vendor/modernizr-2.6.2.min.js"></script>
							
							<?php /*<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script> 
<script src="<?php echo get_template_directory_uri();?>/js/multiselectautocomplete/jquery.autocomplete.multiselect.js"></script> */?>
<?php /*<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"/>*/?>
<style>/* Layout helpers
----------------------------------*/
.ui-helper-hidden {
	display: none;
}
.ui-helper-hidden-accessible {
	border: 0;
	clip: rect(0 0 0 0);
	height: 1px;
	margin: -1px;
	overflow: hidden;
	padding: 0;
	position: absolute;
	width: 1px;
}
.ui-helper-reset {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	line-height: 1.3;
	text-decoration: none;
	font-size: 100%;
	list-style: none;
}
.ui-helper-clearfix:before,
.ui-helper-clearfix:after {
	content: "";
	display: table;
	border-collapse: collapse;
}
.ui-helper-clearfix:after {
	clear: both;
}
.ui-helper-clearfix {
	min-height: 0; /* support: IE7 */
}
.ui-helper-zfix {
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	position: absolute;
	opacity: 0;
	filter:Alpha(Opacity=0);
}

.ui-front {
	z-index: 100;
}


/* Interaction Cues
----------------------------------*/
.ui-state-disabled {
	cursor: default !important;
}


/* Icons
----------------------------------*/

/* states and images */
.ui-icon {
	display: block;
	text-indent: -99999px;
	overflow: hidden;
	background-repeat: no-repeat;
}


/* Misc visuals
----------------------------------*/


/* Overlays */
.ui-widget-overlay {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
.ui-accordion .ui-accordion-header {
	display: block;
	cursor: pointer;
	position: relative;
	margin-top: 2px;
	padding: .5em .5em .5em .7em;
	min-height: 0; /* support: IE7 */
}
.ui-accordion .ui-accordion-icons {
	padding-left: 2.2em;
}
.ui-accordion .ui-accordion-noicons {
	padding-left: .7em;
}
.ui-accordion .ui-accordion-icons .ui-accordion-icons {
	padding-left: 2.2em;
}
.ui-accordion .ui-accordion-header .ui-accordion-header-icon {
	position: absolute;
	left: .5em;
	top: 50%;
	margin-top: -8px;
}
.ui-accordion .ui-accordion-content {
	padding: 1em 2.2em;
	border-top: 0;
	overflow: auto;
}
.ui-autocomplete {
	position: absolute;
	top: 0;
	left: 0;
	cursor: default;
}
.ui-button {
	display: inline-block;
	position: relative;
	padding: 0;
	line-height: normal;
	margin-right: .1em;
	cursor: pointer;
	vertical-align: middle;
	text-align: center;
	overflow: visible; /* removes extra width in IE */
}
.ui-button,
.ui-button:link,
.ui-button:visited,
.ui-button:hover,
.ui-button:active {
	text-decoration: none;
}
/* to make room for the icon, a width needs to be set here */
.ui-button-icon-only {
	width: 2.2em;
}
/* button elements seem to need a little more width */
button.ui-button-icon-only {
	width: 2.4em;
}
.ui-button-icons-only {
	width: 3.4em;
}
button.ui-button-icons-only {
	width: 3.7em;
}

/* button text element */
.ui-button .ui-button-text {
	display: block;
	line-height: normal;
}
.ui-button-text-only .ui-button-text {
	padding: .4em 1em;
}
.ui-button-icon-only .ui-button-text,
.ui-button-icons-only .ui-button-text {
	padding: .4em;
	text-indent: -9999999px;
}
.ui-button-text-icon-primary .ui-button-text,
.ui-button-text-icons .ui-button-text {
	padding: .4em 1em .4em 2.1em;
}
.ui-button-text-icon-secondary .ui-button-text,
.ui-button-text-icons .ui-button-text {
	padding: .4em 2.1em .4em 1em;
}
.ui-button-text-icons .ui-button-text {
	padding-left: 2.1em;
	padding-right: 2.1em;
}
/* no icon support for input elements, provide padding by default */
input.ui-button {
	padding: .4em 1em;
}

/* button icon element(s) */
.ui-button-icon-only .ui-icon,
.ui-button-text-icon-primary .ui-icon,
.ui-button-text-icon-secondary .ui-icon,
.ui-button-text-icons .ui-icon,
.ui-button-icons-only .ui-icon {
	position: absolute;
	top: 50%;
	margin-top: -8px;
}
.ui-button-icon-only .ui-icon {
	left: 50%;
	margin-left: -8px;
}
.ui-button-text-icon-primary .ui-button-icon-primary,
.ui-button-text-icons .ui-button-icon-primary,
.ui-button-icons-only .ui-button-icon-primary {
	left: .5em;
}
.ui-button-text-icon-secondary .ui-button-icon-secondary,
.ui-button-text-icons .ui-button-icon-secondary,
.ui-button-icons-only .ui-button-icon-secondary {
	right: .5em;
}

/* button sets */
.ui-buttonset {
	margin-right: 7px;
}
.ui-buttonset .ui-button {
	margin-left: 0;
	margin-right: -.3em;
}

/* workarounds */
/* reset extra padding in Firefox, see h5bp.com/l */
input.ui-button::-moz-focus-inner,
button.ui-button::-moz-focus-inner {
	border: 0;
	padding: 0;
}
.ui-datepicker {
	width: 17em;
	padding: .2em .2em 0;
	display: none;
}
.ui-datepicker .ui-datepicker-header {
	position: relative;
	padding: .2em 0;
}
.ui-datepicker .ui-datepicker-prev,
.ui-datepicker .ui-datepicker-next {
	position: absolute;
	top: 2px;
	width: 1.8em;
	height: 1.8em;
}
.ui-datepicker .ui-datepicker-prev-hover,
.ui-datepicker .ui-datepicker-next-hover {
	top: 1px;
}
.ui-datepicker .ui-datepicker-prev {
	left: 2px;
}
.ui-datepicker .ui-datepicker-next {
	right: 2px;
}
.ui-datepicker .ui-datepicker-prev-hover {
	left: 1px;
}
.ui-datepicker .ui-datepicker-next-hover {
	right: 1px;
}
.ui-datepicker .ui-datepicker-prev span,
.ui-datepicker .ui-datepicker-next span {
	display: block;
	position: absolute;
	left: 50%;
	margin-left: -8px;
	top: 50%;
	margin-top: -8px;
}
.ui-datepicker .ui-datepicker-title {
	margin: 0 2.3em;
	line-height: 1.8em;
	text-align: center;
}
.ui-datepicker .ui-datepicker-title select {
	font-size: 1em;
	margin: 1px 0;
}
.ui-datepicker select.ui-datepicker-month,
.ui-datepicker select.ui-datepicker-year {
	width: 49%;
}
.ui-datepicker table {
	width: 100%;
	font-size: .9em;
	border-collapse: collapse;
	margin: 0 0 .4em;
}
.ui-datepicker th {
	padding: .7em .3em;
	text-align: center;
	font-weight: bold;
	border: 0;
}
.ui-datepicker td {
	border: 0;
	padding: 1px;
}
.ui-datepicker td span,
.ui-datepicker td a {
	display: block;
	padding: .2em;
	text-align: right;
	text-decoration: none;
}
.ui-datepicker .ui-datepicker-buttonpane {
	background-image: none;
	margin: .7em 0 0 0;
	padding: 0 .2em;
	border-left: 0;
	border-right: 0;
	border-bottom: 0;
}
.ui-datepicker .ui-datepicker-buttonpane button {
	float: right;
	margin: .5em .2em .4em;
	cursor: pointer;
	padding: .2em .6em .3em .6em;
	width: auto;
	overflow: visible;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current {
	float: left;
}

/* with multiple calendars */
.ui-datepicker.ui-datepicker-multi {
	width: auto;
}
.ui-datepicker-multi .ui-datepicker-group {
	float: left;
}
.ui-datepicker-multi .ui-datepicker-group table {
	width: 95%;
	margin: 0 auto .4em;
}
.ui-datepicker-multi-2 .ui-datepicker-group {
	width: 50%;
}
.ui-datepicker-multi-3 .ui-datepicker-group {
	width: 33.3%;
}
.ui-datepicker-multi-4 .ui-datepicker-group {
	width: 25%;
}
.ui-datepicker-multi .ui-datepicker-group-last .ui-datepicker-header,
.ui-datepicker-multi .ui-datepicker-group-middle .ui-datepicker-header {
	border-left-width: 0;
}
.ui-datepicker-multi .ui-datepicker-buttonpane {
	clear: left;
}
.ui-datepicker-row-break {
	clear: both;
	width: 100%;
	font-size: 0;
}

/* RTL support */
.ui-datepicker-rtl {
	direction: rtl;
}
.ui-datepicker-rtl .ui-datepicker-prev {
	right: 2px;
	left: auto;
}
.ui-datepicker-rtl .ui-datepicker-next {
	left: 2px;
	right: auto;
}
.ui-datepicker-rtl .ui-datepicker-prev:hover {
	right: 1px;
	left: auto;
}
.ui-datepicker-rtl .ui-datepicker-next:hover {
	left: 1px;
	right: auto;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane {
	clear: right;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane button {
	float: left;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane button.ui-datepicker-current,
.ui-datepicker-rtl .ui-datepicker-group {
	float: right;
}
.ui-datepicker-rtl .ui-datepicker-group-last .ui-datepicker-header,
.ui-datepicker-rtl .ui-datepicker-group-middle .ui-datepicker-header {
	border-right-width: 0;
	border-left-width: 1px;
}
.ui-dialog {
	overflow: hidden;
	position: absolute;
	top: 0;
	left: 0;
	padding: .2em;
	outline: 0;
}
.ui-dialog .ui-dialog-titlebar {
	padding: .4em 1em;
	position: relative;
}
.ui-dialog .ui-dialog-title {
	float: left;
	margin: .1em 0;
	white-space: nowrap;
	width: 90%;
	overflow: hidden;
	text-overflow: ellipsis;
}
.ui-dialog .ui-dialog-titlebar-close {
	position: absolute;
	right: .3em;
	top: 50%;
	width: 20px;
	margin: -10px 0 0 0;
	padding: 1px;
	height: 20px;
}
.ui-dialog .ui-dialog-content {
	position: relative;
	border: 0;
	padding: .5em 1em;
	background: none;
	overflow: auto;
}
.ui-dialog .ui-dialog-buttonpane {
	text-align: left;
	border-width: 1px 0 0 0;
	background-image: none;
	margin-top: .5em;
	padding: .3em 1em .5em .4em;
}
.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {
	float: right;
}
.ui-dialog .ui-dialog-buttonpane button {
	margin: .5em .4em .5em 0;
	cursor: pointer;
}
.ui-dialog .ui-resizable-se {
	width: 12px;
	height: 12px;
	right: -5px;
	bottom: -5px;
	background-position: 16px 16px;
}
.ui-draggable .ui-dialog-titlebar {
	cursor: move;
}
.ui-menu {
	list-style: none;
	padding: 2px;
	margin: 0;
	display: block;
	outline: none;
}
.ui-menu .ui-menu {
	margin-top: -3px;
	position: absolute;
}
.ui-menu .ui-menu-item {
	margin: 0;
	padding: 0;
	width: 100%;
	/* support: IE10, see #8844 */
	list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
}
.ui-menu .ui-menu-divider {
	margin: 5px -2px 5px -2px;
	height: 0;
	font-size: 0;
	line-height: 0;
	border-width: 1px 0 0 0;
}
.ui-menu .ui-menu-item a {
	text-decoration: none;
	display: block;
	padding: 2px .4em;
	line-height: 1.5;
	min-height: 0; /* support: IE7 */
	font-weight: normal;
}
.ui-menu .ui-menu-item a.ui-state-focus,
.ui-menu .ui-menu-item a.ui-state-active {
	font-weight: normal;
	margin: -1px;
}

.ui-menu .ui-state-disabled {
	font-weight: normal;
	margin: .4em 0 .2em;
	line-height: 1.5;
}
.ui-menu .ui-state-disabled a {
	cursor: default;
}

/* icon support */
.ui-menu-icons {
	position: relative;
}
.ui-menu-icons .ui-menu-item a {
	position: relative;
	padding-left: 2em;
}

/* left-aligned */
.ui-menu .ui-icon {
	position: absolute;
	top: .2em;
	left: .2em;
}

/* right-aligned */
.ui-menu .ui-menu-icon {
	position: static;
	float: right;
}
.ui-progressbar {
	height: 2em;
	text-align: left;
	overflow: hidden;
}
.ui-progressbar .ui-progressbar-value {
	margin: -1px;
	height: 100%;
}
.ui-progressbar .ui-progressbar-overlay {
	background: url("<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/animated-overlay.gif");
	height: 100%;
	filter: alpha(opacity=25);
	opacity: 0.25;
}
.ui-progressbar-indeterminate .ui-progressbar-value {
	background-image: none;
}
.ui-resizable {
	position: relative;
}
.ui-resizable-handle {
	position: absolute;
	font-size: 0.1px;
	display: block;
}
.ui-resizable-disabled .ui-resizable-handle,
.ui-resizable-autohide .ui-resizable-handle {
	display: none;
}
.ui-resizable-n {
	cursor: n-resize;
	height: 7px;
	width: 100%;
	top: -5px;
	left: 0;
}
.ui-resizable-s {
	cursor: s-resize;
	height: 7px;
	width: 100%;
	bottom: -5px;
	left: 0;
}
.ui-resizable-e {
	cursor: e-resize;
	width: 7px;
	right: -5px;
	top: 0;
	height: 100%;
}
.ui-resizable-w {
	cursor: w-resize;
	width: 7px;
	left: -5px;
	top: 0;
	height: 100%;
}
.ui-resizable-se {
	cursor: se-resize;
	width: 12px;
	height: 12px;
	right: 1px;
	bottom: 1px;
}
.ui-resizable-sw {
	cursor: sw-resize;
	width: 9px;
	height: 9px;
	left: -5px;
	bottom: -5px;
}
.ui-resizable-nw {
	cursor: nw-resize;
	width: 9px;
	height: 9px;
	left: -5px;
	top: -5px;
}
.ui-resizable-ne {
	cursor: ne-resize;
	width: 9px;
	height: 9px;
	right: -5px;
	top: -5px;
}
.ui-selectable-helper {
	position: absolute;
	z-index: 100;
	border: 1px dotted black;
}
.ui-slider {
	position: relative;
	text-align: left;
}
.ui-slider .ui-slider-handle {
	position: absolute;
	z-index: 2;
	width: 1.2em;
	height: 1.2em;
	cursor: default;
}
.ui-slider .ui-slider-range {
	position: absolute;
	z-index: 1;
	font-size: .7em;
	display: block;
	border: 0;
	background-position: 0 0;
}

/* For IE8 - See #6727 */
.ui-slider.ui-state-disabled .ui-slider-handle,
.ui-slider.ui-state-disabled .ui-slider-range {
	filter: inherit;
}

.ui-slider-horizontal {
	height: .8em;
}
.ui-slider-horizontal .ui-slider-handle {
	top: -.3em;
	margin-left: -.6em;
}
.ui-slider-horizontal .ui-slider-range {
	top: 0;
	height: 100%;
}
.ui-slider-horizontal .ui-slider-range-min {
	left: 0;
}
.ui-slider-horizontal .ui-slider-range-max {
	right: 0;
}

.ui-slider-vertical {
	width: .8em;
	height: 100px;
}
.ui-slider-vertical .ui-slider-handle {
	left: -.3em;
	margin-left: 0;
	margin-bottom: -.6em;
}
.ui-slider-vertical .ui-slider-range {
	left: 0;
	width: 100%;
}
.ui-slider-vertical .ui-slider-range-min {
	bottom: 0;
}
.ui-slider-vertical .ui-slider-range-max {
	top: 0;
}
.ui-spinner {
	position: relative;
	display: inline-block;
	overflow: hidden;
	padding: 0;
	vertical-align: middle;
}
.ui-spinner-input {
	border: none;
	background: none;
	color: inherit;
	padding: 0;
	margin: .2em 0;
	vertical-align: middle;
	margin-left: .4em;
	margin-right: 22px;
}
.ui-spinner-button {
	width: 16px;
	height: 50%;
	font-size: .5em;
	padding: 0;
	margin: 0;
	text-align: center;
	position: absolute;
	cursor: default;
	display: block;
	overflow: hidden;
	right: 0;
}
/* more specificity required here to override default borders */
.ui-spinner a.ui-spinner-button {
	border-top: none;
	border-bottom: none;
	border-right: none;
}
/* vertically center icon */
.ui-spinner .ui-icon {
	position: absolute;
	margin-top: -8px;
	top: 50%;
	left: 0;
}
.ui-spinner-up {
	top: 0;
}
.ui-spinner-down {
	bottom: 0;
}

/* TR overrides */
.ui-spinner .ui-icon-triangle-1-s {
	/* need to fix icons sprite */
	background-position: -65px -16px;
}
.ui-tabs {
	position: relative;/* position: relative prevents IE scroll bug (element with position: relative inside container with overflow: auto appear as "fixed") */
	padding: .2em;
}
.ui-tabs .ui-tabs-nav {
	margin: 0;
	padding: .2em .2em 0;
}
.ui-tabs .ui-tabs-nav li {
	list-style: none;
	float: left;
	position: relative;
	top: 0;
	margin: 1px .2em 0 0;
	border-bottom-width: 0;
	padding: 0;
	white-space: nowrap;
}
.ui-tabs .ui-tabs-nav .ui-tabs-anchor {
	float: left;
	padding: .5em 1em;
	text-decoration: none;
}
.ui-tabs .ui-tabs-nav li.ui-tabs-active {
	margin-bottom: -1px;
	padding-bottom: 1px;
}
.ui-tabs .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor,
.ui-tabs .ui-tabs-nav li.ui-state-disabled .ui-tabs-anchor,
.ui-tabs .ui-tabs-nav li.ui-tabs-loading .ui-tabs-anchor {
	cursor: text;
}
.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor {
	cursor: pointer;
}
.ui-tabs .ui-tabs-panel {
	display: block;
	border-width: 0;
	padding: 1em 1.4em;
	background: none;
}
.ui-tooltip {
	padding: 8px;
	position: absolute;
	z-index: 9999;
	max-width: 300px;
	-webkit-box-shadow: 0 0 5px #aaa;
	box-shadow: 0 0 5px #aaa;
}
body .ui-tooltip {
	border-width: 2px;
}

/* Component containers
----------------------------------*/
.ui-widget {
	font-family: Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;
	font-size: 1.1em;
}
.ui-widget .ui-widget {
	font-size: 1em;
}
.ui-widget input,
.ui-widget select,
.ui-widget textarea,
.ui-widget button {
	font-family: Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;
	font-size: 1em;
}
.ui-widget-content {
	border: 1px solid #dddddd;
	background: #eeeeee url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_highlight-soft_100_eeeeee_1x100.png) 50% top repeat-x;
	color: #333333;
}
.ui-widget-content a {
	color: #333333;
}
.ui-widget-header {
	border: 1px solid #e78f08;
	background: #f6a828 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x;
	color: #ffffff;
	font-weight: bold;
}
.ui-widget-header a {
	color: #ffffff;
}

/* Interaction states
----------------------------------*/
.ui-state-default,
.ui-widget-content .ui-state-default,
.ui-widget-header .ui-state-default {
	border: 1px solid #cccccc;
	background: #f6f6f6 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_glass_100_f6f6f6_1x400.png) 50% 50% repeat-x;
	font-weight: bold;
	color: #1c94c4;
}
.ui-state-default a,
.ui-state-default a:link,
.ui-state-default a:visited {
	color: #1c94c4;
	text-decoration: none;
}
.ui-state-hover,
.ui-widget-content .ui-state-hover,
.ui-widget-header .ui-state-hover,
.ui-state-focus,
.ui-widget-content .ui-state-focus,
.ui-widget-header .ui-state-focus {
	border: 1px solid #fbcb09;
	background: #fdf5ce url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_glass_100_fdf5ce_1x400.png) 50% 50% repeat-x;
	font-weight: bold;
	color: #c77405;
}
.ui-state-hover a,
.ui-state-hover a:hover,
.ui-state-hover a:link,
.ui-state-hover a:visited,
.ui-state-focus a,
.ui-state-focus a:hover,
.ui-state-focus a:link,
.ui-state-focus a:visited {
	color: #c77405;
	text-decoration: none;
}
.ui-state-active,
.ui-widget-content .ui-state-active,
.ui-widget-header .ui-state-active {
	border: 1px solid #fbd850;
	background: #ffffff url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_glass_65_ffffff_1x400.png) 50% 50% repeat-x;
	font-weight: bold;
	color: #eb8f00;
}
.ui-state-active a,
.ui-state-active a:link,
.ui-state-active a:visited {
	color: #eb8f00;
	text-decoration: none;
}

/* Interaction Cues
----------------------------------*/
.ui-state-highlight,
.ui-widget-content .ui-state-highlight,
.ui-widget-header .ui-state-highlight {
	border: 1px solid #fed22f;
	background: #ffe45c url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_highlight-soft_75_ffe45c_1x100.png) 50% top repeat-x;
	color: #363636;
}
.ui-state-highlight a,
.ui-widget-content .ui-state-highlight a,
.ui-widget-header .ui-state-highlight a {
	color: #363636;
}
.ui-state-error,
.ui-widget-content .ui-state-error,
.ui-widget-header .ui-state-error {
	border: 1px solid #cd0a0a;
	background: #b81900 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_diagonals-thick_18_b81900_40x40.png) 50% 50% repeat;
	color: #ffffff;
}
.ui-state-error a,
.ui-widget-content .ui-state-error a,
.ui-widget-header .ui-state-error a {
	color: #ffffff;
}
.ui-state-error-text,
.ui-widget-content .ui-state-error-text,
.ui-widget-header .ui-state-error-text {
	color: #ffffff;
}
.ui-priority-primary,
.ui-widget-content .ui-priority-primary,
.ui-widget-header .ui-priority-primary {
	font-weight: bold;
}
.ui-priority-secondary,
.ui-widget-content .ui-priority-secondary,
.ui-widget-header .ui-priority-secondary {
	opacity: .7;
	filter:Alpha(Opacity=70);
	font-weight: normal;
}
.ui-state-disabled,
.ui-widget-content .ui-state-disabled,
.ui-widget-header .ui-state-disabled {
	opacity: .35;
	filter:Alpha(Opacity=35);
	background-image: none;
}
.ui-state-disabled .ui-icon {
	filter:Alpha(Opacity=35); /* For IE8 - See #6059 */
}

/* Icons
----------------------------------*/

/* states and images */
.ui-icon {
	width: 16px;
	height: 16px;
}
.ui-icon,
.ui-widget-content .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_222222_256x240.png);
}
.ui-widget-header .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ffffff_256x240.png);
}
.ui-state-default .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ef8c08_256x240.png);
}
.ui-state-hover .ui-icon,
.ui-state-focus .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ef8c08_256x240.png);
}
.ui-state-active .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ef8c08_256x240.png);
}
.ui-state-highlight .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_228ef1_256x240.png);
}
.ui-state-error .ui-icon,
.ui-state-error-text .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ffd27a_256x240.png);
}

/* positioning */
.ui-icon-blank { background-position: 16px 16px; }
.ui-icon-carat-1-n { background-position: 0 0; }
.ui-icon-carat-1-ne { background-position: -16px 0; }
.ui-icon-carat-1-e { background-position: -32px 0; }
.ui-icon-carat-1-se { background-position: -48px 0; }
.ui-icon-carat-1-s { background-position: -64px 0; }
.ui-icon-carat-1-sw { background-position: -80px 0; }
.ui-icon-carat-1-w { background-position: -96px 0; }
.ui-icon-carat-1-nw { background-position: -112px 0; }
.ui-icon-carat-2-n-s { background-position: -128px 0; }
.ui-icon-carat-2-e-w { background-position: -144px 0; }
.ui-icon-triangle-1-n { background-position: 0 -16px; }
.ui-icon-triangle-1-ne { background-position: -16px -16px; }
.ui-icon-triangle-1-e { background-position: -32px -16px; }
.ui-icon-triangle-1-se { background-position: -48px -16px; }
.ui-icon-triangle-1-s { background-position: -64px -16px; }
.ui-icon-triangle-1-sw { background-position: -80px -16px; }
.ui-icon-triangle-1-w { background-position: -96px -16px; }
.ui-icon-triangle-1-nw { background-position: -112px -16px; }
.ui-icon-triangle-2-n-s { background-position: -128px -16px; }
.ui-icon-triangle-2-e-w { background-position: -144px -16px; }
.ui-icon-arrow-1-n { background-position: 0 -32px; }
.ui-icon-arrow-1-ne { background-position: -16px -32px; }
.ui-icon-arrow-1-e { background-position: -32px -32px; }
.ui-icon-arrow-1-se { background-position: -48px -32px; }
.ui-icon-arrow-1-s { background-position: -64px -32px; }
.ui-icon-arrow-1-sw { background-position: -80px -32px; }
.ui-icon-arrow-1-w { background-position: -96px -32px; }
.ui-icon-arrow-1-nw { background-position: -112px -32px; }
.ui-icon-arrow-2-n-s { background-position: -128px -32px; }
.ui-icon-arrow-2-ne-sw { background-position: -144px -32px; }
.ui-icon-arrow-2-e-w { background-position: -160px -32px; }
.ui-icon-arrow-2-se-nw { background-position: -176px -32px; }
.ui-icon-arrowstop-1-n { background-position: -192px -32px; }
.ui-icon-arrowstop-1-e { background-position: -208px -32px; }
.ui-icon-arrowstop-1-s { background-position: -224px -32px; }
.ui-icon-arrowstop-1-w { background-position: -240px -32px; }
.ui-icon-arrowthick-1-n { background-position: 0 -48px; }
.ui-icon-arrowthick-1-ne { background-position: -16px -48px; }
.ui-icon-arrowthick-1-e { background-position: -32px -48px; }
.ui-icon-arrowthick-1-se { background-position: -48px -48px; }
.ui-icon-arrowthick-1-s { background-position: -64px -48px; }
.ui-icon-arrowthick-1-sw { background-position: -80px -48px; }
.ui-icon-arrowthick-1-w { background-position: -96px -48px; }
.ui-icon-arrowthick-1-nw { background-position: -112px -48px; }
.ui-icon-arrowthick-2-n-s { background-position: -128px -48px; }
.ui-icon-arrowthick-2-ne-sw { background-position: -144px -48px; }
.ui-icon-arrowthick-2-e-w { background-position: -160px -48px; }
.ui-icon-arrowthick-2-se-nw { background-position: -176px -48px; }
.ui-icon-arrowthickstop-1-n { background-position: -192px -48px; }
.ui-icon-arrowthickstop-1-e { background-position: -208px -48px; }
.ui-icon-arrowthickstop-1-s { background-position: -224px -48px; }
.ui-icon-arrowthickstop-1-w { background-position: -240px -48px; }
.ui-icon-arrowreturnthick-1-w { background-position: 0 -64px; }
.ui-icon-arrowreturnthick-1-n { background-position: -16px -64px; }
.ui-icon-arrowreturnthick-1-e { background-position: -32px -64px; }
.ui-icon-arrowreturnthick-1-s { background-position: -48px -64px; }
.ui-icon-arrowreturn-1-w { background-position: -64px -64px; }
.ui-icon-arrowreturn-1-n { background-position: -80px -64px; }
.ui-icon-arrowreturn-1-e { background-position: -96px -64px; }
.ui-icon-arrowreturn-1-s { background-position: -112px -64px; }
.ui-icon-arrowrefresh-1-w { background-position: -128px -64px; }
.ui-icon-arrowrefresh-1-n { background-position: -144px -64px; }
.ui-icon-arrowrefresh-1-e { background-position: -160px -64px; }
.ui-icon-arrowrefresh-1-s { background-position: -176px -64px; }
.ui-icon-arrow-4 { background-position: 0 -80px; }
.ui-icon-arrow-4-diag { background-position: -16px -80px; }
.ui-icon-extlink { background-position: -32px -80px; }
.ui-icon-newwin { background-position: -48px -80px; }
.ui-icon-refresh { background-position: -64px -80px; }
.ui-icon-shuffle { background-position: -80px -80px; }
.ui-icon-transfer-e-w { background-position: -96px -80px; }
.ui-icon-transferthick-e-w { background-position: -112px -80px; }
.ui-icon-folder-collapsed { background-position: 0 -96px; }
.ui-icon-folder-open { background-position: -16px -96px; }
.ui-icon-document { background-position: -32px -96px; }
.ui-icon-document-b { background-position: -48px -96px; }
.ui-icon-note { background-position: -64px -96px; }
.ui-icon-mail-closed { background-position: -80px -96px; }
.ui-icon-mail-open { background-position: -96px -96px; }
.ui-icon-suitcase { background-position: -112px -96px; }
.ui-icon-comment { background-position: -128px -96px; }
.ui-icon-person { background-position: -144px -96px; }
.ui-icon-print { background-position: -160px -96px; }
.ui-icon-trash { background-position: -176px -96px; }
.ui-icon-locked { background-position: -192px -96px; }
.ui-icon-unlocked { background-position: -208px -96px; }
.ui-icon-bookmark { background-position: -224px -96px; }
.ui-icon-tag { background-position: -240px -96px; }
.ui-icon-home { background-position: 0 -112px; }
.ui-icon-flag { background-position: -16px -112px; }
.ui-icon-calendar { background-position: -32px -112px; }
.ui-icon-cart { background-position: -48px -112px; }
.ui-icon-pencil { background-position: -64px -112px; }
.ui-icon-clock { background-position: -80px -112px; }
.ui-icon-disk { background-position: -96px -112px; }
.ui-icon-calculator { background-position: -112px -112px; }
.ui-icon-zoomin { background-position: -128px -112px; }
.ui-icon-zoomout { background-position: -144px -112px; }
.ui-icon-search { background-position: -160px -112px; }
.ui-icon-wrench { background-position: -176px -112px; }
.ui-icon-gear { background-position: -192px -112px; }
.ui-icon-heart { background-position: -208px -112px; }
.ui-icon-star { background-position: -224px -112px; }
.ui-icon-link { background-position: -240px -112px; }
.ui-icon-cancel { background-position: 0 -128px; }
.ui-icon-plus { background-position: -16px -128px; }
.ui-icon-plusthick { background-position: -32px -128px; }
.ui-icon-minus { background-position: -48px -128px; }
.ui-icon-minusthick { background-position: -64px -128px; }
.ui-icon-close { background-position: -80px -128px; }
.ui-icon-closethick { background-position: -96px -128px; }
.ui-icon-key { background-position: -112px -128px; }
.ui-icon-lightbulb { background-position: -128px -128px; }
.ui-icon-scissors { background-position: -144px -128px; }
.ui-icon-clipboard { background-position: -160px -128px; }
.ui-icon-copy { background-position: -176px -128px; }
.ui-icon-contact { background-position: -192px -128px; }
.ui-icon-image { background-position: -208px -128px; }
.ui-icon-video { background-position: -224px -128px; }
.ui-icon-script { background-position: -240px -128px; }
.ui-icon-alert { background-position: 0 -144px; }
.ui-icon-info { background-position: -16px -144px; }
.ui-icon-notice { background-position: -32px -144px; }
.ui-icon-help { background-position: -48px -144px; }
.ui-icon-check { background-position: -64px -144px; }
.ui-icon-bullet { background-position: -80px -144px; }
.ui-icon-radio-on { background-position: -96px -144px; }
.ui-icon-radio-off { background-position: -112px -144px; }
.ui-icon-pin-w { background-position: -128px -144px; }
.ui-icon-pin-s { background-position: -144px -144px; }
.ui-icon-play { background-position: 0 -160px; }
.ui-icon-pause { background-position: -16px -160px; }
.ui-icon-seek-next { background-position: -32px -160px; }
.ui-icon-seek-prev { background-position: -48px -160px; }
.ui-icon-seek-end { background-position: -64px -160px; }
.ui-icon-seek-start { background-position: -80px -160px; }
/* ui-icon-seek-first is deprecated, use ui-icon-seek-start instead */
.ui-icon-seek-first { background-position: -80px -160px; }
.ui-icon-stop { background-position: -96px -160px; }
.ui-icon-eject { background-position: -112px -160px; }
.ui-icon-volume-off { background-position: -128px -160px; }
.ui-icon-volume-on { background-position: -144px -160px; }
.ui-icon-power { background-position: 0 -176px; }
.ui-icon-signal-diag { background-position: -16px -176px; }
.ui-icon-signal { background-position: -32px -176px; }
.ui-icon-battery-0 { background-position: -48px -176px; }
.ui-icon-battery-1 { background-position: -64px -176px; }
.ui-icon-battery-2 { background-position: -80px -176px; }
.ui-icon-battery-3 { background-position: -96px -176px; }
.ui-icon-circle-plus { background-position: 0 -192px; }
.ui-icon-circle-minus { background-position: -16px -192px; }
.ui-icon-circle-close { background-position: -32px -192px; }
.ui-icon-circle-triangle-e { background-position: -48px -192px; }
.ui-icon-circle-triangle-s { background-position: -64px -192px; }
.ui-icon-circle-triangle-w { background-position: -80px -192px; }
.ui-icon-circle-triangle-n { background-position: -96px -192px; }
.ui-icon-circle-arrow-e { background-position: -112px -192px; }
.ui-icon-circle-arrow-s { background-position: -128px -192px; }
.ui-icon-circle-arrow-w { background-position: -144px -192px; }
.ui-icon-circle-arrow-n { background-position: -160px -192px; }
.ui-icon-circle-zoomin { background-position: -176px -192px; }
.ui-icon-circle-zoomout { background-position: -192px -192px; }
.ui-icon-circle-check { background-position: -208px -192px; }
.ui-icon-circlesmall-plus { background-position: 0 -208px; }
.ui-icon-circlesmall-minus { background-position: -16px -208px; }
.ui-icon-circlesmall-close { background-position: -32px -208px; }
.ui-icon-squaresmall-plus { background-position: -48px -208px; }
.ui-icon-squaresmall-minus { background-position: -64px -208px; }
.ui-icon-squaresmall-close { background-position: -80px -208px; }
.ui-icon-grip-dotted-vertical { background-position: 0 -224px; }
.ui-icon-grip-dotted-horizontal { background-position: -16px -224px; }
.ui-icon-grip-solid-vertical { background-position: -32px -224px; }
.ui-icon-grip-solid-horizontal { background-position: -48px -224px; }
.ui-icon-gripsmall-diagonal-se { background-position: -64px -224px; }
.ui-icon-grip-diagonal-se { background-position: -80px -224px; }


/* Misc visuals
----------------------------------*/

/* Corner radius */
.ui-corner-all,
.ui-corner-top,
.ui-corner-left,
.ui-corner-tl {
	border-top-left-radius: 4px;
}
.ui-corner-all,
.ui-corner-top,
.ui-corner-right,
.ui-corner-tr {
	border-top-right-radius: 4px;
}
.ui-corner-all,
.ui-corner-bottom,
.ui-corner-left,
.ui-corner-bl {
	border-bottom-left-radius: 4px;
}
.ui-corner-all,
.ui-corner-bottom,
.ui-corner-right,
.ui-corner-br {
	border-bottom-right-radius: 4px;
}

/* Overlays */
.ui-widget-overlay {
	background: #666666 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_diagonals-thick_20_666666_40x40.png) 50% 50% repeat;
	opacity: .5;
	filter: Alpha(Opacity=50);
}
.ui-widget-shadow {
	margin: -5px 0 0 -5px;
	padding: 5px;
	background: #000000 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_flat_10_000000_40x100.png) 50% 50% repeat-x;
	opacity: .2;
	filter: Alpha(Opacity=20);
	border-radius: 5px;
}
#myAutocomplete{width:100% !important;}
.ui-autocomplete-multiselect{border:none !important;}
</style>
                                   <div class="form-group col-sm-6">
									   <div class="form-group  wrapper intrest_sec margin0">
										<label for="bio" class="hide"><?php esc_html_e( 'Interests', 'classiera' );?></label>
											<textarea type="text" name="my_interest" placeholder="interests" id="myAutocomplete" class="form-control form-control-sm"  value="" <?php if($user_info->interests==''){/*?> placeholder="<?php esc_html_e( 'interests:', 'classiera' ); ?>"<?php */}?>></textarea>
											<input type="hidden" name="my_interest1" id="myAutocomplete1" value="<?php echo $user_info->interests; ?>"/>
											<div class="hide">	<?php 
												if($user_info->interests!='')
												{
													echo 'Selected Interests: '.$user_info->interests;
												}
												?>
											</div>	
										</div>
									  </div>	
								  </div>	  
                             </div>
							 <script>
								jQuery(document).ready(function(){
								  var $=jQuery.noConflict();
								});
							</script>
							<?php /*<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> */?>
							
							
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script> 


<?php /*<script src="<?php echo get_template_directory_uri();?>/js/multiselectautocomplete/jquery.autocomplete.multiselect.js"></script> */?>
							<script>
								// http://jsfiddle.net/mekwall/sgxKJ/

								$.widget("ui.autocomplete", $.ui.autocomplete, {
									options : $.extend({}, this.options, {
										multiselect: true
									}),
									_create: function(){
										this._super();
								
										var self = this,
											o = self.options;
								
										if (o.multiselect) {
											console.log('multiselect true');
								
											self.selectedItems = {};           
											self.multiselect = $("<div></div>")
												.addClass("ui-autocomplete-multiselect ui-state-default ui-widget")
												.css("width", self.element.width())
												.insertBefore(self.element)
												.append(self.element)
												.bind("click.autocomplete", function(){
													self.element.focus();
													
												});
											
											var fontSize = parseInt(self.element.css("fontSize"), 10);
											function autoSize(e){
												// Hackish autosizing
												var $this = $(this);
												$this.width(1).width(this.scrollWidth+fontSize-1);
											};
								
											var kc = $.ui.keyCode;
											self.element.bind({
												"keydown.autocomplete": function(e){
													if ((this.value === "") && (e.keyCode == kc.BACKSPACE)) {
														var prev = self.element.prev();
														delete self.selectedItems[prev.text()];
														
														prev.remove();
													}
												},
												// TODO: Implement outline of container
												"focus.autocomplete blur.autocomplete": function(){
													self.multiselect.toggleClass("ui-state-active");
												},
												"keypress.autocomplete change.autocomplete focus.autocomplete blur.autocomplete": autoSize
											}).trigger("change");
								
											// TODO: There's a better way?
											o.select = o.select || function(e, ui) {
											if($("#myAutocomplete1").val()!='')
											{
												$("#myAutocomplete1").val($("#myAutocomplete1").val()+','+ui.item.label);
											}
											else
											{
												$("#myAutocomplete1").val(ui.item.label);
											}
											
												$("<div></div>")
													.addClass("ui-autocomplete-multiselect-item")
													.text(ui.item.label)
													.append(
														$("<span></span>")
															.addClass("ui-icon ui-icon-close")
															.click(function(){
																var item = $(this).parent();
																delete self.selectedItems[item.text()];
																
																item.remove();
																
															})
													)
													.insertBefore(self.element);
												self.selectedItems[ui.item.label] = ui.item;
												self._value("");
												
												return false;
											}
								
											self.options.open = function(e, ui) {
												var pos = self.multiselect.position();
												pos.top += self.multiselect.height();
												self.menu.element.position(pos);
											}
										}
								
										return this;
									}
								});
								
								$(function(){
									var availableTags = [
									<?php
									$results = $wpdb->get_results( "SELECT * FROM wp_interest order by id desc" );
									 if(count($results)>0)
  									 {
									 	$count=1;
									 	foreach($results as $key=>$val)
										{
											if($count==count($results))
											{?>
													"<?php echo $val->name;?>"
												<?php
											
											}
											else
											{?>
													"<?php echo $val->name;?>",
												<?php
											}
											$count++;

										}
									 }
									?>
									];
									$('#myAutocomplete').autocomplete({
										source: availableTags,
										multiselect: true
									});
								})
							</script>
								<!--biography-->
								
								<div class="clearfix"></div>
								<div class="top-buffer2"></div>
								<div class="clearfix"></div>
								
								    <p class="user-detail-section-heading poppins-lite pad0">
										 <img src="<?=get_template_directory_uri().'/../classiera-child/images/share.png' ?>" width="50" height="50"/><br/>
										<?php esc_html_e( 'social media links', 'classiera' ); ?>
									</p>

									<div class="col-md-10 col-md-offset-1">
									
									    <!-- Social Profile Links -->
											<div class="form-inline form-inline-margin">
												<div class="form-group col-sm-6">
													<label for="facebook" class="hide"><?php esc_html_e( 'Facebook', 'classiera' ); ?></label>
													<div class="inner-addon">
														<div class="input-group">
															<div class="input-group-addon input-group-addon-width-sm hide"><i class="fa fa fa-facebook"></i></div>
															<input type="url" id="facebook" name="facebook" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'facebook', 'classiera' ); ?>" value="<?php echo $user_info->facebook; ?>">
														</div>
													</div>
												</div><!--facebook-->
												<div class="form-group col-sm-6">
													<label for="twitter" class="hide"><?php esc_html_e( 'Twitter', 'classiera' ); ?></label>
													<div class="inner-addon">
														<div class="input-group">
															<div class="input-group-addon input-group-addon-width-sm hide"><i class="fa fa fa-twitter"></i></div>
															<input type="url" id="twitter" name="twitter" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'twitter', 'classiera' ); ?>" value="<?php echo $user_info->twitter; ?>">
														</div>
													</div>
												</div><!--twitter-->
												<div class="form-group col-sm-6">
													<label for="googleplus" class="hide"><?php esc_html_e( 'Google Plus', 'classiera' ); ?></label>
													<div class="inner-addon">
														<div class="input-group">
															<div class="input-group-addon input-group-addon-width-sm hide"><i class="fa fa fa-google-plus"></i></div>
															<input type="url" id="googleplus" name="google-plus" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Google +', 'classiera' ); ?>" value="<?php echo $user_info->googleplus; ?>">
														</div>
													</div>
												</div><!--Google Plus-->
												<div class="form-group col-sm-6">
													<label for="instagram" class="hide"><?php esc_html_e( 'instagram', 'classiera' ); ?></label>
													<div class="inner-addon">
														<div class="input-group">
															<div class="input-group-addon input-group-addon-width-sm hide"><i class="fa fa fa-instagram"></i></div>
															<input type="url" id="instagram" name="instagram" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'instagram', 'classiera' ); ?>" value="<?php echo $user_info->instagram; ?>">
														</div>
													</div>
												</div><!--instagram-->
												<div class="form-group col-sm-6">
													<label for="pinterest" class="hide"><?php esc_html_e( 'Pinterest', 'classiera' ); ?></label>
													<div class="inner-addon">
														<div class="input-group">
															<div class="input-group-addon input-group-addon-width-sm hide"><i class="fa fa fa-pinterest-p"></i></div>
															<input type="url" id="pinterest" name="pinterest" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Pinterest', 'classiera' ); ?>" value="<?php echo $user_info->pinterest; ?>">
														</div>
													</div>
												</div><!--pinterest-->
												<div class="form-group col-sm-6">
													<label for="linkedin" class="hide"><?php esc_html_e( 'Linkedin', 'classiera' ); ?></label>
													<div class="inner-addon">
														<div class="input-group">
															<div class="input-group-addon input-group-addon-width-sm hide"><i class="fa fa fa-linkedin"></i></div>
															<input type="url" id="linkedin" name="linkedin" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'linkedin', 'classiera' ); ?>" value="<?php echo $user_info->linkedin; ?>">
														</div>
													</div>
												</div><!--linkedin-->
												<div class="form-group col-sm-6">
													<label for="vimeo" class="hide"><?php esc_html_e( 'Vimeo', 'classiera' ); ?></label>
													<div class="inner-addon">
														<div class="input-group">
															<div class="input-group-addon input-group-addon-width-sm hide"><i class="fa fa fa-vimeo"></i></div>
															<input type="url" id="vimeo" name="vimeo" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'vimeo', 'classiera' ); ?>" value="<?php echo $user_info->vimeo; ?>">
														</div>
													</div>
												</div><!--vimeo-->
												<div class="form-group col-sm-6">
													<label for="youtube" class="hide"><?php esc_html_e( 'Youtube', 'classiera' ); ?></label>
													<div class="inner-addon">
														<div class="input-group">
															<div class="input-group-addon input-group-addon-width-sm hide"><i class="fa fa fa-youtube"></i></div>
															<input type="url" id="youtube" name="youtube" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'you tube', 'classiera' ); ?>" value="<?php echo $user_info->youtube; ?>">
														</div>
													</div>
												</div><!--youtube-->
											</div><!--form-inline-->
											<!-- Socail Profile Links -->							
											 
									
									</div>
								
						    </div>
							<!-- user basic information -->
						
							<div class="clearfix"></div>
							<div class="top-buffer2"></div>							
							<div class="clearfix"></div>	
							
							
							
							
                            <div class="branch_location_sec">							
								<div class="single_title">
									<h2>branch locations</h2>
								</div>

								<div class="clearfix"></div>
								<div class="top-buffer2"></div>							
								<div class="clearfix"></div>							
								
								<div class="col-md-10 col-md-offset-1">	
								  <div class="row">
									<div class="form-group col-sm-6">
										<label for="Location:" class="hide"><?php esc_html_e( 'branch location:', 'classiera' ); ?></label>
										<div class="inner-addon">
											<a id="getLocation" href="#" name="location" class="form-icon form-icon-size-small" title="<?php esc_html_e('Click here to get your own location', 'classiera'); ?>"><i class="fa fa-crosshairs"></i></a>
											<?php /* <input type="text" id="getCity" name="<?php echo $classieraLocationName; ?>" class="form-control form-control-sm sharp-edge" placeholder="<?php esc_html_e('location', 'classiera'); ?>" onFocus="geolocate()">*/
								   
								   ?>
											 <input type="text" id="getCity" name="branch_location1" class="form-control form-control-sm sharp-edge" placeholder="<?php if(!empty($branch_location1)){echo $branch_location1[0];}else{echo 'location';} ?>" onFocus="geolocate1()" value="<?php //echo $user_info->location; ?>">
											
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<label for="Location:" class="hide"><?php esc_html_e( 'Branch Location:', 'classiera' ); ?></label>
										<div class="inner-addon">
											<a id="getLocation" href="#" name="location" class="form-icon form-icon-size-small" title="<?php esc_html_e('Click here to get your own location', 'classiera'); ?>">
												<i class="fa fa-crosshairs"></i>
											</a>
										   <?php /* <input type="text" id="getCity" name="<?php echo $classieraLocationName; ?>" class="form-control form-control-sm sharp-edge" placeholder="<?php esc_html_e('location', 'classiera'); ?>" onFocus="geolocate()">*/?>
										   <input type="text" id="getCity1" name="branch_location2" class="form-control form-control-sm sharp-edge" placeholder="<?php if(!empty($branch_location2)){ echo $branch_location2[0];}else{echo 'location';} ?>" onFocus="geolocate2()" value="<?php //echo $user_info->location; ?>">
														
										</div>
									</div>
									<div class="form-group col-sm-6">
										<label for="Location:" class="hide"><?php esc_html_e( 'Branch Location:', 'classiera' ); ?></label>
										<div class="inner-addon">
											<a id="getLocation" href="#" name="location" class="form-icon form-icon-size-small" title="<?php esc_html_e('Click here to get your own location', 'classiera'); ?>">
													<i class="fa fa-crosshairs"></i>
												</a>
											   <?php /* <input type="text" id="getCity" name="<?php echo $classieraLocationName; ?>" class="form-control form-control-sm sharp-edge" placeholder="<?php esc_html_e('location', 'classiera'); ?>" onFocus="geolocate()">*/?>
											   <input type="text" id="getCity2" name="branch_location3" class="form-control form-control-sm sharp-edge" placeholder="<?php if(!empty($branch_location3)){echo $branch_location3[0];}else{echo 'location';} ?>" onFocus="geolocate3()" value="<?php //echo $user_info->location; ?>">
															
										</div>
									</div>
							   
								
								
									
										<?php
										//echo '=>'.$custuid;
										$branch_count=get_user_meta($custuid, "branch_count");
										if($branch_count[0]>3)
										{?>
											<input type="hidden" value="<?php echo $branch_count[0];?>" name="hdn_total_branch" id="hdn_total_branch" />
											<?php
											for($lp=4;$lp<=$branch_count[0];$lp++)
											{?>
											<div class="form-group col-sm-6">
													<label for="Location:" class="hide"><?php esc_html_e( 'Branch Location:', 'classiera' ); ?></label>
													<div class="inner-addon">
														<a id="getLocation" href="#" name="location" class="form-icon form-icon-size-small" title="<?php esc_html_e('Click here to get your own location', 'classiera'); ?>">
															   <i class="fa fa-crosshairs"></i>
															</a>
													<?php
													$cblc=get_user_meta($custuid, "branch_location".$lp);
													?>
													 <input type="text" id="getCity<?php echo $lp;?>" name="branch_location<?php echo $lp;?>" class="form-control form-control-sm sharp-edge" placeholder="<?php if(!empty($cblc)){echo $cblc[0];}else{echo 'location';} ?>" onFocus="geolocate<?php echo $lp;?>()" value="<?php echo $cblc[0]; ?>">
														
													</div>
											</div>
												<?php
											}
										}
										else
										{?>
											<input type="hidden" value="3" name="hdn_total_branch" id="hdn_total_branch" /><?php
										}
										?>
										
										<div id="more_branches">
										<?php
										
										for($lp=$branch_count[0];$lp<=10;$lp++)
										{?>
											<div class="form-group col-sm-6" id="custom_created_location_div_<?php echo $lp;?>" style="display:none;">
													<label for="Location:" class="hide"><?php esc_html_e( 'Branch Location:', 'classiera' ); ?></label>
													<div class="inner-addon">
														<a id="getLocation" href="#" name="location" class="form-icon form-icon-size-small" title="<?php esc_html_e('Click here to get your own location', 'classiera'); ?>">
															   <i class="fa fa-crosshairs"></i>
															</a>
													<?php
													$cblc=get_user_meta($custuid, "branch_location".$lp);
													?>
													 <input type="text" id="getCity<?php echo $lp;?>" name="branch_location<?php echo $lp;?>" class="form-control form-control-sm sharp-edge" placeholder="<?php if(!empty($cblc)){echo $cblc[0];}else{echo 'location';} ?>" onFocus="geolocate<?php echo $lp;?>()" value="<?php echo $cblc[0]; ?>">
														
													</div>
											</div>
										<?php
										}
										?>
										</div>
									 
										<div class="clearfix"></div>
										<div class="form-group col-md-6 col-md-offset-3">
										
											<button type="button" class="add_branch-btn form-control form-control-sm" onclick="return add_branch();"><span class="pull-left">Add more branch</span><img src="<?=get_template_directory_uri().'/../classiera-child/images/spider.png' ?>" class="pull-right" width="35" height="35"/></button>
											<script type="text/javascript" language="javascript">
											var is_added=0;
											function add_branch()
											{
												
												var tot_branch=jQuery("#hdn_total_branch").val();
												if(tot_branch>=10)
												{
													alert("You cannot add more than 10 branches");
													return false;
												}
												tot_branch=parseInt(tot_branch)+1;
												//alert("#custom_created_location_div_"+tot_branch);
												jQuery("#custom_created_location_div_"+tot_branch).show();
												//alert(tot_branch); 
												//jQuery("#more_branches").append("<div class='form-group col-sm-6'><label for='Location:' class='hide'>Branch Location</label><div class='inner-addon'><a id='getLocation' href='#' name='location' class='form-icon form-icon-size-small' title='Click here to get your own location'><i class='fa fa-crosshairs' style='position:  relative;left: 300px;'></i></a><input type='text' id='getCity"+tot_branch+"' name='branch_location"+tot_branch+"' class='form-control form-control-sm sharp-edge' placeholder='' onFocus='geolocate"+tot_branch+"()' value=''></div></div>");
												jQuery("#hdn_total_branch").val(tot_branch);
											}
											</script>
										</div>
										<div class="clearfix"></div>
									</div>
							    </div>
							</div>
								
								
							<div class="clearfix"></div>
							<div class="top-buffer2"></div>							
							<div class="clearfix"></div>

						<!-- Contact Details -->
							<div class="single_title">
									<h2><?php esc_html_e( 'User Details', 'classiera' ); ?></h2>
							</div>
							
							<div class="form-inline row form-inline-margin">
							    <div class="top-buffer2"></div>
							    <div class="col-md-10 col-md-offset-1">
									<div class="form-group col-sm-6">
										<label for="phone" class="hide"><?php esc_html_e( 'Phone Number', 'classiera' ); ?></label>
										<div class="inner-addon">
											<input type="tel" id="phone" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'phone number', 'classiera' ); ?>" name="phone" value="<?php echo $user_info->phone; ?>">
										</div>
									</div><!--Phone Number-->
									<div class="form-group col-sm-6">
										<label for="mobile" class="hide"><?php esc_html_e( 'Mobile Number', 'classiera' ); ?></label>
										<div class="inner-addon">
											<input type="tel" id="mobile" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'mobile number', 'classiera' ); ?>" name="phone2" value="<?php echo $user_info->phone2; ?>">
										</div>
									</div><!--Mobile Number-->
									<div class="form-group col-sm-6">
										<label for="username" class="hide"><?php esc_html_e( 'Username', 'classiera' ); ?></label>
										<div class="inner-addon">
											<input type="text" id="username" name="username" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'username', 'classiera' ); ?>" value="<?php echo $user_info->user_login; ?>">
										</div>
									</div><!--Your Website-->
									<div class="form-group col-sm-6">
										<label for="email" class="hide"><?php esc_html_e( 'Your Email', 'classiera' ); ?></label>
										<div class="inner-addon">
											<input type="text" id="email" name="email" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'your e-mail', 'classiera' ); ?>" value="<?php echo $user_info->user_email; ?>">
											<input type="hidden" name="current_email" value="<?php echo $user_info->user_email; ?>">
										</div>
									</div><!--Your Email-->
									<?php /*<div class="form-group col-sm-5">
										<label for="email"><?php esc_html_e( 'Your Country', 'classiera' ); ?></label>
										<div class="inner-addon">
											<input type="text" id="country" name="country" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'enter your country', 'classiera' ); ?>" value="<?php echo $user_info->country; ?>">
										</div>
									</div><!--Your Country-->
									<div class="form-group col-sm-5">
										<label for="email"><?php esc_html_e( 'Your State', 'classiera' ); ?></label>
										<div class="inner-addon">
											<input type="text" id="state" name="state" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'enter your State', 'classiera' ); ?>" value="<?php echo $user_info->state; ?>">
										</div>
									</div><!--Your State-->
									<div class="form-group col-sm-5">
										<label for="email"><?php esc_html_e( 'Your City', 'classiera' ); ?></label>
										<div class="inner-addon">
											<input type="text" id="city" name="city" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'enter your City', 'classiera' ); ?>" value="<?php echo $user_info->city; ?>">
										</div>
									</div><!--Your City-->
									<div class="form-group col-sm-5">
										<label for="email"><?php esc_html_e( 'Your Post Code', 'classiera' ); ?></label>
										<div class="inner-addon">
											<input type="text" id="post_code" name="post_code" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'enter your Post Code', 'classiera' ); ?>" value="<?php echo $user_info->postcode; ?>">
										</div>
									</div><!--Your Post Code-->
									<div class="form-group col-sm-12">
										<label for="address"><?php esc_html_e( 'Address', 'classiera' ); ?></label>
										<div class="inner-addon">
											<textarea name="address" id="address" placeholder="<?php esc_html_e( 'enter your address', 'classiera' ); ?>"><?php echo $user_info->address; ?></textarea>
										</div>
									</div><!--Address-->*/?>
								</div>	
							</div>
							<!-- Contact Details -->
							
							
							
							
							
							<!-- Update your Password -->
							
							<p class="user-detail-section-heading poppins-lite">
								 <img src="<?=get_template_directory_uri().'/../classiera-child/images/update_pass.png' ?>" width="50" height="50"/><br/>
								 <?php esc_html_e( 'update password', 'classiera' ); ?>
							</p>
							
							<div class="clearfix"></div>
							<div class="top-buffer1"></div>							
							<div class="clearfix"></div>
							
							<div class="col-md-10 col-md-offset-1">
									<div class="form-group col-sm-6">
										<label for="current-pass" class="hide">
											<?php esc_html_e( 'Enter Current Password', 'classiera' ); ?>
										</label>
											<input type="password" id="current-pass" name="pwd" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Enter your current password', 'classiera' ); ?>">
									</div>
									<div class="form-group col-sm-6">
										<label for="new-pass" class="hide">
											<?php esc_html_e( 'Enter New Password', 'classiera' ); ?>
										</label>
											<input type="password" name="confirm" data-minlength="5" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Enter Password', 'classiera' ); ?>" id="new-pass" data-error="<?php esc_html_e( 'Password required', 'classiera' ); ?>">
											<div class="help-block hide">
												<?php esc_html_e( 'Minimum of 5 characters', 'classiera' ); ?>
											</div>
									</div>
									<div class="form-group col-sm-6 col-sm-offset-3">
										<label for="re-enter" class="hide">
											<?php esc_html_e( 'Re-enter New Password', 'classiera' ); ?>
										</label>
											<input type="password" id="re-enter" name="confirm2" class="form-control form-control-sm sharp-edge" placeholder="<?php esc_html_e( 'Re-enter New Password', 'classiera' ); ?>" data-match="#new-pass" data-match-error="<?php esc_html_e( 'Whoops, these dont match', 'classiera' ); ?>">
											<div class="help-block with-errors"></div>
									</div>
									
									
							</div>	
							
							
							
							<p class="hide"><?php esc_html_e( 'If you would like to change the password type a new one. Otherwise leave this blank.', 'classiera' ); ?></p>
							<?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>
							<input type="hidden" name="submitted" id="submitted" value="true" />
							<div class="top-buffer1"></div>
							<div class="clearfix"></div>
							<hr class="line1">
							<div class="col-md-10 col-md-offset-1">
								<div class="col-md-6 col-md-offset-3">
									 <div class="top-buffer1"></div>
									 <button type="submit" name="op" value="update_profile" class="btn btn-primary update_btn sharp btn-sm btn-style-one"><?php esc_html_e('Update Now', 'classiera') ?></button>
									 <div class="top-buffer1"></div>
								</div>
							</div>
							<hr class="line1">
							<div class="clearfix"></div>
							<div class="top-buffer2"></div>
							
							<!-- Update your Password -->
						</form>
					</div><!--user-ads user-profile-settings-->
				</div><!--user-detail-section-->
			</div><!--col-lg-9-->
		</div><!--row-->
	</div><!--container-->	
</section><!--user-pages section-gray-bg-->	
<!-- user pages -->
<!--Verify Profile Modal-->
<div class="modal fade" id="verifyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<?php 
	$authorEmail = $user_info->user_email;
	echo $dbcode = get_the_author_meta('author_vcode', $user_ID);
	$classieraVerifyCode = md5($authorEmail);
	?>
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title text-uppercase text-center" id="myModalLabel">
					<?php esc_html_e('Verify Your Account Now', 'classiera') ?>
				</h4>
				<p class="text-uppercase text-center">
					<?php esc_html_e('Following these Steps', 'classiera') ?>
				</p>
			</div><!--modal-header-->
			<div class="modal-body">
				<form method="post" class="form-inline row classiera_get_code_form">
					<span class="classiera--loader"><img src="<?php echo get_template_directory_uri().'/images/loader.gif' ?>" alt="classiera loader"></span>
					<div class="form-group col-sm-9">
						<input type="text" class="form-control verify_email" placeholder="<?php esc_html_e('example@email.com', 'classiera') ?>" value="<?php echo $authorEmail ?>" disabled>
						<input type="hidden" value="<?php echo $classieraVerifyCode; ?>" name="" class="verify_code">
						<input type="hidden" value="<?php echo $user_ID; ?>" name="" class="verify_user_id">
					</div>
					<div class="form-group col-sm-3">
						<button type="submit" class="btn btn-primary sharp btn-sm btn-style-one verify_get_code">
							<?php esc_html_e('Get verification Code', 'classiera') ?>
						</button>
					</div>
				</form><!--classiera_get_code_form-->			
				<form method="post" class="form-inline row classiera_verify_form" <?php if($dbcode){ ?>style="display:block;" <?php } ?>>
					<span class="classiera--loader"><img src="<?php echo get_template_directory_uri().'/images/loader.gif' ?>" alt="classiera loader"></span>
					<h5 class="text-center text-uppercase">
						<?php esc_html_e('Check your email inbox and paste code below', 'classiera') ?>
					</h5>
					<div class="form-group col-sm-9">
						<input type="text" class="form-control verification_code" placeholder="<?php esc_html_e('Enter your verified code', 'classiera') ?>" value="" required>
						<input type="hidden" value="<?php echo $user_ID; ?>" name="" class="verify_user_id">
						<input type="hidden" value="<?php echo $authorEmail; ?>" name="" class="verify_email">
					</div>
					<div class="form-group col-sm-3">
						<button type="submit" class="btn btn-primary sharp btn-sm btn-style-one verify_code_btn">
							<?php esc_html_e('Verify Now', 'classiera') ?>
						</button>
					</div>
				</form><!--classiera_verify_form-->
				
				 <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete1;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete1 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity')),
            {types: ['geocode']});
			
		 autocomplete2 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity1')),
            {types: ['geocode']});
		 autocomplete3 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity2')),
            {types: ['geocode']});
			
		 autocomplete4 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity4')),
            {types: ['geocode']});
		
		 autocomplete5 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity5')),
            {types: ['geocode']});
		 autocomplete6 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity6')),
            {types: ['geocode']});
		 autocomplete7 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity7')),
            {types: ['geocode']});
		
		 autocomplete8 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity8')),
            {types: ['geocode']});
		 autocomplete9 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity9')),
            {types: ['geocode']});
		 autocomplete10 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('getCity10')),
            {types: ['geocode']});
		

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        //autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete1.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
     /* function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete1.setBounds(circle.getBounds());
          });
        }
      }*/
	   function geolocate1() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete1.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate2() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete2.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate3() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete3.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate4() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete4.setBounds(circle.getBounds());
          });
        }
      }
	  function geolocate5() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete5.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate6() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete6.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate7() {
	   
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
		 // alert("in if");
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete7.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate8() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete8.setBounds(circle.getBounds());
          });
        }
      }function geolocate9() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete9.setBounds(circle.getBounds());
          });
        }
      }
	  function geolocate10() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete10.setBounds(circle.getBounds());
          });
        }
      }
	  
	// jQuery("#more_branches").empty(); 
    </script>	
	
				<div class="classiera_verify_congrats text-center">
					
				</div><!--classiera_verify_congrats-->
			</div><!--modal-body-->
		</div><!--modal-content-->
	</div><!--modal-dialog modal-lg-->
</div><!--modal fade-->
<!--Verify Profile Modal-->

<?php get_footer(); ?>