<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage classiera
 * @since classiera 1.0
 */
get_header(); ?>
	
	<?php while ( have_posts() ) : the_post(); ?>


<?php 

global $redux_demo; 
global $current_user; wp_get_current_user(); $user_ID == $current_user->ID;

$profileLink = get_the_author_meta( 'user_url', $user_ID );
$contact_email = get_the_author_meta('user_email');
$login = $redux_demo['login'];
$classieraContactEmailError = $redux_demo['contact-email-error'];
$classieraContactNameError = $redux_demo['contact-name-error'];
$classieraConMsgError = $redux_demo['contact-message-error'];
$classieraContactThankyou = $redux_demo['contact-thankyou-message'];
$classieraRelatedCount = $redux_demo['classiera_related_ads_count'];
$classieraSearchStyle = $redux_demo['classiera_search_style'];
$classieraSingleAdStyle = $redux_demo['classiera_single_ad_style'];
$classieraPartnersStyle = $redux_demo['classiera_partners_style'];
$classieraComments = $redux_demo['classiera_sing_post_comments'];
$googleMapadPost = $redux_demo['google-map-adpost'];
$classieraToAuthor = $redux_demo['author-msg-box-off'];
$classieraReportAd = $redux_demo['classiera_report_ad'];
$locShownBy = $redux_demo['location-shown-by'];
$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
$classieraAuthorInfo = $redux_demo['classiera_author_contact_info'];
$classieraPriceSection = $redux_demo['classiera_sale_price_off'];
$classiera_bid_system = $redux_demo['classiera_bid_system'];
$category_icon_code = "";
$category_icon_color = "";
$your_image_url = "";

global $errorMessage;
global $emailError;
global $commentError;
global $subjectError;
global $humanTestError;
global $hasError;

//If the form is submitted
if(isset($_POST['submit'])) {
	if($_POST['submit'] == 'send_message'){
		//echo "send_message";
		//Check to make sure that the name field is not empty
		if(trim($_POST['contactName']) === '') {
			$errorMessage = $classieraContactNameError;
			$hasError = true;
		} elseif(trim($_POST['contactName']) === 'Name*') {
			$errorMessage = $classieraContactNameError;
			$hasError = true;
		}	else {
			$name = trim($_POST['contactName']);
		}

		//Check to make sure that the subject field is not empty
		if(trim($_POST['subject']) === '') {
			$errorMessage = $classiera_contact_subject_error;
			$hasError = true;
		} elseif(trim($_POST['subject']) === 'Subject*') {
			$errorMessage = $classiera_contact_subject_error;
			$hasError = true;
		}	else {
			$subject = trim($_POST['subject']);
		}
		
		//Check to make sure sure that a valid email address is submitted
		if(trim($_POST['email']) === ''){
			$errorMessage = $classieraContactEmailError;
			$hasError = true;		
		}else{
			$email = trim($_POST['email']);
		}
			
		//Check to make sure comments were entered	
		if(trim($_POST['comments']) === '') {
			$errorMessage = $classieraConMsgError;
			$hasError = true;
		} else {
			if(function_exists('stripslashes')) {
				$comments = stripslashes(trim($_POST['comments']));
			} else {
				$comments = trim($_POST['comments']);
			}
		}

		//Check to make sure that the human test field is not empty
		$classieraCheckAnswer = $_POST['humanAnswer'];
		if(trim($_POST['humanTest']) != $classieraCheckAnswer) {
			$errorMessage = esc_html__('Not Human', 'classiera');			
			$hasError = true;
		}
		$classieraPostTitle = $_POST['classiera_post_title'];	
		$classieraPostURL = $_POST['classiera_post_url'];
		
		//If there is no error, send the email		
		if(!isset($hasError)) {

			$emailTo = $contact_email;
			//$emailTo = 'votivewp.awadhesh@gmail.com';
			$subject = $subject;	
			$body = "Name: $name \n\nEmail: $email \n\nMessage: $comments";
			$headers = 'From <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;
			
			//wp_mail($emailTo, $subject, $body, $headers);
			contactToAuthor($emailTo, $subject, $name, $email, $comments, $headers, $classieraPostTitle, $classieraPostURL);
			$emailSent = true;			

		}
	}
	if($_POST['submit'] == 'report_to_admin'){		
		$displayMessage = '';
		$report_ad = $_POST['report_ad_val'];
		if($report_ad == "illegal") {
			$message = esc_html__('This is illegal/fraudulent Ads, please take action.', 'classiera');
		}
		if($report_ad == "spam") {
			$message = esc_html__('This Ad is SPAM, please take action', 'classiera');			
		}
		if($report_ad == "duplicate") {
			$message = esc_html__('This ad is a duplicate, please take action', 'classiera');			
		}
		if($report_ad == "wrong_category") {
			$message = esc_html__('This ad is in the wrong category, please take action', 'classiera');			
		}
		/*if($report_ad == "post_rules") {
			$message = esc_html__('The ad goes against posting rules, please take action', 'classiera');			
		}*/
		if($report_ad == "post_other") {
			$message = $_POST['other_report'];				
		}		
		$classieraPostTitle = $_POST['classiera_post_title'];	
		$classieraPostURL = $_POST['classiera_post_url'];
		//print_r($message); exit();
		classiera_reportAdtoAdmin($message, $classieraPostTitle, $classieraPostURL);
		if(!empty($message)){
			$displayMessage = esc_html__('Thanks for reporting. HYST will review and take action accordingly.', 'classiera');
		}
	}
	
}
if(isset($_POST['btn_submit']) && $_POST['btn_submit']!='')
{	
	global $wpdb;
	
		$found_latlong1=explode("#",$_POST['found_latlong']);
		$found_latitude=$found_latlong1[0];
		$found_longitude=$found_latlong1[1];
				
			if (filter_var($_POST['found_location'], FILTER_VALIDATE_URL) === FALSE) {
				$wpdb->query("insert into wp_found_hysts(hyst_id, hyst_owner_id, found_user_id, found_price, found_location, found_latitude, found_longitude, is_url) values('".$_POST['hyst_id']."','".$_POST['hyst_owner_id']."','".$_POST['found_user_id']."','".$_POST['found_price']."','".$_POST['found_location']."','".$found_latitude."','".$found_longitude."', '0');");
				update_post_meta( $_POST['hyst_id'], 'is_found', 1 );
				$wpdb->query("insert into wp_hyst_likes(hyst_id, found_user_id, owner_user_id, like_time) values('".$_POST['hyst_id']."', '".$_POST['found_user_id']."', '".$_POST['hyst_owner_id']."', '".date('Y-m-d h:i:s')."')");
			
			
			}
			else
			{
				$wpdb->query("insert into wp_found_hysts(hyst_id, hyst_owner_id, found_user_id, found_price, found_location, found_latitude, found_longitude, is_url) values('".$_POST['hyst_id']."','".$_POST['hyst_owner_id']."','".$_POST['found_user_id']."','".$_POST['found_price']."','".$_POST['found_location']."','','','1');");
	update_post_meta( $_POST['hyst_id'], 'is_found', 1 );
				$wpdb->query("insert into wp_hyst_likes(hyst_id, found_user_id, owner_user_id, like_time) values('".$_POST['hyst_id']."', '".$_POST['found_user_id']."', '".$_POST['hyst_owner_id']."', '".date('Y-m-d h:i:s')."')");
			}
	
	
}
if(isset($_POST['brn_confirm']) && $_POST['brn_confirm']!='')
{
	global $wpdb;
	if((isset($_POST['found_id']) && $_POST['found_id']!='') && (isset($_POST['confirm_user_id']) && $_POST['confirm_user_id']!=''))
	{
		//echo "SELECT * FROM wp_found_hysts WHERE id='".$_POST['found_id']."'";exit;
		$found_p = $wpdb->get_results("SELECT * FROM wp_found_hysts WHERE id='".$_POST['found_id']."'");
		//print_r($found_p);
		if($_POST['confirm_user_id']==$found_p[0]->hyst_owner_id)
		{
			//echo 'if is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_owner=1 where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user3==0)
		{
			//echo 'else if executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user3=1 , confirmed_by_user3_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user3==1 && $found_p[0]->confirmed_by_user3_userid==$found_p[0]->hyst_owner_id)
		{
			//echo 'else if executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user3=1 , confirmed_by_user3_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user4==0)
		{
			//echo '2nd elseif is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user4=1 , confirmed_by_user4_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user4==1 && $found_p[0]->confirmed_by_user4_userid==$found_p[0]->hyst_owner_id)
		{
			//echo '2nd elseif is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user4=1 , confirmed_by_user4_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
	}
}
else if(isset($_POST['brn_not_confirm']) && $_POST['brn_not_confirm']!='')
{
	global $wpdb;
	if((isset($_POST['found_id']) && $_POST['found_id']!='') && (isset($_POST['confirm_user_id']) && $_POST['confirm_user_id']!=''))
	{
		//echo "SELECT * FROM wp_found_hysts WHERE id='".$_POST['found_id']."'";exit;
		$found_p = $wpdb->get_results("SELECT * FROM wp_found_hysts WHERE id='".$_POST['found_id']."'");
		//print_r($found_p);
		if($_POST['confirm_user_id']==$found_p[0]->hyst_owner_id)
		{
			//echo 'if is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_owner=2 where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user3==0)
		{
			//echo 'else if executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user3=2 , confirmed_by_user3_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user3==1 && $found_p[0]->confirmed_by_user3_userid==$found_p[0]->hyst_owner_id)
		{
			//echo 'else if executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user3=2 , confirmed_by_user3_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user4==0)
		{
			//echo '2nd elseif is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user4=2 , confirmed_by_user4_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user4==1 && $found_p[0]->confirmed_by_user4_userid==$found_p[0]->hyst_owner_id)
		{
			//echo '2nd elseif is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user4=2 , confirmed_by_user4_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
	}
}
if(isset($_POST['favorite'])){
	$author_id = $_POST['author_id'];
	$post_id = $_POST['post_id'];
	$hyst_owner_id = $_POST['hyst_owner_id'];
	echo classiera_favorite_insert($author_id, $post_id, $hyst_owner_id);
}
if(isset($_POST['follower'])){	
	$author_id = $_POST['author_id'];
	$follower_id = $_POST['follower_id'];
	echo classiera_authors_insert($author_id, $follower_id);
}
if(isset($_POST['unfollow'])){
	$author_id = $_POST['author_id'];
	$follower_id = $_POST['follower_id'];
	echo classiera_authors_unfollow($author_id, $follower_id);
}

?>
<?php 
	//Search Styles//
	if($classieraSearchStyle == 1){
		get_template_part( 'templates/searchbar/searchstyle1' );
	}elseif($classieraSearchStyle == 2){
		get_template_part( 'templates/searchbar/searchstyle2' );
	}elseif($classieraSearchStyle == 3){
		get_template_part( 'templates/searchbar/searchstyle3' );
	}elseif($classieraSearchStyle == 4){
		get_template_part( 'templates/searchbar/searchstyle4' );
	}elseif($classieraSearchStyle == 5){
		get_template_part( 'templates/searchbar/searchstyle5' );
	}elseif($classieraSearchStyle == 6){
		get_template_part( 'templates/searchbar/searchstyle6' );
	}elseif($classieraSearchStyle == 7){
		get_template_part( 'templates/searchbar/searchstyle7' );
	}
?>
<style>
.comment-awaiting-moderation{display:none;}
</style>
<section class="inner-page-content single-post-page">
	<div class="container-fluid">
		<div class="bg_gray">
		<!-- breadcrumb -->
		<?php classiera_breadcrumbs();?>
		<!-- breadcrumb -->
		<!--Google Section-->
		<?php 
		$homeAd1 = '';		
		global $redux_demo;
		$homeAdImg1 = $redux_demo['home_ad2']['url']; 
		$homeAdImglink1 = $redux_demo['home_ad2_url']; 
		$homeHTMLAds = $redux_demo['home_html_ad2'];
		
		if(!empty($homeHTMLAds) || !empty($homeAdImg1)){
			if(!empty($homeHTMLAds)){
				$homeAd1 = $homeHTMLAds;
			}else{
				$homeAd1 = '<a href="'.$homeAdImglink1.'" target="_blank"><img class="img-responsive" alt="image" src="'.$homeAdImg1.'" /></a>';
			}
		}
		if(!empty($homeAd1)){
		?>
		<section id="classieraDv">
			<div class="container">
				<div class="row">							
					<div class="col-lg-12 col-md-12 col-sm-12 center-block text-center">
						<?php echo $homeAd1; ?>
					</div>
				</div>
			</div>	
		</section>
		<?php } ?>
		<?php if ( get_post_status ( $post->ID ) == 'private' || get_post_status ( $post->ID ) == 'pending' ) {?>
		<div class="alert alert-info" role="alert">
		  <p>
		  <strong><?php esc_html_e('Congratulation!', 'classiera') ?></strong> <?php esc_html_e('Your Ad has submitted and pending for review. After review your Ad will be live for all users. You may not preview it more than once.!', 'classiera') ?>
		  </p>
		</div>
		<?php } ?>
		<!--Google Section-->
		<?php if($classieraSingleAdStyle == 2){
			get_template_part( 'templates/singlev2' );
		}
		$is_competition=get_post_meta($post->ID, 'is_competition');
		//print_r($is_competition);
		if($is_competition[0]==1)
		{
			$colmid='col-md-12';
		}
		else
		{
			$colmid='col-md-8';
		}?>
		<div class="row">
			<div class="col-md-12">
				<!-- single post -->
				<div class="single-post">
					<!-- <?php //if($classieraSingleAdStyle == 1){
						//get_template_part( 'templates/singlev1');
					//}?> -->
					<?php 
					$post_price = get_post_meta($post->ID, 'post_price', true); 
					$post_old_price = get_post_meta($post->ID, 'post_old_price', true);
					$postVideo = get_post_meta($post->ID, 'post_video', true);
					$dateFormat = get_option( 'date_format' );
					$postDate = get_the_date($dateFormat, $post->ID);
					$itemCondition = get_post_meta($post->ID, 'item-condition', true); 
					$post_location = get_post_meta($post->ID, 'post_location', true);
					$post_state = get_post_meta($post->ID, 'post_state', true);
					$post_city = get_post_meta($post->ID, 'post_city', true);
					$post_phone = get_post_meta($post->ID, 'post_phone', true);
					$post_latitude = get_post_meta($post->ID, 'post_latitude', true);
					$post_longitude = get_post_meta($post->ID, 'post_longitude', true);
					$post_address = get_post_meta($post->ID, 'post_address', true);
					$classieraCustomFields = get_post_meta($post->ID, 'custom_field', true);					
					$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
					$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);

					$post_id = $wpdb->get_results("SELECT id FROM wp_hyst_likes WHERE (hyst_id = '".$post->ID."' AND like_user_id = '". $current_user->ID ."' AND owner_user_id='".$post->post_author."')");
					?>
					<div class="hyst_social" style="width:100%;">
							<div class="social_icon">
								<?php
								if(count($post_id[0]->id)>0)
								{
									$myrows = $wpdb->get_results( "SELECT count(id) as tot_like FROM wp_hyst_likes where like_user_id!='0' and hyst_id='".$post->ID."'" );
									if($myrows[0]->tot_like > 1)
									{
										$tot_like=$myrows[0]->tot_like;
										$like='Likes';
									}
									else
									{
										$like='Like';
										if($myrows[0]->tot_like>0)
										{
											$tot_like=$myrows[0]->tot_like;
										}
										else
										{
											$tot_like=0;
										}
										
									}
									?>
								<?php if ( is_user_logged_in()) { ?>
									<p><a href="javascript:void(0);" id="like_click1"><img class="icon-single-page" src="<?=get_template_directory_uri().'/../classiera-child/images/liked.png'; ?>"/>
									</br><?php echo '<span id="tot_like">'.$tot_like.'</span>';?>&nbsp;<span><?php echo $like?></span></a></p>
								<?php } ?>	
									<?php /*<div class="sharethis-inline-share-buttons"></div>*/?>
									<p class="share" style="display:block;">Share: </p>
									<?php echo do_shortcode('[addthis tool="addthis_inline_share_toolbox"]');?> 
									
									<style>.st-custom-button{float:left; width:25px;}</style>		 
						
						<?php
						}
						else
						{
							$myrows = $wpdb->get_results( "SELECT count(id) as tot_like FROM wp_hyst_likes where like_user_id!='0' and hyst_id='".$post->ID."'" );
							if($myrows[0]->tot_like > 1)
							{
								$tot_like=$myrows[0]->tot_like;
								$like='Likes';
							}
							else
							{
								$like='Like';
								if($myrows[0]->tot_like>0)
								{
									$tot_like=$myrows[0]->tot_like;
								}
								else
								{
									$tot_like=0;
								}
							}
						?>
						
							<p><a href="javascript:void(0);" id="like_click"><img class="icon-single-page" src="<?=get_template_directory_uri().'/../classiera-child/images/liked.png'; ?>"/></br><?php echo '<span id="tot_like">'.$tot_like.'</span>';?>&nbsp;<span><?php echo $like;?></span></a></p><?php /*<div class="sharethis-inline-share-buttons"></div>*/?>
							<p class="share">Share: </p>
													<?php echo do_shortcode('[addthis tool="addthis_inline_share_toolbox"]');?>
						<style>.st-custom-button{float:left; width:25px;}</style>
						
								<?php
								}
								?>
						
						   </div>
						   <div class="clearfix"></div>							
							<?php if($classieraReportAd == 1){ ?>
						<div class="col-lg-12 col-md-12 col-sm-12 match-height ecom_buttton">
							<div class="widget-box <?php if($classieraSingleAdStyle == 2){echo "border-none";}?>">
								<!--ReportAd-->
								<div class="widget-content widget-content-post">
									<div class="user-make-offer-message border-bottom widget-content-post-area">
									    <div class="top-buffer2"></div>
                                        <ul class="nav nav-tabs buttons_list" role="tablist">
                                            <li role="presentation" class="btnWatch">    
											    <?php if ( is_user_logged_in()){ 
													$current_user = wp_get_current_user();
													$user_id = $current_user->ID;
												}
												if(isset($user_id)){
													echo classiera_authors_favorite_check($user_id,$post->ID,$post->post_author); 
												}
												?>
                                            </li>
														<script>
															jQuery(document).ready(function(){
   																 jQuery("#preview").click(function(){
        															jQuery("#menu").toggle();
    																});
																});
															</script>
										    <?php if($classieraToAuthor == 1 && is_user_logged_in()){?>
                                            <li role="presentation" class="" id="views">
												    <button type="submit" id="popmsg" data-target="#message" aria-controls="message" role="tab" data-toggle="tab" name="send_email" class="form-control-sm form-control"><img class="icon-single-page" src="<?=get_template_directory_uri().'/../classiera-child/images/email.png'; ?>"/><span><?php esc_html_e('Send Email', 'classiera') ?></span></button>
                                            </li>
										     <?php } ?>
										
											<?php 	if(is_user_logged_in()) {
											$sel_lists=$wpdb->get_results("select * from wp_user_lists where user_id='".$current_user->ID."'");
											if(count($sel_lists)>0) { ?>
											<li role="presentation">
												 <button type="submit" name="found_it" class="form-control-sm form-control" onclick="return disp_list_block();"><img class="icon-single-page" src="<?=get_template_directory_uri().'/../classiera-child/images/found-it-button.png'; ?>"/>&nbsp;<span>Add to List</span></button>
											</li>
											 <?php
											 } 
											}?>

											<?php 	if(is_user_logged_in()) { ?>
											<li role="presentation">
												<button type="submit" name="report" class="form-control-sm form-control" onclick="return disp_report_block();"><i class="fa fa-exclamation-triangle"></i>&nbsp;<span><?php esc_html_e( 'Report', 'classiera' ); ?></span></button>
											</li>     
										<?php }	if(is_user_logged_in()) { ?>
                                        </ul>
										<center><ul class="nav nav-tabs buttons_list"><li role="presentation">
										<a href="javascript:void(0);" onclick="return disp_found_block();"><img class="icon-single-page" src="<?=get_template_directory_uri().'/../classiera-child/images/Found_it_001.png'; ?>" style="width:200px;" /></a>
											</li></ul></center>
											<?php
											}
											else
											{?>
										<center><ul class="nav nav-tabs buttons_list"><li role="presentation">
										<a href="<?php echo $login; ?>"><img class="icon-single-page" src="<?=get_template_directory_uri().'/../classiera-child/images/Found_it_001.png'; ?>" style="width:200px;" /></a>
											</li></ul></center>
											<?php
											}?>
										<div class="tab-content edit_pages" id="def" style="display:none;">
											<?php if($classieraToAuthor == 1){?>
											<div role="tabpanel" class="tab-pane " id="message">
											<!--ShownMessage-->
											<div class="top-buffer2"></div>
											<div class="single_title row">
												 <h2>e-mail</h2>
											</div>
											<div class="top-buffer2"></div>
											<?php if(isset($_POST['submit']) && $_POST['submit'] == 'send_message'){?>
												<div class="row">
													<div class="col-lg-12">
														<?php if($hasError == true){ ?>
														<div class="alert alert-warning">
															<?php echo $errorMessage; ?>
														</div>
														<?php } ?>
														<?php if($emailSent == true){ ?>
														<div class="alert alert-success">
															<?php echo $classieraContactThankyou; ?>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php } ?>
												<!--ShownMessage-->
												<div class="col-md-6 col-md-offset-3"> 
													<form method="post" class="form-horizontal" data-toggle="validator" name="contactForm" action="<?php the_permalink(); ?>">
														<div class="form-group">
															<label class="col-sm-3 hide control-label" for="name"><?php esc_html_e('Name', 'classiera') ?> :</label>
																<input id="name" data-minlength="5" type="text" class="form-control form-control-xs" name="contactName" placeholder="<?php esc_html_e('Type your name', 'classiera') ?>" required>
														</div><!--name-->
														<div class="form-group">
															<label class="col-sm-3 control-label hide" for="email"><?php esc_html_e('Email', 'classiera') ?> :</label>
																<input id="email" type="email" class="form-control form-control-xs" name="email" placeholder="<?php esc_html_e('Type your email', 'classiera') ?>" required>
														</div><!--Email-->
														<div class="form-group">
															<label class="col-sm-3 control-label hide" for="subject"><?php esc_html_e('Subject', 'classiera') ?> :</label>
																<input id="subject" type="text" class="form-control form-control-xs" name="subject" placeholder="<?php esc_html_e('Type your subject', 'classiera') ?>" required>
														</div><!--Subject-->
														<div class="form-group">
															<label class="col-sm-3 hide control-label" for="msg"><?php esc_html_e('Msg', 'classiera') ?> :</label>
																<textarea id="msg" name="comments" class="form-control" placeholder="<?php esc_html_e('Type Message', 'classiera') ?>" required></textarea>
														</div><!--Message-->
														<?php 
															$classieraFirstNumber = rand(1,9);
															$classieraLastNumber = rand(1,9);
															$classieraNumberAnswer = $classieraFirstNumber + $classieraLastNumber;
														?>
														<p class="font-16">
														<?php esc_html_e("Please input the result of ", "classiera"); ?>
														<?php echo $classieraFirstNumber; ?> + <?php echo $classieraLastNumber;?> = 
														</p>
														
														<div class="form-group">
															<label class="col-sm-3 hide control-label" for="humanTest"><?php esc_html_e('Answer', 'classiera') ?> :</label>
																<input id="humanTest" type="text" class="form-control form-control-xs" name="humanTest" placeholder="<?php esc_html_e('Your answer', 'classiera') ?>" required>
																<input type="hidden" name="humanAnswer" id="humanAnswer" value="<?php echo $classieraNumberAnswer; ?>" />
																<input type="hidden" name="classiera_post_title" id="classiera_post_title" value="<?php the_title(); ?>" />
																<input type="hidden" name="classiera_post_url" id="classiera_post_url" value="<?php the_permalink(); ?>"  />
														</div><!--answer-->
														<input type="hidden" name="submit" value="send_message" />
														<div class="text-center">
														    <button class="btn btn-primary btn-block btn-sm theme-btn sharp btn-style-one" name="send_message" value="send_message" type="submit"><?php esc_html_e( 'Send Message', 'classiera' ); ?></button>
														</div>
													</form>
												</div>
											</div><!--message-->
											<?php } ?>
											<?php 
											$classieraPriceSection = $redux_demo['classiera_sale_price_off'];
											if($classieraPriceSection == 1){
											?>											
											<?php } ?>
										</div><!--tab-content-->
                                        <!-- Tab panes -->
                                      
                                    </div><!--user-make-offer-message-->
								</div><!--widget-content-->
								<!--ReportAd-->
							</div><!--widget-box-->
						</div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
						<?php } ?>
						</div>
						<div class="clearfix"></div>
						<!-- <div class="top-buffer2"></div> -->
						<?php
							//print_r($sel_lists);
							if(count($sel_lists)>0) { ?>
								<div class="add_list_item" style="display:none;">
									<div class="single_title">
										 <h2>add to list</h2>
									</div>
									<div class="clearfix"></div>
						            <div class="top-buffer2"></div>
									<?php
									if($error_savelist!='')
									{
										for($lp=0;$lp<count($error_savelist);$lp++)
										{
											echo '<span>'.$error_savelist[$lp].'</span>';
										}
									}
									?>
									<form id="frm_list" name="frm_list" method="post" action="">
										<div class="single_description">
											<div class="inner-addon flex" style="display:none;">
												<input type="text" placeholder="add text" class="form-control form-control-sm" id="list_title" name="list_title">
												<button type="submit" name="btn_save_list" class="btn btn-primary btn-sm btn-style-one btn-block" value="save">save</button>
											</div>
											<div class="inner-addon" style="text-align:center;">
											     <select name="list_title" id="list_title" class="form-control form-control-sm">
													<option value="">Select List Name</option>
													<?php
													foreach($sel_lists as $key=>$val)
													{
														$slistitem=$wpdb->get_results("select * from wp_user_list_item where list_id='".$sel_lists[$key]->id."' and hyst_id='".$post->ID."' and user_id='".$current_user->ID."'");?>
														<?php
														if(count($slistitem)<=0)
														{
														?>
															<option value="<?php echo $sel_lists[$key]->id;?>"><?php echo $sel_lists[$key]->list_title;?></option>
													<?php
														}
													} ?>
												</select> 	
												<button type="submit" name="btn_save_list" value="Save" class="cust_sub_button" style="color:#fff; width:240px; background-color:#000 !important; margin-top:20px;"><img src="<?php echo site_url();?>/wp-content/themes/classiera-child/images/click.png" width="30px" height="30px">&nbsp; Save</button>										 
											</div>
										</div>
										
										<input name="hyst_owner_id" type="hidden" value="<?php echo $post->post_author;?>"/>
										<input type="hidden" name="user_id" value="<?php echo $current_user->ID;?>" />
										<input type="hidden" name="hyst_id" value="<?php echo $post->ID;?>" />
										
									</form>
									
									<div class="clearfix"></div>
						            <div class="top-buffer2"></div>
								</div>
								<div style="clear:left;"></div>
						<?php } ?>
						<?php if(is_user_logged_in()) { ?>
							<script type="text/javascript" language="javascript">
								function disp_found_block() {
									jQuery("#def").hide();
									jQuery(".add_list_item").hide();
									jQuery("#report_hyst_block_div").hide();
									jQuery("#found_hyst_block_div").toggle();
								}
								function disp_list_block() {
									jQuery("#def").hide();
									jQuery("#found_hyst_block_div").hide();
									jQuery("#report_hyst_block_div").hide();
									jQuery(".add_list_item").toggle();
								}
								function disp_report_block() {
									jQuery("#def").hide();
									jQuery(".add_list_item").hide();
									jQuery("#found_hyst_block_div").hide();
									jQuery("#report_hyst_block_div").toggle();
								}
							</script>
							<div class="found_hyst" id="found_hyst_block_div" style="display:none;">
							
								<div id="accordion">
									<div class="single_title title_link">
										 <a href="javascript:void(0);" target-id="#found_form" parent-id="#accordion" class="accordian_toggle"><h2>Have you found this HYST?</h2></a>
									</div>							
								
									<div class="found_form text-center inner-toggle show" id="found_form">
										<div class="top-buffer2"></div>
										<div id="search_form">
											<form name="frm_found" id="frm_found" action="" method="post">
												<?php /*<label style="width:100px;">location:</label>*/?>
												<input type="text" name="found_location" id="found_location" value="" class="form-control form-control-sm getlatlog" placeholder="enter location" style="width:300px;"/>
												<hr>
												<?php /*<label style="width:100px;">Price:</label>*/?>
												<input type="text" name="found_price" value="" class="form-control form-control-sm" style="width:300px; margin-top:10px;" placeholder="Price" />
												<input type="hidden" name="found_latlong" id="found_latlong" value=""/>
												<input type="hidden" name="hyst_id" value="<?php echo $post->ID;?>" />
												<input name="found_user_id" type="hidden" value="<?php echo $current_user->ID;?>"/>
												<input name="hyst_owner_id" type="hidden" value="<?php echo $post->post_author;?>"/>
												<hr>
												<button type="submit" name="btn_submit" value="Submit" class="cust_sub_button"><img src="<?=get_template_directory_uri().'/../classiera-child/images/click.png'; ?>" height="30px" width="30px"/>&nbsp; submit</button>
											</form>
										</div>
									</div>
								</div>
							</div>

							<div class="report_hyst" id="report_hyst_block_div" style="display:none;">
								
								<div id="accordion_report">
									<div class="single_title title_link">
										<a href="javascript:void(0);" target-id="#report_form" parent-id="#accordion_report" class="accordian_toggle"><h2>Report To HYST</h2></a>
									</div>
									
									
									<div class="report_form inner-toggle show" id="report_form">
										<div class="top-buffer2"></div>
										<form method="post" class="form-horizontal" data-toggle="validator">
											<?php if(!empty($displayMessage)){?>
											<div class="alert alert-success">
												<?php echo $displayMessage; ?>
											</div>
											<?php } ?>
											<div class="radio">
												<input id="illegal" value="illegal" type="radio" name="report_ad_val">
												<label for="illegal"><?php esc_html_e( 'This is illegal/fraudulent', 'classiera' ); ?></label>
												<input id="spam" value="spam" type="radio" name="report_ad_val">
												<label for="spam"><?php esc_html_e( 'This HYST is spam', 'classiera' ); ?></label>
												<input id="duplicate" value="duplicate" type="radio" name="report_ad_val">
												<label for="duplicate"><?php esc_html_e( 'This HYST is a duplicate', 'classiera' ); ?></label>
												<input id="wrong_category" value="wrong_category" type="radio" name="report_ad_val">
												<label for="wrong_category"><?php esc_html_e( 'This HYST is in the wrong category', 'classiera' ); ?></label>
												<input id="post_other" value="post_other" type="radio" name="report_ad_val">
												<label for="post_other"><?php esc_html_e( 'Other', 'classiera' ); ?></label>
											</div>
											<div class="otherMSG" style="display: none;">
												<textarea id="other_report" name="other_report" class="form-control"placeholder="<?php esc_html_e( 'Type here..!', 'classiera' ); ?>"></textarea>
											</div>
											<input type="hidden" name="classiera_post_title" id="classiera_post_title" value="<?php the_title(); ?>" />
											<input type="hidden" name="classiera_post_url" id="classiera_post_url" value="<?php the_permalink(); ?>"  />
											<input type="hidden" name="submit" value="report_to_admin" />
											<button class="btn btn-primary btn-block btn-sm sharp btn-style-one" name="report_ad" value="report_ad" type="submit"><?php esc_html_e( 'Report', 'classiera' ); ?></button>
										</form>
									</div>
								</div>
							</div>
									
							<div style="clear:left;"></div>
						<?php } ?>
						<?php
						if($classieraSingleAdStyle == 1){
							get_template_part( 'templates/singlev1');
						}?>
						<input type="hidden" name="current_latitude" id="current_latitude" value="" />
						<input type="hidden" name="current_longitude" id="current_longitude" value="" />
						<?php
						$current_post_id=$post->ID; 
						//if(is_user_logged_in()) {
	
	$sel_found_count=$wpdb->get_results("select * from wp_found_hysts where hyst_id='".$post->ID."'");?>
	                  
					    <div class="clearfix"></div>
						<?php
						if(count($sel_found_count)>0)
						{?>
						<div class="single_title" id="top_location_div">
							 <h2>Top Locations</h2>
						</div>
	 					<?php
						}?>
						<div style="float:left; width:100%; margin-top:20px;" id="found_main_div">				
							
													
							<?php
								$sel_found = $wpdb->get_results("select * from wp_found_hysts where hyst_id='".$post->ID."'");
							?>							
							
						</div>
	    				
						<div class="clearfix"></div>
						<?php
						if(count($sel_found_count)>3)
						{?>
						<div class="top-buffer2"></div>
                        <div class="single_title title_link">
								 <h2><a href="javascript:void(0);" onclick="get_location_to_display_11(1);">View all Locations</a></h2>
								<a href="javascript:void(0);" onclick="get_location_to_display_11(1);"> <img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png'; ?>"></a></i>
						</div>
						<?php
						}?> 
						<div class="clearfix" id="cust_display_all_location"></div>
						<div class="top-buffer5"></div>
						<?php
						
						//}
						$error_savelist=array();
						if(isset($_POST['btn_save_list']) && $_POST['btn_save_list']!='')
						{
							if($_POST['list_title']  && $_POST['list_title']=='')
							{
								$error_savelist[]="Please select List name";
							}
							if(empty($error_savelist))
							{
								$wpdb->query("insert into wp_user_list_item(list_id, user_id, hyst_id) values('".$_POST['list_title']."', '".$_POST['user_id']."', '".$_POST['hyst_id']."');");
								
								$wpdb->query("insert into wp_hyst_likes(hyst_id, list_user_id, list_id, owner_user_id, like_time) values('".$_POST['hyst_id']."', '".$_POST['user_id']."', '".$_POST['list_title']."', '".$_POST['hyst_owner_id']."', '".date('Y-m-d h:i:s')."')");
								
				
							}
						}
						//global $wpdb;
							?>
							<div class="clearfix"></div>
							<div class="google_map">
							    <div class="single_title">
									 <h2>map</h2> 
								</div>
								<div class="widget-box <?php if($classieraSingleAdStyle == 2){echo "border-none";}?>">
									<!--GoogleMAP-->
									<?php 
									global $redux_demo;
									$googleMapadPost = $redux_demo['google-map-adpost'];
									$locShownBy = $redux_demo['location-shown-by'];
									$post_location = get_post_meta($post->ID, $locShownBy, true);
									$post_latitude = get_post_meta($post->ID, 'post_latitude', true);
									$post_longitude = get_post_meta($post->ID, 'post_longitude', true);
									$post_address = get_post_meta($post->ID, 'post_address', true);
									$classieraMapStyle = $redux_demo['map-style'];
									$postCatgory = get_the_category( $post->ID );
									$postCurCat = $postCatgory[0]->name;
									if( has_post_thumbnail()){
										$classieraIMG = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
										$classieraIMGURL = $classieraIMG[0];
									}else{
										$classieraIMGURL = get_template_directory_uri() . '/images/nothumb.png';
									}								
									
									$getfoundslocations=$wpdb->get_results("select * from wp_found_hysts where hyst_id='".$post->ID."' and is_url=0");
								//	print_r($getfoundslocations);exit;
									if(count($getfoundslocations)>0)
									{
										unset($iconPath); // $foo is gone
										$iconPath = array();
										unset($found_latitude); // $foo is gone
										$found_latitude = array();
										unset($found_longitude); // $foo is gone
										$found_longitude = array();
										unset($found_price); // $foo is gone
										$found_price = array();
										unset($found_id); // $foo is gone
										$found_id = array();
										foreach($getfoundslocations as $key=>$val)
										{
											if($getfoundslocations[$key]->found_latitude!='' && $getfoundslocations[$key]->found_longitude!='')
											{
												if($getfoundslocations[$key]->confirmed_by_owner==1 && $getfoundslocations[$key]->confirmed_by_user3==1 && $getfoundslocations[$key]->confirmed_by_user4==1)
												{
													$iconPath[]=get_template_directory_uri().'/../classiera-child/images/green.png';
												}
												else if($getfoundslocations[$key]->confirmed_by_owner==2 && $getfoundslocations[$key]->confirmed_by_user3==2 && $getfoundslocations[$key]->confirmed_by_user4==2)
												{
													$iconPath[]=get_template_directory_uri().'/../classiera-child/images/red.png';
												}
												else
												{
													$iconPath[]=get_template_directory_uri().'/../classiera-child/images/yellow.png';
												}
												$found_latitude[]=$getfoundslocations[$key]->found_latitude;
												$found_longitude[]=$getfoundslocations[$key]->found_longitude;
												$found_price[]=$getfoundslocations[$key]->found_price;
												$found_id[]=$getfoundslocations[$key]->id;
											}
										}
									}
									else
									{
										$iconPath_1 = get_template_directory_uri().'/../classiera-child/images/looking_for_marker.png';
									}
							
									if($googleMapadPost == 1){
									?>
									
									<div class="widget-content widget-content-post">
									<!--<div class="share widget-content-post-area" style="width:100%;">-->
										<div class="widget-content-post-area" style="width:100%;">
											<!--<h5><?php //echo $post_location; ?></h5>-->
											<?php //if(!empty($post_latitude)){?>
											
	<div id="classiera_single_map">
	
		<script type="text/javascript">			
			jQuery(document).ready(function(){
				var addressPoints = [							
				<?php 
				if(count($getfoundslocations)>0)
				{
					for($lp=0;$lp<count($iconPath);$lp++)
					{
						if($found_price[$lp]=='')
						{
							$dispprice=$classieraPostPrice;
						}
						else
						{
							$dispprice=$found_price[$lp];
						}
						$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$dispprice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';?>
						<?php /*['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php*/
						if($found_latitude[$lp]!='' && $found_longitude[$lp]!='')
						{
						/*?>
						[<?php echo $found_latitude[$lp]; ?>, <?php echo $found_longitude[$lp]; ?>, '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php*/
						?>
						[<?php echo $found_latitude[$lp]; ?>, <?php echo $found_longitude[$lp]; ?>, '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>", "<?php echo $found_id[$lp]?>"],<?php
						}
						/*else
						{?>
							['<?php echo $post_latitude; ?>', '<?php echo $post_longitude; ?>', '<?php echo $content; ?>', "<?php echo $iconPath[$lp]; ?>"],<?php
						}*/
						
						
					}
				}
				else
				{
					$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$classieraPostPrice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';
				?>
					
					[<?php echo $post_latitude; ?>, <?php echo $post_longitude; ?>, '<?php echo $content; ?>', "<?php echo $iconPath_1; ?>"],<?php
				}
			?>						
			];
				var mapopts;
				if(window.matchMedia("(max-width: 1024px)").matches){
					var mapopts =  {
						dragging:false,
						tap:false,
					};					
				};
				var map = L.map('classiera_single_map',mapopts).setView([0,0],1);
		
				map.dragging.disable;
				map.scrollWheelZoom.disable();
	
				var roadMutant = L.gridLayer.googleMutant({
				<?php if($classieraMapStyle){?>styles: <?php echo $classieraMapStyle; ?>,<?php }?>
					maxZoom: 13,
					type:'roadmap',
				}).addTo(map);
				var markers = L.markerClusterGroup({
					spiderfyOnMaxZoom: true,
					showCoverageOnHover: true,
					zoomToBoundsOnClick: true,
					maxClusterRadius: 10,
				});
				markers.on('clusterclick', function(e) {
					map.setView(e.latlng, 13);				
				});			
				/*my code start*/
				if ("geolocation" in navigator){
		
						navigator.geolocation.getCurrentPosition(function(position){
								infoWindow = new google.maps.InfoWindow({map: map});
								var pos = {lat: position.coords.latitude, lng: position.coords.longitude};
								
								
								
								/*infoWindow.setPosition(pos);
								infoWindow.setContent("Found your location <br />Lat : "+position.coords.latitude+" </br>Lang :"+ position.coords.longitude);
								map.panTo(pos);
								*/
								
							
							//var latitude = lat: position.coords.latitude;
							//var longitude = lng: position.coords.longitude;
						//rrr	map.setView(pos,18);	
								
								
							});
					}else{
						console.log("Browser doesn't support geolocation!");
				}
				/*my code end*/
				var markerArray = [];
				for (var i = 0; i < addressPoints.length; i++){
					var a = addressPoints[i];
					var newicon = new L.Icon({iconUrl: a[3],
						iconSize: [36, 36], // size of the icon
						//iconAnchor: [20, 10], // point of the icon which will correspond to marker's location
						iconAnchor: [23, 36], // point of the icon which will correspond to marker's location
						popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor                                 
					});
					var title = a[2];
					var marker = L.marker(new L.LatLng(a[0], a[1]));
					marker.setIcon(newicon);
					marker.bindPopup(title, {minWidth:"400"});
					marker.title = title;
					//marker.on('click', function(e) {
						//map.setView(e.latlng, 13);
						
					//});				
					markers.addLayer(marker);
					markerArray.push(marker);
					if(i==addressPoints.length-1){//this is the case when all the markers would be added to array
						var group = L.featureGroup(markerArray); //add markers array to featureGroup
						map.fitBounds(group.getBounds());   
					}
				}
				jQuery(document).on('click','.marker-link', function (e) {
					//alert(jQuery(this).data('markerid'));
					 jQuery('html,body').animate({
					    scrollTop: jQuery(".top-buffer5").offset().top - 125},
					    'slow');
					var lat = jQuery(this).data('lat');
					var long = jQuery(this).data('long');
					var price = jQuery(this).data('price');

					var content = '<a class="classiera_map_div" href="<?php echo get_the_permalink(); ?>"><img class="classiera_map_div__img" src="<?php echo $classieraIMGURL; ?>" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">Price : <span>'+price+'</span></p><h5 class="classiera_map_div__heading"><?php echo get_the_title(); ?></h5><p class="classiera_map_div__cat">Category : <?php echo $postCurCat; ?></p></div></a>';

					map.setView(new L.LatLng(lat, long), 13);					
					var popup = L.popup()
					.setLatLng(new L.LatLng(lat, long)) 
					.setContent(content)
					.openOn(map);
			    });
				var circle;
				map.addLayer(markers);
				function getLocation(){
					if(navigator.geolocation){
						navigator.geolocation.getCurrentPosition(showPosition);
					}else{
						x.innerHTML = "Geolocation is not supported by this browser.";
					}
				}
				
				
				function showPosition(position){	
						
					jQuery('#latitude').val(position.coords.latitude);
					jQuery('#longitude').val(position.coords.longitude);
					var latitude = jQuery('#latitude').val();
					var longitude = jQuery('#longitude').val();
					map.setView([latitude,longitude],13);
					circle = new L.circle([latitude, longitude], {radius: 2500}).addTo(map);
				}
				jQuery('#getLocation').on('click', function(e){
					e.preventDefault();
					getLocation();
				});
				
				//Search on MAP//
				var geocoder;
				function initialize(){
					geocoder = new google.maps.Geocoder();     
				}
				
			});	
		</script>
		
		<script>
			// This example displays an address form, using the autocomplete feature
			// of the Google Places API to help users fill in the information.

			// This example requires the Places library. Include the libraries=places
			// parameter when you first load the API. For example:
			// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
			jQuery(document).ready(function() {
				var placeSearch, autocomplete;
				var componentForm = {
					street_number: 'short_name',
					route: 'long_name',
					locality: 'long_name',
					administrative_area_level_1: 'short_name',
					country: 'long_name',
					postal_code: 'short_name'
				};

				function initAutocomplete() {
					// Create the autocomplete object, restricting the search to geographical
					// location types.

					var input = document.getElementById('found_location');
					autocomplete = new google.maps.places.Autocomplete(input);

					// When the user selects an address from the dropdown, populate the address
					// fields in the form.
					//autocomplete.addListener('place_changed', fillInAddress);
				}

				//initAutocomplete();

				function initMap() {
					//map = new google.maps.Map();
					google.maps.event.addDomListener(window, 'load', initAutocomplete);
					//var card = document.getElementById('pac-card');
					var input = document.getElementById('found_location');
					//var types = document.getElementById('type-selector');
					//var strictBounds = document.getElementById('strict-bounds-selector');

					//map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

					autocomplete = new google.maps.places.Autocomplete(
					/** @type {!HTMLInputElement} */(document.getElementById('found_location')),
					{types: ['geocode']});

					// Bind the map's bounds (viewport) property to the autocomplete object,
					// so that the autocomplete requests use the current map bounds for the
					// bounds option in the request.			
				}

				initMap();

				function fillInAddress() {
					// Get the place details from the autocomplete object.
					var place = autocomplete.getPlace();

					for (var component in componentForm) {
						document.getElementById(component).value = '';
						document.getElementById(component).disabled = false;
					}

					// Get each component of the address from the place details
					// and fill the corresponding field on the form.
					for (var i = 0; i < place.address_components.length; i++) {
						var addressType = place.address_components[i].types[0];
						if (componentForm[addressType]) {
							var val = place.address_components[i][componentForm[addressType]];
							document.getElementById(addressType).value = val;
						}
					}
				}

				// Bias the autocomplete object to the user's geographical location,
				// as supplied by the browser's 'navigator.geolocation' object.
				jQuery(document).on('focus', '.getlatlog',  function(e){
				
					if (navigator.geolocation) {
					  navigator.geolocation.getCurrentPosition(function(position) {
						var geolocation = {
						  lat: position.coords.latitude,
						  lng: position.coords.longitude
						};
						jQuery("#found_latitude").val(position.coords.latitude);
						jQuery("#found_longitude").val(position.coords.longitude);
						var circle = new google.maps.Circle({
						  center: geolocation,
						  radius: position.coords.accuracy
						});
						autocomplete.setBounds(circle.getBounds());
					  });
					}
				});

				jQuery('.getlatlog').change(function(){
					var addval = jQuery(".getlatlog").val();
					$.ajax({
						url: '<?php echo site_url()?>/get_latlong_from_address.php',
						type: 'POST',
						// This is query string i.e. country_id=123
						data: {address : addval},
						success: function(data) {							
							jQuery("#found_latlong").val(data);							
						},
						error: function(jqXHR, textStatus, errorThrown) {
							alert(errorThrown);
						}
					});
				});

			});
		</script>
		
	</div>
											<?php //} ?>
											
											<div class="map_discription single_page_map">
												<ul>
												     <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/looking_for.png'; ?>"/> &nbsp; <span>Looking For</span></li>
													 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/green_marker.png'; ?>"/> &nbsp; <span>Verified</span></li>
													 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/yellow_marker.png'; ?>"/> &nbsp; <span>Pending</span></li>
													 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/red_marker.png'; ?>"/> &nbsp; <span>unconfirmed</span></li>
												</ul>
											</div>	
										
										</div>
									</div>
									<?php } ?>
									<!--GoogleMAP-->
								</div><!--widget-box-->
							</div><!--col-lg-12-->
							<!--SidebarWidgets-->
							<?php //dynamic_sidebar('single'); ?>
							<!--SidebarWidgets-->
						</div><!--row-->
					
					    <div class="clearfix"></div>
							
							
							
						</div>
								<script language="javascript" type="text/javascript">
								var is_like=0;
								var tot_like=<?php echo $tot_like;?>;
								jQuery("#like_click").click(function(){
								//alert("here");
									if(is_like==0)
									{
										is_like=1;
										jQuery.post("<?php echo site_url();?>/ajax_like_hyst.php",
										{
											hyst_id: <?php echo $post->ID;?>,
											hyst_owner_id: <?php echo $post->post_author;?>,
											like_user_id: <?php echo $current_user->ID;?>
											
										},
										function(data, status){
											//alert("Data: " + data + "\nStatus: " + status);
											jQuery("#like_click").css('color', 'green');
											jQuery("#like_click i").css('color', 'green');
											tot_like+=1;
											jQuery("#tot_like").html(tot_like);
											
										});
									}
								}); 
								</script>
							
						
						<div style="clear:left;margin-top:20px;"></div>
					
					<!--PostVideo-->
					<?php if(!empty($postVideo)) { ?>
					<div class="border-section border postvideo">
						<h4 class="border-section-heading text-uppercase">
						<?php esc_html_e( 'Video', 'classiera' ); ?>
						</h4>
						<?php 
						if(preg_match("/youtu.be\/[a-z1-9.-_]+/", $postVideo)) {
							preg_match("/youtu.be\/([a-z1-9.-_]+)/", $postVideo, $matches);
							if(isset($matches[1])) {
								$url = 'https://www.youtube.com/embed/'.$matches[1];
								$video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen></iframe>';
							}
						}elseif(preg_match("/youtube.com(.+)v=([^&]+)/", $postVideo)) {
							preg_match("/v=([^&]+)/", $postVideo, $matches);
							if(isset($matches[1])) {
								$url = 'https://www.youtube.com/embed/'.$matches[1];
								$video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen></iframe>';
							}
						}elseif(preg_match("#https?://(?:www\.)?vimeo\.com/(\w*/)*(([a-z]{0,2}-)?\d+)#", $postVideo)) {
							preg_match("/vimeo.com\/([1-9.-_]+)/", $postVideo, $matches);
							//print_r($matches); exit();
							if(isset($matches[1])) {
								$url = 'https://player.vimeo.com/video/'.$matches[1];
								$video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>';
							}
						}else{
							$video = $postVideo;
						}
						?>
						<div class="embed-responsive embed-responsive-16by9">
							<?php echo $video; ?>
						</div>
					</div>
					<?php } ?>
					<!--PostVideo-->
					
					<!--comments-->
					<?php if($classieraComments == 1){?>
					<div class="col-sm-12">
							<div class="border-section comments comment_sec">
							<?php
							if(is_user_logged_in())
							{?>
								<div class="single_title">
									 <h2><?php esc_html_e( 'leave a comment', 'classiera' ); ?></h2>
								</div> 
								
								<?php
							} 
								$file ='';
								$separate_comments ='';
								comments_template( $file, $separate_comments );
								?>
         						<div class="top-buffer2"></div>
							</div>
						</div>
					<?php } ?>
					<!--comments-->
				</div>
				<!-- single post -->
			</div><!--col-md-8-->
		</div><!--row-->
	</div><!--container-->
</section>
<?php endwhile; ?>
<!-- related post section -->
<?php 
global $redux_demo;
$relatedAdsOn = $redux_demo['related-ads-on'];
if($relatedAdsOn == 1){
	function related_Post_ID(){
		global $post;
		$post_Id = $post->ID;
		return $post_Id;
	}
	get_template_part( 'templates/related-ads' );
}
?>
<!-- Company Section Start-->
<?php 
	global $redux_demo; 
	$classieraCompany = $redux_demo['partners-on'];
	$classieraPartnersStyle = $redux_demo['classiera_partners_style'];
	if($classieraCompany == 1){
		if($classieraPartnersStyle == 1){
			get_template_part('templates/members/memberv1');
		}elseif($classieraPartnersStyle == 2){
			get_template_part('templates/members/memberv2');
		}elseif($classieraPartnersStyle == 3){
			get_template_part('templates/members/memberv3');
		}elseif($classieraPartnersStyle == 4){
			get_template_part('templates/members/memberv4');
		}elseif($classieraPartnersStyle == 5){
			get_template_part('templates/members/memberv5');
		}elseif($classieraPartnersStyle == 6){
			get_template_part('templates/members/memberv6');
		}
	}
?>
<!-- Company Section End-->	
<!-- related post section -->
<?php get_footer(); ?>

<script type="text/javascript" language="javascript">


$(document).ready(function() {
var visibility = false;

	$('#navtoggle').click(function(){
		if(visibility == true){
		    $("#navbar2").css("bottom","-290px");	
			visibility = false;
	    }else{
		    $("#navbar2").css("bottom","90px");	
		    visibility = true;
		}
	});
});

jQuery( document ).ready(function() {
//alert($("#current_latitude").val());
		/*if($("#current_latitude").val()!='')
		{
			get_found_prices_locations();
		}*/
		get_found_prices_locations_top();
	/*function get_found_prices_locations_1()
	{
		alert("1");
		get_found_prices_locations_2();
	}
	function get_found_prices_locations_2()
	{
		alert("2");
		get_found_prices_locations_3();
	}
	function get_found_prices_locations_3()
	{
		alert("3");
		get_found_prices_locations();
	}*/
	function get_found_prices_locations(lat,long,limit=0)
	{
	//alert("i am here");
	var curlat='';
			var curlong='';
		 navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {
			  lat: position.coords.latitude,
			  lng: position.coords.longitude
			};
			//alert(position.coords.latitude);
			//alert(position.coords.longitude);
			$("#current_latitude").val(position.coords.latitude);
			$("#current_longitude").val(position.coords.longitude);
			curlat=position.coords.latitude;
			curlong=position.coords.longitude;
			get_location_to_display(curlat,curlong,limit);
			var circle = new google.maps.Circle({
			  center: geolocation,
			  radius: position.coords.accuracy
			});
			//autocomplete.setBounds(circle.getBounds());
		  });
		//alert("4");
		
	}
	function get_found_prices_locations_top(lat,long,limit=0)
	{
	//alert("i am here");
	var curlat='';
			var curlong='';
		 navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {
			  lat: position.coords.latitude,
			  lng: position.coords.longitude
			};
			//alert(position.coords.latitude);
			//alert(position.coords.longitude);
			$("#current_latitude").val(position.coords.latitude);
			$("#current_longitude").val(position.coords.longitude);
			curlat=position.coords.latitude;
			curlong=position.coords.longitude;
			get_location_to_display_top(curlat,curlong,limit);
			var circle = new google.maps.Circle({
			  center: geolocation,
			  radius: position.coords.accuracy
			});
			//autocomplete.setBounds(circle.getBounds());
		  });
		//alert("4");
		
	}
	
	});
	function get_location_to_display(curlat,curlong,limit=0)
	{
		$.ajax({
			url: '<?php echo site_url()?>/get_default_found_location_price.php',
			
			type: 'POST',
			// This is query string i.e. country_id=123
			<?php
			if ( is_user_logged_in() )
			{?>
				data: {current_latitude : curlat, current_longitude: curlong, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>, displimit:limit},<?php
			}
			else
			{?>
				data: {current_latitude : curlat, current_longitude: curlong, current_post_id: <?php echo $current_post_id;?>, displimit:limit},<?php
			}?>
			
		<?php /*data: {current_latitude : lat, current_longitude: long, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>},*/?>
			success: function(data) {
			//alert("Result");
				//alert(data);
				//$("#found_main_div").html(data);
				$("#cust_display_all_location").html(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	}
	var disp=0;
	function get_location_to_display_11(limit=0)
	{
		//alert('get_location_to_display_11');
		var curlat=jQuery("#current_latitude").val();
		var curlong=jQuery("#current_longitude").val();
		if(disp==0)
				{
					$("#cust_display_all_location").show();
					disp=1;
				}
				else
				{
					disp=0;
					$("#cust_display_all_location").hide();
				}
		$.ajax({
			url: '<?php echo site_url()?>/get_default_found_location_price.php',
			
			type: 'POST',
			// This is query string i.e. country_id=123
			data: {current_latitude : curlat, current_longitude: curlong, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>, displimit:limit},
		<?php /*data: {current_latitude : lat, current_longitude: long, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>},*/?>
			success: function(data) {
			//alert("Result");
				//alert(data);
				//$("#found_main_div").html(data);
				$("#cust_display_all_location").html(data);
				/*if(disp==0)
				{
					$("#cust_display_all_location").show();
					disp=1;
				}
				else
				{
					disp=;
					$("#cust_display_all_location").hide();
				}*/
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	}
	function get_location_to_display_top(curlat,curlong,limit=0)
	{
		$.ajax({
			url: '<?php echo site_url()?>/get_default_found_location_price_top.php',
			
			type: 'POST',
			// This is query string i.e. country_id=123
			data: {current_latitude : curlat, current_longitude: curlong, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>, displimit:limit},
		<?php /*data: {current_latitude : lat, current_longitude: long, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>},*/?>
			success: function(data) {
			//alert("Result");
				//alert(data);
				$("#found_main_div").html(data);
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	}
	
</script>