<?php
/**
* Template name: Home Test page
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Classiera
 * @since Classiera 1.0
 */
get_header(); ?>
<?php
/*function get_posts_by_author_role($role) {
    global $wpdb;
    return $wpdb->get_results( "SELECT p.`ID` FROM {$wpdb->posts} p, {$wpdb->usermeta} u"
                                ." WHERE    p.post_type     = 'post'"
                                ." AND      p.post_status   = 'publish'"
                                ." AND      u.user_id       = p.`post_author`"
                                ." AND      u.meta_key      = 'wp_capabilities'"
                                ." AND      u.meta_value    LIKE '%\"{$role}\"%'" );
}

$posts = get_posts_by_author_role('Subscriber');
echo "<pre>"; print_r($posts);*/
?>	
<?php
$wp_user_search = new WP_User_Query( array('role' => 'Subscriber'));
$admins = $wp_user_search->get_results();
$admin_ids = array();
foreach($admins as $admin) {
    $admin_ids[] = $admin->ID;
}
$args = implode(',', $admin_ids);
$wpquery = query_posts("author=$args&posts_per_page=-1");
?>
<?php
$wp_user_search2 = new WP_User_Query( array('role__in' => array('inventory_user','company_user')));
$admins2 = $wp_user_search2->get_results();
$admin_ids2 = array();
foreach($admins2 as $admin2) {
    $admin_ids2[] = $admin2->ID;
}
$args2 = implode(',', $admin_ids2);
$wpquery2 = query_posts("author=$args2&posts_per_page=-1");
//echo "<pre>";print_r($wpquery2);
?>
<?php 
	global $redux_demo;
	$classieraMAPStyle = $redux_demo['map-style'];	
	$classieraMAPPostType = $redux_demo['classiera_map_post_type'];	
	$classieraMAPPostCount = $redux_demo['classiera_map_post_count'];	
	$category_icon_code = "";
	$category_icon_color = "";
	$catIcon = "";
	$$iconPath = "";
	
	$classieraPriceRange = $redux_demo['classiera_pricerange_on_off'];
	$classieraPriceRangeStyle = $redux_demo['classiera_pricerange_style'];
	$postCurrency = $redux_demo['classierapostcurrency'];
	$classieraMultiCurrency = $redux_demo['classiera_multi_currency'];
		$classieraTagDefault = $redux_demo['classiera_multi_currency_default'];
	$classieraMaxPrice = $redux_demo['classiera_max_price_input'];
	$classieraLocationSearch = $redux_demo['classiera_search_location_on_off'];
	$locationsStateOn = $redux_demo['location_states_on'];
	$locationsCityOn= $redux_demo['location_city_on'];
	
	if($classieraMultiCurrency == 'multi'){
		$classieraPriceTagForSearch = classiera_Display_currency_sign($classieraTagDefault);
	}elseif(!empty($postCurrency) && $classieraMultiCurrency == 'single'){
		$classieraPriceTagForSearch = $postCurrency;
	}	
?>

<link rel="stylesheet" href="https://hyst2.temp.co.za/mymaps/leaflet.css" />
<!-- <script src="https://hyst2.temp.co.za/mymaps/jquery-1.11.0.min.js"></script> -->
<script src="https://hyst2.temp.co.za/mymaps/leaflet.js"></script>
<link href="https://hyst2.temp.co.za/mymaps/mapstyle.css" rel="stylesheet">

<section id="classiera_map">
	<div id="log" style="display:none;"></div>
	<input id="latitude" type="hidden" value="">
	<input id="longitude" type="hidden" value="">
	<div class="toggle-comp"><button onclick="togglePoints();">Company/Inventor</button></div>
	<div id="classiera_main_map" style="width:100%; height:600px;">

<script>
function init() {
  var data1 = {
    "type": "FeatureCollection",
    "features": [
		<?php
		foreach ($wpquery as $qid) {
			$post_latitude = get_post_meta($qid->ID, 'post_latitude', true);
			$post_longitude = get_post_meta($qid->ID, 'post_longitude', true);
		?>
	      {
	        "type": "Feature",
	        "properties": {},
	        "geometry": {
	          "type": "Point",
	          "coordinates": [
	            <?php echo $post_latitude; ?>,
	            <?php echo $post_longitude; ?>
	          ]
	        }
	      },
		<?php } ?>
    ]
  };

  var data2 = {
    "type": "FeatureCollection",
    "features": [
		<?php
		foreach ($wpquery2 as $qid2) {
			$post_latitude2 = get_post_meta($qid2->ID, 'post_latitude', true);
			$post_longitude2 = get_post_meta($qid2->ID, 'post_longitude', true);
		?>
	      {
	        "type": "Feature",
	        "properties": {},
	        "geometry": {
	          "type": "Point",
	          "coordinates": [
	            <?php echo $post_latitude2; ?>,
	            <?php echo $post_longitude2; ?>
	          ]
	        }
	      },
		<?php } ?>
    ]
  };

  // initialize map object with view
  window.map = L.map('classiera_main_map').setView([0, 0], 3);

  // add tile layer
  L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

  // add multiple data layers (defined above ... sloppy!)
  window.data1 = L.geoJson(data1).addTo(map);
  window.data2 = L.geoJson(data2).addTo(map);

  // toggle variable to use in togglePoints();
  window.toggle = false;
}

function togglePoints() {
  if(!toggle) {
    map.removeLayer(data1);
  } else {
    map.addLayer(data1);
  }
  toggle = !toggle;
}

window.onload = init();
</script>
</div>
			<div class="map_search">
		     <form method="get" action="" name="frm_header_search" id="frm_header_search">
			    <div class="search_icon">
				    <div class="search-image">
				       <img src="<?=get_template_directory_uri().'/../classiera-child/images/search.png'; ?>"/>
					</div>
				</div>
				<div class="search-form">
					<div id="innerSearch" class="collapse in classiera__inner">
					     
						<div class="map_title hide">
						     <h2>search</h2>
						</div>

						<!--Username-->
						<div class="inner-search-box">
							<div class="inner-addon right-addon">
								<?php /*<input type="search" name="s" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'description', 'classiera' ); ?>">*/?>
								<input type="text" name="username" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'username', 'classiera' ); ?>">
							</div>
						</div>
						<!--Username-->				
					   
					    <!--Keywords-->
						<div class="inner-search-box">
							<div class="inner-addon right-addon">
								<?php /*<input type="search" name="s" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'description', 'classiera' ); ?>">*/?>
								<input type="text" name="s" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'description', 'classiera' ); ?>">
							</div>
						</div>
						<!--Keywords-->
						
					
						<!--Price Range-->
						<?php if($classieraPriceRange == 1){?>
						<div class="inner-search-box">
								<?php 
								if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
									//echo $postCurrency;
									$currencySign = $postCurrency;
								}elseif($classieraMultiCurrency == 'multi'){
									//echo classiera_Display_currency_sign($classieraTagDefault);
									$currencySign = classiera_Display_currency_sign($classieraTagDefault);
								}else{
									//echo "&dollar;";
									$currencySign = "&dollar;";
								}
								?>							
							<?php if($classieraPriceRangeStyle == 'slider'){?>
								<?php 
								$startPrice = $classieraMaxPrice*10/100; 
								$secondPrice = $startPrice+$startPrice; 
								$thirdPrice = $startPrice+$secondPrice; 
								$fourthPrice = $startPrice+$thirdPrice; 
								$fivePrice = $startPrice+$fourthPrice; 
								$sixPrice = $startPrice+$fivePrice; 
								$sevenPrice = $startPrice+$sixPrice; 
								$eightPrice = $startPrice+$sevenPrice; 
								$ninePrice = $startPrice+$eightPrice; 
								$tenPrice = $startPrice+$ninePrice;
								?>

								<div class="classiera_price_slider">
									<p>
									  <!--<label for="amount" style="color:#fff;"><?php // esc_html_e( 'Price range', 'classiera' ); ?>:</label>  -->
									  <input data-cursign="<?php echo $currencySign; ?>" type="hidden" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;background:none;">
									</p>					 
									<div id="slider-range-home"></div>
									<input type="hidden" id="classieraMaxPrice" value="<?php echo $classieraMaxPrice; ?>">
									<input type="hidden" id="range-first-val" name="search_min_price" value="">
									<input type="hidden" id="range-second-val" name="search_max_price" value="">
								</div>	
												
								
							<?php }else{?>
								<!--Price Range input-->
								<div class="inner-addon right-addon">
									<input type="text" name="search_min_price" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Min price', 'classiera' ); ?>">
								</div>
								<div class="inner-addon right-addon">
									<input type="text" name="search_max_price" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'Max price', 'classiera' ); ?>">
								</div>
								<!--Price Range input-->
							<?php } ?>
						</div>
						<?php } ?>
						<!--Price Range-->
						
						<!--km range -->
						<div class="inner-search-box visible-xs">
							<div class="classiera_km_slider">
								
								<p>
								  <!--<label for="amount" style="color:#fff;"><?php // esc_html_e( 'Price range', 'classiera' ); ?>:</label>  -->
								  <input data-cursign="<?php echo $currencySign; ?>" type="hidden" id="amount1" readonly style="border:0; color:#f6931f; font-weight:bold;background:none;">
								</p>					 
								<div id="slider-range-km"></div>
								<?php /*<input type="hidden" id="classieraMaxPrice1" value="<?php echo $classieraMaxPrice; ?>">
								<input type="hidden" id="range-first-val1" name="search_min_price" value="">
								<input type="hidden" id="range-second-val1" name="search_max_price" value="">*/?>
								
							</div>
						</div>
						<!--km range -->
						
						<!--Locations-->
						<?php if($classieraLocationSearch == 1){?>
						<div class="inner-search-box flex">
							<!--SelectCountry-->
							<?php
							$args = array(
								'post_type' => 'countries',
								'posts_per_page'   => -1,
								'orderby'          => 'title',
								'order'            => 'ASC',
								'post_status'      => 'publish',
								'suppress_filters' => false 
							);
							$country = get_posts($args);
							if(!empty($country)){
							?>
							<div class="inner-addon right-addon">
							<!--	<i class="right-addon form-icon fa fa-sort"></i>
								<select name="post_location" class="form-control form-control-sm" id="post_location">
									<option value="-1" selected disabled>
										<?php // esc_html_e('Select Country', 'classiera'); ?>
									</option>
									<?php // foreach( $country as $singleCountry ){?>
									<option value="<?php // echo $singleCountry->ID; ?>"><?php // echo $singleCountry->post_title; ?></option>
									<?php // } ?>
								</select> -->
								
								<input type="text" name="post_location" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'location', 'classiera' ); ?>">
								
							</div>
							<?php } ?>
							 <?php wp_reset_postdata(); ?>
							<!--SelectCountry-->
							<!--Select State-->
							<?php if($locationsStateOn == 1){?>
							<div class="inner-addon right-addon post_sub_loc">
							  <!--	<i class="right-addon form-icon fa fa-sort"></i>
								<select name="post_state" class="form-control form-control-sm" id="post_state">
									<option value=""><?php // esc_html_e('Select State', 'classiera'); ?></option>
								</select>  -->
								
								<input type="text" name="post_state" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'location', 'classiera' ); ?>">
								
								
							</div>
							<?php } ?>
							<!--Select State-->
							<!--Select City-->
							<?php if($locationsCityOn == 1){?>
							<div class="inner-addon right-addon post_sub_loc">
							<!--	<i class="right-addon form-icon fa fa-sort"></i>
								<select name="post_city" class="form-control form-control-sm" id="post_city">
									<option value=""><?php // esc_html_e('Select City', 'classiera'); ?></option>
								</select>  -->
								
								<input type="text" name="post_city" class="form-control form-control-sm" placeholder="<?php esc_html_e( 'location', 'classiera' ); ?>">
								
							</div>
							<?php } ?>
							<!--Select City-->
						   <button type="submit" name="search" class="btn btn-primary sharp btn-sm btn-style-one btn-block" value="<?php esc_html_e( 'Search', 'classiera') ?>"><?php esc_html_e( 'Search', 'classiera') ?></button>
						</div>
						<?php } ?>
						<!--Locations-->
						<!--Categories-->
					</div><!--innerSearch-->
				</div><!--search-form-->
			</form>
		</div>
	<div class="map_discription" style="background:black;">
		<ul>
		     <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/looking_for.png'; ?>"/>
		     	<span>Looking For</span>
		     </li>
			 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/green_marker.png'; ?>"/>
			 	<span>Verified</span>
			 </li>
			 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/yellow_marker.png'; ?>"/>
			 	<span>Pending</span>
			 </li>
			 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/red_marker.png'; ?>"/>
			 	<span>unconfirmed</span>
			 </li>
		</ul>
	</div>
<div class="clearfix"></div>
</section>
<?php get_footer(); ?>