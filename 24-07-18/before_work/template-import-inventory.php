<?php
 /* Template name: Import Inventory
 */
 
 if ( !is_user_logged_in() ) { 

	global $redux_demo; 
	$login = $redux_demo['login'];
	wp_redirect( $login ); exit;

}

if(!current_user_can('inventory_user'))
{
	if(!current_user_can('company_user'))
	{
		get_template_part('error');
		exit;
	}
}
if(isset($_POST["btn_import"])){
		
		$filename=$_FILES["file"]["tmp_name"];		
 
 //print_r($_POST);exit;
		 if($_FILES["file"]["size"] > 0)
		 {
		  	$file = fopen($filename, "r");
			$getData = fgetcsv($file, 10000, ",");
			//print_r($getData);exit;
	        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
	         {
			// echo $getData[29];exit;
// echo $getData[0];exit;
 
	          /* $sql = "INSERT into employeeinfo (emp_id,firstname,lastname,email,reg_date) 
                   values ('".$getData[0]."','".$getData[1]."','".$getData[2]."','".$getData[3]."','".$getData[4]."')";
                   $result = mysqli_query($con, $sql);
				if(!isset($result))
				{
					echo "<script type=\"text/javascript\">
							alert(\"Invalid File:Please Upload CSV File.\");
							window.location = \"index.php\"
						  </script>";		
				}
				else {
					  echo "<script type=\"text/javascript\">
						alert(\"CSV File has been successfully Imported.\");
						window.location = \"index.php\"
					</script>";
				}*/
				$post_information = array(
					'post_title' => esc_attr(strip_tags($getData[2])),			
					'post_content' => strip_tags($getData[3], '<h1><h2><h3><strong><b><ul><ol><li><i><a><blockquote><center><embed><iframe><pre><table><tbody><tr><td><video><br>'),
					'post-type' => 'post',
					'post_category' => array($getData[0], $getData[1]),
					'tags_input'    => explode(',', $getData[4]),
					'tax_input' => array(
					'location' => $getData[6],
					),
					'comment_status' => 'open',
					'ping_status' => 'open',
					'post_status' => 'publish'
				);

				$post_id = wp_insert_post($post_information);
				global $wpdb;
				//echo "insert into wp_postmeta(post_id, meta_key, meta_value) values('".$post_id."', '_thumbnail_id', '$getData[29]')";
				$wpdb->query("insert into wp_postmeta(post_id, meta_key, meta_value) values('".$post_id."', '_thumbnail_id', '$getData[29]')");
				
				//Setup Price//
				//$postMultiTag = $_POST['post_currency_tag'];
				$post_price = trim($getData[5]);
				//$post_old_price = trim($_POST['post_old_price']);
				
				/*Check If Latitude is OFF */
				$googleLat = $getData[6];
				if(empty($googleLat)){
					$latitude = $classieraLatitude;
				}else{
					$latitude = $googleLat;
				}
				/*Check If longitude is OFF */
				$googleLong = $getData[7];
				if(empty($googleLong)){
					$longitude = $classieraLongitude;
				}else{
					$longitude = $googleLong;
				}
				
				//$featuredIMG = $_POST['classiera_featured_img'];
				//$itemCondition = $_POST['item-condition'];		
				//$catID = $classieraCategory.'custom_field';		
				//$custom_fields = $_POST[$catID];
				/*If We are using CSC Plugin*/
				
				/*Get Country Name*/
							
				
				/*If We are using CSC Plugin*/
							
				update_post_meta($post_id, 'post_price', $post_price, $allowed);
				
				update_post_meta($post_id, 'post_perent_cat', $getData[0], $allowed);
				update_post_meta($post_id, 'post_child_cat', $getData[1], $allowed);				
				update_post_meta($post_id, 'is_inventory', '1', $allowed);
				
				update_post_meta( $post_id, 'featured_post', '1' );
				
				

				//update_post_meta($post_id, 'post_location', wp_kses($postCounty, $allowed));
				//update_post_meta($post_id, 'post_location', $_POST['my_location'], $allowed);
				update_post_meta($post_id, 'post_location', wp_kses($getData[6], $allowed));
				
				//update_post_meta($post_id, 'post_state', wp_kses($poststate, $allowed));
				//update_post_meta($post_id, 'post_city', wp_kses($postCity, $allowed));

				update_post_meta($post_id, 'post_latitude', wp_kses($getData[7], $allowed));

				update_post_meta($post_id, 'post_longitude', wp_kses($getData[8], $allowed));

				update_post_meta($post_id, 'post_address', wp_kses($getData[6], $allowed));
				update_post_meta($post_id, 'wpb_post_views_count', '0', $allowed);
				

				update_post_meta($post_id, 'post_video', '', $allowed);
				//update_post_meta($post_id, 'featured_img', $featuredIMG, $allowed);
				update_post_meta($post_id, 'is_found', '1');
				
				
				//if($getData[9]!='' && $getData[10]!='')
				{
					$wpdb->query("insert into wp_found_hysts(hyst_id, hyst_owner_id, found_user_id, found_price, found_location, confirmed_by_owner, confirmed_by_user3, confirmed_by_user3_userid, confirmed_by_user4, confirmed_by_user4_userid, found_latitude, found_longitude) values('".$post_id."', '".get_current_user_id()."', '".get_current_user_id()."', '".$getData[10]."', '".$getData[9]."', '1', '1', '".get_current_user_id()."', '1', '".get_current_user_id()."', '".$getData[11]."', '".$getData[12]."')");
					
					

				}
				//if($getData[13]!='' && $getData[14]!='')
				{
					$wpdb->query("insert into wp_found_hysts(hyst_id, hyst_owner_id, found_user_id, found_price, found_location, confirmed_by_owner, confirmed_by_user3, confirmed_by_user3_userid, confirmed_by_user4, confirmed_by_user4_userid, found_latitude, found_longitude) values('".$post_id."', '".get_current_user_id()."', '".get_current_user_id()."', '".$getData[14]."', '".$getData[13]."', '1', '1', '".get_current_user_id()."', '1', '".get_current_user_id()."', '".$getData[15]."', '".$getData[16]."')");
				}
				//if($getData[17]!='' && $getData[18]!='')
				{
					$wpdb->query("insert into wp_found_hysts(hyst_id, hyst_owner_id, found_user_id, found_price, found_location, confirmed_by_owner, confirmed_by_user3, confirmed_by_user3_userid, confirmed_by_user4, confirmed_by_user4_userid, found_latitude, found_longitude) values('".$post_id."', '".get_current_user_id()."', '".get_current_user_id()."', '".$getData[18]."', '".$getData[17]."', '1', '1', '".get_current_user_id()."', '1', '".get_current_user_id()."', '".$getData[19]."', '".$getData[20]."')");
				}
				//if($getData[21]!='' && $getData[22]!='')
				{
					$wpdb->query("insert into wp_found_hysts(hyst_id, hyst_owner_id, found_user_id, found_price, found_location, confirmed_by_owner, confirmed_by_user3, confirmed_by_user3_userid, confirmed_by_user4, confirmed_by_user4_userid, found_latitude, found_longitude) values('".$post_id."', '".get_current_user_id()."', '".get_current_user_id()."', '".$getData[22]."', '".$getData[21]."', '1', '1', '".get_current_user_id()."', '1', '".get_current_user_id()."', '".$getData[23]."', '".$getData[24]."')");
				}
				//if($getData[25]!='' && $getData[26]!='')
				{
					$wpdb->query("insert into wp_found_hysts(hyst_id, hyst_owner_id, found_user_id, found_price, found_location, confirmed_by_owner, confirmed_by_user3, confirmed_by_user3_userid, confirmed_by_user4, confirmed_by_user4_userid, found_latitude, found_longitude) values('".$post_id."', '".get_current_user_id()."', '".get_current_user_id()."', '".$getData[26]."', '".$getData[25]."', '1', '1', '".get_current_user_id()."', '1', '".get_current_user_id()."', '".$getData[27]."', '".$getData[28]."')");
				}
				
				
				//update_post_meta($post_id,'_thumbnail_id', $getData[29], $allowed);
				//set_post_thumbnail( $post_id,  $getData[29] );
				
				update_post_meta($post_id, 'classiera_post_type', 'classiera_regular', $allowed );
				
				update_post_meta($post_id, 'regular-ads-enable', '', $allowed);
				update_post_meta($post_id, 'custom_field','');
				update_post_meta($post_id, 'classiera_CF_Front_end', '');
				update_post_meta($post_id, 'classiera_sub_fields', '');

				update_post_meta($post_id, 'post_currency_tag', '', $allowed);
				
				update_post_meta($post_id, 'post_old_price', '', $allowed);
				
		
				update_post_meta($post_id, 'post_inner_cat', '', $allowed);
				update_post_meta($post_id, 'is_inventory', '1', $allowed);
				
				update_post_meta( $post_id, 'featured_post', '1' );
				
				update_post_meta($post_id, 'post_phone', '', $allowed);
				
				update_post_meta($post_id, 'classiera_ads_type', '', $allowed);
				
				//update_post_meta($post_id, 'post_location', wp_kses($postCounty, $allowed));
				//update_post_meta($post_id, 'post_location', $_POST['my_location'], $allowed);
				//update_post_meta($post_id, 'post_location', wp_kses($_POST['address'], $allowed));
				
				update_post_meta($post_id, 'post_state','');
				update_post_meta($post_id, 'post_city', '');

				

				update_post_meta($post_id, 'post_video', '', $allowed);
				//update_post_meta($post_id, 'featured_img', $getData[29], $allowed);
				
				
				
				
				update_post_meta($post_id, 'days_to_expire', '', $allowed);
				
				
				$msg='Inventory uploaded successfully';
				}
								
	         }
			
	         fclose($file);	
		 }
	
	
	get_header();
?>
<section class="user-pages section-gray-bg edit_pages import_form all_usertemplate">
    <div class="container-fluid">
        <div class="row">
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
				<?php if($_POST){?>
				<div class="alert alert-success" role="alert">
					<?php echo $msg; ?>
				</div>
				<?php } ?>
				<div class="user-detail-section section-bg-white">

				<script>
				function display_loading()
				{
					jQuery(".la-line-spin-fade-rotating").show();
					return true;
				}
				</script>
                <form class="form-horizontal" action="" method="post" name="upload_excel" id="upload_excel" enctype="multipart/form-data" onsubmit="return display_loading();">
                       <?php /* <div class="col-md-8 col-md-offset-2">
							  <div class="progress">
									<div class="progress-bar progress-bar-gradiant progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div>
							  </div>
						</div>
						
						<div class="clearfix"></div>*/?>
                        <!-- Form Name -->
						<div class="row">
							<div class="single_title"> 
								 <h1 class="user_name">Import inventory</h1>
							</div>	 
						</div>

						<div class="col-sm-4 col-sm-offset-4">
                            <!-- File Button -->
                            <div class="form-group">
                                <label for="file" class="file_label"><img src="<?=get_template_directory_uri().'/../classiera-child/images/inventry1.png' ?>" height="40px" width="40px"/>&nbsp;Select <span class="uppercase">CSV</span> file</label>
                                <input type="file" name="file" id="file" class="input-large visiblity_hidden">
                            </div>

							<!-- Button -->
							<div class="form-group">
								<div class="la-line-spin-fade-rotating" style="display:none;">
									<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
								</div>
								<button type="submit" value="Import" name="btn_import" class="btn inport_btn"><img src="<?=get_template_directory_uri().'/../classiera-child/images/inventry2.png' ?>" height="40px" width="40px"/>&nbsp; Import data</button>
								
							</div>
							
							<!-- Button -->
							<div class="form-group">
								<a href="<?php echo site_url();?>/demo_new.csv" class="btn"><img src="<?=get_template_directory_uri().'/../classiera-child/images/inventry3.png' ?>" height="40px" width="40px"/>&nbsp;Download Sample<?php /*<span class="uppercase">Sample</span>*/?></a>
							</div>
						</div>
                </form>
                <div class="clearfix"></div>
            </div>
           
        </div>
    </div>
	</div>
	</section>
<?php
	get_footer();
?>