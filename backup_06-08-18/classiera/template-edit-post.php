<?php

/**
 * Template name: Edit Ads
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage classiera
 * @since classiera 1.0
 */

if ( !is_user_logged_in() ) {
	global $redux_demo; 
	$login = $redux_demo['login'];
	wp_redirect( $login ); exit;
}
/*if(current_user_can('inventory_user')) {	
	get_template_part('error');
	exit;	
}
if(current_user_can('company_user')) {
	get_template_part('error');
	exit;
}*/
global $current_user;
wp_get_current_user();
$hasError = false;
$userID = $current_user->ID;
$query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>'-1') );
if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();

	if(isset($_GET['post'])) {	

		if($_GET['post'] == $post->ID)

		{
			$posts_id = $_GET['post'];
			$author = get_post_field( 'post_author', $posts_id );
			if(current_user_can('administrator') ){
				
			}else{
				if($author != $userID) {
					wp_redirect( home_url() ); exit;
				}
			}
			$current_post = $post->ID;
			$title = get_the_title();
			$content = get_the_content();
			$posttags = get_the_tags($current_post);
			if ($posttags) {
			  foreach($posttags as $tag) {
				$tags_list = $tag->name . ' '; 
			  }
			}

			$postcategory = get_the_category( $current_post );
			
			$category_id = $postcategory[0]->cat_ID;
			
			$post_category_type = get_post_meta($post->ID, 'post_category_type', true);
			$classieraPostDate = get_the_date('Y-m-d H:i:s', $posts_id);			
			$post_price = get_post_meta($post->ID, 'post_price', true);			
			$post_old_price = get_post_meta($post->ID, 'post_old_price', true);
			$classieraTagDefault = get_post_meta($post->ID, 'post_currency_tag', true);
			
			$post_phone = get_post_meta($post->ID, 'post_phone', true);
			
			$post_main_cat = get_post_meta($post->ID, 'post_perent_cat', true);
			$post_child_cat = get_post_meta($post->ID, 'post_child_cat', true);
			$post_inner_cat = get_post_meta($post->ID, 'post_inner_cat', true);
			if(empty($post_inner_cat)){
				$category_id = $post_child_cat;
			}else{
				$category_id = $post_inner_cat;
			}
			if(empty($category_id)){
				$category_id = $post_main_cat;
			}
			$post_location = get_post_meta($post->ID, 'post_location', true);
			
			$post_state = get_post_meta($post->ID, 'post_state', true);
			$post_city = get_post_meta($post->ID, 'post_city', true);

			$post_latitude = get_post_meta($post->ID, 'post_latitude', true);

			$post_longitude = get_post_meta($post->ID, 'post_longitude', true);

			$post_price_plan_id = get_post_meta($post->ID, 'post_price_plan_id', true);

			$post_address = get_post_meta($post->ID, 'post_address', true);

			$post_video = get_post_meta($post->ID, 'post_video', true);
			
			$featuredIMG = get_post_meta($post->ID, 'featured_img', true);
			
			$itemCondition = get_post_meta($post->ID, 'item-condition', true);
			
			$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);
			
			$classiera_post_type = get_post_meta($post->ID, 'classiera_post_type', true);
			$pay_per_post_product_id = get_post_meta($post->ID, 'pay_per_post_product_id', true);
			$days_to_expire = get_post_meta($post->ID, 'days_to_expire', true);

			$chk_is_featured = get_post_meta($post->ID, 'featured_post', true);
			
			$featured_post = "0";
			$post_price_plan_activation_date = get_post_meta($post->ID, 'post_price_plan_activation_date', true);
			$post_price_plan_expiration_date = get_post_meta($post->ID, 'post_price_plan_expiration_date', true);
			$todayDate = strtotime(date('d/m/Y H:i:s'));
			$expireDate = strtotime($post_price_plan_expiration_date);  
			if(!empty($post_price_plan_activation_date)) {
				if(($todayDate < $expireDate) or empty($post_price_plan_expiration_date)) {
					$featured_post = "1";
				}
			}
			if(empty($post_latitude)) {
				$post_latitude = 0;
			}			
			if(empty($post_longitude)) {
				$post_longitude = 0;
				$mapZoom = 2;
			} else {
				$mapZoom = 16;
			}	

			if ( has_post_thumbnail() ) {	

				$post_thumbnail = get_the_post_thumbnail($current_post, 'thumbnail');		

			} 

		}

	}
endwhile; endif;
wp_reset_query();

$postTitleError = '';
$post_priceError = '';
$catError = '';
$featPlanMesage = '';
$postContent = '';
$hasError ='';
$allowed ='';
$caticoncolor="";
$classieraCatIconCode ="";
$category_icon="";
$category_icon_color="";
global $redux_demo;
$classieraMAPStyle = $redux_demo['map-style'];
$featuredADS = 0;
$primaryColor = $redux_demo['color-primary'];
$googleFieldsOn = $redux_demo['google-lat-long'];
$classieraLatitude = $redux_demo['contact-latitude'];
$classieraLongitude = $redux_demo['contact-longitude'];
$classieraAddress = $redux_demo['classiera_address_field_on'];
$postCurrency = $redux_demo['classierapostcurrency'];
$classieraIconsStyle = $redux_demo['classiera_cat_icon_img'];
$termsandcondition = $redux_demo['termsandcondition'];
$classieraProfileURL = $redux_demo['profile'];
$classiera_ads_typeOn = $redux_demo['classiera_ads_type'];
//print_r($_POST);exit;
$error=array();
if(isset($_POST['updatepost'])){
//print_r($_POST);exit;
	if(trim($_POST['postTitle']) != '' && $_POST['classiera-main-cat-field'] != ''){
			
		if(isset($_POST['submitted']) && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) {
			
			
			
			if(empty($_POST['postTitle'])){
				$postTitleError =  esc_html__( 'Please enter a title.', 'classiera' );
				$hasError = true;
			}else{
				$postTitle = trim($_POST['postTitle']);
			}
			if(empty($_POST['classiera-main-cat-field'])){
				$catError = esc_html__( 'Please select a category', 'classiera' );
				$hasError = true;
				$error[]='Please select a category.';
			}
			
			if(isset($_POST['postTitle']) && $_POST['postTitle']=='')
			{
				$hasError = true;
				$error[]='Please enter a title.';
			}
			if(isset($_POST['postContent']) && $_POST['postContent']=='')
			{
				$hasError = true;
				$error[]='Please enter a description.';
			}
			//print_r($error);
			
			//Image Count check//
			//if(isset($_POST['']))
			if($hasError != true && !empty($_POST['classiera_post_type']) || isset($_POST['regular-ads-enable'])) {
				$classieraPostType = $_POST['classiera_post_type'];
				//Set Post Status//
				if(is_super_admin() ){
					$postStatus = 'publish';
				}elseif(!is_super_admin()){
					if($redux_demo['post-options-on'] == 1){
						$postStatus = 'private';
					}else{
						$postStatus = 'publish';
					}
					if($classieraPostType == 'payperpost'){
						$postStatus = 'pending';
					}
				}
				//Set Post Status//
				//Check Category//
				$classieraMainCat = $_POST['classiera-main-cat-field'];
				$classieraChildCat = $_POST['classiera-sub-cat-field'];
				$classieraThirdCat = $_POST['classiera_third_cat'];
				if(empty($classieraThirdCat)){
					$classieraCategory = $classieraChildCat;
				}else{
					$classieraCategory = $classieraThirdCat;
				}
				if(empty($classieraCategory)){
					$classieraCategory = $classieraMainCat;
				}
				//Check Category//
				//Setup Post Data//
				$post_information = array(
					'ID'           => $current_post,
					'post_title' => esc_attr(strip_tags($_POST['postTitle'])),			
					'post_content' => strip_tags($_POST['postContent'], '<h1><h2><h3><strong><b><ul><ol><li><i><a><blockquote><center><embed><iframe><pre><table><tbody><tr><td><video><br>'),
					'post-type' => 'post',
					'post_category' => array($classieraMainCat, $classieraChildCat, $classieraThirdCat),
					'tags_input'    => explode(',', $_POST['hdn_post_tags']),
					'tax_input' => array(
					'location' => $_POST['post_location'],
					),
					'comment_status' => 'open',
					'ping_status' => 'open',
					'post_status' => $postStatus
				);

				wp_update_post($post_information);
				$post_id = $current_post;
				if(isset($_POST['chk_is_featured']) && $_POST['chk_is_featured']=='1')
				{
					update_post_meta( $post_id, 'featured_post', '1' );
				} else {
					update_post_meta( $post_id, 'featured_post', '0' );
				}
				//Setup Price//
				$postMultiTag = $_POST['post_currency_tag'];
				$post_price = trim($_POST['post_price']);
				$post_old_price = trim($_POST['post_old_price']);
				
				/*Check If Latitude is OFF */
				$googleLat = $_POST['latitude'];
				if(empty($googleLat)){
					$latitude = $classieraLatitude;
				}else{
					$latitude = $googleLat;
				}
				/*Check If longitude is OFF */
				$googleLong = $_POST['longitude'];
				if(empty($googleLong)){
					$longitude = $classieraLongitude;
				}else{
					$longitude = $googleLong;
				}
				
				$featuredIMG = $_POST['classiera_featured_img'];
				$itemCondition = $_POST['item-condition'];		
				$catID = $classieraCategory.'custom_field';		
				$custom_fields = $_POST[$catID];
				/*If We are using CSC Plugin*/
				
				/*Get Country Name*/
				if(isset($_POST['post_location'])){
					$postLo = $_POST['post_location'];
					$allCountry = get_posts( array( 'include' => $postLo, 'post_type' => 'countries', 'posts_per_page' => -1, 'suppress_filters' => 0, 'orderby'=>'post__in' ) );
					foreach( $allCountry as $country_post ){
						$postCounty = $country_post->post_title;
					}
				}				
				$poststate = $_POST['post_state'];
				$postCity = $_POST['post_city'];
				$classiera_CF_Front_end = $_POST['classiera_CF_Front_end'];
				$classiera_sub_fields = $_POST['classiera_sub_fields'];
				
				/*If We are using CSC Plugin*/
				if(isset($_POST['post_category_type'])){
					update_post_meta($post_id, 'post_category_type', esc_attr( $_POST['post_category_type'] ) );
				}				
				update_post_meta($post_id, 'custom_field', $custom_fields);
				update_post_meta($post_id, 'classiera_CF_Front_end', $classiera_CF_Front_end);
				update_post_meta($post_id, 'classiera_sub_fields', $classiera_sub_fields);

				update_post_meta($post_id, 'post_currency_tag', $postMultiTag, $allowed);
				update_post_meta($post_id, 'post_price', $post_price, $allowed);
				update_post_meta($post_id, 'post_old_price', $post_old_price, $allowed);
				
				update_post_meta($post_id, 'post_perent_cat', $classieraMainCat, $allowed);
				update_post_meta($post_id, 'post_child_cat', $classieraChildCat, $allowed);				
				update_post_meta($post_id, 'post_inner_cat', $classieraThirdCat, $allowed);
				
				
				update_post_meta($post_id, 'post_phone', $_POST['post_phone'], $allowed);
				
				update_post_meta($post_id, 'classiera_ads_type', $_POST['classiera_ads_type'], $allowed);
				if(isset($_POST['seller'])){
					update_post_meta($post_id, 'seller', $_POST['seller'], $allowed);
				}

				//update_post_meta($post_id, 'post_location', wp_kses($postCounty, $allowed));
				//update_post_meta($post_id, 'post_location', $_POST['my_location'], $allowed);
				update_post_meta($post_id, 'post_location', wp_kses($_POST['address'], $allowed));
				
				update_post_meta($post_id, 'post_state', wp_kses($poststate, $allowed));
				update_post_meta($post_id, 'post_city', wp_kses($postCity, $allowed));

				update_post_meta($post_id, 'post_latitude', wp_kses($latitude, $allowed));

				update_post_meta($post_id, 'post_longitude', wp_kses($longitude, $allowed));

				update_post_meta($post_id, 'post_address', wp_kses($_POST['address'], $allowed));

				update_post_meta($post_id, 'post_video', $_POST['video'], $allowed);
				update_post_meta($post_id, 'featured_img', $featuredIMG, $allowed);
				update_post_meta($post_id, 'is_found', '0');
				if(isset($_POST['item-condition'])){
					update_post_meta($post_id, 'item-condition', $itemCondition, $allowed);
				}
				update_post_meta($post_id, 'classiera_post_type', $_POST['classiera_post_type'], $allowed);
				update_post_meta($post_id, 'pay_per_post_product_id', $_POST['pay_per_post_product_id'], $allowed);
				update_post_meta($post_id, 'days_to_expire', $_POST['days_to_expire'], $allowed);
				
				if($classieraPostType == 'payperpost'){
					$permalink = $classieraProfileURL;
				}else{
					$permalink = get_permalink( $post_id );
				}
				//If Its posting featured image//
				if(trim($_POST['classiera_post_type']) != 'classiera_regular'){
					if($_POST['classiera_post_type'] == 'payperpost'){
						//Do Nothing on Pay Per Post//
					}elseif($_POST['classiera_post_type'] == 'classiera_regular_with_plan'){
						//Regular Ads Posting with Plans//
						$classieraPlanID = trim($_POST['regular_plan_id']);
						global $wpdb;
						$current_user = wp_get_current_user();
						$userID = $current_user->ID;
						$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE id = $classieraPlanID" );
						if($result){
							$tablename = $wpdb->prefix . 'classiera_plans';
							foreach ( $result as $info ){
								$newRegularUsed = $info->regular_used +1;
								$update_data = array('regular_used' => $newRegularUsed);
								$where = array('id' => $classieraPlanID);
								$update_format = array('%s');
								$wpdb->update($tablename, $update_data, $where, $update_format);
							}
						}
					}else{
						//Featured Post with Plan Ads//
						$featurePlanID = trim($_POST['classiera_post_type']);
						global $wpdb;
						$current_user = wp_get_current_user();
						$userID = $current_user->ID;
						$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE id = $featurePlanID" );
						if ($result){
							$featuredADS = 0;
							$tablename = $wpdb->prefix . 'classiera_plans';
							foreach ( $result as $info ){
								$totalAds = $info->ads;
								if (is_numeric($totalAds)){
									$totalAds = $info->ads;
									$usedAds = $info->used;
									$infoDays = $info->days;
								}								
								if($totalAds == 'unlimited'){
									$availableADS = 'unlimited';
								}else{
									$availableADS = $totalAds-$usedAds;
								}								
								if($usedAds < $totalAds && $availableADS != "0" || $totalAds == 'unlimited'){
									global $wpdb;
									$newUsed = $info->used +1;
									$update_data = array('used' => $newUsed);
									$where = array('id' => $featurePlanID);
									$update_format = array('%s');
									$wpdb->update($tablename, $update_data, $where, $update_format);
									update_post_meta($post_id, 'post_price_plan_id', $featurePlanID );

									$dateActivation = date('m/d/Y H:i:s');
									update_post_meta($post_id, 'post_price_plan_activation_date', $dateActivation );		
									
									$daysToExpire = $infoDays;
									$dateExpiration_Normal = date("m/d/Y H:i:s", strtotime("+ ".$daysToExpire." days"));
									update_post_meta($post_id, 'post_price_plan_expiration_date_normal', $dateExpiration_Normal );



									$dateExpiration = strtotime(date("m/d/Y H:i:s", strtotime("+ ".$daysToExpire." days")));
									update_post_meta($post_id, 'post_price_plan_expiration_date', $dateExpiration );
									update_post_meta($post_id, 'featured_post', "1" );
								}
							}
						}
					}
				}
				//If Its posting featured image//
				if ( isset($_FILES['uploadattachment']) ) {
					//echo "<>";print_r($_FILES);
					$count = 0;
					$files = $_FILES['uploadattachment'];
					foreach ($files['name'] as $key => $value) {				
						if ($files['name'][$key]) {
							$file = array(
								'name'     => $files['name'][$key],
								'type'     => $files['type'][$key],
								'tmp_name' => $files['tmp_name'][$key],
								'error'    => $files['error'][$key],
								'size'     => $files['size'][$key]
							);
							$_FILES = array("upload_attachment" => $file);
							
							foreach ($_FILES as $file => $array){								
								$featuredimg = $_POST['classiera_featured_img'];
								if($count == $featuredimg){
									$attachment_id = classiera_insert_attachment($file,$post_id);
									set_post_thumbnail( $post_id, $attachment_id );
								}else{
									$attachment_id = classiera_insert_attachment($file,$post_id);
								}								
								$count++;
							}
							
						}						
					}/*Foreach*/
				}
					
				wp_redirect($permalink); exit();
			}
		}
	}else{
		if(trim($_POST['postTitle']) === '') {
			$postTitleError = esc_html__( 'Please enter a title.', 'classiera' );	
			$hasError = true;
		}
		if($_POST['classiera-main-cat-field'] === '-1') {
			$catError = esc_html__( 'Please select a category.', 'classiera' );
			$hasError = true;
			$error[]='Please select category.';
		} 
		
		if(isset($_POST['postTitle']) && $_POST['postTitle']=='')
		{
			$hasError = true;
			$error[]='Please enter a title.';
		}
		if(isset($_POST['postContent']) && $_POST['postContent']=='')
		{
			$hasError = true;
			$error[]='Please enter a description.';
		}
	}

} 
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php 
	$page = get_page($post->ID);
	$current_page_id = $page->ID;
	$featuredUsed = '';
	$featuredAds = '';
	$regularUsed = '';
	$regularAds = '';
?>
<style>
.form-main-section .list-unstyled li a{padding: 8px 15px;
display: table;
text-align: left;text-align: center;
width: 100%;
height: 100%;
border-radius: 3px;moz-border-radius: 3px;webkit-border-radius: 3px;}
.list-unstyled li a:hover, .list-unstyled li a:hover i{background:#000; color:#fff !important;moz-border-radius: 3px;webkit-border-radius: 3px;}
.classieraSubReturn li{float:left; width:20%;}
/*.submit-post{padding:30px 15px 100px 15px;}*/
</style>

<section id="edit_post" class="user-pages section-gray-bg user-detail-section edit_section all_usertemplate">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
				<?php 
				global $redux_demo;
				global $wpdb;
				$current_user = wp_get_current_user();
				$userID = $current_user->ID;			
				$featured_plans = $redux_demo['featured_plans'];
				$classieraRegularAdsOn = $redux_demo['regular-ads'];
				$postLimitOn = $redux_demo['regular-ads-posting-limit'];
				$regularCount = $redux_demo['regular-ads-user-limit'];
				$cUserCheck = current_user_can( 'administrator' );
				$role = $current_user->roles;
				$currentRole = $role[0];
				$classieraAllowPosts = false;
				$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
				foreach ($result as $info){											
					$featuredAdscheck = $info->ads;											
					if (is_numeric($featuredAdscheck)){
						$featuredAds += $info->ads;
						$featuredUsed += $info->used;
					}
					$regularAdscheck = $info->regular_ads;
					if (is_numeric($regularAdscheck)){
						$regularAds += $info->regular_ads;
						$regularUsed += $info->regular_used;
					}
				}
				if (is_numeric($featuredAds) && is_numeric($featuredUsed)){
					$featuredAvailable = $featuredAds-$featuredUsed;
				}
				if (is_numeric($regularAds) && is_numeric($regularUsed)){
					$regularAvailable = $regularAds-$regularUsed;
				}
				
				$curUserargs = array(					
					'author' => $user_ID,
					'post_status' => array('publish', 'pending', 'draft', 'private', 'trash')    
				);
				$countPosts = count(get_posts($curUserargs));
				if($currentRole == "administrator"){
					$classieraAllowPosts = true;
				}else{
					if($postLimitOn == true){
						if($regularAvailable == 0 && $featuredAvailable == 0 && $countPosts >= $regularCount){
							$classieraAllowPosts = false;
						}else{
							$classieraAllowPosts = true;
						}
					}else{
						$classieraAllowPosts = true;
					}
				}
				//echo $postLimitOn.' Limit<br />';
				//echo $regularAvailable.' regularAvailable<br />';
				//echo $featuredAvailable.' featuredAvailable<br />';
				//echo $countPosts.' countPosts<br />';
				//echo $regularCount.' regularCount<br />';
				//echo $classieraAllowPosts.' classieraAllowPosts<br />';
				if($classieraAllowPosts == false){
					?>
					<div class="alert alert-warning" role="alert">
					  <strong><?php esc_html_e('Hello.', 'classiera') ?></strong><?php esc_html_e('You HYST Posts limit are exceeded, Please Purchase a Plan for posting More Ads.', 'classiera') ?>&nbsp;&nbsp;<a class="btn btn-primary btn-sm" href="<?php echo $featured_plans; ?>"><?php esc_html_e('Purchase Plan', 'classiera') ?></a>
					</div>
					<?php
				}elseif($classieraAllowPosts == true){
				?>
				<div class="submit-post section-bg-white">
				<div class="fileupload-wrapper">
				
				   <div id="myUpload">
					<form class="form-horizontal" action="" role="form" id="primaryPostForm" method="POST" data-toggle="validator" enctype="multipart/form-data">
						<h2 class="user_name">
						<?php
						$user = wp_get_current_user();
							$roles = $user->roles;
							
							if(in_array('inventory_user',$roles))
							{
								 esc_html_e(' NEW INVENTORY', 'classiera');
							}
							else
							{
								 esc_html_e('EDIT YOUR HYST', 'classiera');
							}
						 ?></h2>
						<hr class="line1">

						 

						 
						 
						<?php
						// print_r($error);
						 for($lp=0;$lp<count($error);$lp++)
						 {
						 	echo '<span style="float:left; clear:left; color:red;">'.$error[$lp].'</span>';
						 }
                        ?>
						
						<div style="clear:left;"></div>
						<div class="top-buffer3"></div>
						<!--Category-->
						<div class="form-main-section classiera-post-cat">
							<div class="classiera-post-main-cat" id="accordion">
							    <div class="row">
									<div class="single_title title_link">
										<a href="javascript:void(0);" target-id="#category" parent-id="#accordion" class="accordian_toggle"><h2 class="classiera-post-inner-heading"><?php esc_html_e('Select a Category', 'classiera') ?></h2>
										<img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>" /></a>
									</div>
								</div>
								<div class="top-buffer2"></div>
								<style>
								/*.mycustselectcat{background:#000;}*/
								#category .active{background:#000; color:#fff;}
								#category .active a span{color:#fff !important;}
								#subcategory li{width:14% !important;}
								#subcategory .active{background:#000; color:#fff;}
								#subcategory .active a{color:#fff !important;}
								@media only screen and (max-width: 800px) {
								#subcategory li{width:100% !important;}
								}
								</style>
								
								<ul class="list-unstyled list-inline list_icon inner-toggle" id="category">
									<?php 
									$categories = get_terms('category', array(
											'hide_empty' => 0,
											'parent' => 0,
											'order'=> 'ASC'
										)	
									);
									$count=0;
									foreach ($categories as $category){
									$count++;
										//print_r($category);
										$tag = $category->term_id;
										$classieraCatFields = get_option(MY_CATEGORY_FIELDS);
										if (isset($classieraCatFields[$tag])){
											$classieraCatIconCode = $classieraCatFields[$tag]['category_icon_code'];
											$classieraCatIcoIMG = $classieraCatFields[$tag]['your_image_url'];
											$classieraCatIconClr = $classieraCatFields[$tag]['category_icon_color'];
										}
										if(empty($classieraCatIconClr)){
											$iconColor = $primaryColor;
										}else{
											$iconColor = $classieraCatIconClr;
										}
										$category_icon = stripslashes($classieraCatIconCode);
										?>
										<li class="match-height maincatli <?php echo ($post_main_cat==$tag)?'active':''; ?>"  id="maincat_div_<?php echo $count;?>" onclick="return changemaincatbg('<?php echo $count;?>', '<?php echo $tag;?>');">
											<a href="#" id="<?php echo $tag; ?>" class="border">
												<?php 
												if($classieraIconsStyle == 'icon' && $category_icon!=''){
													?>
													<i class="<?php echo $category_icon; ?>" style="color:<?php echo $iconColor; ?>;"></i>
													<?php
												}elseif($classieraIconsStyle == 'img'){
													?>
													<img src="<?php echo $classieraCatIcoIMG; ?>" alt="<?php echo get_cat_name( $catName ); ?>">
													<?php
												}
												?>
												<span><?php echo get_cat_name( $tag ); ?></span>
											</a>
										</li>
										<?php
									}
									?>
								</ul><!--list-unstyled-->
								<script type="text/javascript" language="javascript">
								function changemaincatbg(cnt, catid)
								{
									var totcount='<?php echo $count;?>';
									jQuery("#maincat_div_"+cnt).addClass("mycustselectcat");
									jQuery("#classiera-main-cat-field").val(catid);
									for(var lp=1;lp<=totcount;lp++)
									{
										if(lp!=cnt)
											jQuery("#maincat_div_"+cnt).removeClass("mycustselectcat");
									}
								}

								jQuery(document).ready(function($){
									var hascls = $("#category li.maincatli.active").hasClass('active');
							    	if (hascls) {
							    	jQuery("#category").show();		
							        var mainCatId = jQuery("#category li.maincatli.active a").attr('id');
									var data = {
										'action': 'classieraGetSubCatOnClick',
										'mainCat': mainCatId,
									};
									jQuery.post(ajaxurl, data, function(response){
										jQuery('.classieraSubReturn').html(response);
										if(response){
											jQuery('.classiera-post-sub-cat').show();
											var post_child_cat = '<?php echo $post_child_cat ?>';
											jQuery('.classiera-post-sub-cat #subcategory li#subcatli_'+post_child_cat).addClass('active');
										}			
									});
							    	}
								});
								</script>
								
								<input class="classiera-main-cat-field" id="classiera-main-cat-field" name="classiera-main-cat-field" type="hidden" value="<?php echo $post_main_cat; ?>">
							</div><!--classiera-post-main-cat-->
						 <script type="text/javascript" language="javascript">
							/*$('#subcategory li').click(function() {
							alert("i am here");
								$('#subcategory li.active').removeClass('active');
								$(this).addClass('active');
							});*/
							/*jQuery( document ).ready(function() {
								//console.log( "ready!" );
									jQuery('#subcategory li').click(function() {
									alert("abc");
									}
								});
							});
							jQuery( document ).ready(function() {
								jQuery('#subcategory li a').on('click', function(){
									alert("ac");
									jQuery(this).parent().addClass('active').siblings().removeClass('active');
								});
							});*/
							
							function changesubcatbg(id)
							{
								
									/*jQuery("#maincat_div_"+cnt).addClass("mycustselectcat");
									for(var lp=1;lp<=totcount;lp++)
									{
										if(lp!=cnt)
											jQuery("#maincat_div_"+cnt).removeClass("mycustselectcat");
									}*/
									jQuery('#subcategory li').removeClass('active');
									jQuery("#subcatli_"+id).addClass("active");
							}
							</script>

						    <div class="classiera-post-sub-cat show" id="accordion1">
							    <div class="row">
									<div class="single_title title_link">
										<a href="javascript:void(0);" target-id="#subcategory" parent-id="#accordion1" class="accordian_toggle"><h2 class="classiera-post-inner-heading"><?php esc_html_e('Select a Sub Category', 'classiera') ?></h2>
										<img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>" /></a>
									</div>
								</div>
								<div class="top-buffer2"></div>
								<ul class="list-unstyled list-inline classieraSubReturn list_icon list_icon1 inner-toggle show" id="subcategory"></ul>
								
								<input class="classiera-sub-cat-field" name="classiera-sub-cat-field" id="classiera-sub-cat-field" type="hidden" value="<?php echo $post_child_cat; ?>">
							</div><!--classiera-post-sub-cat-->
							
							
							
							<!--ThirdLevel-->
							<div class="classiera_third_level_cat hide">
								<h4 class="classiera-post-inner-heading">
									<?php esc_html_e('Select a Sub Category', 'classiera') ?> :
								</h4>
								<ul class="list-unstyled classieraSubthird">
								</ul>
								<input class="classiera_third_cat" name="classiera_third_cat" id="classiera_third_cat" type="hidden" value="">
								
							</div>
							<!--ThirdLevel-->
						</div>
						<!--Category-->
						<div style="clear:left;"></div>
						
						<div class="top-buffer2"></div>
						
						
						<div class="form-main-section post-detail">
							<h4 class="text-uppercase border-bottom hide"><?php esc_html_e('HYST Details', 'classiera') ?> :</h4>
							
							<div class="form-group hide">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Selected Category', 'classiera') ?> : </label>
                                <div class="col-sm-9">
                                    <p class="form-control-static"></p>
									<input type="text" id="selectCatCheck" value="" data-error="<?php esc_html_e('Please select a category.', 'classiera') ?>" required >
									<div class="help-block with-errors selectCatDisplay"></div>
                                </div>
                            </div><!--Selected Category-->
							<?php if($classiera_ads_typeOn == 1){?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Type of HYST', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <input id="sell" value="sell" type="radio" name="classiera_ads_type" checked>
                                        <label for="sell"><?php esc_html_e('I want to sell', 'classiera') ?></label>
										
                                        <input id="buy" value="buy" type="radio" name="classiera_ads_type">
                                        <label for="buy"><?php esc_html_e('I want to buy', 'classiera') ?></label>
										
										<input type="radio" name="classiera_ads_type" value="rent" id="rent">
										<label for="rent"><?php esc_html_e('I want to rent', 'classiera') ?></label>
										
										<input type="radio" name="classiera_ads_type" value="hire" id="hire">
										<label for="hire"><?php esc_html_e('I want to hire', 'classiera') ?></label>
                                    </div>
                                </div>
                            </div><!--Type of Ad-->
							<?php } ?>
							
						   <div class="clearfix"></div>
						
							<div class="form-group checkbox text-center black-checkbox">								
								<label class="custom-checkbox-lbl">
									<?php $checked = ( ($chk_is_featured=='1')?'checked':'' ); ?>
									<input type="checkbox" <?php echo $checked; ?> name="chk_is_featured" id="is_featured" value="1" style="opacity:1; visibility:visible; position:static !important;"/>
									<i class="fa fa-3x icon-checkbox custom-checkbox-icon"></i>
								</label>
								<p class="poppins-lite image_title"><label for="is_featured" class="white"><?php esc_html_e( 'is featured', 'classiera' ); ?></label></p>
                            </div>
						
						
							
							<div style="clear:left;"></div>
							<div class="top-buffer2"></div>
							<div class="col-md-8 col-md-offset-2">
								<div class="col-sm-6">
									<div class="form-group">
										<input id="title" data-minlength="5" name="postTitle" type="text" class="form-control form-control-md" value="<?php echo $title; ?>" placeholder="<?php esc_html_e('title', 'classiera') ?>" required>
									</div><!--Ad title-->
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<?php /*<input id="title" data-minlength="5" name="description" type="text" class="form-control form-control-md" placeholder="description" required>*/?>
										<input id="post_price" name="post_price" type="text" class="form-control form-control-md" value="<?php echo $post_price; ?>" placeholder="<?php esc_html_e(' price', 'classiera') ?>" required>
									</div><!--Ad title-->
								</div>
								<div class="col-sm-12">
									<div class="form-group">
											<textarea name="postContent" id="description" cols="5" rows="5" class="form-control" data-error="<?php esc_html_e('description', 'classiera') ?>" placeholder="<?php esc_html_e('description', 'classiera') ?>" required><?php echo $content;?></textarea> 
											<div class="help-block with-errors"></div>
									</div><!--Ad description-->
								</div>
							</div>
						
                            <div class="clearfix"></div>						
				            <!--Ad Tags-->
							
							
							 <div class="classiera-post-sub-cat show" <?php /*id="accordion2"*/?>>
							    <div class="row">
									<div class="single_title title_link">
										<a href="javascript:void(0);" target-id="#post_tag" parent-id="#accordion2" class="accordian_toggle"><h2 class="classiera-post-inner-heading"><?php esc_html_e('HYST Tags', 'classiera') ?></h2>
										<img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>" /></a>
									</div>
								</div>
								<div class="top-buffer2"></div>
								<style>
			 
hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}

img {
    vertical-align: middle;
}

fieldset {
    border: 0;
    margin: 0;
    padding: 0;
}

textarea {
    resize: vertical;
}

.chromeframe {
    margin: 0.2em 0;
    background: #ccc;
    color: #000;
    padding: 0.2em 0;
}


/* ==========================================================================
   Author's custom styles
   ========================================================================== */


.wrapper {
    margin: 20px auto;
    width: 400px;
}
#post_tags {
    width: 100%;
}

.ui-autocomplete-multiselect.ui-state-default {
    display: block;
    background: #fff;
    border: 1px solid #ccc;
    padding: 3px 3px;
    padding-bottom: 0px;
    overflow: hidden;
    cursor: text;
}

.ui-autocomplete-multiselect .ui-autocomplete-multiselect-item .ui-icon {
    float: right;
    cursor: pointer;
}

.ui-autocomplete-multiselect .ui-autocomplete-multiselect-item {
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    padding: 1px 3px;
    margin-right: 2px;
    margin-bottom: 3px;
    color: #333;
    background-color: #f6f6f6;
}

.ui-autocomplete-multiselect input {
    display: inline-block;
    border: none;
    outline: none;
    height: auto;
    margin: 2px;
    overflow: visible;
    margin-bottom: 5px;
    text-align: left;
}

.ui-autocomplete-multiselect.ui-state-active {
    outline: none;
    border: 1px solid #7ea4c7;
    -moz-box-shadow: 0 0 5px rgba(50,150,255,0.5);
    -webkit-box-shadow: 0 0 5px rgba(50,150,255,0.5);
    -khtml-box-shadow: 0 0 5px rgba(50,150,255,0.5);
    box-shadow: 0 0 5px rgba(50,150,255,0.5);
}

.ui-autocomplete {
    border-top: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}












/* ==========================================================================
   Media Queries
   ========================================================================== */

@media only screen and (min-width: 35em) {

}

@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
       only screen and (min-resolution: 144dpi) {

}

/* ==========================================================================
   Helper classes
   ========================================================================== */

.ir {
    background-color: transparent;
    border: 0;
    overflow: hidden;
    *text-indent: -9999px;
}

.ir:before {
    content: "";
    display: block;
    width: 0;
    height: 100%;
}

.hidden {
    display: none !important;
    visibility: hidden;
}

.visuallyhidden {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}

.visuallyhidden.focusable:active,
.visuallyhidden.focusable:focus {
    clip: auto;
    height: auto;
    margin: 0;
    overflow: visible;
    position: static;
    width: auto;
}

.invisible {
    visibility: hidden;
}

.clearfix:before,
.clearfix:after {
    content: " ";
    display: table;
}

.clearfix:after {
    clear: both;
}

.clearfix {
    *zoom: 1;
}
a, a:hover{
    text-decoration: none;
}

/* ==========================================================================
   Print styles
   ========================================================================== */

@media print {
    * {
        background: transparent !important;
        color: #000 !important; /* Black prints faster: h5bp.com/s */
        box-shadow: none !important;
        text-shadow: none !important;
    }

    a,
    a:visited {
        text-decoration: underline;
    }

    a[href]:after {
        content: " (" attr(href) ")";
    }

    abbr[title]:after {
        content: " (" attr(title) ")";
    }

    /*
     * Don't show links for images, or javascript/internal links
     */

    .ir a:after,
    a[href^="javascript:"]:after,
    a[href^="#"]:after {
        content: "";
    }

    pre,
    blockquote {
        border: 1px solid #999;
        page-break-inside: avoid;
    }

    thead {
        display: table-header-group; /* h5bp.com/t */
    }

    tr,
    img {
        page-break-inside: avoid;
    }

    img {
        max-width: 100% !important;
    }

    @page {
        margin: 0.5cm;
    }

    p,
    h2,
    h3 {
        orphans: 3;
        widows: 3;
    }

    h2,
    h3 {
        page-break-after: avoid;}
		}
   
			 </style>
							<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"/>
							<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/main.css">
							<script>
							var $=jQuery.noConflict();
							</script>
							
							<script src="<?php echo get_template_directory_uri();?>/js/multiselectautocomplete/vendor/modernizr-2.6.2.min.js"></script>
							
							<?php /*<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script> 
<script src="<?php echo get_template_directory_uri();?>/js/multiselectautocomplete/jquery.autocomplete.multiselect.js"></script> */?>
<?php /*<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"/>*/?>
<style>/* Layout helpers
----------------------------------*/
.ui-helper-hidden {
	display: none;
}
.ui-helper-hidden-accessible {
	border: 0;
	clip: rect(0 0 0 0);
	height: 1px;
	margin: -1px;
	overflow: hidden;
	padding: 0;
	position: absolute;
	width: 1px;
}
.ui-helper-reset {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	line-height: 1.3;
	text-decoration: none;
	font-size: 100%;
	list-style: none;
}
.ui-helper-clearfix:before,
.ui-helper-clearfix:after {
	content: "";
	display: table;
	border-collapse: collapse;
}
.ui-helper-clearfix:after {
	clear: both;
}
.ui-helper-clearfix {
	min-height: 0; /* support: IE7 */
}
.ui-helper-zfix {
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	position: absolute;
	opacity: 0;
	filter:Alpha(Opacity=0);
}

.ui-front {
	z-index: 100;
}


/* Interaction Cues
----------------------------------*/
.ui-state-disabled {
	cursor: default !important;
}


/* Icons
----------------------------------*/

/* states and images */
.ui-icon {
	display: block;
	text-indent: -99999px;
	overflow: hidden;
	background-repeat: no-repeat;
}


/* Misc visuals
----------------------------------*/

/* Overlays */
.ui-widget-overlay {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
.ui-accordion .ui-accordion-header {
	display: block;
	cursor: pointer;
	position: relative;
	margin-top: 2px;
	padding: .5em .5em .5em .7em;
	min-height: 0; /* support: IE7 */
}
.ui-accordion .ui-accordion-icons {
	padding-left: 2.2em;
}
.ui-accordion .ui-accordion-noicons {
	padding-left: .7em;
}
.ui-accordion .ui-accordion-icons .ui-accordion-icons {
	padding-left: 2.2em;
}
.ui-accordion .ui-accordion-header .ui-accordion-header-icon {
	position: absolute;
	left: .5em;
	top: 50%;
	margin-top: -8px;
}
.ui-accordion .ui-accordion-content {
	padding: 1em 2.2em;
	border-top: 0;
	overflow: auto;
}
.ui-autocomplete {
	position: absolute;
	top: 0;
	left: 0;
	cursor: default;
}
.ui-button {
	display: inline-block;
	position: relative;
	padding: 0;
	line-height: normal;
	margin-right: .1em;
	cursor: pointer;
	vertical-align: middle;
	text-align: center;
	overflow: visible; /* removes extra width in IE */
}
.ui-button,
.ui-button:link,
.ui-button:visited,
.ui-button:hover,
.ui-button:active {
	text-decoration: none;
}
/* to make room for the icon, a width needs to be set here */
.ui-button-icon-only {
	width: 2.2em;
}
/* button elements seem to need a little more width */
button.ui-button-icon-only {
	width: 2.4em;
}
.ui-button-icons-only {
	width: 3.4em;
}
button.ui-button-icons-only {
	width: 3.7em;
}

/* button text element */
.ui-button .ui-button-text {
	display: block;
	line-height: normal;
}
.ui-button-text-only .ui-button-text {
	padding: .4em 1em;
}
.ui-button-icon-only .ui-button-text,
.ui-button-icons-only .ui-button-text {
	padding: .4em;
	text-indent: -9999999px;
}
.ui-button-text-icon-primary .ui-button-text,
.ui-button-text-icons .ui-button-text {
	padding: .4em 1em .4em 2.1em;
}
.ui-button-text-icon-secondary .ui-button-text,
.ui-button-text-icons .ui-button-text {
	padding: .4em 2.1em .4em 1em;
}
.ui-button-text-icons .ui-button-text {
	padding-left: 2.1em;
	padding-right: 2.1em;
}
/* no icon support for input elements, provide padding by default */
input.ui-button {
	padding: .4em 1em;
}

/* button icon element(s) */
.ui-button-icon-only .ui-icon,
.ui-button-text-icon-primary .ui-icon,
.ui-button-text-icon-secondary .ui-icon,
.ui-button-text-icons .ui-icon,
.ui-button-icons-only .ui-icon {
	position: absolute;
	top: 50%;
	margin-top: -8px;
}
.ui-button-icon-only .ui-icon {
	left: 50%;
	margin-left: -8px;
}
.ui-button-text-icon-primary .ui-button-icon-primary,
.ui-button-text-icons .ui-button-icon-primary,
.ui-button-icons-only .ui-button-icon-primary {
	left: .5em;
}
.ui-button-text-icon-secondary .ui-button-icon-secondary,
.ui-button-text-icons .ui-button-icon-secondary,
.ui-button-icons-only .ui-button-icon-secondary {
	right: .5em;
}

/* button sets */
.ui-buttonset {
	margin-right: 7px;
}
.ui-buttonset .ui-button {
	margin-left: 0;
	margin-right: -.3em;
}

/* workarounds */
/* reset extra padding in Firefox, see h5bp.com/l */
input.ui-button::-moz-focus-inner,
button.ui-button::-moz-focus-inner {
	border: 0;
	padding: 0;
}
.ui-datepicker {
	width: 17em;
	padding: .2em .2em 0;
	display: none;
}
.ui-datepicker .ui-datepicker-header {
	position: relative;
	padding: .2em 0;
}
.ui-datepicker .ui-datepicker-prev,
.ui-datepicker .ui-datepicker-next {
	position: absolute;
	top: 2px;
	width: 1.8em;
	height: 1.8em;
}
.ui-datepicker .ui-datepicker-prev-hover,
.ui-datepicker .ui-datepicker-next-hover {
	top: 1px;
}
.ui-datepicker .ui-datepicker-prev {
	left: 2px;
}
.ui-datepicker .ui-datepicker-next {
	right: 2px;
}
.ui-datepicker .ui-datepicker-prev-hover {
	left: 1px;
}
.ui-datepicker .ui-datepicker-next-hover {
	right: 1px;
}
.ui-datepicker .ui-datepicker-prev span,
.ui-datepicker .ui-datepicker-next span {
	display: block;
	position: absolute;
	left: 50%;
	margin-left: -8px;
	top: 50%;
	margin-top: -8px;
}
.ui-datepicker .ui-datepicker-title {
	margin: 0 2.3em;
	line-height: 1.8em;
	text-align: center;
}
.ui-datepicker .ui-datepicker-title select {
	font-size: 1em;
	margin: 1px 0;
}
.ui-datepicker select.ui-datepicker-month,
.ui-datepicker select.ui-datepicker-year {
	width: 49%;
}
.ui-datepicker table {
	width: 100%;
	font-size: .9em;
	border-collapse: collapse;
	margin: 0 0 .4em;
}
.ui-datepicker th {
	padding: .7em .3em;
	text-align: center;
	font-weight: bold;
	border: 0;
}
.ui-datepicker td {
	border: 0;
	padding: 1px;
}
.ui-datepicker td span,
.ui-datepicker td a {
	display: block;
	padding: .2em;
	text-align: right;
	text-decoration: none;
}
.ui-datepicker .ui-datepicker-buttonpane {
	background-image: none;
	margin: .7em 0 0 0;
	padding: 0 .2em;
	border-left: 0;
	border-right: 0;
	border-bottom: 0;
}
.ui-datepicker .ui-datepicker-buttonpane button {
	float: right;
	margin: .5em .2em .4em;
	cursor: pointer;
	padding: .2em .6em .3em .6em;
	width: auto;
	overflow: visible;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current {
	float: left;
}

/* with multiple calendars */
.ui-datepicker.ui-datepicker-multi {
	width: auto;
}
.ui-datepicker-multi .ui-datepicker-group {
	float: left;
}
.ui-datepicker-multi .ui-datepicker-group table {
	width: 95%;
	margin: 0 auto .4em;
}
.ui-datepicker-multi-2 .ui-datepicker-group {
	width: 50%;
}
.ui-datepicker-multi-3 .ui-datepicker-group {
	width: 33.3%;
}
.ui-datepicker-multi-4 .ui-datepicker-group {
	width: 25%;
}
.ui-datepicker-multi .ui-datepicker-group-last .ui-datepicker-header,
.ui-datepicker-multi .ui-datepicker-group-middle .ui-datepicker-header {
	border-left-width: 0;
}
.ui-datepicker-multi .ui-datepicker-buttonpane {
	clear: left;
}
.ui-datepicker-row-break {
	clear: both;
	width: 100%;
	font-size: 0;
}

/* RTL support */
.ui-datepicker-rtl {
	direction: rtl;
}
.ui-datepicker-rtl .ui-datepicker-prev {
	right: 2px;
	left: auto;
}
.ui-datepicker-rtl .ui-datepicker-next {
	left: 2px;
	right: auto;
}
.ui-datepicker-rtl .ui-datepicker-prev:hover {
	right: 1px;
	left: auto;
}
.ui-datepicker-rtl .ui-datepicker-next:hover {
	left: 1px;
	right: auto;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane {
	clear: right;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane button {
	float: left;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane button.ui-datepicker-current,
.ui-datepicker-rtl .ui-datepicker-group {
	float: right;
}
.ui-datepicker-rtl .ui-datepicker-group-last .ui-datepicker-header,
.ui-datepicker-rtl .ui-datepicker-group-middle .ui-datepicker-header {
	border-right-width: 0;
	border-left-width: 1px;
}
.ui-dialog {
	overflow: hidden;
	position: absolute;
	top: 0;
	left: 0;
	padding: .2em;
	outline: 0;
}
.ui-dialog .ui-dialog-titlebar {
	padding: .4em 1em;
	position: relative;
}
.ui-dialog .ui-dialog-title {
	float: left;
	margin: .1em 0;
	white-space: nowrap;
	width: 90%;
	overflow: hidden;
	text-overflow: ellipsis;
}
.ui-dialog .ui-dialog-titlebar-close {
	position: absolute;
	right: .3em;
	top: 50%;
	width: 20px;
	margin: -10px 0 0 0;
	padding: 1px;
	height: 20px;
}
.ui-dialog .ui-dialog-content {
	position: relative;
	border: 0;
	padding: .5em 1em;
	background: none;
	overflow: auto;
}
.ui-dialog .ui-dialog-buttonpane {
	text-align: left;
	border-width: 1px 0 0 0;
	background-image: none;
	margin-top: .5em;
	padding: .3em 1em .5em .4em;
}
.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {
	float: right;
}
.ui-dialog .ui-dialog-buttonpane button {
	margin: .5em .4em .5em 0;
	cursor: pointer;
}
.ui-dialog .ui-resizable-se {
	width: 12px;
	height: 12px;
	right: -5px;
	bottom: -5px;
	background-position: 16px 16px;
}
.ui-draggable .ui-dialog-titlebar {
	cursor: move;
}
.ui-menu {
	list-style: none;
	padding: 2px;
	margin: 0;
	display: block;
	outline: none;
}
.ui-menu .ui-menu {
	margin-top: -3px;
	position: absolute;
}
.ui-menu .ui-menu-item {
	margin: 0;
	padding: 0;
	width: 100%;
	/* support: IE10, see #8844 */
	list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
}
.ui-menu .ui-menu-divider {
	margin: 5px -2px 5px -2px;
	height: 0;
	font-size: 0;
	line-height: 0;
	border-width: 1px 0 0 0;
}
.ui-menu .ui-menu-item a {
	text-decoration: none;
	display: block;
	padding: 2px .4em;
	line-height: 1.5;
	min-height: 0; /* support: IE7 */
	font-weight: normal;
}
.ui-menu .ui-menu-item a.ui-state-focus,
.ui-menu .ui-menu-item a.ui-state-active {
	font-weight: normal;
	margin: -1px;
}

.ui-menu .ui-state-disabled {
	font-weight: normal;
	margin: .4em 0 .2em;
	line-height: 1.5;
}
.ui-menu .ui-state-disabled a {
	cursor: default;
}

/* icon support */
.ui-menu-icons {
	position: relative;
}
.ui-menu-icons .ui-menu-item a {
	position: relative;
	padding-left: 2em;
}

/* left-aligned */
.ui-menu .ui-icon {
	position: absolute;
	top: .2em;
	left: .2em;
}

/* right-aligned */
.ui-menu .ui-menu-icon {
	position: static;
	float: right;
}
.ui-progressbar {
	height: 2em;
	text-align: left;
	overflow: hidden;
}
.ui-progressbar .ui-progressbar-value {
	margin: -1px;
	height: 100%;
}
.ui-progressbar .ui-progressbar-overlay {
	background: url("<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/animated-overlay.gif");
	height: 100%;
	filter: alpha(opacity=25);
	opacity: 0.25;
}
.ui-progressbar-indeterminate .ui-progressbar-value {
	background-image: none;
}
.ui-resizable {
	position: relative;
}
.ui-resizable-handle {
	position: absolute;
	font-size: 0.1px;
	display: block;
}
.ui-resizable-disabled .ui-resizable-handle,
.ui-resizable-autohide .ui-resizable-handle {
	display: none;
}
.ui-resizable-n {
	cursor: n-resize;
	height: 7px;
	width: 100%;
	top: -5px;
	left: 0;
}
.ui-resizable-s {
	cursor: s-resize;
	height: 7px;
	width: 100%;
	bottom: -5px;
	left: 0;
}
.ui-resizable-e {
	cursor: e-resize;
	width: 7px;
	right: -5px;
	top: 0;
	height: 100%;
}
.ui-resizable-w {
	cursor: w-resize;
	width: 7px;
	left: -5px;
	top: 0;
	height: 100%;
}
.ui-resizable-se {
	cursor: se-resize;
	width: 12px;
	height: 12px;
	right: 1px;
	bottom: 1px;
}
.ui-resizable-sw {
	cursor: sw-resize;
	width: 9px;
	height: 9px;
	left: -5px;
	bottom: -5px;
}
.ui-resizable-nw {
	cursor: nw-resize;
	width: 9px;
	height: 9px;
	left: -5px;
	top: -5px;
}
.ui-resizable-ne {
	cursor: ne-resize;
	width: 9px;
	height: 9px;
	right: -5px;
	top: -5px;
}
.ui-selectable-helper {
	position: absolute;
	z-index: 100;
	border: 1px dotted black;
}
.ui-slider {
	position: relative;
	text-align: left;
}
.ui-slider .ui-slider-handle {
	position: absolute;
	z-index: 2;
	width: 1.2em;
	height: 1.2em;
	cursor: default;
}
.ui-slider .ui-slider-range {
	position: absolute;
	z-index: 1;
	font-size: .7em;
	display: block;
	border: 0;
	background-position: 0 0;
}

/* For IE8 - See #6727 */
.ui-slider.ui-state-disabled .ui-slider-handle,
.ui-slider.ui-state-disabled .ui-slider-range {
	filter: inherit;
}

.ui-slider-horizontal {
	height: .8em;
}
.ui-slider-horizontal .ui-slider-handle {
	top: -.3em;
	margin-left: -.6em;
}
.ui-slider-horizontal .ui-slider-range {
	top: 0;
	height: 100%;
}
.ui-slider-horizontal .ui-slider-range-min {
	left: 0;
}
.ui-slider-horizontal .ui-slider-range-max {
	right: 0;
}

.ui-slider-vertical {
	width: .8em;
	height: 100px;
}
.ui-slider-vertical .ui-slider-handle {
	left: -.3em;
	margin-left: 0;
	margin-bottom: -.6em;
}
.ui-slider-vertical .ui-slider-range {
	left: 0;
	width: 100%;
}
.ui-slider-vertical .ui-slider-range-min {
	bottom: 0;
}
.ui-slider-vertical .ui-slider-range-max {
	top: 0;
}
.ui-spinner {
	position: relative;
	display: inline-block;
	overflow: hidden;
	padding: 0;
	vertical-align: middle;
}
.ui-spinner-input {
	border: none;
	background: none;
	color: inherit;
	padding: 0;
	margin: .2em 0;
	vertical-align: middle;
	margin-left: .4em;
	margin-right: 22px;
}
.ui-spinner-button {
	width: 16px;
	height: 50%;
	font-size: .5em;
	padding: 0;
	margin: 0;
	text-align: center;
	position: absolute;
	cursor: default;
	display: block;
	overflow: hidden;
	right: 0;
}
/* more specificity required here to override default borders */
.ui-spinner a.ui-spinner-button {
	border-top: none;
	border-bottom: none;
	border-right: none;
}
/* vertically center icon */
.ui-spinner .ui-icon {
	position: absolute;
	margin-top: -8px;
	top: 50%;
	left: 0;
}
.ui-spinner-up {
	top: 0;
}
.ui-spinner-down {
	bottom: 0;
}

/* TR overrides */
.ui-spinner .ui-icon-triangle-1-s {
	/* need to fix icons sprite */
	background-position: -65px -16px;
}
.ui-tabs {
	position: relative;/* position: relative prevents IE scroll bug (element with position: relative inside container with overflow: auto appear as "fixed") */
	padding: .2em;
}
.ui-tabs .ui-tabs-nav {
	margin: 0;
	padding: .2em .2em 0;
}
.ui-tabs .ui-tabs-nav li {
	list-style: none;
	float: left;
	position: relative;
	top: 0;
	margin: 1px .2em 0 0;
	border-bottom-width: 0;
	padding: 0;
	white-space: nowrap;
}
.ui-tabs .ui-tabs-nav .ui-tabs-anchor {
	float: left;
	padding: .5em 1em;
	text-decoration: none;
}
.ui-tabs .ui-tabs-nav li.ui-tabs-active {
	margin-bottom: -1px;
	padding-bottom: 1px;
}
.ui-tabs .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor,
.ui-tabs .ui-tabs-nav li.ui-state-disabled .ui-tabs-anchor,
.ui-tabs .ui-tabs-nav li.ui-tabs-loading .ui-tabs-anchor {
	cursor: text;
}
.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor {
	cursor: pointer;
}
.ui-tabs .ui-tabs-panel {
	display: block;
	border-width: 0;
	padding: 1em 1.4em;
	background: none;
}
.ui-tooltip {
	padding: 8px;
	position: absolute;
	z-index: 9999;
	max-width: 300px;
	-webkit-box-shadow: 0 0 5px #aaa;
	box-shadow: 0 0 5px #aaa;
}
body .ui-tooltip {
	border-width: 2px;
}

/* Component containers
----------------------------------*/
.ui-widget {
	font-family: Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;
	font-size: 1.1em;
}
.ui-widget .ui-widget {
	font-size: 1em;
}
.ui-widget input,
.ui-widget select,
.ui-widget textarea,
.ui-widget button {
	font-family: Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;
	font-size: 1em;
}
.ui-widget-content {
	border: 1px solid #dddddd;
	background: #eeeeee url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_highlight-soft_100_eeeeee_1x100.png) 50% top repeat-x;
	color: #333333;
}
.ui-widget-content a {
	color: #333333;
}
.ui-widget-header {
	border: 1px solid #e78f08;
	background: #f6a828 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x;
	color: #ffffff;
	font-weight: bold;
}
.ui-widget-header a {
	color: #ffffff;
}

/* Interaction states
----------------------------------*/
.ui-state-default,
.ui-widget-content .ui-state-default,
.ui-widget-header .ui-state-default {
	border: 1px solid #cccccc;
	background: #f6f6f6 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_glass_100_f6f6f6_1x400.png) 50% 50% repeat-x;
	font-weight: bold;
	color: #1c94c4;
}
.ui-state-default a,
.ui-state-default a:link,
.ui-state-default a:visited {
	color: #1c94c4;
	text-decoration: none;
}
.ui-state-hover,
.ui-widget-content .ui-state-hover,
.ui-widget-header .ui-state-hover,
.ui-state-focus,
.ui-widget-content .ui-state-focus,
.ui-widget-header .ui-state-focus {
	border: 1px solid #fbcb09;
	background: #fdf5ce url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_glass_100_fdf5ce_1x400.png) 50% 50% repeat-x;
	font-weight: bold;
	color: #c77405;
}
.ui-state-hover a,
.ui-state-hover a:hover,
.ui-state-hover a:link,
.ui-state-hover a:visited,
.ui-state-focus a,
.ui-state-focus a:hover,
.ui-state-focus a:link,
.ui-state-focus a:visited {
	color: #c77405;
	text-decoration: none;
}
.ui-state-active,
.ui-widget-content .ui-state-active,
.ui-widget-header .ui-state-active {
	border: 1px solid #fbd850;
	background: #ffffff url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_glass_65_ffffff_1x400.png) 50% 50% repeat-x;
	font-weight: bold;
	color: #eb8f00;
}
.ui-state-active a,
.ui-state-active a:link,
.ui-state-active a:visited {
	color: #eb8f00;
	text-decoration: none;
}

/* Interaction Cues
----------------------------------*/
.ui-state-highlight,
.ui-widget-content .ui-state-highlight,
.ui-widget-header .ui-state-highlight {
	border: 1px solid #fed22f;
	background: #ffe45c url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_highlight-soft_75_ffe45c_1x100.png) 50% top repeat-x;
	color: #363636;
}
.ui-state-highlight a,
.ui-widget-content .ui-state-highlight a,
.ui-widget-header .ui-state-highlight a {
	color: #363636;
}
.ui-state-error,
.ui-widget-content .ui-state-error,
.ui-widget-header .ui-state-error {
	border: 1px solid #cd0a0a;
	background: #b81900 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_diagonals-thick_18_b81900_40x40.png) 50% 50% repeat;

	color: #ffffff;
}
.ui-state-error a,
.ui-widget-content .ui-state-error a,
.ui-widget-header .ui-state-error a {
	color: #ffffff;
}
.ui-state-error-text,
.ui-widget-content .ui-state-error-text,
.ui-widget-header .ui-state-error-text {
	color: #ffffff;
}
.ui-priority-primary,
.ui-widget-content .ui-priority-primary,
.ui-widget-header .ui-priority-primary {
	font-weight: bold;
}
.ui-priority-secondary,
.ui-widget-content .ui-priority-secondary,
.ui-widget-header .ui-priority-secondary {
	opacity: .7;
	filter:Alpha(Opacity=70);
	font-weight: normal;
}
.ui-state-disabled,
.ui-widget-content .ui-state-disabled,
.ui-widget-header .ui-state-disabled {
	opacity: .35;
	filter:Alpha(Opacity=35);
	background-image: none;
}
.ui-state-disabled .ui-icon {
	filter:Alpha(Opacity=35); /* For IE8 - See #6059 */
}

/* Icons
----------------------------------*/

/* states and images */
.ui-icon {
	width: 16px;
	height: 16px;
}
.ui-icon,
.ui-widget-content .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_222222_256x240.png);
}
.ui-widget-header .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ffffff_256x240.png);
}
.ui-state-default .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ef8c08_256x240.png);
}
.ui-state-hover .ui-icon,
.ui-state-focus .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ef8c08_256x240.png);
}
.ui-state-active .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ef8c08_256x240.png);
}
.ui-state-highlight .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_228ef1_256x240.png);
}
.ui-state-error .ui-icon,
.ui-state-error-text .ui-icon {
	background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_ffd27a_256x240.png);
}

/* positioning */
.ui-icon-blank { background-position: 16px 16px; }
.ui-icon-carat-1-n { background-position: 0 0; }
.ui-icon-carat-1-ne { background-position: -16px 0; }
.ui-icon-carat-1-e { background-position: -32px 0; }
.ui-icon-carat-1-se { background-position: -48px 0; }
.ui-icon-carat-1-s { background-position: -64px 0; }
.ui-icon-carat-1-sw { background-position: -80px 0; }
.ui-icon-carat-1-w { background-position: -96px 0; }
.ui-icon-carat-1-nw { background-position: -112px 0; }
.ui-icon-carat-2-n-s { background-position: -128px 0; }
.ui-icon-carat-2-e-w { background-position: -144px 0; }
.ui-icon-triangle-1-n { background-position: 0 -16px; }
.ui-icon-triangle-1-ne { background-position: -16px -16px; }
.ui-icon-triangle-1-e { background-position: -32px -16px; }
.ui-icon-triangle-1-se { background-position: -48px -16px; }
.ui-icon-triangle-1-s { background-position: -64px -16px; }
.ui-icon-triangle-1-sw { background-position: -80px -16px; }
.ui-icon-triangle-1-w { background-position: -96px -16px; }
.ui-icon-triangle-1-nw { background-position: -112px -16px; }
.ui-icon-triangle-2-n-s { background-position: -128px -16px; }
.ui-icon-triangle-2-e-w { background-position: -144px -16px; }
.ui-icon-arrow-1-n { background-position: 0 -32px; }
.ui-icon-arrow-1-ne { background-position: -16px -32px; }
.ui-icon-arrow-1-e { background-position: -32px -32px; }
.ui-icon-arrow-1-se { background-position: -48px -32px; }
.ui-icon-arrow-1-s { background-position: -64px -32px; }
.ui-icon-arrow-1-sw { background-position: -80px -32px; }
.ui-icon-arrow-1-w { background-position: -96px -32px; }
.ui-icon-arrow-1-nw { background-position: -112px -32px; }
.ui-icon-arrow-2-n-s { background-position: -128px -32px; }
.ui-icon-arrow-2-ne-sw { background-position: -144px -32px; }
.ui-icon-arrow-2-e-w { background-position: -160px -32px; }
.ui-icon-arrow-2-se-nw { background-position: -176px -32px; }
.ui-icon-arrowstop-1-n { background-position: -192px -32px; }
.ui-icon-arrowstop-1-e { background-position: -208px -32px; }
.ui-icon-arrowstop-1-s { background-position: -224px -32px; }
.ui-icon-arrowstop-1-w { background-position: -240px -32px; }
.ui-icon-arrowthick-1-n { background-position: 0 -48px; }
.ui-icon-arrowthick-1-ne { background-position: -16px -48px; }
.ui-icon-arrowthick-1-e { background-position: -32px -48px; }
.ui-icon-arrowthick-1-se { background-position: -48px -48px; }
.ui-icon-arrowthick-1-s { background-position: -64px -48px; }
.ui-icon-arrowthick-1-sw { background-position: -80px -48px; }
.ui-icon-arrowthick-1-w { background-position: -96px -48px; }
.ui-icon-arrowthick-1-nw { background-position: -112px -48px; }
.ui-icon-arrowthick-2-n-s { background-position: -128px -48px; }
.ui-icon-arrowthick-2-ne-sw { background-position: -144px -48px; }
.ui-icon-arrowthick-2-e-w { background-position: -160px -48px; }
.ui-icon-arrowthick-2-se-nw { background-position: -176px -48px; }
.ui-icon-arrowthickstop-1-n { background-position: -192px -48px; }
.ui-icon-arrowthickstop-1-e { background-position: -208px -48px; }
.ui-icon-arrowthickstop-1-s { background-position: -224px -48px; }
.ui-icon-arrowthickstop-1-w { background-position: -240px -48px; }
.ui-icon-arrowreturnthick-1-w { background-position: 0 -64px; }
.ui-icon-arrowreturnthick-1-n { background-position: -16px -64px; }
.ui-icon-arrowreturnthick-1-e { background-position: -32px -64px; }
.ui-icon-arrowreturnthick-1-s { background-position: -48px -64px; }
.ui-icon-arrowreturn-1-w { background-position: -64px -64px; }
.ui-icon-arrowreturn-1-n { background-position: -80px -64px; }
.ui-icon-arrowreturn-1-e { background-position: -96px -64px; }
.ui-icon-arrowreturn-1-s { background-position: -112px -64px; }
.ui-icon-arrowrefresh-1-w { background-position: -128px -64px; }
.ui-icon-arrowrefresh-1-n { background-position: -144px -64px; }
.ui-icon-arrowrefresh-1-e { background-position: -160px -64px; }
.ui-icon-arrowrefresh-1-s { background-position: -176px -64px; }
.ui-icon-arrow-4 { background-position: 0 -80px; }
.ui-icon-arrow-4-diag { background-position: -16px -80px; }
.ui-icon-extlink { background-position: -32px -80px; }
.ui-icon-newwin { background-position: -48px -80px; }
.ui-icon-refresh { background-position: -64px -80px; }
.ui-icon-shuffle { background-position: -80px -80px; }
.ui-icon-transfer-e-w { background-position: -96px -80px; }
.ui-icon-transferthick-e-w { background-position: -112px -80px; }
.ui-icon-folder-collapsed { background-position: 0 -96px; }
.ui-icon-folder-open { background-position: -16px -96px; }
.ui-icon-document { background-position: -32px -96px; }
.ui-icon-document-b { background-position: -48px -96px; }
.ui-icon-note { background-position: -64px -96px; }
.ui-icon-mail-closed { background-position: -80px -96px; }
.ui-icon-mail-open { background-position: -96px -96px; }
.ui-icon-suitcase { background-position: -112px -96px; }
.ui-icon-comment { background-position: -128px -96px; }
.ui-icon-person { background-position: -144px -96px; }
.ui-icon-print { background-position: -160px -96px; }
.ui-icon-trash { background-position: -176px -96px; }
.ui-icon-locked { background-position: -192px -96px; }
.ui-icon-unlocked { background-position: -208px -96px; }
.ui-icon-bookmark { background-position: -224px -96px; }
.ui-icon-tag { background-position: -240px -96px; }
.ui-icon-home { background-position: 0 -112px; }
.ui-icon-flag { background-position: -16px -112px; }
.ui-icon-calendar { background-position: -32px -112px; }
.ui-icon-cart { background-position: -48px -112px; }
.ui-icon-pencil { background-position: -64px -112px; }
.ui-icon-clock { background-position: -80px -112px; }
.ui-icon-disk { background-position: -96px -112px; }
.ui-icon-calculator { background-position: -112px -112px; }
.ui-icon-zoomin { background-position: -128px -112px; }
.ui-icon-zoomout { background-position: -144px -112px; }
.ui-icon-search { background-position: -160px -112px; }
.ui-icon-wrench { background-position: -176px -112px; }
.ui-icon-gear { background-position: -192px -112px; }
.ui-icon-heart { background-position: -208px -112px; }
.ui-icon-star { background-position: -224px -112px; }
.ui-icon-link { background-position: -240px -112px; }
.ui-icon-cancel { background-position: 0 -128px; }
.ui-icon-plus { background-position: -16px -128px; }
.ui-icon-plusthick { background-position: -32px -128px; }
.ui-icon-minus { background-position: -48px -128px; }
.ui-icon-minusthick { background-position: -64px -128px; }
.ui-icon-close { background-position: -80px -128px; }
.ui-icon-closethick { background-position: -96px -128px; }
.ui-icon-key { background-position: -112px -128px; }
.ui-icon-lightbulb { background-position: -128px -128px; }
.ui-icon-scissors { background-position: -144px -128px; }
.ui-icon-clipboard { background-position: -160px -128px; }
.ui-icon-copy { background-position: -176px -128px; }
.ui-icon-contact { background-position: -192px -128px; }
.ui-icon-image { background-position: -208px -128px; }
.ui-icon-video { background-position: -224px -128px; }
.ui-icon-script { background-position: -240px -128px; }
.ui-icon-alert { background-position: 0 -144px; }
.ui-icon-info { background-position: -16px -144px; }
.ui-icon-notice { background-position: -32px -144px; }
.ui-icon-help { background-position: -48px -144px; }
.ui-icon-check { background-position: -64px -144px; }
.ui-icon-bullet { background-position: -80px -144px; }
.ui-icon-radio-on { background-position: -96px -144px; }
.ui-icon-radio-off { background-position: -112px -144px; }
.ui-icon-pin-w { background-position: -128px -144px; }
.ui-icon-pin-s { background-position: -144px -144px; }
.ui-icon-play { background-position: 0 -160px; }
.ui-icon-pause { background-position: -16px -160px; }
.ui-icon-seek-next { background-position: -32px -160px; }
.ui-icon-seek-prev { background-position: -48px -160px; }
.ui-icon-seek-end { background-position: -64px -160px; }
.ui-icon-seek-start { background-position: -80px -160px; }
/* ui-icon-seek-first is deprecated, use ui-icon-seek-start instead */
.ui-icon-seek-first { background-position: -80px -160px; }
.ui-icon-stop { background-position: -96px -160px; }
.ui-icon-eject { background-position: -112px -160px; }
.ui-icon-volume-off { background-position: -128px -160px; }
.ui-icon-volume-on { background-position: -144px -160px; }
.ui-icon-power { background-position: 0 -176px; }
.ui-icon-signal-diag { background-position: -16px -176px; }
.ui-icon-signal { background-position: -32px -176px; }
.ui-icon-battery-0 { background-position: -48px -176px; }
.ui-icon-battery-1 { background-position: -64px -176px; }
.ui-icon-battery-2 { background-position: -80px -176px; }
.ui-icon-battery-3 { background-position: -96px -176px; }
.ui-icon-circle-plus { background-position: 0 -192px; }
.ui-icon-circle-minus { background-position: -16px -192px; }
.ui-icon-circle-close { background-position: -32px -192px; }
.ui-icon-circle-triangle-e { background-position: -48px -192px; }
.ui-icon-circle-triangle-s { background-position: -64px -192px; }
.ui-icon-circle-triangle-w { background-position: -80px -192px; }
.ui-icon-circle-triangle-n { background-position: -96px -192px; }
.ui-icon-circle-arrow-e { background-position: -112px -192px; }
.ui-icon-circle-arrow-s { background-position: -128px -192px; }
.ui-icon-circle-arrow-w { background-position: -144px -192px; }
.ui-icon-circle-arrow-n { background-position: -160px -192px; }
.ui-icon-circle-zoomin { background-position: -176px -192px; }
.ui-icon-circle-zoomout { background-position: -192px -192px; }
.ui-icon-circle-check { background-position: -208px -192px; }
.ui-icon-circlesmall-plus { background-position: 0 -208px; }
.ui-icon-circlesmall-minus { background-position: -16px -208px; }
.ui-icon-circlesmall-close { background-position: -32px -208px; }
.ui-icon-squaresmall-plus { background-position: -48px -208px; }
.ui-icon-squaresmall-minus { background-position: -64px -208px; }
.ui-icon-squaresmall-close { background-position: -80px -208px; }
.ui-icon-grip-dotted-vertical { background-position: 0 -224px; }
.ui-icon-grip-dotted-horizontal { background-position: -16px -224px; }
.ui-icon-grip-solid-vertical { background-position: -32px -224px; }
.ui-icon-grip-solid-horizontal { background-position: -48px -224px; }
.ui-icon-gripsmall-diagonal-se { background-position: -64px -224px; }
.ui-icon-grip-diagonal-se { background-position: -80px -224px; }


/* Misc visuals
----------------------------------*/

/* Corner radius */
.ui-corner-all,
.ui-corner-top,
.ui-corner-left,
.ui-corner-tl {
	border-top-left-radius: 4px;
}
.ui-corner-all,
.ui-corner-top,
.ui-corner-right,
.ui-corner-tr {
	border-top-right-radius: 4px;
}
.ui-corner-all,
.ui-corner-bottom,
.ui-corner-left,
.ui-corner-bl {
	border-bottom-left-radius: 4px;
}
.ui-corner-all,
.ui-corner-bottom,
.ui-corner-right,
.ui-corner-br {
	border-bottom-right-radius: 4px;
}

/* Overlays */
.ui-widget-overlay {
	background: #666666 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_diagonals-thick_20_666666_40x40.png) 50% 50% repeat;
	opacity: .5;
	filter: Alpha(Opacity=50);
}
.ui-widget-shadow {
	margin: -5px 0 0 -5px;
	padding: 5px;
	background: #000000 url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-bg_flat_10_000000_40x100.png) 50% 50% repeat-x;
	opacity: .2;
	filter: Alpha(Opacity=20);
	border-radius: 5px;
}
#post_tags{width:100% !important;}
.ui-autocomplete-multiselect{border:none !important; width:721px !important; height:auto !important;}
.ui-autocomplete-multiselect textarea{border: 0px !important;
box-shadow: unset !important;
min-height: 50px !important;
height: auto !important;}
.ui-state-default textarea{margin:0px;}
.ui-autocomplete-multiselect .ui-icon {
    background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_222222_256x240.png) !important;
    right: -4px;
}


.ui-state-default{height: auto !important;  width: 100% !important;  padding: 0px; background: none !important;}
.ui-state-default textarea{margin:0px;height:110px !important;}

.ui-autocomplete-multiselect{padding:0px !important;background: #1e2021 !important;border-radius:5px !important;min-height:110px !important;}
.ui-autocomplete-multiselect .ui-autocomplete-multiselect-item{float: left; margin: 3px 5px;}
.ui-autocomplete-multiselect textarea{border:0px !important;box-shadow: unset !important;min-height:50px !important;height:auto !important;}

.ui-autocomplete-multiselect .ui-icon{background-image: url(<?php echo get_template_directory_uri();?>/css/multiselectautocomplete/ui-lightness/images/ui-icons_222222_256x240.png) !important;right: -4px;}
</style>
								<div <?php /*class="inner-toggle"*/?> id="post_tag">
									<div class="form-inline row">
										<div class="col-md-6 col-md-offset-3">
											<div class="ui-autocomplete-multiselect ui-state-default ui-widget" style="width: 513px;">
												<?php 
													$posttags = get_the_tags($current_post);
													$tags_list = array();
													if ($posttags) {
													  foreach($posttags as $tag) {
														$tags_list[] = $tag->name;
														//echo $tags_list; ?>
														<div class="ui-autocomplete-multiselect-item"><?php echo $tag->name; ?><span tagnm="<?php echo $tag->name; ?>" class="ui-icon ui-icon-close"></span></div>
													<?php }
													$tags_list = implode(", ", $tags_list);
													}
												?>											   
												<textarea type="text" name="post_tags" placeholder="Tags" cols="5" rows="5" id="post_tags" class="form-control form-control-sm ui-autocomplete-input" value="" autocomplete="off" style="width: 774px;"></textarea>
												<input type='hidden' id='hdn_post_tags' placeholder='tags' name='hdn_post_tags' value='<?php echo $tags_list; ?>' size='' maxlength='' class='form-control form-control-md'>
											</div>	 
										</div>
									</div>
									<div class="help-block"><?php esc_html_e('Tags Example : ads, car, cat, business', 'classiera') ?></div>
									 <script>
										jQuery(document).ready(function(){
										  var $=jQuery.noConflict();
										});
									</script>
									<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
									<script>
								// http://jsfiddle.net/mekwall/sgxKJ/

								$.widget("ui.autocomplete", $.ui.autocomplete, {
									options : $.extend({}, this.options, {
										multiselect: true
									}),
									_create: function(){
										this._super();
								
										var self = this,
											o = self.options;
								
										if (o.multiselect) {
											console.log('multiselect true');
								
											self.selectedItems = {};           
											self.multiselect = $("<div></div>")
												.addClass("ui-autocomplete-multiselect ui-state-default ui-widget")
												.css("width", self.element.width())
												.insertBefore(self.element)
												.append(self.element)
												.bind("click.autocomplete", function(){
													self.element.focus();
													
												});
											
											var fontSize = parseInt(self.element.css("fontSize"), 10);
											function autoSize(e){
												// Hackish autosizing
												var $this = $(this);
												$this.width(1).width(this.scrollWidth+fontSize-1);
											};
								
											var kc = $.ui.keyCode;
											self.element.bind({
												"keydown.autocomplete": function(e){
													if ((this.value === "") && (e.keyCode == kc.BACKSPACE)) {
														var prev = self.element.prev();
														delete self.selectedItems[prev.text()];
														
														prev.remove();
													}
												},
												// TODO: Implement outline of container
												"focus.autocomplete blur.autocomplete": function(){
													self.multiselect.toggleClass("ui-state-active");
												},
												"keypress.autocomplete change.autocomplete focus.autocomplete blur.autocomplete": autoSize
											}).trigger("change");
								
											// TODO: There's a better way?
											o.select = o.select || function(e, ui) {
											if($("#hdn_post_tags").val()!='')
											{
												$("#hdn_post_tags").val($("#hdn_post_tags").val()+','+ui.item.label);
											}
											else
											{
												$("#hdn_post_tags").val(ui.item.label);
											}
											
												$("<div></div>")
													.addClass("ui-autocomplete-multiselect-item")
													.text(ui.item.label)
													.append(
														$("<span></span>")
															.addClass("ui-icon ui-icon-close")
															.click(function(){
																var item = $(this).parent();
																delete self.selectedItems[item.text()];
																
																item.remove();
																
															})
													)
													.insertBefore(self.element);
												self.selectedItems[ui.item.label] = ui.item;
												self._value("");
												
												return false;
											}
								
											self.options.open = function(e, ui) {
												var pos = self.multiselect.position();
												pos.top += self.multiselect.height();
												self.menu.element.position(pos);
											}
										}
								
										return this;
									}
								});
								
								$(function(){
									var availableTags = [
									<?php
									//$results = $wpdb->get_results( "SELECT * FROM wp_interest order by id desc" );
									$tags = get_tags();
									 if(count($tags)>0)
  									 {
									 	$count=1;
									 	foreach ( $tags as $tag )
										{
											if($count==count($tags))
											{?>
													"<?php echo $tag->name;?>"
												<?php
											
											}
											else
											{?>
													"<?php echo $tag->name;?>",
												<?php
											}
											$count++;
										}
									 }
									?>
									];
									$('#post_tags').autocomplete({
										source: availableTags,
										multiselect: true
									});
									/*$("#post_tags").change(function(){
										if(this.length>=3)
										{
										}
									});*/
									 $("#post_tags").keydown(function (e) {
									// alert(e.keyCode);
										//if (e.keyCode == 32)
										if (e.keyCode == 13)
										{
										 //return 109;
										// $(".ui-autocomplete-multiselect").append('<div class="ui-autocomplete-multiselect-item">'+$("#post_tags").val()+'<span class="ui-icon ui-icon-close"></span></div>');
										//$( "h2" ).insertBefore( $( ".container" ) );
										//$(".ui-autocomplete-multiselect").append('<div class="ui-autocomplete-multiselect-item">'+$("#post_tags").val()+'<span class="ui-icon ui-icon-close"></span></div>').insertBefore($("#post_tags"));
										//$( "#post_tags" ).insertBefore('<div class="ui-autocomplete-multiselect-item">'+$("#post_tags").val()+'<span class="ui-icon ui-icon-close"></span></div>');
										/*var item_val=$("#post_tags").val();
										$("#post_tags").remove();
										$(".ui-autocomplete-multiselect")
											.append('<div class="ui-autocomplete-multiselect-item">'+item_val+'<span class="ui-icon ui-icon-close"></span></div><textarea type="text" name="post_tags" placeholder="Tags" cols="5" rows="5" id="post_tags" class="form-control form-control-sm ui-autocomplete-input" value="" autocomplete="off" style="width: 785px;"></textarea>');
											//.append("#post_tags");
											*/
											$(".ui-autocomplete-multiselect").prepend('<div class="ui-autocomplete-multiselect-item">'+$("#post_tags").val()+'<span class="ui-icon ui-icon-close"></span></div>');
											if($("#hdn_post_tags").val()!='')
											{
												$("#hdn_post_tags").val($("#hdn_post_tags").val()+','+$("#post_tags").val());
											}
											else
											{
												$("#hdn_post_tags").val($("#post_tags").val());
											}
											$("#post_tags").val('');
										}
									}); 
								})
							</script>
									<div class="top-buffer2"></div>
								</div>
							</div><!--classiera-post-sub-cat-->
							<div class="single_title row">
										<h2 class="classiera-post-inner-heading"><?php esc_html_e('map', 'classiera') ?></h2>
									</div>
									<div class="help-block"><?php esc_html_e('Please select location on map that will be linked to your HYST.', 'classiera') ?></div>
									<?php /*<div>
										<span style="color:red;"><?php esc_html_e('Please select location on map that will be linked to your HYST.', 'classiera') ?></span>
									</div>*/?>
							
							<div class="col-md-6 col-md-offset-3">
                                        <?php /*<input type="text" name="location_search" id="location_search" class="form-control form-control-md" placeholder=" search for your location">*/?>
										<?php /*<input type="text" name="address" id="address" class="form-control form-control-md" placeholder=" search for your location" required style="position:relative;">*/?>
                                    </div>
							
							
							<?php 
							$classieraPriceSecOFF = $redux_demo['classiera_sale_price_off'];
							$classieraMultiCurrency = $redux_demo['classiera_multi_currency'];
							$regularpriceon= $redux_demo['regularpriceon'];
							$postCurrency = $redux_demo['classierapostcurrency'];
							$classieraTagDefault = $redux_demo['classiera_multi_currency_default'];
							?>
							<?php if($classieraPriceSecOFF == 1){?>
							<div class="form-group">
                                    <div class="form-inline row">
										<?php if($classieraMultiCurrency == 'multi'){?>
										<div class="col-sm-12">
                                            <div class="inner-addon right-addon input-group price__tag">
                                                <div class="input-group-addon">
                                                    <span class="currency__symbol">
														<?php echo classiera_Display_currency_sign($classieraTagDefault); ?>
													</span>
                                                </div>
                                                <i class="form-icon right-form-icon fa fa-angle-down"></i>
												<?php echo classiera_Select_currency_dropdow($classieraTagDefault); ?>
                                            </div>
                                        </div>
										<?php } ?>
										
										<div class="clearfix"></div>
							
                                        <?php /*<div class="col-md-6 col-md-offset-3"> 
                                                <div class="input-group-addon hide">
													<span class="currency__symbol">
													<?php 
													if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
														echo $postCurrency;
													}elseif($classieraMultiCurrency == 'multi'){
														echo classiera_Display_currency_sign($classieraTagDefault);
													}else{
														echo "&dollar;";
													}
													?>	
													</span>
												</div>
                                                <input type="text" name="post_price" id="post_price" class="form-control form-control-md" placeholder="<?php esc_html_e(' price', 'classiera') ?>">
                                        </div>	*/?>	
										
										<div class="clearfix"></div>
										
										<?php if($regularpriceon == 1){?>
                                        <div class="col-sm-6" style="display:none;">
                                            <div class="input-group">
                                                <div class="input-group-addon">
													<span class="currency__symbol">
													<?php 
													if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
														echo $postCurrency;
													}elseif($classieraMultiCurrency == 'multi'){
														echo classiera_Display_currency_sign($classieraTagDefault);
													}else{
														echo "&dollar;";
													}
													?>	
													</span>
												</div>
                                                <input type="text" name="post_old_price" class="form-control form-control-md" placeholder="<?php esc_html_e('REMOVE', 'classiera') ?>">
                                            </div>
                                        </div>
										<?php } ?>
                                    </div>									
									<?php if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){?>
                                    <div class="help-block hide"><?php esc_html_e('Currency sign is already set as', 'classiera') ?>&nbsp;<?php echo $postCurrency; ?>&nbsp;<?php esc_html_e('Please do not use currency sign in price field. Only use numbers ex: 12345', 'classiera') ?></div>
									<?php } ?>
                            </div><!--Ad Price-->
							<?php } ?>
							
							
							<!--ContactPhone-->
							<?php $classieraAskingPhone = $redux_demo['phoneon'];?>
							<?php if($classieraAskingPhone == 1){?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Phone/Mobile', 'classiera') ?> :</label>
                                <div class="col-sm-9">
                                    <div class="form-inline row">
                                        <div class="col-sm-12">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                                                <input type="text" name="post_phone" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter your phone number or Mobile number', 'classiera') ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="help-block"><?php esc_html_e('Its Not required, but if you will put phone here then it will show publicly', 'classiera') ?></div>
                                </div>
                            </div>
							<?php } ?>
							<!--ContactPhone-->							
							<?php 
								$adpostCondition= $redux_demo['adpost-condition'];
								if($adpostCondition == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Item Condition', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <input id="new" type="radio" name="item-condition" value="<?php esc_html_e('new', 'classiera') ?>" name="item-condition" checked>
                                        <label for="new"><?php esc_html_e('Brand New', 'classiera') ?></label>
                                        <input id="used" type="radio" name="item-condition" value="<?php esc_html_e('used', 'classiera') ?>" name="item-condition">
                                        <label for="used"><?php esc_html_e('Used', 'classiera') ?></label>
                                    </div>
                                </div>
                            </div><!--Item condition-->
								<?php } ?>
						</div><!---form-main-section post-detail-->
						<!-- extra fields -->
						<div class="classieraExtraFields" style="display:none;"></div>
						<!-- extra fields -->
						<!-- add photos and media -->
						<?php								
							/*Image Count Check*/
							global $redux_demo;
							global $wpdb;
							$paidIMG = $redux_demo['premium-ads-limit'];
							$regularIMG = $redux_demo['regular-ads-limit'];								
							$current_user = wp_get_current_user();
							$userID = $current_user->ID;
							$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
							$totalAds = 0;
							$usedAds = 0;
							$availableADS = '';
							if(!empty($result)){
								foreach ( $result as $info ){
									$availAds = $info->ads;
									if (is_numeric($availAds)) {
										$totalAds += $info->ads;
										$usedAds += $info->used;
									}
								}
							}
							$availableADS = $totalAds-$usedAds;							
							if($availableADS == "0" || empty($result)){
								$imageLimit = $regularIMG;
							}else{
								$imageLimit = $paidIMG;
							}
							if($currentRole == "administrator"){
								$imageLimit = $paidIMG;
							}
						if($imageLimit != 0){
						/*?>
						<div class="form-main-section media-detail">
							
                            <h4 class="text-uppercase border-bottom"><?php esc_html_e('Image And Video', 'classiera') ?> :</h4>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Photos and Video for your HYST', 'classiera') ?> :</label>
                                <div class="col-sm-9">
                                    <div class="classiera-dropzone-heading">
                                        <i class="classiera-dropzone-heading-text fa fa-cloud-upload" aria-hidden="true"></i>
                                        <div class="classiera-dropzone-heading-text">
                                            <p><?php esc_html_e('Select files to Upload', 'classiera') ?></p>
                                            <p><?php esc_html_e('You can add multiple images. HYST With photo get 50% more Responses', 'classiera') ?></p>
											<p class="limitIMG"><?php esc_html_e('', 'classiera') ?>&nbsp;&nbsp;<?php esc_html_e('', 'classiera') ?></p>
                                        </div>
                                    </div>
                                    <!-- HTML heavily inspired by http://blueimp.github.io/jQuery-File-Upload/ -->
                                    <div id="mydropzone" class="classiera-image-upload clearfix" data-maxfile="<?php echo $imageLimit; ?>">
										<!--Imageloop-->
										<?php 
										for ($i = 0; $i <8 ; $i++){
										?>
                                        <div class="classiera-image-box">
                                            <div class="classiera-upload-box">
												<input name="image-count" type="hidden" value="<?php echo $imageLimit; ?>" />
                                                <input class="classiera-input-file imgInp" id="imgInp<?php echo $i; ?>" type="file" name="upload_attachment[]">												
                                                <label class="img-label" for="imgInp<?php echo $i; ?>"><i class="fa fa-plus-square-o"></i></label>
                                                <div class="classiera-image-preview">
                                                    <img class="my-image" src=""/>
                                                    <span class="remove-img"><i class="fa fa-times-circle"></i></span>
                                                </div>
                                            </div>
                                        </div>
										<?php } ?>
										<input type="hidden" name="classiera_featured_img" id="classiera_featured_img" value="0">
										<!--Imageloop-->
                                    </div>
									
									<?php 
									$classiera_video_postads = $redux_demo['classiera_video_postads'];
									if($classiera_video_postads == 1){
									?>
                                    <div class="iframe">
                                        <div class="iframe-heading">
                                            <i class="fa fa-video-camera"></i>
                                            <span><?php esc_html_e('Put here iframe or video url.', 'classiera') ?></span>
                                        </div>
                                        <input type="file" class="form-control" name="video" id="video-code" placeholder="<?php esc_html_e('Put here video ', 'classiera') ?>">
                                        <div class="help-block">
                                            <p><?php esc_html_e('Add  video ', 'classiera') ?></p>
                                        </div>
                                    </div>
									<?php } ?>
                                </div>
                            </div>
                        </div>
						
						*/?>
						
						<?php } ?>
						
						
					<?php	/*<style>
							.image-upload > input {
							  display:none;
							  
							}
						</style>
						<div class="image-upload" style="margin-bottom:20px;">
						  <label for="file-input">
							<img src="<?php echo site_url();?>/wp-content/uploads/2018/04/file-upload-icon.png" style="width:100px;height:auto;" style="pointer-events: none"/>
						  </label>
						<script type="text/javascript" language="javascript">
							function makeFileList() {
							  var input = document.getElementById("file-input");
							  var ul = document.getElementById("fileList");
							  while (ul.hasChildNodes()) {
								ul.removeChild(ul.firstChild);
							  }
							  for (var i = 0; i < input.files.length; i++) {
								var li = document.createElement("li");
								li.innerHTML = input.files[i].name;
								ul.appendChild(li);
							  }
							  if(!ul.hasChildNodes()) {
								var li = document.createElement("li");
								li.innerHTML = 'No Files Selected';
								ul.appendChild(li);
							  }
							}
						</script>
						  <input id="file-input" name="upload_attachment[]" type="file"  onChange="makeFileList();" multiple/>
						  
						  <p style="margin-left:50px;">
							  <strong>Files You Selected:</strong>
							</p>
							
							<ul id="fileList" style="margin-left:50px;">
							  <li>No Files Selected</li>
							</ul>
						</div>
						*/?>

						<!-- add photos and media -->
						<!-- post location -->
						<?php
						$classiera_ad_location_remove = $redux_demo['classiera_ad_location_remove'];
						if($classiera_ad_location_remove == 1){
						?>
						<div class="form-main-section post-location" style="margin-bottom:15px !important;">
							<h4 class="text-uppercase border-bottom hide"><?php esc_html_e(' Location', 'classiera') ?> :</h4>
							<?php 
							$args = array(
								'post_type' => 'countries',
								'posts_per_page'   => -1,
								'orderby'          => 'title',
								'order'            => 'ASC',
								'post_status'      => 'publish',
								'suppress_filters' => false 
							);
							$country_posts = get_posts($args);
							if(!empty($country_posts)){
							?>
							<!--Select Country-->
							<div class="form-group" style="display:none;">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Select Country', 'classiera') ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <div class="inner-addon right-addon">
                                        <i class="form-icon right-form-icon fa fa-angle-down"></i>
                                        <select name="post_location" id="post_location" class="form-control form-control-md">
                                            <option value="-1" selected disabled><?php esc_html_e('Select Country', 'classiera'); ?></option>
                                            <?php 
											foreach( $country_posts as $country_post ){
												?>
												<option value="<?php echo $country_post->ID; ?>"><?php echo $country_post->post_title; ?></option>
												<?php
											}
											?>
                                        </select>
                                    </div>
                                </div>
                            </div>
							<?php } ?>
							<!--Select Country-->	
							<!--Select States-->
							<?php 
							$locationsStateOn = $redux_demo['location_states_on'];
							if($locationsStateOn == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Select State', 'classiera') ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <div class="inner-addon right-addon">
                                        <i class="form-icon right-form-icon fa fa-angle-down"></i>
										<select name="post_state" id="post_state" class="selectState form-control form-control-md" >
											<option value=""><?php esc_html_e('Select State', 'classiera'); ?></option>
										</select>
                                    </div>
                                </div>
                            </div>
							<?php } ?>
							<!--Select States-->
							<!--Select City-->
							<?php 
							$locationsCityOn= $redux_demo['location_city_on'];
							if($locationsCityOn == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Select City', 'classiera'); ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <div class="inner-addon right-addon">
                                        <i class="form-icon right-form-icon fa fa-angle-down"></i>
										<select name="post_city" id="post_city" class="selectCity form-control form-control-md" >
											<option value=""><?php esc_html_e('Select City', 'classiera'); ?></option>
										</select>
                                    </div>
                                </div>
                            </div>
							<?php } ?>
							<!--Select City-->
							<!--Address-->
							<?php if($classieraAddress == 1){?>
							<div class="form-group">
                                <label class="col-sm-3 text-left hide flip"><?php esc_html_e('Address', 'classiera'); ?> : <span>*</span></label>
                                <div class="col-sm-9 hide"></div>
                                    <input id="address" type="text" name="address" value="<?php echo $post_address; ?>" class="form-control form-control-md address_search" placeholder="<?php esc_html_e('Address or City', 'classiera') ?>" required>
                                
                            </div>
							<?php } ?>
							<!--Address-->
							<!--Google Value-->
							<div class="form-group">
								<?php 
									$googleFieldsOn = $redux_demo['google-lat-long']; 
									if($googleFieldsOn == 1){
								?>
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Set Latitude & Longitude', 'classiera') ?> : <span>*</span></label>
									<?php } ?>
                                <div class="row">
								<?php 
									$googleFieldsOn = $redux_demo['google-lat-long']; 
									if($googleFieldsOn == 1){
								?>
                                    <div class="form-inline row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                <input type="text" name="latitude" id="latitude" value="<?php echo $post_latitude; ?>" class="form-control form-control-md" placeholder="<?php esc_html_e('Latitude', 'classiera') ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                <input type="text" name="longitude" value="<?php echo $post_longitude; ?>" id="longitude" class="form-control form-control-md" placeholder="<?php esc_html_e('Longitude', 'classiera') ?>" required>
                                            </div>
                                        </div>
                                    </div>
									<?php }else{ ?>
										<input type="hidden" id="latitude" name="latitude" value="<?php echo $post_latitude; ?>">
										<input type="hidden" id="longitude" name="longitude" value="<?php echo $post_longitude; ?>">
									<?php } ?>
									<?php 
									
								$googleMapadPost = $redux_demo['google-map-adpost']; 
								if($googleMapadPost == 1){
								
								?>
								
                                    <div id="post-map" class="submitMAp">
									
                                        <?php /*<div id="map-canvas"></div>*/?>
										<div id="newmappost" style="width: 100%; height: 500px"> </div>
										<script>
     
     
									   var map, infoWindow;
									   var get_latitude = parseFloat('<?php echo $post_latitude; ?>');
									   var get_long = parseFloat('<?php echo $post_longitude; ?>');
								
									  function initMap() {
										map = new google.maps.Map(document.getElementById('newmappost'), {
											<?php if($classieraMAPStyle) { ?>
												styles: <?php echo $classieraMAPStyle; ?>,
											<?php } ?>
											zoom: 10,
										  	type:'roadmap',
											zoomControl: true,
							                zoomControlOptions: {
							                    style: google.maps.ZoomControlStyle.SMALL,
							                    position: google.maps.ControlPosition.LEFT_CENTER
							                }
										});

										var marker = new google.maps.Marker({
								          map: map,
								          position: {lat: get_latitude, lng: get_long},
								        });
										
										infoWindow = new google.maps.InfoWindow;
								
										// Try HTML5 geolocation.
										if (navigator.geolocation) {
										  navigator.geolocation.getCurrentPosition(function(position) {
											var pos = {
											  lat: get_latitude,
											  lng: get_long
											};
											document.getElementById("latitude").value =  get_latitude;
											document.getElementById("longitude").value = get_long;
											
											infoWindow.setPosition(pos);
											infoWindow.setContent('<?php echo $post_address; ?>');
											infoWindow.open(map);
											map.setCenter(pos);
										  }, function() {
											handleLocationError(true, infoWindow, map.getCenter());
										  });
										} else {
										  // Browser doesn't support Geolocation
										  handleLocationError(false, infoWindow, map.getCenter());
										}
									  
									  	var geocoder = new google.maps.Geocoder();
										google.maps.event.addListener(map, 'click', function(event) {
										  geocoder.geocode({
										    'latLng': event.latLng
										  }, function(results, status) {
										    if (status == google.maps.GeocoderStatus.OK) {
										      if (results[0]) {
										        //alert(results[0].formatted_address);
										        var latitude = event.latLng.lat();
											    var longitude = event.latLng.lng();
											    var uppos = {
												  lat: latitude,
												  lng: longitude
												};
											    //console.log( latitude + ', ' + longitude );
											    infoWindow.setPosition(uppos);
										        jQuery('#address').val(results[0].formatted_address);
										        infoWindow.setContent(results[0].formatted_address);
										      }
										    }
										  });
										});
										
										// Create the search box and link it to the UI element.
										var input = document.getElementById('address');
										var searchBox = new google.maps.places.SearchBox(input);
										map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
								
										// Bias the SearchBox results towards current map's viewport.
										map.addListener('bounds_changed', function() {
										  searchBox.setBounds(map.getBounds());
										});
								
										var markers = [];
										// Listen for the event fired when the user selects a prediction and retrieve
										// more details for that place.
										//To capture click event.
											 google.maps.event.addListener(map, 'click', function (e) {
											 	marker.setMap(null);
												document.getElementById("latitude").value = e.latLng.lat();
												document.getElementById("longitude").value = e.latLng.lng();
												placeMarker(e.latLng,map);
											 });
										
										searchBox.addListener('places_changed', function() {
										  var places = searchBox.getPlaces();
								
										  if (places.length == 0) {
											return;
										  }
								
										  // Clear out the old markers.
										  markers.forEach(function(marker) {
											marker.setMap(null);
										  });
										  markers = [];
								
										  // For each place, get the icon, name and location.
										  var bounds = new google.maps.LatLngBounds();
										  places.forEach(function(place) {
											if (!place.geometry) {
											  console.log("Returned place contains no geometry");
											  return;
											}
											
								
											if (place.geometry.viewport) {
											  // Only geocodes have viewport.
											  bounds.union(place.geometry.viewport);
											} else {
											  bounds.extend(place.geometry.location);
											}
										  });
										  map.fitBounds(bounds);
										});
									  }
								
									 var marker;
									function placeMarker(location,map) {
									  if ( marker ) {
										marker.setPosition(location);
									  } else {
										marker = new google.maps.Marker({
										  position: location,
										  map: map
										});
									  }
									}
									function handleLocationError(browserHasGeolocation, infoWindow, pos) {
										infoWindow.setPosition(pos);
										infoWindow.setContent(browserHasGeolocation ?
															  'Error: The Geolocation service failed.' :
															  'Error: Your browser doesn\'t support geolocation.');
										infoWindow.open(map);
									}
								  
									</script>
                                    </div>
								<?php } ?>
                                </div>
                            </div>
							<!--Google Value-->
							
						</div>
						<?php } ?>
						
						<style>
					 #jquery-script-menu {
					position: fixed;
					height: 90px;
					width: 100%;
					top: 0;
					left: 0;
					border-top: 5px solid #316594;
					background: #fff;
					-moz-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					-webkit-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					z-index: 999999;
					padding: 10px 0;
					-webkit-box-sizing:content-box;
					-moz-box-sizing:content-box;
					box-sizing:content-box;
					}
					
					.jquery-script-center {
					width: 960px;
					margin: 0 auto;
					}
					.jquery-script-center ul {
					width: 212px;
					float:left;
					line-height:45px;
					margin:0;
					padding:0;
					list-style:none;
					}
					.jquery-script-center a {
						text-decoration:none;
					}
					.jquery-script-ads {
					width: 728px;
					height:90px;
					float:right;
					}
					.jquery-script-clear {
					clear:both;
					height:0;
					}
					table tr{background:#ffffff !important;}
					 </style>
					 <?php //echo 'line 60'?>
					<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/bootstrap.min.css">
					<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
					 <?php //echo 'line 63';?>
					<link href="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/dist/bootstrap-FileUpload.css" rel="stylesheet" type="text/css">
					<div>
						
						<?php /*<div class="fileupload-wrapper"><div id="myUpload"></div></div>*/?>
					  </div>
					 
					 <?php /* <script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/jquery-1.12.4.min.js" ></script>*/?>
					  <script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/bootstrap.min.js"></script>
					<script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/dist/bootstrap-FileUpload.js"></script>
					<script>
					  jQuery("#myUpload").bootstrapFileUpload({
					  url: "<?php echo site_url();?>/submit-new-hyst/"
					});
					
					jQuery("#title").change(function() {
					  jQuery("#hdn_title").val(jQuery("#title").val());
					});
					jQuery("#description").change(function() {
					  jQuery("#hdn_description").val(jQuery("#description").val());
					});
					jQuery("#post_tags").change(function() {
					  jQuery("#hdn_post_tags1").val(jQuery("#post_tags").val());
					});
					jQuery("#post_price").change(function() {
					  jQuery("#hdn_post_price").val(jQuery("#post_price").val());
					});
					jQuery("#address").change(function() {
					  jQuery("#hdn_address").val(jQuery("#address").val());
					});
					jQuery("#latitude").change(function() {
					  jQuery("#hdn_latitude").val(jQuery("#latitude").val());
					});
					jQuery("#longitude").change(function() {
					  jQuery("#hdn_longitude").val(jQuery("#longitude").val());
					});
					</script>
						
						
						<!-- post location -->
						<!-- seller information without login-->
						<?php if( !is_user_logged_in()){?>
						<div class="form-main-section seller">
                            <h4 class="text-uppercase border-bottom"><?php esc_html_e('Seller Information', 'classiera') ?> :</h4>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Are', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <input id="individual" type="radio" name="seller" checked>
                                        <label for="individual"><?php esc_html_e('Individual', 'classiera') ?></label>
                                        <input id="dealer" type="radio" name="seller">
                                        <label for="dealer"><?php esc_html_e('Dealer', 'classiera') ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Name', 'classiera') ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" name="user_name" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter Your Name', 'classiera') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Email', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-6">
                                    <input type="email" name="user_email" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter your email', 'classiera') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Phone or Mobile No', 'classiera') ?> :<span>*</span></label>
                                <div class="col-sm-6">
                                    <input type="tel" name="user_phone" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter your Mobile or Phone number', 'classiera') ?>">
                                </div>
                            </div>
                        </div>
						<?php }?>
						<!-- seller information without login -->
						<!--Select Ads Type-->
						<?php 
						$totalAds = '';
						$usedAds = '';
						$availableADS = '';
						$planCount = 0;						
						$regular_ads = $redux_demo['regular-ads'];
						$classieraRegularAdsDays = $redux_demo['ad_expiry'];
						$current_user = wp_get_current_user();
						$userID = $current_user->ID;
						$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
						?>
						<div class="form-main-section post-type" style="display:none;">
                            <h4 class="text-uppercase border-bottom"><?php esc_html_e('Select HYST Post Type', 'classiera') ?> :</h4>
                            <p class="help-block"><?php esc_html_e('Select an Option to make your  featured or regular', 'classiera') ?></p>
                            <div class="form-group">
							<!--Regular Ad with plans-->
							<?php							
							if($postLimitOn == true && $countPosts >= $regularCount && $currentRole != "administrator"){
								if(!empty($result)){
									$count = 0;
									foreach( $result as $info ){
										$totalRegularAds = $info->regular_ads;
										$usedRegularAds = $info->regular_used;
										$regularID = $info->id;
										$availableRegularADS = $totalRegularAds-$usedRegularAds;
										$planName = $info->plan_name;
										if($availableRegularADS != 0){
											?>
											<div class="col-sm-4 col-md-3 col-lg-3">
												<div class="post-type-box">
													<h3 class="text-uppercase"><?php esc_html_e('Regular with ', 'classiera') ?><?php echo $planName; ?></h3>
													<p>
													<?php esc_html_e('Available Regular ads ', 'classiera') ?> :
													<?php echo $availableRegularADS; ?>
													</p>
													<p>
													<?php esc_html_e('Used Regular ads', 'classiera') ?> : 
													<?php echo $usedRegularAds; ?>
													</p>
													<div class="radio">
														<input id="regularPlan<?php echo $regularID; ?>" class="classieraGetID" type="radio" name="classiera_post_type" value="classiera_regular_with_plan" data-regular-id="<?php echo $info->id; ?>">
														<label for="regularPlan<?php echo $regularID; ?>"><?php esc_html_e('Select', 'classiera') ?></label>
													</div><!--radio-->
												</div><!--post-type-box-->
											</div><!--col-sm-4-->
											<?php
										}
									}
								}
							}else{
								if($regular_ads == 1){
								?>
								<!--Regular Ad-->
								<div class="col-sm-4 col-md-3 col-lg-3 active-post-type">
                                    <div class="post-type-box">
                                        <h3 class="text-uppercase"><?php esc_html_e('Regular', 'classiera') ?></h3>
                                        <p><?php esc_html_e('For', 'classiera') ?>&nbsp;<?php echo $classieraRegularAdsDays; ?>&nbsp;<?php esc_html_e('days', 'classiera') ?></p>
                                        <div class="radio">
                                            <input id="regular" type="radio" name="classiera_post_type" value="classiera_regular" checked>
                                            <label for="regular"><?php esc_html_e('Select', 'classiera') ?></label>
                                        </div>
										<input type="hidden" name="regular-ads-enable" id="regular-ads-enable" value=""  >
                                    </div>
                                </div>
								<!--Regular Ad-->
								<?php
								}
							}
							?>
							<!--Regular Ad with plans-->
							<?php
								if(!empty($result)){
									foreach ( $result as $info ) {
										//print_r($info);
										$premiumID = $info->id;
										$name = $info->plan_name;
										$totalAds = $info->ads;
										$usedAds = $info->used;
										if($totalAds == 'unlimited'){
											$name = esc_html__( 'Unlimited for Admin Only', 'classiera' );
											$availableADS = 'unlimited';
										}else{
											$availableADS = $totalAds-$usedAds;
										}
										
										if($availableADS != 0 || $totalAds == 'unlimited'){
										?>
											<div class="col-sm-4 col-md-3 col-lg-3">
												<div class="post-type-box">
													<h3 class="text-uppercase">
														<?php echo $name; ?>
													</h3>
													<p><?php esc_html_e('Total HYST Available', 'classiera') ?> : <?php echo $availableADS; ?></p>
													<p><?php esc_html_e('Used HYST with this Plan', 'classiera') ?> : <?php echo $usedAds; ?></p>
													<div class="radio">
														<input id="featured<?php echo $premiumID; ?>" type="radio" name="classiera_post_type" value="<?php echo $info->id; ?>">
														<label for="featured<?php echo $premiumID; ?>"><?php esc_html_e('Select', 'classiera') ?></label>
													</div>
												</div>
											</div>
										<?php
										}										
									}
								}
							?>	
								<!--Pay Per Post Per Category Base-->
								<div class="col-sm-4 col-md-3 col-lg-3 classieraPayPerPost">
									<div class="post-type-box">
										<h3 class="text-uppercase">
											<?php esc_html_e('Featured HYST', 'classiera') ?>
										</h3>	
										<p class="classieraPPP"></p>
										<div class="radio">
											<input id="payperpost" type="radio" name="classiera_post_type" value="payperpost">
											<label for="payperpost">
											<?php esc_html_e('select', 'classiera') ?>
											</label>
										</div>										
									</div>
								</div>
								<!--Pay Per Post Per Category Base-->
                            </div>
                        </div>
						<!--Select Ads Type-->
						<?php 
						$featured_plans = $redux_demo['featured_plans'];
						if(!empty($featured_plans)){
							if($featuredADS == "0" || empty($result)){
						?>
						<div class="row">
                            <div class="col-sm-9">
                                <div class="help-block terms-use">
                                    <?php esc_html_e('Currently you have no active plan for featured HYST. You must purchase a', 'classiera') ?> <strong><a href="<?php echo $featured_plans; ?>" target="_blank"><?php esc_html_e('Featured Pricing Plan', 'classiera') ?></a></strong> <?php esc_html_e('to be able to publish a Featured Ad.', 'classiera') ?>
                                </div>
                            </div>
                        </div>
						<?php }} ?>
					<div style="clear:left;"></div>
						<div id="" class="mydropzone classiera-image-upload clearfix" data-maxfile="<?php echo $imageLimit; ?>">
										<!--PreviousImages-->
										<?php require_once get_template_directory() . '/inc/BFI_Thumb.php'; ?>
										<?php
										$imageCount = 0;
										$params = array( 'width' => 110, 'height' => 70, 'crop' => true );
										$imgargs = array(
											'post_parent' => $current_post,
											'post_status' => 'inherit',
											'post_type'   => 'attachment', 
											'post_mime_type'   => 'image', 
											'order' => 'ASC',
											'orderby' => 'menu_order ID',
										);
										$attachments = get_children($imgargs);
										//echo "<pre>";print_r($attachments);
										if($attachments){
										foreach($attachments as $att_id => $attachment){
												$attachment_ID = $attachment->ID;
												$full_img_url = wp_get_attachment_url($attachment->ID);
												$split_pos = strpos($full_img_url, 'wp-content');
												$split_len = (strlen($full_img_url) - $split_pos);
												$abs_img_url = substr($full_img_url, $split_pos, $split_len);
												$full_info = @getimagesize(ABSPATH.$abs_img_url);

												$path_parts = pathinfo($full_img_url);
										?>
											<div id="<?php echo $attachment_ID; ?>" class="edit-post-image-block custom-edit-sec">
												<div class="remove-edit-post-image fileupload-previewrow thumb">
													<!-- <span class="removeImage" attids="<?php echo $attachment_ID; ?>"><?php esc_html_e('Remove', 'classiera');?></span> -->
													<div class="col-lg-1"><img class="edit-post-image" src="<?php echo bfi_thumb( "$full_img_url", $params ) ?>" />
													</div>
													<div class="col-lg-4"><?php echo $path_parts['basename']; ?>
														
													</div>
													<div class="col-lg-5"><div class="progress-bar"></div></div>
													<div class="col-lg-2">
														<button type="button" class="removeImage btn btn-danger fileupload-remove" attids="<?php echo $attachment_ID; ?>" value="<?php echo $path_parts['filename']; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/remove_file.png' ?>" height="40px" width="40px"/>
															<span>Remove File</span>
														</button>
													</div>
													<!-- <input type="hidden" name="attids" class="attids" value="<?php echo $attachment_ID; ?>"> -->
												</div><!--remove-edit-post-image-->
											</div>
											<?php $imageCount++;?>
										<?php }?>
										<?php }?></div>
							<style>
					 #jquery-script-menu {
					position: fixed;
					height: 90px;
					width: 100%;
					top: 0;
					left: 0;
					border-top: 5px solid #316594;
					background: #fff;
					-moz-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					-webkit-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					z-index: 999999;
					padding: 10px 0;
					-webkit-box-sizing:content-box;
					-moz-box-sizing:content-box;
					box-sizing:content-box;
					}
					
					.jquery-script-center {
					width: 960px;
					margin: 0 auto;
					}
					.jquery-script-center ul {
					width: 212px;
					float:left;
					line-height:45px;
					margin:0;
					padding:0;
					list-style:none;
					}
					.jquery-script-center a {
						text-decoration:none;
					}
					.jquery-script-ads {
					width: 728px;
					height:90px;
					float:right;
					}
					.jquery-script-clear {
					clear:both;
					height:0;
					}
					table tr{background:#ffffff !important;}
					 </style>

					<div class="btn btn-success fileupload-add newuploads"><input name="uploadattachment[]" type="file" id="files" multiple="multiple"><img src="https://hyst2.temp.co.za/wp-content/themes/classiera-child/images/file.png" height="40px" width="40px">&nbsp;Add Files…</div>

					<script type="text/javascript">
						  if (window.File && window.FileList && window.FileReader) {
						    jQuery("#files").on("change", function(e) {
						      var files = e.target.files,
						        filesLength = files.length;
						      for (var i = 0; i < filesLength; i++) {
						        var f = files[i];
						        var fileReader = new FileReader();
						        fileReader.onload = (function(e) {
						          var file = e.target;
						          jQuery("<div class=\"edit-post-image-block custom-edit-sec onedit-imageview\"><div class=\"remove-edit-post-image fileupload-previewrow thumb\">" + "<div class=\"col-lg-1\"><img class=\"edit-post-image imageThumb\" src=\"" + e.target.result + "\" title=\"" + f.name + "\"/></div>" 
						          	+ "<div class=\"col-lg-4\">"+ f.name +"</div>"
						          	+ "<div class=\"col-lg-5\"><div class=\"progress-bar\"></div></div>"
						          	+ "<div class=\"col-lg-2\"><a class=\"btn btn-danger fileupload-remove\" href=\"javascript:void(0)\"><img src=\"<?=get_template_directory_uri().'/../classiera-child/images/remove_file.png' ?>\" height=\"40px\" width=\"40px\"/>" 
						          	+ "<span class=\"remove\">Remove File</span></a></div>" 
						          	+ "</div></div>").insertAfter(".mydropzone");
						          jQuery(".remove").click(function(){
						            jQuery(this).parent().parent().parent().parent(".custom-edit-sec").remove();
						          });
						          
						          // Old code here
						          /*$("<img></img>", {
						            class: "imageThumb",
						            src: e.target.result,
						            title: file.name + " | Click to remove"
						          }).insertAfter("#files").click(function(){$(this).remove();});*/
						          
						        });
						        fileReader.readAsDataURL(f);
						        //alert(f.name);
						      }
						    });
						  } else {
						    alert("Your browser doesn't support to File API")
						  }
					</script>	
						
					</div></div>
					<div class="form-main-section" style="margin-top:20px;">
                            <div class="col-sm-12 buttons">
									<input type="hidden" class="regular_plan_id" name="regular_plan_id" value="">
									<?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>
									<input type="hidden" name="submitted" id="submitted" value="true">
									<button class="post-submit" type="submit" name="updatepost" value="<?php esc_html_e('Update Ad', 'classiera') ?>" style="width:20%; margin:auto;"><img src="<?=get_template_directory_uri().'/../classiera-child/images/send.png' ?>" height="40px" width="40px"/><?php esc_html_e('Update HYST', 'classiera') ?></button>									 
							 </div>
                        </div>
						<div class="row">
                            <div class="col-sm-12">
                                <div class="help-block terms-use">
                                    <?php esc_html_e('By clicking "Update ", you agree to our', 'classiera') ?> <a href="<?php echo $termsandcondition; ?>"><?php esc_html_e('Terms of Use', 'classiera') ?></a> <?php esc_html_e('', 'classiera') ?>
                                </div>
                            </div>
                        </div>
					</form>
				</div><!--submit-post-->
				<?php } ?>
			</div><!--col-lg-9 col-md-8 user-content-heigh-->
		</div><!--row-->
	</div><!--container-->
</section><!--user-pages-->
<?php endwhile; ?>

<?php get_footer(); ?>
<script type="text/javascript">
	jQuery('.removeImage').click(function() {
	var idd=jQuery(this).parents(".custom-edit-sec");
	var currrentpost = '<?php echo $current_post ?>';
	var attids = jQuery(this).attr('attids');

	jQuery.ajax({
	    type        : 'POST',
	    url         : '<?php echo admin_url('admin-ajax.php'); ?>',
	    data        : { action : 'custom_delete_post_attachment', postID: currrentpost, att_ids: attids },
	    success     : function(response) {
	    	    idd.remove();
	        }
	    });  
	});
	
	jQuery(document).ready(function(){
		jQuery(document).on("click","span.ui-icon-close", function() { 
			var idd=jQuery(this).parents('div.ui-autocomplete-multiselect-item');
			var tagnm = jQuery(this).attr('tagnm');

			var myAuto = jQuery('#hdn_post_tags').val();
			
			jQuery("#hdn_post_tags").val(myAuto.replace(tagnm+',', ""));

			idd.remove();

	    });
	});
</script>