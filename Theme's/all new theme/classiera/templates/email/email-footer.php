		<?php 
			global $redux_demo;
			$classieraLogo = $redux_demo['classiera_email_logo']['url'];
			$classieraCopyRight = $redux_demo['footer_copyright'];
			$classieraFacebook = $redux_demo['facebook-link'];
			$classieraTwitter = $redux_demo['twitter-link'];
			$classieraDribbble = $redux_demo['dribbble-link'];
			$classieraFlickr = $redux_demo['flickr-link'];
			$classieraGithub = $redux_demo['github-link'];
			$classieraPinterest = $redux_demo['pinterest-link'];	
			$classieraYouTube = $redux_demo['youtube-link'];
			$classieraGoogle = $redux_demo['google-plus-link'];
			$classieraLinkedin = $redux_demo['linkedin-link'];
			$classieraInstagram = $redux_demo['instagram-link'];
			$classieraVimeo = $redux_demo['vimeo-link'];
		?>
			<?php /*?><div class="classiera-email-footer" style="padding: 50px 0; background: #232323; text-align: center;">
				<img style="margin-bottom: 40px;" src="<?php echo $classieraLogo; ?>" alt="<?php bloginfo( 'name' ); ?>">
				<div style="text-align: center;">
					<?php if(!empty($classieraFacebook)){?>
					<?php $facebookIMG = get_template_directory_uri() . '/images/social/facebook.png'; ?>
					<a class="classiera-email-social-icon" href="<?php echo $classieraFacebook; ?>" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="<?php echo $facebookIMG; ?>" style="width:14px;" alt="facebook">
					</a>
					<?php } ?>
					<?php if(!empty($classieraTwitter)){?>
					<?php $twiiterIMG = get_template_directory_uri() . '/images/social/twiiter.png'; ?>
					<a class="classiera-email-social-icon" href="<?php echo $classieraTwitter; ?>" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="<?php echo $twiiterIMG; ?>" style="width:14px;" alt="twitter">
					</a>
					<?php } ?>
					<?php if(!empty($classieraGoogle)){?>
					<?php $googleIMG = get_template_directory_uri() . '/images/social/google-plus.png'; ?>
					<a class="classiera-email-social-icon" href="<?php echo $classieraGoogle; ?>" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="<?php echo $googleIMG; ?>" style="width:14px;" alt="google">
					</a>
					<?php } ?>
					<?php if(!empty($classieraPinterest)){?>
					<?php $pinterestIMG = get_template_directory_uri() . '/images/social/pinterest.png'; ?>
					<a class="classiera-email-social-icon" href="<?php echo $classieraPinterest; ?>" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="<?php echo $pinterestIMG; ?>" style="width:14px;" alt="pinterest">
					</a>
					<?php } ?>
					<?php if(!empty($classieraInstagram)){?>
					<?php $instagramIMG = get_template_directory_uri() . '/images/social/instagram.png'; ?>
					<a class="classiera-email-social-icon" href="<?php echo $classieraInstagram; ?>" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="<?php echo $instagramIMG; ?>" style="width:14px;" alt="instagram">
					</a>
					<?php } ?>
					<?php if(!empty($classieraLinkedin)){?>
					<?php $linkedinIMG = get_template_directory_uri() . '/images/social/linkedin.png'; ?>
					<a class="classiera-email-social-icon" href="<?php echo $classieraLinkedin; ?>" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="<?php echo $linkedinIMG; ?>" style="width:14px;" alt="linkedin">
					</a>
					<?php } ?>
				</div>
			</div><?php */
			$email_template=$wpdb->get_results("select email_content from wp_email_template where id='12'");
			$disp_email_template=$email_template[0]->email_content;
			
			$pos = strpos($disp_email_template, '{social_links}');

			if ($pos === false) {
			
			}
			else
			{
				$customstring='<div class="classiera-email-footer" style="padding: 50px 0; background: #232323; text-align: center;">
				<!--<img style="margin-bottom: 40px;" src="'.$classieraLogo.'" alt="'.bloginfo( 'name' ).'">-->
				<div style="text-align: center;">';
					if(!empty($classieraFacebook)){
					$facebookIMG = get_template_directory_uri() . '/images/social/facebook.png';
					$customstring.='<a class="classiera-email-social-icon" href="'.$classieraFacebook.'" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="'.$facebookIMG.'" style="width:14px;" alt="facebook">
					</a>';
					}
					if(!empty($classieraTwitter)){
					$twiiterIMG = get_template_directory_uri() . '/images/social/twiiter.png';
					$customstring.='<a class="classiera-email-social-icon" href="'.$classieraTwitter.'" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="'.$twiiterIMG.'" style="width:14px;" alt="twitter">
					</a>';
					}
					if(!empty($classieraGoogle)){
					$googleIMG = get_template_directory_uri() . '/images/social/google-plus.png';
					$customstring.='<a class="classiera-email-social-icon" href="'.$classieraGoogle.'" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="'.$googleIMG.'" style="width:14px;" alt="google">
					</a>';
					}
					if(!empty($classieraPinterest)){
					$pinterestIMG = get_template_directory_uri() . '/images/social/pinterest.png';
					$customstring.='<a class="classiera-email-social-icon" href="'.$classieraPinterest.'" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="'.$pinterestIMG.'" style="width:14px;" alt="pinterest">
					</a>';
					}
					if(!empty($classieraInstagram)){
					$instagramIMG = get_template_directory_uri() . '/images/social/instagram.png';
					$customstring.='<a class="classiera-email-social-icon" href="'.$classieraInstagram.'" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;">
						<img src="'.$instagramIMG.'" style="width:14px;" alt="instagram">
					</a>';
					}
					if(!empty($classieraLinkedin)){
					$linkedinIMG = get_template_directory_uri() . '/images/social/linkedin.png';
					$customstring.='<a class="classiera-email-social-icon" href="'.$classieraLinkedin.'" style="background: #444444; height: 40px; width: 40px; text-align: center; display: inline-block; line-height:40px;"><img src="'.$linkedinIMG.'" style="width:14px;" alt="linkedin"></a>';
					}
				$customstring.='</div>
			</div>';
			
			$disp_email_template=str_replace('{classieraLogo}',$classieraLogo,$disp_email_template);
			$disp_email_template=str_replace('{social_links}',$customstring,$disp_email_template);
			}
			$disp_email_template=str_replace('{blogname}',bloginfo( 'name' ),$disp_email_template);
			echo $disp_email_template;?><!--classiera-email-footer-->
			<div class="cassiera-footer-bottom" style="padding: 10px 0; text-align: center; background: #303030;">
				<p style="font-family: 'Lato', sans-serif; font-size:14px; color: #8e8e8e">
				<?php echo $classieraCopyRight; ?>
				</p>
			</div><!--cassiera-footer-bottom-->
		</div>
	</body>
</html>