<!-- post title-->



<?php 
global $redux_demo;
$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
global $post;
$postID = '';
$current_user = wp_get_current_user();
$edit_post_page_id = $redux_demo['edit_post'];
$postID = $post->ID;
global $wp_rewrite;
if ($wp_rewrite->permalink_structure == ''){
	$edit_post = $edit_post_page_id."&post=".$postID;
}else{
	$edit_post = $edit_post_page_id."?post=".$postID;
}
/*PostMultiCurrencycheck*/
$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
if(!empty($post_currency_tag)){
	$classieraCurrencyTag = classiera_Display_currency_sign($post_currency_tag);
}else{
	global $redux_demo;
	$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
}
/*PostMultiCurrencycheck*/
?>
<div class="single-post-title">
	<div class="post-price hide">
		<?php $post_price = get_post_meta($post->ID, 'post_price', true);  ?>
		<h4>
			<?php 
			if(is_numeric($post_price)){
				echo classiera_post_price_display($post_currency_tag, $post_price);
			}else{ 
				echo $post_price; 
			}
			?>
		</h4>
	</div>
	<h4 class="text-uppercase">
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		<?php 
		if($post->post_author == $current_user->ID && get_post_status ( $post->ID ) == 'publish'){		
		
			
				$user = wp_get_current_user();
				$roles = $user->roles;
				if(in_array('inventory_user',$roles))
				{?>
					
					<a href="<?php echo site_url().'/edit-inventory/?post='.$postID; ?>" class="edit-post btn btn-sm btn-default">
						<i class="fa fa-pencil-square-o pull-left"></i>
						<span class="xs-hide"><?php esc_html_e( 'Edit INVENTORY', 'classiera' ); ?></span>
					</a><?php
				}
				else if(in_array('company_user',$roles))
				{?>
					
					<a href="<?php echo site_url().'/edit-inventory/?post='.$postID; ?>" class="edit-post btn btn-sm btn-default">
					<i class="fa fa-pencil-square-o pull-left"></i>
					<span class="xs-hide"><?php esc_html_e( 'Edit INVENTORY', 'classiera' ); ?></span>
				</a><?php
				}
				else
				{?>
					
					<a href="<?php echo $edit_post; ?>" class="edit-post btn btn-sm btn-default">
						<i class="fa fa-pencil-square-o pull-left"></i>
						<span class="xs-hide"><?php esc_html_e( 'Edit HYST', 'classiera' ); ?></span>
					</a><?php
				} ?>
			
			
			<?php
		}elseif( current_user_can('administrator')){
			?>
			<a href="<?php echo $edit_post; ?>" class="edit-post btn btn-sm btn-default">
				<i class="fa fa-pencil-square-o pull-left"></i>
				<span class="xs-hide"><?php esc_html_e( 'Edit HYST', 'classiera' ); ?></span>
			</a>
			<?php
		}
		?>		
		<!--Edit Ads Button-->
	</h4>
	<div class="col-sm-6 rtl">	
		<div class="post-category">
		<?php 
			$category = get_the_category();
		?>
			<p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-1.png'; ?>"/></p>
			
			<p><span>Category :  <?php classiera_Display_cat_level($post->ID); ?>
			</span></p>
		</div>
	</div>
	
	<div class="col-sm-6">	
		<div class="post-category">
		    <?php 
				//$locShownBy = $redux_demo['location-shown-by'];
				//$post_location = get_post_meta($post->ID, $locShownBy, true);
				$post_location = get_post_meta($post->ID, 'post_location', true);
			?>
			<p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-2.png'; ?>"/></p>
			
			<p><a href="#"><span>location : <?php echo $post_location; ?></span></a></p>
		</div>
	</div>

	<div class="col-sm-6 rtl">	
		<div class="post-category">
		    <?php 
			    $user_ID = $post->post_author;
				$authorName = get_the_author_meta('display_name', $user_ID );
				if(empty($authorName)){
					$authorName = get_the_author_meta('user_nicename', $user_ID );
				}
				if(empty($authorName)){
					$authorName = get_the_author_meta('user_login', $user_ID );
				}
			?>
			<?php if ( is_user_logged_in() ) { ?>
			<a href="<?php echo get_author_posts_url($user_ID); ?>"><p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-3.png'; ?>"/></p></a>
			<?php } 
			else { ?>
				<p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-3.png'; ?>"/></p></a>
			<?php } ?>
			<p>
			<?php
			if ( is_user_logged_in() ) {
			?><a href="<?php echo get_author_posts_url($user_ID); ?>"><span>User : <?php echo $authorName; ?></span></a><?php
			}
			else
			{
			?><span>User : <?php echo $authorName; ?></span><?php
			}?>
			</p>
		</div>
	</div>
	
	<div class="col-sm-6">	
		<div class="post-category">
			<p><img class="icon-single-page-top" src="<?=get_template_directory_uri().'/../classiera-child/images/s-4.png'; ?>"/></p>
			
			<p><a href="#"><span>User Price : <b><?php 
				if(is_numeric($post_price)){
					echo classiera_post_price_display($post_currency_tag, $post_price);
				}else{ 
					echo $post_price; 
				}
			?></b></span></a></p>
		</div>
	</div>
	
	
	
	
</div>
<div class="clearfix"></div>
<div class="top-buffer2"></div>

<div class="single_description">
	<div class="single_title">
		 <h2>description</h2>
	</div>
	<div class="top-buffer2"></div>
    <div class="inner-addon flex">
    	<p class="form-control form-control-sm text" id="description"><?php echo $post->post_content;?></p>
	</div>	
</div>
<div class="top-buffer2"></div>

<div class="image_section">
	<div class="single_title">
		 <h2>images</h2>
	</div>




<!-- post title-->
<!-- single post carousel-->
<?php 
		$attachments = get_children(array('post_parent' => $post->ID,
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'order' => 'ASC',
			'orderby' => 'menu_order ID'
			)
		);
?>
<?php if ( has_post_thumbnail() || !empty($attachments)){
		if(!empty($attachments) && count($attachments)>1){?>
			<div class="wpb_wrapper">
						<script>
					$(function() {
					$(document).ready(function(){
						$("#magic_carousel_1").magic_carousel({skin:"white",
							responsive:true,
							responsiveRelativeToBrowser:false,				
							/*width:960,
							height:414,*/
							width100Proc:true,
							height100Proc:false,				
							//autoPlay:5,
							autoPlay:false,
							numberOfVisibleItems:3,
							verticalAdjustment:50,
							elementsHorizontalSpacing:120,
							elementsVerticalSpacing:20,
							animationTime:0.8,
							easing:"easeOutQuad",
							resizeImages:true,
							showElementTitle:true,
							titleColor:"#000000",
							//imageWidth:452,
							//imageWidth:'auto',
							//imageHeight:302,
							//imageHeight:302,
							border:0,
							borderColorOFF:"transparent",
							borderColorON:"#FF0000",				
							enableTouchScreen:true,
							target:"_blank",				
							absUrl:"https://hyst2.temp.co.za/wp-content/plugins/magic_carousel/perspective/",
							showAllControllers:true,
							showNavArrows:true,
							showOnInitNavArrows:true,
							autoHideNavArrows:true,
							showBottomNav:false,
							showOnInitBottomNav:true,
							autoHideBottomNav:true,
							showPreviewThumbs:true,
							nextPrevMarginTop:5,
							playMovieMarginTop:0,
							bottomNavMarginBottom:-8,				
							circleLeftPositionCorrection:3,
							circleTopPositionCorrection:3,
							showCircleTimer:true,
							circleRadius:10,
							circleLineWidth:4,
							circleColor:"#ff0000",
							circleAlpha:100,
							behindCircleColor:"#000000",
							behindCircleAlpha:50});	
						});	
						$(document).ready(function(){
							$("a[rel^='prettyPhoto']").prettyPhoto({
								default_width: $(window).width()/2,
								default_height: $(window).width()/2*9/16,
								social_tools:false,
								callback: function(){
									$.magic_carousel.continueAutoplay();
								}
							});
						});			
					});
				</script>
			<link rel='stylesheet' id='magic_carousel_css-css'  href='<?php echo site_url();?>/wp-content/plugins/magic_carousel/perspective/css/magic_carousel.css?ver=4.9.6' type='text/css' media='all' />
			<link rel='stylesheet' id='lbg_prettyPhoto_css-css'  href='<?php echo site_url();?>/wp-content/plugins/magic_carousel/perspective/css/prettyPhoto.css?ver=4.9.6' type='text/css' media='all' />
			<link rel='stylesheet' id='magic_slider_site_css-css'  href='<?php echo site_url();?>/wp-content/plugins/magic_slider/magic_slider/magic_slider.css?ver=4.9.6' type='text/css' media='all' />
			<link rel='stylesheet' id='magic_slider_text_classes-css'  href='<?php echo site_url();?>/wp-content/plugins/magic_slider/magic_slider/text_classes.css?ver=4.9.6' type='text/css' media='all' />

			<script type='text/javascript' src='<?php echo site_url();?>/wp-content/plugins/magic_carousel/perspective/js/jquery.touchSwipe.min.js?ver=4.9.6'></script>
			<script type='text/javascript' src='<?php echo site_url();?>/wp-content/plugins/magic_carousel/perspective/js/magic_carousel.js?ver=4.9.6'></script>
			<script type='text/javascript' src='<?php echo site_url();?>/wp-content/plugins/magic_carousel/perspective/js/jquery.prettyPhoto.js?ver=4.9.6'></script>
			 		 <div id="magic_carousel_1">
					 	<div class="myloader"></div>
						<ul class="magic_carousel_list">
							<?php /*<li data-bottom-thumb="" data-title="" data-link="" data-target="" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
								<img src="https://hyst2.temp.co.za/wp-content/uploads/2018/05/63d060fe727b45379bb07ca62f04fa0a.jpg" width="615" height="300" alt=""  title="" />
							</li>*/?>
							<?php /*<li data-bottom-thumb="" data-title="" data-link="" data-target="" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
								<img src="https://hyst2.temp.co.za/wp-content/uploads/2018/05/70028-doughnuts.jpg" width="900" height="651" alt=""  title="" />
							</li>
							<li data-bottom-thumb="" data-title="test 1" data-link="" data-target="_blank" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
								<img src="https://hyst2.temp.co.za/wp-content/uploads/2018/06/Vintage-Mini-Sexy-Club-Dresses-Clothing-The-Slav-Epic-Graphic-Vest-Tank-Vestidos-Digital-Print-Summer.jpg_640x640.jpg" width="640" height="640" alt="test 1"  title="test 1" />
							</li>*/
							
						if(empty($attachments)){
							if ( has_post_thumbnail()){
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
								?>
								<li data-bottom-thumb="" data-title="" data-link="" data-target="" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
									<img src="<?php echo $image[0]; ?>"  alt="<?php the_title(); ?>"  title="<?php the_title(); ?>" />
								</li>
							<?php
							}else{
								$image = get_template_directory_uri().'/images/nothumb.png';
								?>
								
								<li data-bottom-thumb="" data-title="" data-link="" data-target="" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
									<img src="<?php echo $image[0]; ?>"  alt="<?php the_title(); ?>"  title="<?php the_title(); ?>" />
								</li>
								<?php
							}
						}else{
							$count = 1;
							foreach($attachments as $att_id => $attachment){
								$full_img_url = wp_get_attachment_url($attachment->ID);
								?>
							   
								<li data-bottom-thumb="" data-title="" data-link="" data-target="" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
									<img src="<?php echo $full_img_url; ?>"  alt="<?php the_title(); ?>"  title="<?php the_title(); ?>" />
								</li>
							
							<?php
							$count++;
							}
						}
						
						if(empty($attachments)){
							if ( has_post_thumbnail()){
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
								?>
							
							<li data-bottom-thumb="" data-title="" data-link="" data-target="" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
									<img src="<?php echo $image[0]; ?>"  alt="<?php the_title(); ?>"  title="<?php the_title(); ?>" />
								</li>
							<?php
							}else{
								$image = get_template_directory_uri().'/images/nothumb.png';
								?>
								
								<li data-bottom-thumb="" data-title="" data-link="" data-target="" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
									<img src="<?php echo $image; ?>"  alt="<?php the_title(); ?>"  title="<?php the_title(); ?>" />
								</li>
								<?php
							}
						}else{
							$count = 1;
							foreach($attachments as $att_id => $attachment){
								$full_img_url = wp_get_attachment_url($attachment->ID);
								?>
							
							<li data-bottom-thumb="" data-title="" data-link="" data-target="" data-large-image="" data-video-vimeo="" data-video-youtube="" data-audio="" data-video-selfhosted="" >
									<img src="<?php echo $full_img_url; ?>"  alt="<?php the_title(); ?>"  title="<?php the_title(); ?>" />
								</li>
							<?php
							$count++;
							}
						}
						?>
						</ul>
					</div>
				<?php
					}
					else
					{?>
						<div class="carousel"> <!-- BEGIN CONTAINER -->
			    <div class="slides"> <!-- BEGIN CAROUSEL -->
				
					<?php 
						if(empty($attachments)){
							if ( has_post_thumbnail()){
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
								?>
							
							<div class="slideItem"> <!-- SLIDE ITEM -->
								<a href="#">
									<img class="img-responsive" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
								</a>           
							</div>
							
							<?php
							}else{
								$image = get_template_directory_uri().'/images/nothumb.png';
								?>
								<div class="slideItem"> <!-- SLIDE ITEM -->
									<a href="#">
										<img class="img-responsive" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
									</a>           
								</div>
								<?php
							}
						}else{
							$count = 1;
							foreach($attachments as $att_id => $attachment){
								$full_img_url = wp_get_attachment_url($attachment->ID);
								?>
							    <div class="slideItem"> <!-- SLIDE ITEM -->
									<a href="#">
										<img class="img-responsive" src="<?php echo $full_img_url; ?>" alt="<?php the_title(); ?>">
									</a>           
								</div>
							
							<?php
							$count++;
							}
						}
						?>
					
					
			    </div>
			</div>      
			</div>


<div id="single-post-carousel" class="carousel slide hide single-carousel" data-ride="carousel" data-interval="3000">
	
	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
	<?php 
	if(empty($attachments)){
		if ( has_post_thumbnail()){
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
			?>
		<div class="item active">
			<img class="img-responsive" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
		</div>
		<?php
		}else{
			$image = get_template_directory_uri().'/images/nothumb.png';
			?>
			<div class="item active">
				<img class="img-responsive" src="<?php echo $image; ?>" alt="<?php the_title(); ?>">
			</div>
			<?php
		}
	}else{
		$count = 1;
		foreach($attachments as $att_id => $attachment){
			$full_img_url = wp_get_attachment_url($attachment->ID);
			?>
		<div class="item <?php if($count == 1){ echo "active"; }?>">
			<img class="img-responsive" src="<?php echo $full_img_url; ?>" alt="<?php the_title(); ?>">
		</div>
		<?php
		$count++;
		}
	}
	?>
	</div>
	<!-- slides number -->
	<div class="num">
		<i class="fa fa-camera"></i>
		<span class="init-num"><?php esc_html_e('1', 'classiera') ?></span>
		<span><?php esc_html_e('of', 'classiera') ?></span>
		<span class="total-num"></span>
	</div>
	<!-- Left and right controls -->
	<div class="single-post-carousel-controls">
		<a class="left carousel-control" href="#single-post-carousel" role="button" data-slide="prev">
			<span class="fa fa-chevron-left" aria-hidden="true"></span>
			<span class="sr-only"><?php esc_html_e('Previous', 'classiera') ?></span>
		</a>
		<a class="right carousel-control" href="#single-post-carousel" role="button" data-slide="next">
			<span class="fa fa-chevron-right" aria-hidden="true"></span>
			<span class="sr-only"><?php esc_html_e('Next', 'classiera') ?></span>
		</a>
	</div>
	<!-- Left and right controls -->
</div><?php
		}
	} ?>
<!-- single post carousel-->