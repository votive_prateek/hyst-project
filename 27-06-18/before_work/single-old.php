<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage classiera
 * @since classiera 1.0
 */
get_header();
 ?>

	
	<?php while ( have_posts() ) : the_post(); ?>


<?php 

global $redux_demo; 
global $current_user; wp_get_current_user(); $user_ID == $current_user->ID;

$profileLink = get_the_author_meta( 'user_url', $user_ID );
$contact_email = get_the_author_meta('user_email');
$classieraContactEmailError = $redux_demo['contact-email-error'];
$classieraContactNameError = $redux_demo['contact-name-error'];
$classieraConMsgError = $redux_demo['contact-message-error'];
$classieraContactThankyou = $redux_demo['contact-thankyou-message'];
$classieraRelatedCount = $redux_demo['classiera_related_ads_count'];
$classieraSearchStyle = $redux_demo['classiera_search_style'];
$classieraSingleAdStyle = $redux_demo['classiera_single_ad_style'];
$classieraPartnersStyle = $redux_demo['classiera_partners_style'];
$classieraComments = $redux_demo['classiera_sing_post_comments'];
$googleMapadPost = $redux_demo['google-map-adpost'];
$classieraToAuthor = $redux_demo['author-msg-box-off'];
$classieraReportAd = $redux_demo['classiera_report_ad'];
$locShownBy = $redux_demo['location-shown-by'];
$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
$classieraAuthorInfo = $redux_demo['classiera_author_contact_info'];
$classieraPriceSection = $redux_demo['classiera_sale_price_off'];
$classiera_bid_system = $redux_demo['classiera_bid_system'];
$category_icon_code = "";
$category_icon_color = "";
$your_image_url = "";

global $errorMessage;
global $emailError;
global $commentError;
global $subjectError;
global $humanTestError;
global $hasError;

//If the form is submitted
if(isset($_POST['submit'])) {
	if($_POST['submit'] == 'send_message'){
		//echo "send_message";
		//Check to make sure that the name field is not empty
		if(trim($_POST['contactName']) === '') {
			$errorMessage = $classieraContactNameError;
			$hasError = true;
		} elseif(trim($_POST['contactName']) === 'Name*') {
			$errorMessage = $classieraContactNameError;
			$hasError = true;
		}	else {
			$name = trim($_POST['contactName']);
		}

		//Check to make sure that the subject field is not empty
		if(trim($_POST['subject']) === '') {
			$errorMessage = $classiera_contact_subject_error;
			$hasError = true;
		} elseif(trim($_POST['subject']) === 'Subject*') {
			$errorMessage = $classiera_contact_subject_error;
			$hasError = true;
		}	else {
			$subject = trim($_POST['subject']);
		}
		
		//Check to make sure sure that a valid email address is submitted
		if(trim($_POST['email']) === ''){
			$errorMessage = $classieraContactEmailError;
			$hasError = true;		
		}else{
			$email = trim($_POST['email']);
		}
			
		//Check to make sure comments were entered	
		if(trim($_POST['comments']) === '') {
			$errorMessage = $classieraConMsgError;
			$hasError = true;
		} else {
			if(function_exists('stripslashes')) {
				$comments = stripslashes(trim($_POST['comments']));
			} else {
				$comments = trim($_POST['comments']);
			}
		}

		//Check to make sure that the human test field is not empty
		$classieraCheckAnswer = $_POST['humanAnswer'];
		if(trim($_POST['humanTest']) != $classieraCheckAnswer) {
			$errorMessage = esc_html__('Not Human', 'classiera');			
			$hasError = true;
		}
		$classieraPostTitle = $_POST['classiera_post_title'];	
		$classieraPostURL = $_POST['classiera_post_url'];
		
		//If there is no error, send the email		
		if(!isset($hasError)) {

			$emailTo = $contact_email;
			$subject = $subject;	
			$body = "Name: $name \n\nEmail: $email \n\nMessage: $comments";
			$headers = 'From <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;
			
			//wp_mail($emailTo, $subject, $body, $headers);
			contactToAuthor($emailTo, $subject, $name, $email, $comments, $headers, $classieraPostTitle, $classieraPostURL);
			$emailSent = true;			

		}
	}
	if($_POST['submit'] == 'report_to_admin'){		
		$displayMessage = '';
		$report_ad = $_POST['report_ad_val'];
		if($report_ad == "illegal") {
			$message = esc_html__('This is illegal/fraudulent Ads, please take action.', 'classiera');
		}
		if($report_ad == "spam") {
			$message = esc_html__('This Ad is SPAM, please take action', 'classiera');			
		}
		if($report_ad == "duplicate") {
			$message = esc_html__('This ad is a duplicate, please take action', 'classiera');			
		}
		if($report_ad == "wrong_category") {
			$message = esc_html__('This ad is in the wrong category, please take action', 'classiera');			
		}
		if($report_ad == "post_rules") {
			$message = esc_html__('The ad goes against posting rules, please take action', 'classiera');			
		}
		if($report_ad == "post_other") {
			$message = $_POST['other_report'];				
		}		
		$classieraPostTitle = $_POST['classiera_post_title'];	
		$classieraPostURL = $_POST['classiera_post_url'];
		//print_r($message); exit();
		classiera_reportAdtoAdmin($message, $classieraPostTitle, $classieraPostURL);
		if(!empty($message)){
			$displayMessage = esc_html__('Thanks for report, Our Team will take action ASAP.', 'classiera');
		}
	}
	
}

if(isset($_POST['btn_submit']) && $_POST['btn_submit']!='')
{
	
	/*if( ! get_post_meta( $_POST['hyst_id'], 'found', true ) ) {
		$data = array(
		array(
			'user_id' => $_POST['user_id'],
			'found_location' => $_POST['found_location'],
			'found_price' => $_POST['found_price'],
			'confirm' => '0',
			'found_id' => time()
		
		));
		add_post_meta($_POST['hyst_id'], 'found',serialize($data));
		update_post_meta($_POST['hyst_id'], 'is_found','1');
	}
	else
	{
		 $get_found=unserialize(get_post_meta( $_POST['hyst_id'], 'found', true ));
		 $get_found[]=array(
			'user_id' => $_POST['user_id'],
			'found_location' => $_POST['found_location'],
			'found_price' => $_POST['found_price'],
			'confirm' => '0',
			'found_id' => time()
		);
		//array_push($get_found,$array);
		update_post_meta($_POST['hyst_id'], 'found', serialize($get_found));
	}*/
	
	global $wpdb;
	
	
		$found_latlong1=explode("#",$_POST['found_latlong']);
		$found_latitude=$found_latlong1[0];
		$found_longitude=$found_latlong1[1];
				
				
	$wpdb->query("insert into wp_found_hysts(hyst_id, hyst_owner_id, found_user_id, found_price, found_location, found_latitude, found_longitude) values('".$_POST['hyst_id']."','".$_POST['hyst_owner_id']."','".$_POST['found_user_id']."','".$_POST['found_price']."','".$_POST['found_location']."','".$found_latitude."','".$found_longitude."');");
	update_post_meta( $_POST['hyst_id'], 'is_found', 1 );
	
}
if(isset($_POST['brn_confirm']) && $_POST['brn_confirm']!='')
{
	global $wpdb;
	if((isset($_POST['found_id']) && $_POST['found_id']!='') && (isset($_POST['confirm_user_id']) && $_POST['confirm_user_id']!=''))
	{
		//echo "SELECT * FROM wp_found_hysts WHERE id='".$_POST['found_id']."'";exit;
		$found_p = $wpdb->get_results("SELECT * FROM wp_found_hysts WHERE id='".$_POST['found_id']."'");
		//print_r($found_p);
		if($_POST['confirm_user_id']==$found_p[0]->hyst_owner_id)
		{
			//echo 'if is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_owner=1 where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user3==0)
		{
			//echo 'else if executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user3=1 , confirmed_by_user3_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user4==0)
		{
			//echo '2nd elseif is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user4=1 , confirmed_by_user4_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
	}
}
else if(isset($_POST['brn_not_confirm']) && $_POST['brn_not_confirm']!='')
{
	global $wpdb;
	if((isset($_POST['found_id']) && $_POST['found_id']!='') && (isset($_POST['confirm_user_id']) && $_POST['confirm_user_id']!=''))
	{
		//echo "SELECT * FROM wp_found_hysts WHERE id='".$_POST['found_id']."'";exit;
		$found_p = $wpdb->get_results("SELECT * FROM wp_found_hysts WHERE id='".$_POST['found_id']."'");
		//print_r($found_p);
		if($_POST['confirm_user_id']==$found_p[0]->hyst_owner_id)
		{
			//echo 'if is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_owner=2 where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user3==0)
		{
			//echo 'else if executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user3=2 , confirmed_by_user3_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
		else if($found_p[0]->confirmed_by_user4==0)
		{
			//echo '2nd elseif is executed';;exit;
			$wpdb->query("update wp_found_hysts set confirmed_by_user4=2 , confirmed_by_user4_userid='".$_POST['confirm_user_id']."' where id='".$_POST['found_id']."'");
		}
	}
}
if(isset($_POST['favorite'])){
	$author_id = $_POST['author_id'];
	$post_id = $_POST['post_id'];
	echo classiera_favorite_insert($author_id, $post_id);
}
if(isset($_POST['follower'])){	
	$author_id = $_POST['author_id'];
	$follower_id = $_POST['follower_id'];
	echo classiera_authors_insert($author_id, $follower_id);
}
if(isset($_POST['unfollow'])){
	$author_id = $_POST['author_id'];
	$follower_id = $_POST['follower_id'];
	echo classiera_authors_unfollow($author_id, $follower_id);
}

?>
<?php 
	//Search Styles//
	if($classieraSearchStyle == 1){
		get_template_part( 'templates/searchbar/searchstyle1' );
	}elseif($classieraSearchStyle == 2){
		get_template_part( 'templates/searchbar/searchstyle2' );
	}elseif($classieraSearchStyle == 3){
		get_template_part( 'templates/searchbar/searchstyle3' );
	}elseif($classieraSearchStyle == 4){
		get_template_part( 'templates/searchbar/searchstyle4' );
	}elseif($classieraSearchStyle == 5){
		get_template_part( 'templates/searchbar/searchstyle5' );
	}elseif($classieraSearchStyle == 6){
		get_template_part( 'templates/searchbar/searchstyle6' );
	}elseif($classieraSearchStyle == 7){
		get_template_part( 'templates/searchbar/searchstyle7' );
	}
?>
<section class="inner-page-content single-post-page">
	<div class="container">
		 <div class="bg_gray"> 
			<!-- breadcrumb -->
			<?php classiera_breadcrumbs();?>
			<!-- breadcrumb -->
			<!--Google Section-->
			<?php 
			$homeAd1 = '';		
			global $redux_demo;
			$homeAdImg1 = $redux_demo['home_ad2']['url']; 
			$homeAdImglink1 = $redux_demo['home_ad2_url']; 
			$homeHTMLAds = $redux_demo['home_html_ad2'];
			
			if(!empty($homeHTMLAds) || !empty($homeAdImg1)){
				if(!empty($homeHTMLAds)){
					$homeAd1 = $homeHTMLAds;
				}else{
					$homeAd1 = '<a href="'.$homeAdImglink1.'" target="_blank"><img class="img-responsive" alt="image" src="'.$homeAdImg1.'" /></a>';
				}
			}
			if(!empty($homeAd1)){
			?>
			<section id="classieraDv">
				<div class="container">
					<div class="row">							
						<div class="col-lg-12 col-md-12 col-sm-12 center-block text-center">
							<?php echo $homeAd1; ?>
						</div>
					</div>
				</div>	
			</section>
			<?php } ?>
			<?php if ( get_post_status ( $post->ID ) == 'private' || get_post_status ( $post->ID ) == 'pending' ) {?>
			<div class="alert alert-info" role="alert">
			  <p>
			  <strong><?php esc_html_e('Congratulation!', 'classiera') ?></strong> <?php esc_html_e('Your HYST has submitted and pending for review. After review your  will be live for all users. You may not preview it more than once.!', 'classiera') ?>
			  </p>
			</div>
			<?php } ?>
			<!--Google Section-->
			<?php if($classieraSingleAdStyle == 2){
				get_template_part( 'templates/singlev2' );
			}
			$is_competition=get_post_meta($post->ID, 'is_competition');
			//print_r($is_competition);
			if($is_competition[0]==1)
			{
				$colmid='col-md-12';
			}
			else
			{
				$colmid='col-md-8';
			}?>
			<div class="row">
				<div class="<?php //echo $colmid;?> col-md-12">
					<!-- single post -->
					<div class="single-post">
						<?php if($classieraSingleAdStyle == 1){
							get_template_part( 'templates/singlev1');
						}?>
						<?php 
						$post_price = get_post_meta($post->ID, 'post_price', true); 
						$post_old_price = get_post_meta($post->ID, 'post_old_price', true);
						$postVideo = get_post_meta($post->ID, 'post_video', true);
						$dateFormat = get_option( 'date_format' );
						$postDate = get_the_date($dateFormat, $post->ID);
						$itemCondition = get_post_meta($post->ID, 'item-condition', true); 
						$post_location = get_post_meta($post->ID, 'post_location', true);
						$post_state = get_post_meta($post->ID, 'post_state', true);
						$post_city = get_post_meta($post->ID, 'post_city', true);
						$post_phone = get_post_meta($post->ID, 'post_phone', true);
						$post_latitude = get_post_meta($post->ID, 'post_latitude', true);
						$post_longitude = get_post_meta($post->ID, 'post_longitude', true);
						$post_address = get_post_meta($post->ID, 'post_address', true);
						$classieraCustomFields = get_post_meta($post->ID, 'custom_field', true);					
						$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
						$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);
						
						
						
						//echo "SELECT id FROM wp_hyst_likes WHERE (hyst_id = '".$post->ID."' AND like_user_id = '". $current_user->ID ."' AND owner_user_id='".$post->post_author."')";
						$post_id = $wpdb->get_results("SELECT id FROM wp_hyst_likes WHERE (hyst_id = '".$post->ID."' AND like_user_id = '". $current_user->ID ."' AND owner_user_id='".$post->post_author."')");
						//echo count($post_id[0]->id);
						?>
			          <div class="hyst_social" style="width:100%;">
							<div class="social_icon">
								<?php
								if(count($post_id[0]->id)>0)
								{
									$myrows = $wpdb->get_results( "SELECT count(id) as tot_like FROM wp_hyst_likes where like_user_id!='0' and hyst_id='".$post->ID."'" );
									if($myrows[0]->tot_like > 1)
									{
										$tot_like=$myrows[0]->tot_like;
										$like='Likes';
									}
									else
									{
										$like='Like';
										if($myrows[0]->tot_like>0)
										{
											$tot_like=$myrows[0]->tot_like;
										}
										else
										{
											$tot_like=0;
										}
										
									}
									?>
								
									<p><a href="javascript:void(0);" id="like_click1"><i class="fa fa-heart" style="color:green;"></i></br><?php echo '<span id="tot_like">'.$tot_like.'</span>';?>&nbsp;<span><?php echo $like?></span></a></p>
									
									<?php /*<div class="sharethis-inline-share-buttons"></div>*/?>
									
									<?php echo do_shortcode('[addtoany]');?> 
									<p class="share">Share: </p>
			<!--                                                <div data-network="twitter" class="st-custom-button"><i class="fa fa-twitter"></i></div>
															<div data-network="facebook" class="st-custom-button"><i class="fa fa-facebook"></i>-->
									
			<style>.st-custom-button{float:left; width:25px;}</style>
							 
						
						<?php
						}
						else
						{
							$myrows = $wpdb->get_results( "SELECT count(id) as tot_like FROM wp_hyst_likes where like_user_id!='0' and hyst_id='".$post->ID."'" );
							if($myrows[0]->tot_like > 1)
							{
								$tot_like=$myrows[0]->tot_like;
								$like='Likes';
							}
							else
							{
								$like='Like';
								if($myrows[0]->tot_like>0)
								{
									$tot_like=$myrows[0]->tot_like;
								}
								else
								{
									$tot_like=0;
								}
							}
						?>
						
							<p><a href="javascript:void(0);" id="like_click"><i class="fa fa-heart"></i></br><?php echo '<span id="tot_like">'.$tot_like.'</span>';?>&nbsp;<span><?php echo $like;?></span></a></p><?php /*<div class="sharethis-inline-share-buttons"></div>*/?>
							
													<?php echo do_shortcode('[addtoany]');?>
	<!--                                                <div data-network="twitter" class="st-custom-button"><i class="fa fa-twitter"></i></div>
													<div data-network="facebook" class="st-custom-button"><i class="fa fa-facebook"></i></div>-->
							<p class="share">Share: </p>
							
						
						<style>.st-custom-button{float:left; width:25px;}</style>
						

								<?php
								}
								?>
						
						   </div>
						   <div class="clearfix"></div>
						    <div class="ecom_buttton inline-flex top-buffer3">
						         <button type="submit" name="watch_later" class="form-control-sm form-control"><i class="fa fa-play-circle"></i><span>watch later</span></button>
						         <button type="submit" name="send_email" class="form-control-sm form-control"><i class="fa fa-envelope-o"></i><span>send e-mail</span></button>
							</div>
							 
						</div>
						
						<input type="hidden" name="current_latitude" id="current_latitude" value="" />
						<input type="hidden" name="current_longitude" id="current_longitude" value="" />
						<?php
						if(is_user_logged_in())
						{
						/*SELECT id, (6371 * 2 * ASIN(SQRT( POWER(SIN(( -6.9831375276568055 - found_latitude) *  pi()/180 / 2), 2) +COS(-25.864029 * pi()/180) * COS(found_latitude * pi()/180) * POWER(SIN(( 28.0888578 -found_longitude) * pi()/180 / 2), 2) ))) as distance  from wp_found_hysts having  distance <= 100000000000000000 order by distance*/
							
							/*SELECT ((ACOS(SIN(-25.864029 * PI() / 180) * SIN(found_latitude * PI() / 180)  + COS(-25.864029 * PI() / 180) * COS(found_latitude * PI() / 180) * COS((28.0888578 - found_longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) as distance 
	FROM `wp_found_hysts` 
	HAVING distance <= 1000000
	ORDER BY distance ASC*/?>
						<div style="float:left; width:100%; margin-top:20px;" id="found_main_div"><?php
						$sel_found = $wpdb->get_results("select * from wp_found_hysts where hyst_id='".$post->ID."'");
						
						$current_post_id=$post->ID; /*
						<style>
							.btnconfirm_css{background: #fff;
	border: 2px solid #000;
	padding: 5px 10px;"}
	.btnconfirm_css:hover{background: #000; color:#fff;}
							</style>
							<?php
							//echo "select * from wp_found_hysts where hyst_id='".$post->ID."'";
							
							?>
							
							<?php
							if(count($sel_found)>0)
							{
								foreach($sel_found as $key=>$val)
								{
									
									?><div style="clear:left;"></div><?php		
											
									if($sel_found[$key]->confirmed_by_owner==1 && $sel_found[$key]->confirmed_by_user3==1 && $sel_found[$key]->confirmed_by_user4==1)
									{?>
										<label style="color: #000;

	background: green;

	padding: 5px 10px;

	border: 1px solid #000;float:left;width:20%;">R <?php echo $sel_found[$key]->found_price;?></label> &nbsp;
											<?php
											$post_location = get_post_meta($post->ID, 'post_location', true);
											echo '<span style="font-size:11px; margin-left:10px; line-height:12px; float:left; width:180px;"><i class="fa fa-map-marker"></i>: '.$post_location.'</span>';
											
											if(($sel_found[$key]->expiry_date!='0000-00-00') && strtotime($sel_found[$key]->expiry_date)<time())
											{
												
												echo '<span style="color:red; margin-left:10px;">Expired</span>';
											}
											?>
												<?php
									}
									else if($sel_found[$key]->confirmed_by_owner==2 && $sel_found[$key]->confirmed_by_user3==2 && $sel_found[$key]->confirmed_by_user4==2)
									{?>
										<label style="color: #000;

	background: red;

	padding: 5px 10px;

	border: 1px solid #000;float:left;width:20%;">R <?php echo $sel_found[$key]->found_price;?></label>
											
											<?php
											$post_location = get_post_meta($post->ID, 'post_location', true);
											echo '<span style="font-size:11px; margin-left:10px; line-height:12px; float:left;width:180px;"><i class="fa fa-map-marker"></i>: '.$post_location.'</span>';
											if(($sel_found[$key]->expiry_date!='0000-00-00') && strtotime($sel_found[$key]->expiry_date)<time())
											{
												
												echo '<span style="color:red; margin-left:10px;">Expired</span>';
											}
											?>
								
												<?php
												
									}
									else
									{?>
										<label style="color: #000;

	background: yellow;

	padding: 5px 10px;

	border: 1px solid #000;float:left;width:20%;">R <?php echo $sel_found[$key]->found_price;?></label><?php
											$post_location = get_post_meta($post->ID, 'post_location', true);
											echo '<span style="font-size:11px; margin-left:10px; line-height:12px; float:left;width:180px;"><i class="fa fa-map-marker"></i>: '.$post_location.'</span>';
										if($current_user->ID==$sel_found[$key]->hyst_owner_id && $sel_found[$key]->confirmed_by_owner==0)
										{
											?>
											<form name="frm_confrim" id="frm_confirm" method="post" action="" style="float:left;width:15%; margin-left:3%;">
												<input type="hidden" name="confirm_user_id" value="<?php echo $current_user->ID;?>" />
												<input type="hidden" name="found_id" value="<?php echo $sel_found[$key]->id;?>" />
												<input type="submit" name="brn_confirm" value="Confirm"  class="btnconfirm_css" />
											</form>
											<style>
											.btnconfirm_css{background: #fff;
	border: 2px solid #000;
	padding: 5px 10px;"}
	.btnconfirm_css:hover{background: #000; color:#fff;}
											</style>
											<?php
										}
										else if($current_user->ID==$sel_found[$key]->confirmed_by_user3_userid)
										{
										}
										else if($current_user->ID==$sel_found[$key]->confirmed_by_user4_userid)
										{
										}
										else if($current_user->ID!=$sel_found[$key]->hyst_owner_id)
										{?>
											<form name="frm_confrim" id="frm_confirm" method="post" action="" style="float:left;width:15%; margin-left:3%;">
												<input type="hidden" name="confirm_user_id" value="<?php echo $current_user->ID;?>" />
												<input type="hidden" name="found_id" value="<?php echo $sel_found[$key]->id;?>" />
												<input type="submit" name="brn_confirm" value="Confirm"  class="btnconfirm_css" />
											</form>
											<style>
											.btnconfirm_css{background: #fff;
	border: 2px solid #000;
	padding: 5px 10px;"}
	.btnconfirm_css:hover{background: #000; color:#fff;}
											</style>
											<?php
										}
										
										
										if($current_user->ID==$sel_found[$key]->hyst_owner_id && $sel_found[$key]->confirmed_by_owner==0)
										{
											?>
											<form name="frm_confrim" id="frm_confirm" method="post" action="" style="float:left;width:20%; margin-left:3%;">
												<input type="hidden" name="confirm_user_id" value="<?php echo $current_user->ID;?>" />
												<input type="hidden" name="found_id" value="<?php echo $sel_found[$key]->id;?>" />
												<input type="submit" name="brn_not_confirm" value="Not Confirm"  class="btnconfirm_css" />
											</form>
											<style>
											.btnconfirm_css{background: #fff;
	border: 2px solid #000;
	padding: 5px 10px;"}
	.btnconfirm_css:hover{background: #000; color:#fff;}
											</style>
											<?php
										}
										else if($current_user->ID==$sel_found[$key]->confirmed_by_user3_userid)
										{
										}
										else if($current_user->ID==$sel_found[$key]->confirmed_by_user4_userid)
										{
										}
										else if($current_user->ID!=$sel_found[$key]->hyst_owner_id)
										{?>
											<form name="frm_confrim" id="frm_confirm" method="post" action="" style="float:left;width:20%; margin-left:3%;">
												<input type="hidden" name="confirm_user_id" value="<?php echo $current_user->ID;?>" />
												<input type="hidden" name="found_id" value="<?php echo $sel_found[$key]->id;?>" />
												<input type="submit" name="brn_not_confirm" value="Not Confirm"  class="btnconfirm_css" />
											</form>
											<style>
											.btnconfirm_css{background: #fff;
	border: 2px solid #000;
	padding: 5px 10px;"}
	.btnconfirm_css:hover{background: #000; color:#fff;}
											</style>
											<?php
										}
									}
									
								}
							}
							*/?>
						</div>
						<?php
						
						}
						$error_savelist=array();
						if(isset($_POST['btn_save_list']) && $_POST['btn_save_list']!='')
						{
							if($_POST['list_title']  && $_POST['list_title']=='')
							{
								$error_savelist[]="Please select List name";
							}
							if(empty($error_savelist))
							{
								$wpdb->query("insert into wp_user_list_item(list_id, user_id, hyst_id) values('".$_POST['list_title']."', '".$_POST['user_id']."', '".$_POST['hyst_id']."');");
							}
						}
						//global $wpdb;
							$sel_lists=$wpdb->get_results("select * from wp_user_lists where user_id='".$current_user->ID."'");
							
							//print_r($sel_lists);
							if(count($sel_lists)>0)
							{?>
								<div style="float:left;">
									<h4>Add item to list</h4>
									<?php
									if($error_savelist!='')
									{
										for($lp=0;$lp<count($error_savelist);$lp++)
										{
											echo '<span>'.$error_savelist[$lp].'</span>';
										}
									}
									?>
									<form id="frm_list" name="frm_list" method="post" action="">
										<select name="list_title" id="list_title" class="form-control form-control-sm">
											<option value="">Select List Name</option>
											<?php
											foreach($sel_lists as $key=>$val)
											{
												$slistitem=$wpdb->get_results("select * from wp_user_list_item where list_id='".$sel_lists[$key]->id."' and hyst_id='".$post->ID."' and user_id='".$current_user->ID."'");?>
												<?php
												if(count($slistitem)<=0)
												{
												?>
													<option value="<?php echo $sel_lists[$key]->id;?>"><?php echo $sel_lists[$key]->list_title;?></option>
											<?php
												}
											}?>
										</select><br/>
										<input type="submit" name="btn_save_list" value="Save" class="cust_sub_button"/>
										<input type="hidden" name="user_id" value="<?php echo $current_user->ID;?>" />
										<input type="hidden" name="hyst_id" value="<?php echo $post->ID;?>" />
									</form>
								</div>
								<div style="clear:left;">
								</div>
							<?php
							}?>
						</div>
						<script language="javascript" type="text/javascript">
						var tot_like=<?php echo $tot_like;?>;
						jQuery("#like_click").click(function(){
						//alert("here");
							jQuery.post("<?php echo site_url();?>/ajax_like_hyst.php",
							{
								hyst_id: <?php echo $post->ID;?>,
								hyst_owner_id: <?php echo $post->post_author;?>,
								like_user_id: <?php echo $current_user->ID;?>
								
							},
							function(data, status){
								//alert("Data: " + data + "\nStatus: " + status);
								jQuery("#like_click").css('color', 'green');
								jQuery("#like_click i").css('color', 'green');
								tot_like+=1;
								jQuery("#tot_like").html(tot_like);
								
							});
						}); 
						</script>
						<?php
										if(is_user_logged_in())
										{?>
										<div style="float:right; width:30%; padding-left:10px;">
										<a href="javascript:void(0);"  onclick="show_found_form()" style="padding:0px 10px 5px 0;"><h4 style="padding:0; margin:0;">Have you found this HYST?</h4></a>
									<div class="found_form" id="found_form" style="display:none;">
									<?php /*<a href="javascript:void(0);" onclick="show_found_form()" style="padding:5px 10px; border:2px solid #000;">Have you found this HYST?</a>*/?>
									
										
										<script>
											function show_found_form()
											{
												//document.getElementById("found_form").style.display='block';
												if(document.getElementById("found_form").style.display=='none')
												{
													jQuery("#found_form").show();
												}
												else
												{
													jQuery("#found_form").hide();
												}
											}
										</script>
										<form name="frm_found" id="frm_found" action="" method="post">
										
												<?php /*<label style="width:100px;">location:</label>*/?><input type="text" name="found_location" id="found_location" value="" class="form-control form-control-sm"  onFocus="geolocate()" onChange="getlatlong1(this.value, 0);"  style="width:200px;"/>
												<?php /*<label style="width:100px;">Price:</label>*/?><input type="text" name="found_price" value="" class="form-control form-control-sm" style="width:200px; margin-top:10px;" placeholder="Price?" />
												<?php /*<input type="hidden" name="found_latitude" id="found_latitude" value=""/>
												<input type="hidden" name="found_longitude" id="found_longitude" value=""/>*/?>
												<input type="hidden" name="found_latlong" id="found_latlong" value=""/>
												<input type="hidden" name="hyst_id" value="<?php echo $post->ID;?>" />
												<input name="found_user_id" type="hidden" value="<?php echo $current_user->ID;?>"/>
												<input name="hyst_owner_id" type="hidden" value="<?php echo $post->post_author;;?>"/>
												<input type="submit" name="btn_submit" value="Submit" class="cust_sub_button" style="margin-top:10px;"/>
											</form>
									</div>
									</div>
									<div style="clear:left;"></div>
									<?php }?>
						<div style="clear:left;margin-top:20px;"></div>
						<!-- ad deails -->
						<?php
						if($is_competition[0]!=1)
						{?>
						<div class="border-section border details" style="margin-top:130px;">
							<h4 class="border-section-heading text-uppercase"><i class="fa fa-file-text-o"></i><?php esc_html_e('HYST Details', 'classiera') ?></h4>
							<div class="post-details">
								<ul class="list-unstyled clearfix">
									<li style="display:none;">
										<p><?php esc_html_e( 'Ad ID', 'classiera' ); ?>: 
										<span class="pull-right flip">
											<i class="fa fa-hashtag IDIcon"></i>
											<?php echo $post->ID; ?>
										</span>
										</p>
									</li><!--PostDate-->
									<li>
										<p><?php esc_html_e( 'Added', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $postDate; ?></span>
										</p>
									</li><!--PostDate-->								
									<!--Price Section -->
									<?php 
									$classieraPriceSection = $redux_demo['classiera_sale_price_off'];
									if($classieraPriceSection == 1){
									?>
									<?php if(!empty($post_price)){?>
									<li>
										<p><?php esc_html_e( ' Price', 'classiera' ); ?>: 
										<span class="pull-right flip">
											<?php 
											if(is_numeric($post_price)){
												$classieraPostPrice =  classiera_post_price_display($post_currency_tag, $post_price);
											}else{ 
												$classieraPostPrice =  $post_price; 
											}
											echo $classieraPostPrice;
											?>
										</span>
										</p>
									</li><!--Sale Price-->
									<?php } ?>
									<?php if(!empty($post_old_price)){?>
									<li>
										<p><?php esc_html_e( ' Price', 'classiera' ); ?>: 
										<span class="pull-right flip">										
											<?php 
											if(is_numeric($post_old_price)){
												echo classiera_post_price_display($post_currency_tag, $post_old_price);
											}else{ 
												echo $post_old_price; 
											}
											?>
										</span>
										</p>
									</li><!--Regular Price-->
									<?php } ?>
									<!--Price Section -->
									<?php } ?>
									<?php if(!empty($itemCondition)){?>
									<li>
										<p><?php esc_html_e( 'Condition', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $itemCondition; ?></span>
										</p>
									</li><!--Condition-->
									<?php } ?>
									<?php if(!empty($post_location)){?>
									<li>
										<p><?php esc_html_e( 'Location', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $post_location; ?></span>
										</p>
									</li><!--Location-->
									<?php } ?>
									<?php if(!empty($post_state)){?>
									<li>
										<p><?php esc_html_e( 'Province', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $post_state; ?></span>
										</p>
									</li><!--state-->
									<?php } ?>
									<?php if(!empty($post_city)){?>
									<li>
										<p><?php esc_html_e( 'City', 'classiera' ); ?>: 
										<span class="pull-right flip"><?php echo $post_city; ?></span>
										</p>
									</li><!--City-->
									<?php } ?>
									<?php if(!empty($post_phone)){?>
									<li>
										<p><?php esc_html_e( 'Phone', 'classiera' ); ?>: 
										<span class="pull-right flip">
											<a href="tel:<?php echo $post_phone; ?>"><?php echo $post_phone; ?></a>
										</span>
										</p>
									</li><!--Phone-->
									<?php } ?>
									<li style="display:none;">
										<p><?php esc_html_e( 'Views', 'classiera' ); ?>: 
										<span class="pull-right flip">
											<?php echo classiera_get_post_views(get_the_ID()); ?>
										</span>
										</p>
									</li><!--Views-->
									<?php 
									if(!empty($classieraCustomFields)) {
										for ($i = 0; $i < count($classieraCustomFields); $i++){
											if($classieraCustomFields[$i][2] != 'dropdown' && $classieraCustomFields[$i][2] != 'checkbox'){
												if(!empty($classieraCustomFields[$i][1]) && !empty($classieraCustomFields[$i][0]) ) {
													?>
												<li>
													<p><?php echo $classieraCustomFields[$i][0]; ?>: 
													<span class="pull-right flip">
														<?php echo $classieraCustomFields[$i][1]; ?>
													</span>
													</p>
												</li><!--test-->	
													<?php
												}
											}
										}
										for ($i = 0; $i < count($classieraCustomFields); $i++){
											if($classieraCustomFields[$i][2] == 'dropdown'){
												if(!empty($classieraCustomFields[$i][1]) && !empty($classieraCustomFields[$i][0]) ){
												?>
												<li>
													<p><?php echo $classieraCustomFields[$i][0]; ?>: 
													<span class="pull-right flip">
														<?php echo $classieraCustomFields[$i][1]; ?>
													</span>
													</p>
												</li><!--dropdown-->
												<?php
												}
											}
										}
										for ($i = 0; $i < count($classieraCustomFields); $i++){
											if($classieraCustomFields[$i][2] == 'checkbox'){
												if(!empty($classieraCustomFields[$i][1]) && !empty($classieraCustomFields[$i][0]) ){
												?>
												<li>
													<p><?php echo $classieraCustomFields[$i][0]; ?>: 
													<span class="pull-right flip">
														<?php esc_html_e( 'Yes', 'classiera' ); ?>
													</span>
													</p>
												</li><!--dropdown-->
												<?php	
												}
											}
										}
									}
									?>
								</ul>
							</div><!--post-details-->
						</div>
						<?php
						}?>
						<!-- ad details -->
						<!--<div class="row">
							<script>
								var window_width = 860;
								var window_height = 800;
								var leftPosition, topPosition;
								(function($) {
									"use strict";
						
									$(function() {
										leftPosition = (window.screen.width / 2) - ((window_width / 2) + 10);
										topPosition = (window.screen.height / 2) - ((window_height / 2) + 50);
									});
								})(jQuery);
							</script>
							<?php 
							//$template = get_template_directory() . '/templates/classiera-print.php';
							//$url = $template.'?'.get_permalink($post->ID);
							//echo $url;
							?>					
							<a href="javascript:void(0);" class="classiera-print-listing-link btn" onClick="window.open('<?php //echo $template; ?>', 'print_window', 'height='+window_height+',width='+window_width+',left='+leftPosition+',top='+topPosition+',menubar=yes,scrollbars=yes');"   data-toggle="tooltip" title="<?php //_e('Print listing', 'classiera'); ?>">
								<?php //_e('Print listing', 'classiera'); ?>
							</a>
						</div>-->
						<!--PostVideo-->
						<?php if(!empty($postVideo)) { ?>
						<div class="border-section border postvideo">
							<h4 class="border-section-heading text-uppercase">
							<?php esc_html_e( 'Video', 'classiera' ); ?>
							</h4>
							<?php 
							if(preg_match("/youtu.be\/[a-z1-9.-_]+/", $postVideo)) {
								preg_match("/youtu.be\/([a-z1-9.-_]+)/", $postVideo, $matches);
								if(isset($matches[1])) {
									$url = 'https://www.youtube.com/embed/'.$matches[1];
									$video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen></iframe>';
								}
							}elseif(preg_match("/youtube.com(.+)v=([^&]+)/", $postVideo)) {
								preg_match("/v=([^&]+)/", $postVideo, $matches);
								if(isset($matches[1])) {
									$url = 'https://www.youtube.com/embed/'.$matches[1];
									$video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen></iframe>';
								}
							}elseif(preg_match("#https?://(?:www\.)?vimeo\.com/(\w*/)*(([a-z]{0,2}-)?\d+)#", $postVideo)) {
								preg_match("/vimeo.com\/([1-9.-_]+)/", $postVideo, $matches);
								//print_r($matches); exit();
								if(isset($matches[1])) {
									$url = 'https://player.vimeo.com/video/'.$matches[1];
									$video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>';
								}
							}
							
							else{
								$video = $postVideo;
							}
							?>
							<div class="embed-responsive embed-responsive-16by9">
								<?php echo $video; ?>
							</div>
						</div>
						<?php } ?>
						<!--PostVideo-->
						<!-- post description -->
						<?php
						if($is_competition[0]!=1)
						{?>
						<div class="border-section border description">
							<h4 class="border-section-heading text-uppercase">
							<?php esc_html_e( 'Description', 'classiera' ); ?>
							</h4>
							<?php echo the_content(); ?>
							<div class="tags">
								<span>
									<i class="fa fa-tags"></i>
									<?php esc_html_e( 'Tags', 'classiera' ); ?> :
								</span>
								<?php the_tags('','',''); ?>
							</div>
						</div>
						<?php
						}
						if($is_competition[0]==1)
						{
							$end_date=get_post_meta($post->ID,"end_date");
							//print_r($end_date);
							echo 'End Date: '.date("Y-m-d", strtotime($end_date[0]));
							echo '<br/><br/>';
						}
						if($is_competition[0]==1)
						{
							echo '<a style="background:#000; padding:10px;color: #fff;" href="https://hyst2.temp.co.za/compitition-entry/?comp_id='.$post->ID.'">Enter Compitition</a><br/><br/>';
						}?>
						
						<!-- post description -->
						<!-- classiera bid system -->
						<?php if($classiera_bid_system == true){ ?>
						<div class="border-section border bids">
							<?php 
								global $wpdb;
								$classieraMaxOffer = null;
								$classieraTotalBids = null;
								$post_id = $post->ID;
								$currentPostOffers = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_inbox WHERE offer_post_id = $post_id ORDER BY id DESC" );							
								$classieraMaxOffer = classiera_max_offer_price($post_id);
								$classieraMaxOffer = classiera_post_price_display($post_currency_tag, $classieraMaxOffer);
								$classieraMinOffer = classiera_min_offer_price($post_id);
								$classieraMinOffer = classiera_post_price_display($post_currency_tag, $classieraMinOffer);
								$classieraTotalBids = classiera_bid_count($post_id);
							?>
							<h4 class="border-section-heading text-uppercase"><?php esc_html_e( 'Bids', 'classiera' ); ?></h4>
							<div class="classiera_bid_stats">
								<p class="classiera_bid_stats_text"> 
									<strong><?php esc_html_e( 'BID Stats', 'classiera' ); ?> :</strong> 
									<?php echo $classieraTotalBids; ?>&nbsp;
									<?php esc_html_e( 'Bids posted on this ad', 'classiera' ); ?>
								</p>
								<div class="classiera_bid_stats_prices">
									<?php if($currentPostOffers){?>
									<p class="classiera_bid_price_btn high_price"> 
										<span><?php esc_html_e( 'Highest Bid', 'classiera' ); ?>:</span>
										<?php echo $classieraMaxOffer;?>
									</p>
									<?php } ?>
									<?php if($currentPostOffers){?>
									<p class="classiera_bid_price_btn"> 
										<span><?php esc_html_e( 'Lowest Bid', 'classiera' ); ?>:</span> 
										<?php echo $classieraMinOffer;?>
									</p>
									<?php } ?>
								</div><!--classiera_bid_stats_prices-->
							</div><!--classiera_bid_stats-->
							<div class="classiera_bid_comment_section">
								<?php 
								if($currentPostOffers){
									foreach ( $currentPostOffers as $offerinfo ) {
									$classieraOfferAuthorID = $offerinfo->offer_author_id;
									$classieraOfferPrice = $offerinfo->offer_price;
									$classieraOfferPrice = classiera_post_price_display($post_currency_tag, $classieraOfferPrice);
									$classieraOfferComment = $offerinfo->offer_comment;	
									$classieraOfferDate = $offerinfo->date;
									$classieraOfferDate = date_i18n($dateFormat,  strtotime($date));
									$classieraOfferAuthor = get_the_author_meta('display_name', $classieraOfferAuthorID );
									if(empty($classieraOfferAuthor)){
										$classieraOfferAuthor = get_the_author_meta('user_nicename', $classieraOfferAuthorID );
									}
									if(empty($classieraOfferAuthor)){
										$classieraOfferAuthor = get_the_author_meta('user_login', $classieraOfferAuthorID );
									}
									$classieraOfferAuthorEmail = get_the_author_meta('user_email', $classieraOfferAuthorID);
									$classieraOfferAuthorIMG = get_user_meta($classieraOfferAuthorID, "classify_author_avatar_url", true);
									$classieraOfferAuthorIMG = classiera_get_profile_img($classieraOfferAuthorIMG);
									if(empty($classieraOfferAuthorIMG)){										
										$classieraOfferAuthorIMG = classiera_get_avatar_url ($classieraOfferAuthorEmail, $size = '150' );
									}
								?>
								<div class="classiera_bid_media">
									<img class="classiera_bid_media_img img-thumbnail" src="<?php echo $classieraOfferAuthorIMG; ?>" alt="<?php echo $classieraOfferAuthor;?>">
									<div class="classiera_bid_media_body">
										<h6 class="classiera_bid_media_body_heading">
											<?php echo $classieraOfferAuthor;?>
											<span><?php esc_html_e( 'Offering', 'classiera' ); ?></span>
										</h6>
										<p class="classiera_bid_media_body_time">
											<i class="fa fa-clock-o"></i><?php echo $classieraOfferDate; ?></p>
										<p class="classiera_bid_media_body_decription">
											<?php echo $classieraOfferComment; ?>
										</p>
									</div>
									<div class="classiera_bid_media_price">
										<p class="classiera_bid_price_btn">
											<span><?php esc_html_e( 'BID', 'classiera' ); ?>:</span> <?php echo $classieraOfferPrice; ?></p>
									</div>
								</div>
								<?php } }?>
								
							</div><!--classiera_bid_comment_section-->
							<div class="comment-form classiera_bid_comment_form">
								<div class="comment-form-heading">
									<h4 class="text-uppercase"><?php esc_html_e( 'Let’s Create Your Bid', 'classiera' ); ?></h4>
									<p><?php esc_html_e( 'Only registered user can post offer', 'classiera' ); ?>
										<span class="text-danger">*</span>
									</p>
								</div><!--comment-form-heading-->
								<div class="classiera_ad_price_comment">
									<p><?php esc_html_e( 'Ad Price', 'classiera' ); ?></p>
									<h3><?php echo $classieraPostPrice; ?></h3>
								</div><!--classiera_ad_price_comment-->
								<form data-toggle="validator" id="classiera_offer_form" method="post">
									<span class="classiera--loader"><img src="<?php echo get_template_directory_uri().'/images/loader.gif' ?>" alt="classiera loader"></span>
									<div class="form-group">
										<div class="form-inline row">
											<div class="form-group col-sm-12">
												<label class="text-capitalize">
													<?php esc_html_e( 'Enter your bidding price', 'classiera' ); ?> :
													<span class="text-danger">*</span>
												</label>
												<div class="inner-addon left-addon">
													<input type="number" class="form-control form-control-sm offer_price" name="offer_price" placeholder="<?php esc_html_e( 'Enter bidding price', 'classiera' ); ?>" data-error="<?php esc_html_e( 'Only integer', 'classiera' ); ?>" required>
													<div class="help-block with-errors"></div>
												</div>
											</div><!--form-group col-sm-12-->                                    
											<div class="form-group col-sm-12">
												<label class="text-capitalize">
													<?php esc_html_e( 'Enter your comment', 'classiera' ); ?> :
													<span class="text-danger">*</span>
												</label>
												<div class="inner-addon">
													<textarea class="offer_comment" data-error="<?php esc_html_e( 'please type your comment', 'classiera' ); ?>" name="offer_comment" placeholder="<?php esc_html_e( 'Type your comment here', 'classiera' ); ?>" required></textarea>
													<div class="help-block with-errors"></div>
												</div>
											</div><!--form-group col-sm-12-->
										</div><!--form-inline row-->
									</div><!--form-group-->
									<?php 
									$postAuthorID = $post->post_author;
									$currentLoggedAuthor = wp_get_current_user();
									$offerAuthorID = $currentLoggedAuthor->ID;
									?>
									<input type="hidden" class="offer_post_id" name="offer_post_id" value="<?php echo $post->ID;?>">
									<input type="hidden" class="post_author_id" name="post_author_id" value="<?php echo $postAuthorID; ?>">
									<input type="hidden" class="offer_author_id" name="offer_author_id" value="<?php echo $offerAuthorID; ?>">
									<input type="hidden" class="offer_post_price" name="offer_post_price" value="<?php echo $classieraPostPrice; ?>">
									<div class="form-group">
										<button type="submit" name="submit_bid" class="btn btn-primary sharp btn-md btn-style-one submit_bid">
											<?php esc_html_e( 'Send offer', 'classiera' ); ?>
										</button>
									</div>
									<div class="form-group">
										<div class="classieraOfferResult bg-success text-center"></div>
									</div>
								</form>
							</div><!--comment-form classiera_bid_comment_form-->
						</div>
						<?php } ?>
						<!-- classiera bid system -->
						<!--comments-->
						<?php if($classieraComments == 1){?>
						<div class="border-section border comments">
							<h4 class="border-section-heading text-uppercase"><?php esc_html_e( 'Comments', 'classiera' ); ?></h4>
							<?php 
							$file ='';
							$separate_comments ='';
							comments_template( $file, $separate_comments );
							?>
						</div>
						<?php } ?>
						<!--comments-->
					</div>
					<!-- single post -->
				</div><!--col-md-8-->
				<?php
				if($is_competition[0]!=1)
				{?>
				<div class="col-md-12">
					<aside class="sidebar">
						<div class="row">
							<?php if($classieraSingleAdStyle == 1){?>
							<!--Widget for style 1-->
							<div class="col-lg-12 col-md-12 col-sm-6 match-height">
								<div class="widget-box <?php if($classieraSingleAdStyle == 2){echo "border-none";}?>" style="display:none;">
									<?php 
									$classieraPriceSection = $redux_demo['classiera_sale_price_off'];
									if($classieraPriceSection == 1){									
										$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
									?>
									<div class="widget-title price">
										<h3 class="post-price" style="float:left;">										
											<?php 
											if(is_numeric($post_price)){
												echo classiera_post_price_display($post_currency_tag, $post_price);
											}else{ 
												echo $post_price; 
											}
											if(!empty($classiera_ads_type)){
												?>
												<span class="ad_type_display">
													<?php classiera_buy_sell($classiera_ads_type); ?>
												</span>
												<?php
											}
											?>
										</h3>
										
									</div><!--price-->
									
									<?php } 
								/*	$founds_val=get_post_meta($post->ID, 'found');
									//$founds_val=unserialize($founds_val);
									//print_r(unserialize($founds_val[0]));
									
								//	print_r($founds_val);?>	
									<?php
									$custarray=unserialize($founds_val[0]);
								//	print_r($custarray);
									if(count($custarray)>0)
									{
										
										for($lp=0;$lp<count($custarray);$lp++)
										{
											//echo '=>'.$founds_val[0][$lp]['confirm'].'<=';
											if($custarray[$lp]['confirm']==0 && $custarray[$lp]['found_price']!='')
											{
												?>
												<form name="frm_confirm_<?php echo $lp;?>" method="post" action="">
													<input type="submit" name="confirm" style="background:yellow; border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;"   value="R <?php echo $custarray[$lp]['found_price'];?>"/>
													<input type="hidden" name="found_id"  value="<?php echo $lp;?>" />
												</form>
												<?php
											}
											else if($custarray[$lp]['confirm']==1 && $custarray[$lp]['found_price']!='')
											{
												?>
													<input type="submit" name="confirm" style="background:green; border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;"   value="R <?php echo $custarray[$lp]['found_price'];?>"/>
												<?php
											}
											else if($custarray[$lp]['confirm']==2 && $custarray[$lp]['found_price']!='')
											{
												?>
													<input type="submit" name="confirm" style="background:red; border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;"   value="R <?php echo $custarray[$lp]['found_price'];?>"/>
												<?php
											}
										}
									}*/
									?>
									<div class="widget-content widget-content-post">
										<div class="author-info border-bottom widget-content-post-area">
										<?php 
										$user_ID = $post->post_author;
										$authorName = get_the_author_meta('display_name', $user_ID );
										if(empty($authorName)){
											$authorName = get_the_author_meta('user_nicename', $user_ID );
										}
										if(empty($authorName)){
											$authorName = get_the_author_meta('user_login', $user_ID );
										}
										$author_avatar_url = get_user_meta($user_ID, "classify_author_avatar_url", true);
										$author_avatar_url = classiera_get_profile_img($author_avatar_url);
										$authorEmail = get_the_author_meta('user_email', $user_ID);
										$authorURL = get_the_author_meta('user_url', $user_ID);
										$authorPhone = get_the_author_meta('phone', $user_ID);
										if(empty($author_avatar_url)){										
											$author_avatar_url = classiera_get_avatar_url ($authorEmail, $size = '150' );
										}
										$UserRegistered = get_the_author_meta( 'user_registered', $user_ID );
										$dateFormat = get_option( 'date_format' );
										$classieraRegDate = date_i18n($dateFormat,  strtotime($UserRegistered));
										?>	
											<div class="media">
												<div class="media-left">
													<img class="media-object" src="<?php echo $author_avatar_url; ?>" alt="<?php echo $authorName; ?>">
												</div><!--media-left-->
												<div class="media-body">
													<h5 class="media-heading text-uppercase">
														<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo $authorName; ?></a>
														<?php echo classiera_author_verified($user_ID);?>
													</h5>
													<p><?php esc_html_e('Member Since', 'classiera') ?>&nbsp;<?php echo $classieraRegDate;?></p>
													<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php esc_html_e( 'see all HYST', 'classiera' ); ?></a>
													<?php if ( is_user_logged_in()){ 
														$current_user = wp_get_current_user();
														$user_id = $current_user->ID;
														if(isset($user_id)){
															if($user_ID != $user_id){							
																echo classiera_authors_follower_check($user_ID, $user_id);
															}
														}
													}												
													?>
													
												</div><!--media-body-->
											</div><!--media-->
										</div><!--author-info-->
									</div><!--widget-content-->
									<?php if($classieraAuthorInfo == 1){?>
									<div class="widget-content widget-content-post" >
									   <div class="contact-details widget-content-post-area" id="abc" style="
		display: none;
	">
											<h5 class="text-uppercase"><?php esc_html_e('Contact Details', 'classiera') ?> :</h5>
											<ul class="list-unstyled fa-ul c-detail">
												<?php if(!empty($authorPhone)){?>
												<li><i class="fa fa-li fa-phone-square"></i>&nbsp;
													<span class="phNum" data-replace="<?php echo $authorPhone;?>"><?php echo $authorPhone;?></span>
													<button type="button" id="showNum"><?php esc_html_e('Reveal', 'classiera') ?></button>
												</li>
												<?php } ?>
												<?php if(!empty($authorURL)){?>
												<li><i class="fa fa-li fa-globe"></i> 
													<a href="<?php echo $authorURL; ?>"><?php echo $authorURL; ?></a>
												</li>
												<?php } ?>
												<?php if(!empty($authorEmail)){?>
												<li><i class="fa fa-li fa-envelope"></i> 
													<a href="mailto:<?php echo $authorEmail; ?>"><?php echo $authorEmail; ?></a>
												</li>
												<?php } ?>
											</ul>
										</div><!--contact-details-->
									</div><!--widget-content-->
									<?php } ?>
								</div><!--widget-box-->
							</div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
							<?php } ?>
							<!--Widget for style 1-->
							<div class="col-lg-12 col-md-12 col-sm-6 match-height">
								<div class="widget-box <?php if($classieraSingleAdStyle == 2){echo "border-none";}?>">
									<?php 
									$classieraWidgetClass = "widget-content-post";
									$classieraMakeOffer = "user-make-offer-message widget-content-post-area";
									if($classieraSingleAdStyle == 1){
										$classieraWidgetClass = "widget-content-post";
										$classieraMakeOffer = "user-make-offer-message widget-content-post-area";
									}elseif($classieraSingleAdStyle == 2){
										$classieraWidgetClass = "removePadding";
										$classieraMakeOffer = "user-make-offer-message widget-content-post-area border-none removePadding";
									}
									?>
									<div class="widget-content <?php echo $classieraWidgetClass; ?>">
										<div class="<?php echo $classieraMakeOffer; ?>">
											<ul class="nav nav-tabs" role="tablist" id="views">
												<?php if($classieraToAuthor == 1){?>
												<li role="presentation" class="" id="views">
													<a href="#message" aria-controls="message" role="tab" data-toggle="tab"><i class="fa fa-envelope"></i><?php esc_html_e('Send Email', 'classiera') ?></a>
												</li>
												<?php } ?>
											</ul><!--nav nav-tabs-->
											<!-- Tab panes -->
											<div class="tab-content" id="def">
												<?php if($classieraToAuthor == 1){?>
												<div role="tabpanel" class="tab-pane " id="message">
												<!--ShownMessage-->
												<?php if(isset($_POST['submit']) && $_POST['submit'] == 'send_message'){?>
													<div class="row">
														<div class="col-lg-12">
															<?php if($hasError == true){ ?>
															<div class="alert alert-warning">
																<?php echo $errorMessage; ?>
															</div>
															<?php } ?>
															<?php if($emailSent == true){ ?>
															<div class="alert alert-success">
																<?php echo $classieraContactThankyou; ?>
															</div>
															<?php } ?>
														</div>
													</div>
													<?php } ?>
													<!--ShownMessage-->
													<form method="post" class="form-horizontal" data-toggle="validator" name="contactForm" action="<?php the_permalink(); ?>">
														<div class="form-group">
															<label class="col-sm-3 control-label" for="name"><?php esc_html_e('Name', 'classiera') ?> :</label>
															<div class="col-sm-9">
																<input id="name" data-minlength="5" type="text" class="form-control form-control-xs" name="contactName" placeholder="<?php esc_html_e('Type your name', 'classiera') ?>" required>
															</div>
														</div><!--name-->
														<div class="form-group">
															<label class="col-sm-3 control-label" for="email"><?php esc_html_e('Email', 'classiera') ?> :</label>
															<div class="col-sm-9">
																<input id="email" type="email" class="form-control form-control-xs" name="email" placeholder="<?php esc_html_e('Type your email', 'classiera') ?>" required>
															</div>
														</div><!--Email-->
														<div class="form-group">
															<label class="col-sm-3 control-label" for="subject"><?php esc_html_e('Subject', 'classiera') ?> :</label>
															<div class="col-sm-9">
																<input id="subject" type="text" class="form-control form-control-xs" name="subject" placeholder="<?php esc_html_e('Type your subject', 'classiera') ?>" required>
															</div>
														</div><!--Subject-->
														<div class="form-group">
															<label class="col-sm-3 control-label" for="msg"><?php esc_html_e('Msg', 'classiera') ?> :</label>
															<div class="col-sm-9">
																<textarea id="msg" name="comments" class="form-control" placeholder="<?php esc_html_e('Type Message', 'classiera') ?>" required></textarea>
															</div>
														</div><!--Message-->
														<?php 
															$classieraFirstNumber = rand(1,9);
															$classieraLastNumber = rand(1,9);
															$classieraNumberAnswer = $classieraFirstNumber + $classieraLastNumber;
														?>
														<div class="form-group">
															<div class="col-sm-9">
																<p>
																<?php esc_html_e("Please input the result of ", "classiera"); ?>
																<?php echo $classieraFirstNumber; ?> + <?php echo $classieraLastNumber;?> = 
																</p>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label" for="humanTest"><?php esc_html_e('Answer', 'classiera') ?> :</label>
															<div class="col-sm-9">
																<input id="humanTest" type="text" class="form-control form-control-xs" name="humanTest" placeholder="<?php esc_html_e('Your answer', 'classiera') ?>" required>
																<input type="hidden" name="humanAnswer" id="humanAnswer" value="<?php echo $classieraNumberAnswer; ?>" />
																<input type="hidden" name="classiera_post_title" id="classiera_post_title" value="<?php the_title(); ?>" />
																<input type="hidden" name="classiera_post_url" id="classiera_post_url" value="<?php the_permalink(); ?>"  />
															</div>
														</div><!--answer-->
														<input type="hidden" name="submit" value="send_message" />
														<button class="btn btn-primary btn-block btn-sm sharp btn-style-one" name="send_message" value="send_message" type="submit"><?php esc_html_e( 'Send Message', 'classiera' ); ?></button>
													</form>
												</div><!--message-->
												<?php } ?>
												<?php 
												$classieraPriceSection = $redux_demo['classiera_sale_price_off'];
												if($classieraPriceSection == 1){
												?>											
												<?php } ?>
											</div><!--tab-content-->
											<!-- Tab panes -->
										</div><!--user-make-offer-message-->
									</div><!--widget-content-->
								</div><!--widget-box-->
							</div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
							<?php if($classieraReportAd == 1){ ?>
							<div class="col-lg-12 col-md-12 col-sm-6 match-height">
								<div class="widget-box <?php if($classieraSingleAdStyle == 2){echo "border-none";}?>">
									<!--ReportAd-->
									<div class="widget-content widget-content-post">
										<div class="user-make-offer-message border-bottom widget-content-post-area">
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="btnWatch">    
												<?php if ( is_user_logged_in()){ 
														$current_user = wp_get_current_user();
														$user_id = $current_user->ID;
													}
													if(isset($user_id)){
														echo classiera_authors_favorite_check($user_id,$post->ID); 
													}
													?>
												</li>
													<?php /*<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>*/?>
	<script>
	jQuery(document).ready(function(){
		jQuery("#preview").click(function(){
			jQuery("#menu").toggle();
		});
	});
	</script>
												<li role="presentation" class="" id="preview">
													<a href="" aria-controls="report" role="tab" data-toggle="tab" ><i class="fa fa-exclamation-triangle"></i> <?php esc_html_e( 'Report', 'classiera' ); ?></a>
												</li>
											</ul>
											<!-- Tab panes -->
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane " id="menu" >
													<form method="post" class="form-horizontal" data-toggle="validator">
														<?php if(!empty($displayMessage)){?>
														<div class="alert alert-success">
															<?php echo $displayMessage; ?>
														</div>
														<?php } ?>
														<div class="radio" >
															<input id="illegal" value="illegal" type="radio" name="report_ad_val">
															<label for="illegal"><?php esc_html_e( 'This is illegal/fraudulent', 'classiera' ); ?></label>
															<input id="spam" value="spam" type="radio" name="report_ad_val">
															<label for="spam"><?php esc_html_e( 'This ad is spam', 'classiera' ); ?></label>
															<input id="duplicate" value="duplicate" type="radio" name="report_ad_val">
															<label for="duplicate"><?php esc_html_e( 'This ad is a duplicate', 'classiera' ); ?></label>
															<input id="wrong_category" value="wrong_category" type="radio" name="report_ad_val">
															<label for="wrong_category"><?php esc_html_e( 'This ad is in the wrong category', 'classiera' ); ?></label>
															<input id="post_rules" value="post_rules" type="radio" name="report_ad_val">
															<label for="post_rules"><?php esc_html_e( 'The ad goes against posting rules', 'classiera' ); ?></label>
															<input id="post_other" value="post_other" type="radio" name="report_ad_val">
															<label for="post_other"><?php esc_html_e( 'Other', 'classiera' ); ?></label>														
														</div>
														<div class="otherMSG">
															<textarea id="other_report" name="other_report" class="form-control"placeholder="<?php esc_html_e( 'Type here..!', 'classiera' ); ?>"></textarea>
														</div>
														<input type="hidden" name="classiera_post_title" id="classiera_post_title" value="<?php the_title(); ?>" />
														<input type="hidden" name="classiera_post_url" id="classiera_post_url" value="<?php the_permalink(); ?>"  />
														<input type="hidden" name="submit" value="report_to_admin" />
														<button class="btn btn-primary btn-block btn-sm sharp btn-style-one" name="report_ad" value="report_ad" type="submit"><?php esc_html_e( 'Report', 'classiera' ); ?></button>
													</form>
												</div><!--tabpanel-->
											</div><!--tab-content-->
										</div><!--user-make-offer-message-->
									</div><!--widget-content-->
									<!--ReportAd-->
								</div><!--widget-box-->
							</div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
							<?php } ?>
							<!--Social Widget-->
							<?php 
							include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
							if(is_plugin_active( "accesspress-social-share/accesspress-social-share.php")){
							?>
							<div class="col-lg-12 col-md-12 col-sm-6 match-height">
								<div class="widget-box <?php if($classieraSingleAdStyle == 2){echo "border-none";}?>">
									<!--Share-->
									<div class="widget-content widget-content-post">
										<div class="share border-bottom widget-content-post-area">
											<h5><?php esc_html_e( 'Share ad', 'classiera' ); ?>:</h5>
											<!--AccessPress Socil Login-->
											<?php echo do_shortcode('[apss-share]'); ?>
											<!--AccessPress Socil Login-->
										</div>
									</div>
									<!--Share-->
								</div><!--widget-box-->
								
							</div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
							<?php } ?>
							<!--Social Widget-->
							<div class="col-lg-12 col-md-12 col-sm-6 match-height">
								<div class="widget-box <?php if($classieraSingleAdStyle == 2){echo "border-none";}?>">
									<!--GoogleMAP-->
									<?php 
									global $redux_demo;
									$googleMapadPost = $redux_demo['google-map-adpost'];
									$locShownBy = $redux_demo['location-shown-by'];
									$post_location = get_post_meta($post->ID, $locShownBy, true);
									$post_latitude = get_post_meta($post->ID, 'post_latitude', true);
									$post_longitude = get_post_meta($post->ID, 'post_longitude', true);
									$post_address = get_post_meta($post->ID, 'post_address', true);
									$classieraMapStyle = $redux_demo['map-style'];
									$postCatgory = get_the_category( $post->ID );
									$postCurCat = $postCatgory[0]->name;
									if( has_post_thumbnail()){
										$classieraIMG = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
										$classieraIMGURL = $classieraIMG[0];
									}else{
										$classieraIMGURL = get_template_directory_uri() . '/images/nothumb.png';
									}								
									$iconPath = get_template_directory_uri() .'/images/icon-services.png';
									if($googleMapadPost == 1){
									?>
									<div class="widget-content widget-content-post">
										<div class="share widget-content-post-area">
											<h5><?php echo $post_location; ?></h5>
											<?php if(!empty($post_latitude)){?>
											<div id="classiera_single_map">
											<!--<div id="single-page-main-map" id="details_adv_map">-->
											<div class="details_adv_map" id="details_adv_map">
											<?php
											//echo '$post_latitude=>'.$post_latitude.'<br/>';
											//echo '$post_longitude=>'.$post_longitude.'<br/>';
											?>
												<script type="text/javascript">
												jQuery(document).ready(function(){
													var addressPoints = [							
														<?php 
														$content = '<a class="classiera_map_div" href="'.get_the_permalink().'"><img class="classiera_map_div__img" src="'.$classieraIMGURL.'" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">'.__( "Price", 'classiera').' : <span>'.$classieraPostPrice.'</span></p><h5 class="classiera_map_div__heading">'.get_the_title().'</h5><p class="classiera_map_div__cat">'.__( "Category", 'classiera').' : '.$postCurCat.'</p></div></a>';
														?>
														[<?php echo $post_latitude; ?>, <?php echo $post_longitude; ?>, '<?php echo $content; ?>', "<?php echo $iconPath; ?>"],							
													];
													var mapopts;
													if(window.matchMedia("(max-width: 1024px)").matches){
														var mapopts =  {
															dragging:false,
															tap:false,
														};
													};
													var map = L.map('details_adv_map', mapopts).setView([<?php echo $post_latitude; ?>,<?php echo $post_longitude; ?>],13);
													map.scrollWheelZoom.disable();
													var roadMutant = L.gridLayer.googleMutant({
													<?php if($classieraMapStyle){?>styles: <?php echo $classieraMapStyle; ?>,<?php }?>
														maxZoom: 13,
														type:'roadmap'
													}).addTo(map);
													var markers = L.markerClusterGroup({
														spiderfyOnMaxZoom: true,
														showCoverageOnHover: true,
														zoomToBoundsOnClick: true,
														maxClusterRadius: 60
													});
													//alert("here");
													if (navigator.geolocation) {
													  navigator.geolocation.getCurrentPosition(function(position) {
														var geolocation = {
														  lat: position.coords.latitude,
														  lng: position.coords.longitude
														};
														//alert(position.coords.latitude);
														//alert(position.coords.longitude);
														$("#current_latitude").val(position.coords.latitude);
														$("#current_longitude").val(position.coords.longitude);
														//get_found_prices_locations(position.coords.latitude, position.coords.longitude);
														var circle = new google.maps.Circle({
														  center: geolocation,
														  radius: position.coords.accuracy
														});
														autocomplete.setBounds(circle.getBounds());
													  });
													}
												
													var markerArray = [];
													for (var i = 0; i < addressPoints.length; i++){
														var a = addressPoints[i];
														var newicon = new L.Icon({iconUrl: a[3],
															iconSize: [50, 50], // size of the icon
															iconAnchor: [20, 10], // point of the icon which will correspond to marker's location
															popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor                                 
														});
														var title = a[2];
														var marker = L.marker(new L.LatLng(a[0], a[1]));
														marker.setIcon(newicon);
														marker.bindPopup(title);
														marker.title = title;
														marker.on('click', function(e) {
															map.setView(e.latlng, 13);
															
														});				
														markers.addLayer(marker);
														markerArray.push(marker);
														if(i==addressPoints.length-1){//this is the case when all the markers would be added to array
															var group = L.featureGroup(markerArray); //add markers array to featureGroup
															map.fitBounds(group.getBounds());   
														}
													}
													map.addLayer(markers);
												});

	  
												</script>
												
												 <script>
											  // This example displays an address form, using the autocomplete feature
											  // of the Google Places API to help users fill in the information.
										
											  // This example requires the Places library. Include the libraries=places
											  // parameter when you first load the API. For example:
											  // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
										
											  var placeSearch, autocomplete;
											  var componentForm = {
												street_number: 'short_name',
												route: 'long_name',
												locality: 'long_name',
												administrative_area_level_1: 'short_name',
												country: 'long_name',
												postal_code: 'short_name'
											  };
										
											  function initAutocomplete() {
												// Create the autocomplete object, restricting the search to geographical
												// location types.
												autocomplete = new google.maps.places.Autocomplete(
													/** @type {!HTMLInputElement} */(document.getElementById('found_location')),
													{types: ['geocode']});
										
												// When the user selects an address from the dropdown, populate the address
												// fields in the form.
												//autocomplete.addListener('place_changed', fillInAddress);
											  }
										
											  function fillInAddress() {
												// Get the place details from the autocomplete object.
												var place = autocomplete.getPlace();
										
												for (var component in componentForm) {
												  document.getElementById(component).value = '';
												  document.getElementById(component).disabled = false;
												}
										
												// Get each component of the address from the place details
												// and fill the corresponding field on the form.
												for (var i = 0; i < place.address_components.length; i++) {
												  var addressType = place.address_components[i].types[0];
												  if (componentForm[addressType]) {
													var val = place.address_components[i][componentForm[addressType]];
													document.getElementById(addressType).value = val;
												  }
												}
											  }
										
											  // Bias the autocomplete object to the user's geographical location,
											  // as supplied by the browser's 'navigator.geolocation' object.
											  function geolocate() {
												if (navigator.geolocation) {
												  navigator.geolocation.getCurrentPosition(function(position) {
													var geolocation = {
													  lat: position.coords.latitude,
													  lng: position.coords.longitude
													};
													jQuery("#found_latitude").val(position.coords.latitude);
													jQuery("#found_longitude").val(position.coords.longitude);
													var circle = new google.maps.Circle({
													  center: geolocation,
													  radius: position.coords.accuracy
													});
													autocomplete.setBounds(circle.getBounds());
												  });
												}
											  }
											  
											   function getlatlong1(val, flg) {
											//alert(val);
											//alert(jQuery("#found_location1").val())
												$.ajax({
													url: '<?php echo site_url()?>/get_latlong_from_address.php',
													
													type: 'POST',
													// This is query string i.e. country_id=123
													data: {address : val},
													success: function(data) {
														if(flg==0)
														{
															 getlatlong1($("#found_location1").val(), 1);
														}
														else
														{
															
																if(data=='')
																{
																	 getlatlong1($("#found_location").val(), 1);
																}
																else
																{
																 $("#found_latlong").val(data);
																}
														}
													
													},
													error: function(jqXHR, textStatus, errorThrown) {
														alert(errorThrown);
													}
												});
											
										
									   }
		   
											</script>	
											
											
				<?php /*<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>*/?>
						 <script type="text/javascript">
						 jQuery(document).ready(function() {
						 
						   jQuery("def").hide();
						  jQuery("#views").click(function() {
						 
						   jQuery("#def").show();
						
						 });
						
						});
						 </script>
											</div>
											<div id="ad-address" style="display:none;">
												<span>
												<i class="fa fa-map-marker"></i>
												<a href="http://maps.google.com/maps?saddr=&daddr=<?php echo $post_address;?>" target="">
													<?php esc_html_e( 'Get Directions on Google MAPS to', 'classiera' ); ?>: <?php echo $post_address;?>
												</a>
												</span>
											</div>
										</div>
											<?php } ?>
											
											<div class="map_discription single_page_map">
												<ul>
													 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/green_marker.png'; ?>"/> &nbsp; <span>Verified</span></li>
													 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/yellow_marker.png'; ?>"/> &nbsp; <span>Pending</span></li>
													 <li><img src="<?=get_template_directory_uri().'/../classiera-child/images/red_marker.png'; ?>"/> &nbsp; <span>unconfirmed</span></li>
												</ul>
											</div>	
										
										</div>
									</div>
									<?php } ?>
									<!--GoogleMAP-->
								</div><!--widget-box-->
							</div><!--col-lg-12-->
							<!--SidebarWidgets-->
							<?php dynamic_sidebar('single'); ?>
							<!--SidebarWidgets-->
						</div><!--row-->
					</aside><!--sidebar-->
				</div><!--col-md-4-->
				<?php 
				}?>
			</div><!--row-->
		</div><!--container-->
    </div>
</section>
<?php endwhile; ?>
<!-- related post section -->
<?php 
global $redux_demo;
$relatedAdsOn = $redux_demo['related-ads-on'];
if($relatedAdsOn == 1){
	function related_Post_ID(){
		global $post;
		$post_Id = $post->ID;
		return $post_Id;
	}
	get_template_part( 'templates/related-ads' );
}
?>
<!-- Company Section Start-->
<?php 
	global $redux_demo; 
	$classieraCompany = $redux_demo['partners-on'];
	$classieraPartnersStyle = $redux_demo['classiera_partners_style'];
	if($classieraCompany == 1){
		if($classieraPartnersStyle == 1){
			get_template_part('templates/members/memberv1');
		}elseif($classieraPartnersStyle == 2){
			get_template_part('templates/members/memberv2');
		}elseif($classieraPartnersStyle == 3){
			get_template_part('templates/members/memberv3');
		}elseif($classieraPartnersStyle == 4){
			get_template_part('templates/members/memberv4');
		}elseif($classieraPartnersStyle == 5){
			get_template_part('templates/members/memberv5');
		}elseif($classieraPartnersStyle == 6){
			get_template_part('templates/members/memberv6');
		}
	}
?>
<!-- Company Section End-->	
<!-- related post section -->
<?php get_footer(); ?>
<script type="text/javascript" language="javascript">
$( document ).ready(function() {
//alert($("#current_latitude").val());
		/*if($("#current_latitude").val()!='')
		{
			get_found_prices_locations();
		}*/
		get_found_prices_locations();
	/*function get_found_prices_locations_1()
	{
		alert("1");
		get_found_prices_locations_2();
	}
	function get_found_prices_locations_2()
	{
		alert("2");
		get_found_prices_locations_3();
	}
	function get_found_prices_locations_3()
	{
		alert("3");
		get_found_prices_locations();
	}*/
	function get_found_prices_locations(lat,long,limit=0)
	{
	//alert("i am here");
	var curlat='';
			var curlong='';
		 navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {
			  lat: position.coords.latitude,
			  lng: position.coords.longitude
			};
			//alert(position.coords.latitude);
			//alert(position.coords.longitude);
			$("#current_latitude").val(position.coords.latitude);
			$("#current_longitude").val(position.coords.longitude);
			curlat=position.coords.latitude;
			curlong=position.coords.longitude;
			get_location_to_display(curlat,curlong,limit);
			var circle = new google.maps.Circle({
			  center: geolocation,
			  radius: position.coords.accuracy
			});
			autocomplete.setBounds(circle.getBounds());
		  });
		//alert("4");
		
	}
	
	});
	function get_location_to_display(curlat,curlong,limit=0)
	{
		$.ajax({
			url: '<?php echo site_url()?>/get_default_found_location_price.php',
			
			type: 'POST',
			// This is query string i.e. country_id=123
			data: {current_latitude : curlat, current_longitude: curlong, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>, displimit:limit},
		<?php /*data: {current_latitude : lat, current_longitude: long, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>},*/?>
			success: function(data) {
			//alert("Result");
				//alert(data);
				$("#found_main_div").html(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	}
	function get_location_to_display_filter_by_price(val)
	{
		if(val=='low')
		{
			$.ajax({
				url: '<?php echo site_url()?>/get_default_found_location_price.php',
				
				type: 'POST',
				// This is query string i.e. country_id=123
				data: {current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>, price:'low'},
			<?php /*data: {current_latitude : lat, current_longitude: long, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>},*/?>
				success: function(data) {
				//alert("Result");
					//alert(data);
					$("#found_main_div").html(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert(errorThrown);
				}
			});
		}
		else if(val=="high")
		{
			$.ajax({
				url: '<?php echo site_url()?>/get_default_found_location_price.php',
				
				type: 'POST',
				// This is query string i.e. country_id=123
				data: {current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>, price:'high'},
			<?php /*data: {current_latitude : lat, current_longitude: long, current_post_id: <?php echo $current_post_id;?>, current_user_id:<?php echo $current_user->ID;?>},*/?>
				success: function(data) {
				//alert("Result");
					//alert(data);
					$("#found_main_div").html(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert(errorThrown);
				}
			});
		}
	}
	
</script>