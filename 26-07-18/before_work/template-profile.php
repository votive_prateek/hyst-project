<?php
/**
 * Template name: Profile Page
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Classiera
 * @since Classiera
 */

if ( !is_user_logged_in() ) { 

	global $redux_demo; 
	$login = $redux_demo['login'];
	wp_redirect( $login ); exit;

}


global $redux_demo; 
$edit = $redux_demo['edit'];
$pagepermalink = get_permalink($post->ID);
if(isset($_GET['delete_id'])){
	$deleteUrl = $_GET['delete_id'];
	wp_update_post(array('ID'=> $deleteUrl,'post_status'=>'draft'));
	//wp_delete_post($deleteUrl);
}
if(isset($_POST['unfavorite'])){
	$author_id = $_POST['author_id'];
	$post_id = $_POST['post_id'];
	echo classiera_authors_unfavorite($author_id, $post_id);	
}
global $current_user, $user_id;
$current_user = wp_get_current_user();

$user_info = get_userdata($user_ID);
$user_id = $current_user->ID; // You can set $user_id to any users, but this gets the current users ID.

    $classieraAuthorEmail = $current_user->user_email;
	$classieraAuthorIMG = get_user_meta($user_id, "classify_author_avatar_url", true);
	$classieraAuthorIMG = classiera_get_profile_img($classieraAuthorIMG);
	if(empty($classieraAuthorIMG)){
		$classieraAuthorIMG = classiera_get_avatar_url ($classieraAuthorEmail, $size = '150' );
	}

get_header(); 
 
$first_name_visible = get_the_author_meta('first_name_visible', $user_id );
$last_name_visible = get_the_author_meta('last_name_visible', $user_id );
$job_visible = get_the_author_meta('job_visible', $user_id );
$location_visible = get_the_author_meta('location_visible', $user_id );
$description_visible = get_the_author_meta('description_visible', $user_id );
$my_interest_visible = get_the_author_meta('my_interest_visible', $user_id );
$phone_visible = get_the_author_meta('phone_visible', $user_id );
$phone2_visible = get_the_author_meta('phone2_visible', $user_id );
$website_visible = get_the_author_meta('website_visible', $user_id );
$email_visible = get_the_author_meta('email_visible', $user_id );
$facebook_visible = get_the_author_meta('facebook_visible', $user_id );
$twitter_visible = get_the_author_meta('twitter_visible', $user_id );
$googleplus_visible = get_the_author_meta('googleplus_visible', $user_id );
$instagram_visible = get_the_author_meta('instagram_visible', $user_id );
$pinterest_visible = get_the_author_meta('pinterest_visible', $user_id );
$linkedin_visible = get_the_author_meta('linkedin_visible', $user_id );
$vimeo_visible = get_the_author_meta('vimeo_visible', $user_id );
$youtube_visible = get_the_author_meta('youtube_visible', $user_id );
?>
<?php 
global $redux_demo; 
$all_adds = $redux_demo['all-ads'];
if (function_exists('icl_object_id')){
	$templateUSERAllAds = 'template-user-all-ads.php';
	$all_adds = classiera_get_template_url($templateAllAds);
}
$classiera_cart_url = $redux_demo['classiera_cart_url'];
$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
$bumpProductID = $redux_demo['classiera_bump_ad_woo_id'];
$dateFormat = get_option( 'date_format' );
?>
<?php 
	$page = get_page($post->ID);
	$current_page_id = $page->ID;
	$iconClass = 'icon-left';
	if(is_rtl()){
		$iconClass = 'icon-right';
	}
?>
<link rel='stylesheet' id='dashicons-css' href='<?php echo site_url(); ?>/wp-includes/css/dashicons.min.css?ver=4.9.7' type='text/css' media='all' />
<!-- user pages -->
<section class="user-pages section-gray-bg user_section other_user_page all_usertemplate">
	<div class="container">
        <div class="row">
 
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
				<div class="user-detail-section section-bg-white ">
				    <div class="details_section"> 
					    <!--author-info-->
						<div class="author-info">
							<div class="media">
							<?php
										$author_id=$current_user->ID; 
										//$author_id=18;
					$profileIMGID = get_user_meta($author_id, "classify_author_avatar_url", true);
									$profileIMG = classiera_get_profile_img($profileIMGID);
									$authorName = get_the_author_meta('display_name', $author_id);
									$author_verified = get_the_author_meta('author_verified', $author_id);
									?>
								<?php /*<img class="media-object" src="<?php echo $classieraAuthorIMG; ?>" alt="<?php echo $classieraDisplayName;  ?>">*/
								$is_approve_prof_pic=get_user_meta($author_id,'profile_pic_approve');
								if($is_approve_prof_pic[0]=='1')
								{
								if(empty($profileIMG)){
										$profileIMG = classiera_get_avatar_url ( get_the_author_meta('user_email', $author_id), $size = '150' );
										?>
									<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="max-width:120px; max-height:120px;">
									<?php
									}else{ ?>
									<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="max-width:120px; max-height:120px;">	
									<?php }
								}
								else
								{?>
									<img src="<?=get_template_directory_uri().'/../classiera-child/images/user-icon.png' ?>"  alt="<?php echo $classieraDisplayName;  ?>"/><?php
									
								} ?>
								<div class="media-body">
								    <hr class="line1"/>
									
									<h2 class="user_name">hello : <?php echo $authorName;?></h2>
								    <hr class="line1"/>
									<div class="join_dis">
									     <div class="top-buffer2"></div>
									     <p>
										 <?php
										  $udata = get_userdata( $current_user->ID );
										 
										  $registered = $udata->user_registered;?>
										 joined <?php echo date("jS F Y", strtotime($registered));?></p>
									     <p>
										 <?php
										// echo '$current_user->ID:'.$current_user->ID;
										 $last_login=get_user_meta($current_user->ID, "last_login");
										// print_r($last_login);
										//echo '=>';
										//print_r($last_login);
										//echo '<=';
										 ?>
										 <!--last log on 11th September 2018-->
										 last log on <?php echo date("jS F Y", $last_login[0]);?></p>
										 <div class="top-buffer2"></div>
									</div>
								    <hr class="line2"/>
								</div><!--media-body-->
							</div><!--media-->
						</div><!--author-info-->
						
						<div class="top-buffer2"></div>
						<!-- about me -->
						<?php if ($description_visible=='publically') { ?>
						<div class="about-me text-center">
							<h4 class="user-detail-section-heading border0 user-title"><?php 
							$user = wp_get_current_user();
							$roles = $user->roles;
							if(in_array('inventory_user',$roles))
							{
								esc_html_e("about us:", 'classiera');
							}
							else if(in_array('company_user',$roles))
							{
								esc_html_e("about us:", 'classiera');
							}
							else
							{
								esc_html_e("about me:", 'classiera');
							} ?></h4>
							<p style="text-align:center;"><?php $user_id = $current_user->ID; $author_desc = get_the_author_meta('description', $user_id); echo $author_desc; ?></p>
							<div class="top-buffer2"></div>
						</div>
						<hr class="line2"/>
						<?php } ?>
						<div class="top-buffer2"></div>
						<!-- about me -->
						
						<?php if ($my_interest_visible=='publically') { ?>
						<div class="interests about-me text-center">
							<h4 class="user-detail-section-heading border0 user-title"><?php esc_html_e("interests", 'classiera') ?></h4>
							<p><?php  $interest=get_user_meta($user_id, 'interests');
							$interest_exp=explode(",",$interest[0]);
							for($lp=0;$lp<count($interest_exp);$lp++)
							{
								if($lp==0)
									echo $interest_exp[$lp];
								else
									echo ', '.$interest_exp[$lp];
									
							}
							 //echo $interest[0];?></p>
							<div class="top-buffer2"></div>
							<hr class="line2"/>
							<div class="top-buffer2"></div>
						</div>
						<?php } ?>
						
						
						<!-- contact details -->
						<?php if($phone_visible=='publically' || $phone2_visible=='publically' || $website_visible=='publically' || $email_visible=='publically') { ?>
						<div class="user-contact-details text-center">
							<h4 class="user-detail-section-heading border0 user-title">
								<?php esc_html_e("contact details", 'classiera') ?>
							</h4>
							<style>
							.user-pages .user-detail-section .user-contact-details ul li{font-size:12px !important;}
							</style>
							<div class="text-center">
								<ul class="list-unstyled text-left contact_icon inline-block">
									<?php 
									$userPhone = get_the_author_meta('phone', $user_id);
									$userPhone2 = get_the_author_meta('phone2', $user_id);
									$userEmail = get_the_author_meta('user_email', $user_id);
									$userwebsite = get_the_author_meta('user_url', $user_id);
									if(in_array('inventory_user',$roles))
									{
										 if(!empty($userwebsite) && ($website_visible=='publically')){?>
										<li>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-1.png'?>"/>
											<a href="<?php echo $userwebsite; ?>"><?php echo $userwebsite; ?></a>
										</li>
										<?php } 
										
									} else if(in_array('company_user',$roles)) { ?>
										<li>
										<?php /*<i class="fa fa-phone-square"></i>*/?>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-4.png'?>"/>
											<a href="tel:<?php echo $userPhone; ?>"><?php echo $userPhone; ?></a>
										</li>
										<li>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-3.png'?>"/>
											<a href="tel:<?php echo $userPhone2; ?>"><?php echo $userPhone2; ?></a>
										</li>
										<?php if(!empty($userwebsite) && ($website_visible=='publically')){ ?> 	
										<li>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-1.png'?>"/>
											<a href="<?php echo $userwebsite; ?>"><?php echo $userwebsite; ?></a>
										</li>
										<?php } ?>
										<li>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-2.png'?>"/>
											<a href="mailto:<?php echo $userEmail; ?>"><?php echo $userEmail; ?></a>
										</li>
									<?php } else { ?>
									<?php if(!empty($userPhone) && $phone_visible=='publically'){?>
										<li>
										<?php /*<i class="fa fa-phone-square"></i>*/?>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-4.png'?>"/>
											<a href="tel:<?php echo $userPhone; ?>"><?php echo $userPhone; ?></a>
										</li>
										<?php } ?>
										<?php if(!empty($userPhone2) && $phone2_visible=='publically'){?>
										<li>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-3.png'?>"/>
											<a href="tel:<?php echo $userPhone2; ?>"><?php echo $userPhone2; ?></a>
										</li>
										<?php } ?>
										<?php if(!empty($userwebsite) && ($website_visible=='publically')){?>
										<li>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-1.png'?>"/>
											<a href="<?php echo $userwebsite; ?>"><?php echo $userwebsite; ?></a>
										</li>
										<?php } ?>
										<?php if(!empty($userEmail) && $email_visible=='publically'){?>
										<li>
											<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-2.png'?>"/>
											<a href="mailto:<?php echo $userEmail; ?>"><?php echo $userEmail; ?></a>
										</li>
										<?php } 
									}?>
								</ul>
							</div>
						</div>
						<div class="top-buffer2"></div>
						 <hr class="line3"/>
						<?php } ?>
						<!-- contact details -->
						<div class="top-buffer2"></div>
						<!-- social profile -->
						<div class="user-social-profile-links">
							<h4 class="user-detail-section-heading border0 user-title">
								<?php esc_html_e("our social media links", 'classiera') ?>
							</h4>
							<ul class="list-unstyled list-inline">
							<?php 
								$userFB = $user_info->facebook;
								$userTW = $user_info->twitter;
								$userGoogle = $user_info->googleplus;
								$userPin = $user_info->pinterest;
								$userLin = $user_info->linkedin;
								$userInsta = $user_info->instagram;
								$userVimeo = $user_info->vimeo;
								$userYouTube = $user_info->youtube;
							?>
							<?php if(!empty($userFB) && $facebook_visible=='publically'){?>
								<li>
									<a href="<?php echo $userFB; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/fb.png'?>"/></a>
								</li>
							<?php } ?>
							<?php if(!empty($userTW) && $twitter_visible=='publically'){?>
								<li>
									<a href="<?php echo $userTW; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/tw.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userGoogle) && $googleplus_visible=='publically'){?>	
								<li>
									<a href="<?php echo $userGoogle; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/gplus.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userInsta) && $instagram_visible=='publically'){?>	
								<li>
									<a href="<?php echo $userInsta; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/insta.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userPin) && $pinterest_visible=='publically'){?>	
								<li>
									<a href="<?php echo $userPin; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/pin.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userLin) && $linkedin_visible=='publically'){?>	
								<li>
									<a href="<?php echo $userLin; ?>" class="linkin_icon"><img src="<?=get_template_directory_uri().'/../classiera-child/images/linkin.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userVimeo) && $vimeo_visible=='publically'){?>	
								<li>
									<a href="<?php echo $userVimeo; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/vimeo.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userYouTube) && $youtube_visible=='publically'){?>	
								<li>
									<a href="<?php echo $userYouTube; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/wl.png'?>"/></a>
								</li>
							<?php } ?>	
							</ul>
						</div>
						<!-- social profile -->
						<div class="clearfix"></div>
						<div class="top-buffer2"></div>
						 <hr class="line3"/>
						<div class="top-buffer2"></div>
					
					</div>
					<?php
					$branch_count=get_user_meta($user_id, "branch_count");
					if(count($branch_count)>0 && ($location_visible=='publically'))
					{?>
					<div class="branch-section">
					     <div class="row">
							<div class="single_title">
								<h2 class="user-detail-section-heading">our branches</h2>
							</div>
						</div>					 
						
						<div class="top-buffer2"></div>
						<div class="clearfix"></div>
						<div class="col-md-10 col-md-offset-1">
						     <?php
								
								for($lp=1;$lp<=$branch_count[0];$lp++)
								{?>
									<?php $branch_location=get_user_meta($user_id, "branch_location".$lp);
									if($branch_location[0]!='')
									{ ?>
									<div class="col-sm-6">
										 <div class="our_branch">
											<a href="#">
											
											    <p><img src="<?=get_template_directory_uri().'/../classiera-child/images/branch_icon.png' ?>" height="85px" width="85px"/></p>
										           											   
										         <p><span><?php echo $branch_location[0];?></span></p>
											</a>
										</div>
									</div>
									<?php
									}
								}
								?>
						</div>
					</div>
					<?php
					}?>
					
					<div class="clearfix"></div>
					<div class="top-buffer2"></div>
					<div class="clearfix"></div>
					<!-- my ads -->
					<div class="user-ads user-my-ads">
					    <div class="row">
							<div class="single_title">
								<h2 class="user-detail-section-heading">
									<?php
									$user = wp_get_current_user();
									$roles = $user->roles;
									//print_r($roles);
									if(in_array('inventory_user',$roles))
									{
										esc_html_e("MY Inventory", 'classiera');
									}
									else if(in_array('company_user',$roles))
									{
										esc_html_e("MY Inventory", 'classiera');
									}
									else
									{
										esc_html_e("MY HYSTS", 'classiera');
									} ?>	
								</h2>
							</div>
						
						<div class="top-buffer2"></div>
						<div class="clearfix"></div>
						<?php 
							global $wp_query, $wp;
							$wp_query = new WP_Query();
							$kulPost = array(
								'post_type'  => 'post',
								'author' => $user_id,
								'posts_per_page' => 10,	
								'post_status' => array( 'publish', 'pending', 'future', 'private' ),
							);
							$wp_query = new WP_Query($kulPost);
						while ($wp_query->have_posts()) : $wp_query->the_post();
						$title = get_the_title($post->ID);						
						$classieraPstatus = get_post_status( $post->ID );						
						$postDate = get_the_date($dateFormat, $post->ID);
						$postStatus = get_post_status($post->ID);
						$productID = get_post_meta($post->ID, 'pay_per_post_product_id', true);
						$days_to_expire = get_post_meta($post->ID, 'days_to_expire', true);
						$chk_is_featured = get_post_meta($post->ID, 'featured_post', true);
						?>
						<div class="search_page_item">
							<div class="col-lg-3 col-md-4 col-sm-6 match-height item item-grid">
								<div class="classiera-box-div classiera-box-div-v1">
									<figure class="clearfix">
										<?php if ($chk_is_featured==1) { ?>
										<div class="feature_image">
											 <img src="<?=get_template_directory_uri().'/../classiera-child/images/featured.png' ?>" />
										</div>	
										<div class="xs-feature_img visible-xs">
										     <img src="<?=get_template_directory_uri().'/../classiera-child/images/mob-nav-1.png' ?>" height="50px" width="50px">
											 <div class="clearfix"></div>
										     <span class="label label-large label-grey arrowed-in-right arrowed-in">featured</span>
										</div>
										<?php } ?>
										
										
										
										<div class="premium-img">
											<?php 
												if ( has_post_thumbnail()){								
												$imgURL = get_the_post_thumbnail_url();
												?>
												<a href="<?php echo get_permalink($post->ID);?>"><img class="media-object img-responsive" src="<?php echo $imgURL; ?>" alt="<?php echo $title; ?>"></a>
												<?php } ?>
										</div><!--premium-img-->
										
										
										<figcaption class="pad0 text-left">
											<div class="product-body pad15">
												<h5 class="media-heading"><a href="<?php echo get_permalink($post->ID);?>"><?php echo $title; ?></a></h5>
												
												<div class="top-buffer1"></div>
												<p class="justify pad0 black"> This is for test<a href="<?php echo get_permalink($post->ID);?>" class="expend_icon"><img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>"></a> </p>
												
												<div class="top-buffer1"></div>
												<?php /*<h3 class="user_details"><img src="<?=get_template_directory_uri().'/../classiera-child/images/user.png'?>"> user : jane doe</h3>*/?>
												<div class="top-buffer1"></div>	
											
												<p>
													<?php /*<span class="published">
														<i class="fa fa-check-circle"></i>
														<?php classieraPStatusTrns($classieraPstatus); ?>
													</span><br/>
													<span>
														<i class="fa fa-eye"></i>
														<?php echo classiera_get_post_views($post->ID); ?>
														</span><br/>*/?>
													<span>
														<i class="fa fa-clock-o"></i>
														<?php echo $postDate; ?>
													</span><br/>
													<span>
														<i class="removeMargin fa fa-hashtag"></i>
														<?php esc_html_e( 'ID', 'classiera' ); ?> :                                        
														<?php echo $post->ID; ?>
													</span>
												</p>
												<!--BumpAds-->
													<?php 
														global $redux_demo;
														$edit_post_page_id = $redux_demo['edit_post'];
														$postID = $post->ID;
														global $wp_rewrite;
														if ($wp_rewrite->permalink_structure == ''){
															//we are using ?page_id
															$edit_post = $edit_post_page_id."&post=".$post->ID;
															$del_post = $pagepermalink."&delete_id=".$post->ID;
														}else{
															//we are using permalinks
															$edit_post = $edit_post_page_id."?post=".$post->ID;
															$del_post = $pagepermalink."?delete_id=".$post->ID;
														}
													if(get_post_status( $post->ID ) !== 'private'){ 	
													?>
													<div class="col-xs-6">
													    <a href="<?php echo $edit_post; ?>" class="btn btn-primary sharp btn-style-one btn-sm"><i class="<?php echo $iconClass; ?> fa fa-pencil-square-o"></i><?php esc_html_e("Edit", 'classiera') ?></a>
													</div>
													<?php } ?>
													<div class="col-xs-6">
													     <a class="thickbox btn btn-primary sharp btn-style-one btn-sm" href="#TB_inline?height=150&amp;width=400&amp;inlineId=examplePopup<?php echo $post->ID; ?>"><i class="<?php echo $iconClass; ?> fa fa-trash-o"></i><?php esc_html_e("Delete", 'classiera') ?></a>
														 <div class="delete-popup" id="examplePopup<?php echo $post->ID; ?>" style="display:none">
															<h4><?php esc_html_e("Are you sure you want to delete this?", 'classiera') ?></h4>
															<a class="btn btn-primary sharp btn-style-one btn-sm" href="<?php echo $del_post; ?>">
															<span class="button-inner"><?php esc_html_e("Confirm", 'classiera') ?></span>
															</a>
														</div>
											        </div>
													<div class="clearfix"></div>
											</div>
										</figcaption>
										
									</figure>
								</div>
							</div>
						</div>
						<?php endwhile;	?>
						<?php wp_reset_query(); ?>
						</div>
						<div class="user-view-all text-center">
                            <a href="<?php echo $all_adds; ?>" class="btn btn-primary btn-md btn-style-one sharp">
								<?php esc_html_e("View All", 'classiera') ?>
							</a>
                        </div>
					</div>
					<!-- my ads -->
					<!-- favorite ads -->
					<div class="user-ads favorite-ads favorite_add_sec">
					    <div class="row">
							<div class="single_title">
									<h2 class="user-detail-section-heading">
									<?php esc_html_e("Favorite HYST", 'classiera') ?>
									</h2>
							</div>
						</div>
						
						<div class="top-buffer2"></div>
						<div class="clearfix"></div>
						<?php 
							global $paged, $wp_query, $wp;
							$args = wp_parse_args($wp->matched_query);
							if ( !empty ( $args['paged'] ) && 0 == $paged ) {
								$wp_query->set('paged', $args['paged']);
								$paged = $args['paged'];
							}
							$cat_id = get_cat_ID(single_cat_title('', false));
							$temp = $wp_query;
							$wp_query= null;
							$wp_query = new WP_Query();
							global $current_user;
							wp_get_current_user();
							$user_id = $current_user->ID;
							$myarray = classiera_authors_all_favorite($user_id);
							if(!empty($myarray)){
								$args = array(
								   'post_type' => 'post',
								   'posts_per_page' => 10,
								   'post__in' => $myarray,
								);
								// The Query
							$wp_query = new WP_Query( $args );
							while ($wp_query->have_posts()) : $wp_query->the_post();
							$postDate = get_the_date($dateFormat, $post->ID);
							$title = get_the_title($post->ID);
							$post_price = get_post_meta($post->ID, 'post_price', true);
							$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
							if(is_numeric($post_price)){
								$post_price =  classiera_post_price_display($post_currency_tag, $post_price);
							}else{ 
								$post_price =  $post_price; 
							}
						?>
					
					<div class="col-sm-6  col-lg-4">
						<div class="media">							
							<div class="product_image">
									<?php 
									if ( has_post_thumbnail()){								
									$imgURL = get_the_post_thumbnail_url();
									?>
									<img class="media-object" src="<?php echo $imgURL; ?>" alt="<?php echo $title; ?>">
									<?php } ?>
							</div><!--media-left-->							
							<div class="product-body">
								<h5 class="media-heading">title : <a href="<?php echo get_permalink($post->ID);?>"><?php echo $title; ?></a>
								</h5>
								<?php /*<h5 class="media-heading">Colour: Black</h5>*/
									
									$post_price = get_post_meta($post->ID, 'post_price', true);
									if($post_price!='')
									{?>
									<h5 class="media-heading">Price R<?php echo $post_price;?></h5>
									<?php
									}?>
								<h5 class="media-heading">Description:</h5>
								<p class="justify"><span><?php echo substr(get_the_excerpt($post->ID), 0,260).'</span>&nbsp;<a class="more-link" href="' . get_permalink($post->ID) . '">&nbsp;<img src="'.get_template_directory_uri().'/../classiera-child/images/down_arrow.png" class="read_more" height="10px" width="10px"></a>'; ?></p>
									
								<p>
									<?php $post_user_ID = $post->post_author; ?>
                                    <span>
                                        <i class="fa fa-user"></i>
                                        <?php echo get_the_author_meta('display_name', $post_user_ID ); ?>
                                    </span>                                    
                                    <span>
                                        <i class="fa fa-clock-o"></i>
                                        <?php echo $postDate; ?>
                                    </span>
                                </p>
								
								<?php  ?>
                               <?php echo classiera_authors_favorite_remove($user_id, $post->ID);?>
								
							</div>
								
						</div>
					</div>	<!--media border-bottom-->
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>
						<?php }else{ ?>
							<p><?php esc_html_e("You do not have any favorite HYST yet!", 'classiera') ?></p>
						<?php } ?>
					 
					</div>
					<div class="clearfix"></div>
					<!-- favorite ads -->
				</div>
			</div>
		</div><!--row-->
	</div><!--container-->
</section>
<!-- user pages -->
<?php get_footer(); ?>