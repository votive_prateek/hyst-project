<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage ClassiEra
 * @since ClassiEra 1.0
 */

?>
<?php 
	global $redux_demo;	
	$classieraCopyRight = $redux_demo['footer_copyright'];
	$classierabackToTop = $redux_demo['backtotop'];
	$classieraGoogleAnalytics = $redux_demo['google_analytics'];
	$classieraFooterWidgets = $redux_demo['footer_widgets_area_on'];
	$classieraFooterStyle = $redux_demo['classiera_footer_style'];
	$classieraFooterBottomStyle = $redux_demo['classiera_footer_bottom_style'];
	$footerStyleClass = 'section-bg-black section-bg-light-img';
	$classieraFacebook = $redux_demo['facebook-link'];
	$classieraTwitter = $redux_demo['twitter-link'];
	$classieraDribbble = $redux_demo['dribbble-link'];
	$classieraFlickr = $redux_demo['flickr-link'];
	$classieraGithub = $redux_demo['github-link'];
	$classieraPinterest = $redux_demo['pinterest-link'];	
	$classieraYouTube = $redux_demo['youtube-link'];
	$classieraGoogle = $redux_demo['google-plus-link'];
	$classieraLinkedin = $redux_demo['linkedin-link'];
	$classieraInstagram = $redux_demo['instagram-link'];
	$classieraVimeo = $redux_demo['vimeo-link'];
	$classieraInbox = $redux_demo['classiera_inbox_page_url'];
	if($classieraFooterStyle == 'three'){
		$footerStyleClass = 'section-bg-black section-bg-light-img';
	}elseif($classieraFooterStyle == 'four'){
		$footerStyleClass = 'section-bg-black four-columns-footer';
	}
	if (function_exists('icl_object_id')){
		$templateMessage = 'template-message.php';
		$classieraInbox = classiera_get_template_url($templateMessage);
	}
	if($classieraGoogleAnalytics){
		echo $classieraGoogleAnalytics;
	}
?>

<div class="clearfix"></div>


<footer class="<?php if($classieraFooterWidgets == 1){ echo "section-pad"; }?> <?php echo $footerStyleClass; ?>">
	<div class="container">
		<div class="row">
			<?php if($classieraFooterWidgets == 1){ ?>
			<?php dynamic_sidebar( 'footer-one' ); ?>
			<?php } ?>
		</div><!--row-->
	</div><!--container-->
</footer>
<section class="footer-bottom">
	<div class="container">
		<div class="row">
				<h3 class="text-center"><?php echo $classieraCopyRight; ?></h3> 
				<ul id="menu-footer-menu" class="">
					<li id="menu-item-1219" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1219">
						<a title="" href="https://hyst2.temp.co.za/about/">About us</a>
					</li>
					<li id="menu-item-1219" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1219">
						<a title="" href="https://hyst2.temp.co.za/contact-us/">Contact us</a>
					</li>
			</ul>
				<?php if($classieraFooterBottomStyle == 'menu'){?>
				
				   <?php classieraFooterNav(); ?>
				
				<?php }elseif($classieraFooterBottomStyle == 'icon'){
					?>
					<ul class="footer-bottom-social-icon">
						<li><span><?php esc_html_e( 'Follow Us', 'classiera' ); ?> :</span></li>
						
						<?php if(!empty($classieraFacebook)){?>
						<li>
							<a href="<?php echo $classieraFacebook; ?>" class="rounded text-center" target="_blank">
								<i class="fa fa-facebook"></i>
							</a>
						</li>
						<?php } ?>
						<?php if(!empty($classieraTwitter)){?>
						<li>
							<a href="<?php echo $classieraTwitter; ?>" class="rounded text-center" target="_blank">
								<i class="fa fa-twitter"></i>
							</a>
						</li>
						<?php } ?>
						<?php if(!empty($classieraGoogle)){?>
						<li>
							<a href="<?php echo $classieraGoogle; ?>" class="rounded text-center" target="_blank">
								<i class="fa fa-google-plus"></i>
							</a>
						</li>
						<?php } ?>
						<?php if(!empty($classieraPinterest)){?>
						<li>
							<a href="<?php echo $classieraPinterest; ?>" class="rounded text-center" target="_blank">
								<i class="fa fa-pinterest-p"></i>
							</a>
						</li>
						<?php } ?>
						<?php if(!empty($classieraInstagram)){?>
						<li>
							<a href="<?php echo $classieraInstagram; ?>" class="rounded text-center" target="_blank">
								<i class="fa fa-instagram"></i>
							</a>
						</li>
						<?php } ?>
					</ul>
					<?php
				}?>
		</div><!--row-->
	</div><!--container-->
</section>
<?php 
if(is_user_logged_in()){
	$current_user = wp_get_current_user();
	$user_ID = $current_user->ID;
	$classieraRead = classiera_unread_message_by_user($user_ID);	
	if($classieraRead > 0 && $classieraRead != 0){
?>
	<a href="<?php echo $classieraInbox; ?>" class="bid_notification">
		<span class="bid_notification__icon"><i class="fa fa-bell"></i></span>
		<span class="bid_notification__count"><?php echo $classieraRead; ?></span>
	</a>
<?php
	}
}
?>
<?php if($classierabackToTop == 1){?>
	<!-- back to top -->
	<a href="#" id="back-to-top" title="<?php esc_html_e( 'Back to top', 'classiera' ); ?>" class="social-icon social-icon-md"><img src="<?php echo  get_template_directory_uri() . '/images/up-scroll.png';?>" alt="up scroll"></a>
<?php } ?>
<?php wp_footer(); ?>

<script src="<?=get_template_directory_uri().'/../classiera-child/custom/jquery.mousewheel.min.js'?>" type="text/javascript"></script>
<script src="<?=get_template_directory_uri().'/../classiera-child/custom/jquery.carousel-1.1.min.js'?>" type="text/javascript"></script>
<?php if(is_user_logged_in()) { ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		//jQuery("#popmsg").click(jQuery("#def").css(":display","block"));
		jQuery("#popmsg").on("click",function(){
			//jQuery("#def").hide();
			//jQuery(".add_list_item").hide();
			jQuery("#def").toggle();
		   // $("#def").css("display","block")
		    //$("#dvAlbums").css("display","block")

		});
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function($) {

		$('.premium-img img').each(function() {
			var imgsrc = $(this).attr('src');
			var IMGURL = "<?php echo get_template_directory_uri().'/images/nothumb.png'?>";
			if (imgsrc=='') {
				$(this).attr('src', IMGURL);
			}
		});

		$('.premium-img a.outline.active img').each(function() {
			var imgsrc = $(this).attr('src');
			var IMGURL = "<?php echo get_template_directory_uri().'/images/nothumb.png'?>";
			if (imgsrc=='') {
				$(this).attr('src', IMGURL);
			}
		});
	});
</script>
<?php }?>
</body>
</html>