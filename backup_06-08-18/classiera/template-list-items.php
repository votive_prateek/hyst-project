<?php
/**
 * Template name: List Items
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Classiera
 * @since Classiera
 */

if ( !is_user_logged_in() ) { 

	global $redux_demo; 
	$login = $redux_demo['login'];
	wp_redirect( $login ); exit;

}
if(!isset($_GET['list_id']))
{
	wp_redirect(site_url()); exit;
}
global $redux_demo; 
$edit = $redux_demo['edit'];
$pagepermalink = get_permalink($post->ID);

global $current_user, $user_id;
wp_get_current_user();
$user_info = get_userdata($user_ID);
$user_id = $current_user->ID; // You can set $user_id to any users, but this gets the current users ID.


if(isset($_GET['remove_id']) && $_GET['remove_id']!='')
{
	global $wpdb;
	//echo "delete from wp_user_list_item where id='".$_GET['remove_id']."'";
	$wpdb->query("delete from wp_user_list_item where hyst_id='".$_GET['remove_id']."' and list_id='".$_GET['list_id']."' and user_id='".$user_id."'");
}

get_header(); 


?>
<?php 
global $redux_demo; 
$profile = $redux_demo['profile'];
$all_adds = $redux_demo['all-ads'];
$allFavourite = $redux_demo['all-favourite'];
$newPostAds = $redux_demo['new_post'];
$bumpProductID = $redux_demo['classiera_bump_ad_woo_id'];
$classiera_cart_url = $redux_demo['classiera_cart_url'];
$caticoncolor="";
$category_icon_code ="";
$category_icon="";
$category_icon_color="";
?>
<?php 
	$page = get_page($post->ID);
	$current_page_id = $page->ID;
	$iconClass = 'icon-left';
	if(is_rtl()){
		$iconClass = 'icon-right';
	}
?>
<!-- user pages -->
<section class="user-pages section-gray-bg all_usertemplate">
	<div class="container-fluid">
        <div class="row"> 
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
				<div class="user-detail-section section-bg-white inline-block">
					<!-- my ads -->
					<div class="user-ads user-my-ads">	
                        <div class="row">	 				
							<div class="single_title">
							   <h2>
								<?php
									global $wpdb;
									$is_public=0;
									$list_user_id=0;
									if(isset($_GET['list_id']) && $_GET['list_id']!='')
									{
										$listname=$wpdb->get_results("select * from wp_user_lists where id='".$_GET['list_id']."'");
										//print_r($listname);
										echo  esc_html_e($listname[0]->list_title);
										$is_public=$listname[0]->is_public;
										$list_user_id=$listname[0]->user_id;
									}
									$user = wp_get_current_user();
										$roles = $user->roles;
									 ?>
								 </h2>
								   
							</div>
							<?php
							if($is_public==1)
							{
							$get_items=$wpdb->get_results("select hyst_id from wp_user_list_item where list_id='".$_GET['list_id']."'");
							$hyst_lists=array();
							foreach($get_items as $key=>$val)
							{
								if($hyst_lists=='')
								{
									$hyst_lists[]=$get_items[$key]->hyst_id;
								}
								else
								{
									$hyst_lists[]=$get_items[$key]->hyst_id;
								}
							}
							$kulPost = array(
								'post_type'  => 'post',
								//'post__in' => array($hyst_lists),
								'post__in' => $hyst_lists, 
								'posts_per_page' => 1,
								'post_status' => array( 'publish', 'pending', 'future', 'draft', 'private' ),
							);
							$wp_query = new WP_Query($kulPost);
							while ($wp_query->have_posts()) : $wp_query->the_post();
							$imagetoshare=get_the_post_thumbnail_url($post->ID, 'medium');
							endwhile;?>
							<div style="margin:20 0px;">
							<p>&nbsp;</p>
							<?php //echo do_shortcode('[addtoany]');
							//echo do_shortcode('[addtoany url="'.site_url().'/list-items/?list_id='.$_GET['list_id'].'" title="'.$listname[0]->list_title.'" media="'.$imagetoshare.'"]');?>
							<?php echo do_shortcode('[addthis tool="addthis_inline_share_toolbox"]'); ?>
							<div class="sharethis-inline-share-buttons"></div>
							</div>
							<p class="share">Share: </p>
							<?php
							}?>
						</div>
						<div class="top-buffer3"></div>
						
						<?php 
							if(isset($_GET['list_id']) && $_GET['list_id']!='')
							{
							
							//$get_items=$wpdb->get_results("select hyst_id from wp_user_list_item where list_id='".$_GET['list_id']."' and '".$user_id."'");
							$get_items=$wpdb->get_results("select hyst_id from wp_user_list_item where list_id='".$_GET['list_id']."'");
							$hyst_lists=array();
							foreach($get_items as $key=>$val)
							{
								if($hyst_lists=='')
								{
									$hyst_lists[]=$get_items[$key]->hyst_id;
								}
								else
								{
									$hyst_lists[]=$get_items[$key]->hyst_id;
								}
							}
							if(empty($hyst_lists))
							{
								echo '<span>You dont have any hyst in list</span>';
							}
							else
							{
							//echo 'hyst_items->'.$hyst_lists;
							global $paged, $wp_query, $wp;
							$args = wp_parse_args($wp->matched_query);
							if ( !empty ( $args['paged'] ) && 0 == $paged ) {
								$wp_query->set('paged', $args['paged']);
								$paged = $args['paged'];
							}
							$wp_query = null;
							/*$kulPost = array(
								'post_type'  => 'post',
								'author' => $user_id,
								'posts_per_page' => 12,
								'paged' => $paged,
								'post_status' => array( 'publish', 'pending', 'future', 'draft', 'private' ),
							);*/
							$kulPost = array(
								'post_type'  => 'post',
								//'post__in' => array($hyst_lists),
								'post__in' => $hyst_lists, 
								'posts_per_page' => 12,
								'paged' => $paged,
								'post_status' => array( 'publish', 'pending', 'future', 'draft', 'private' ),
							);
							$wp_query = new WP_Query($kulPost);
							while ($wp_query->have_posts()) : $wp_query->the_post();
							$title = get_the_title($post->ID); 
							$classieraPstatus = get_post_status( $post->ID );
							$dateFormat = get_option( 'date_format' );
							$postDate = get_the_date($dateFormat, $post->ID);
							$postStatus = get_post_status($post->ID);
							$productID = get_post_meta($post->ID, 'pay_per_post_product_id', true);
							$days_to_expire = get_post_meta($post->ID, 'days_to_expire', true);
						?>
						
						
						
					
						<div class="search_page_item">
							<div class="col-lg-3 col-md-4 col-sm-6 match-height item item-grid">
								<div class="classiera-box-div classiera-box-div-v1">
									<figure class="clearfix">
										<div class="feature_image">
											 <img src="<?=get_template_directory_uri().'/../classiera-child/images/featured.png' ?>" />
										</div>	
										<div class="premium-img">
											<?php 
												if ( has_post_thumbnail()){								
												$imgURL = get_the_post_thumbnail_url();
												?>
												<a href="<?php echo get_permalink($post->ID);?>"><img class="media-object img-responsive" src="<?php echo $imgURL; ?>" alt="<?php echo $title; ?>"></a>
												<?php } ?>
										</div><!--premium-img-->
										
										
										<figcaption class="pad0 text-left">
											<div class="product-body pad15">
												<h5 class="media-heading"><a href="<?php echo get_permalink($post->ID);?>"><?php echo $title; ?></a></h5>
											    <?php	$post_price = get_post_meta($post->ID, 'post_price', true);
												if($post_price!='')
												{?>
												<a href="<?php echo get_permalink($post->ID);?>"><h5 class="price media-heading">Price : <?php echo $post_price;?></h5></a>
												<?php
												}?>
												
												
												
												
												<a href="<?php echo get_permalink($post->ID);?>"><h5 class="media-heading">Category : 
												       <?php $postCatgory = get_the_category( $post->ID );  echo $postCatgory[0]->name; ?></h5></a>
											
												<div class="top-buffer1"></div>
												
									            <p class="justify"><?php echo substr(get_the_excerpt($post->ID), 0,260).'&nbsp;<a class="more-link" href="' . get_permalink($post->ID) . '">&nbsp;<img src="'.get_template_directory_uri().'/../classiera-child/images/down_arrow.png" class="read_more" height="10px" width="10px"></a>'; ?></p>
												
												<div class="top-buffer1"></div>
												<?php /*<a class="user_details" href="#"><img src="https://hyst2.temp.co.za/wp-content/themes/classiera/../classiera-child/images/user.png"> user : jane doe</a>*/
												
					$author_id=$post->post_author; 
					$profileIMGID = get_user_meta($author_id, "classify_author_avatar_url", true);
									$profileIMG = classiera_get_profile_img($profileIMGID);
									$authorName = get_the_author_meta('display_name', $author_id);
									$author_verified = get_the_author_meta('author_verified', $author_id);
									//$authoremail=the_author_meta( 'email' , $author_id );
									if ( is_user_logged_in() ) {
									$is_approve_prof_pic=get_user_meta($author_id,'profile_pic_approve');
									//echo '$is_approve_prof_pic:'.$is_approve_prof_pic;
									if($is_approve_prof_pic[0]=='1')
									{
										if(empty($profileIMG)){
										$profileIMG = classiera_get_avatar_url ( get_the_author_meta('user_email', $author_id), $size = '50' );
										?>
									<a href="<?php echo get_author_posts_url( $author_id, get_the_author_meta( 'user_nicename' ) ); ?>"><img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="float:left;"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;"> user: <?php echo $authorName;?></span>	</a>
									<?php
									
									}else{ ?>
									<a href="<?php echo get_author_posts_url($author_id, get_the_author_meta( 'user_nicename' ) ); ?>"><img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="float:left;"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;">  user: <?php echo $authorName;?></span></a>	
									<?php }
									}
									else
									{
										?><a href="<?php echo get_author_posts_url($author_id, get_the_author_meta( 'user_nicename' ) ); ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/user.png' ?>"/><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;">  user: <?php echo $authorName;?></span></a><?php
									}
									}
									else
									{
										$is_approve_prof_pic=get_user_meta($author_id,'profile_pic_approve');
									if($is_approve_prof_pic[0]=='1')
									{
										if(empty($profileIMG)){
										$profileIMG = classiera_get_avatar_url ( get_the_author_meta('user_email', $author_id), $size = '50' );
										?>
									<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="float:left;"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;"> user: <?php echo $authorName;?></span>
									<?php
									}else{ ?>
									<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="float:left;"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;">  user: <?php echo $authorName;?></span>	
									<?php }
									}
									else
									{?><img src="<?=get_template_directory_uri().'/../classiera-child/images/user.png' ?>"/>  user: <?php echo $authorName;?><?php
									}
									} 
									//echo '$user_id: '.$user_id;
									//echo '$list_user_id: '.$list_user_id;
									if($user_id==$list_user_id)
									{?>
									<br/>
									<a class="thickbox btn btn-primary sharp btn-style-one btn-sm" style="padding: 0px;
margin: 10px 20% !important;
float: right;" href="#TB_inline?height=150&amp;width=400&amp;inlineId=examplePopup<?php echo $post->ID; ?>"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #000 !important; background-color:#fff !important; padding:8px 10px !important; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; float:right;">Remove from list</span></a>
										<div class="delete-popup" id="examplePopup<?php echo $post->ID; ?>" style="display:none">
										<h4><?php esc_html_e("Are you sure you want to remove this from list?", 'classiera') ?></h4>
										<a class="btn btn-primary sharp btn-style-one btn-sm" href="<?php echo site_url();?>/list-items/?list_id=<?php echo $_GET['list_id'];?>&remove_id=<?php echo $post->ID;?>">
										<span class="button-inner"><?php esc_html_e("Confirm", 'classiera') ?></span>
										</a>
									</div>
								<?php
								}?>
										<div class="top-buffer1"></div>	
											</div>
										</figcaption>
									</figure>
								</div>
							</div>
						</div>
						
						
						
						
						
						
						
						
						
					<!-- old code -->
					
						<div class="col-sm-6 col-md-4" style="display:none;">
							<div class="media">
								<div class="product_image">
									<?php 
									if ( has_post_thumbnail()){								
									$imgURL = get_the_post_thumbnail_url();
									?>
									<a href="<?php echo get_permalink($post->ID);?>"><img class="media-object" src="<?php echo $imgURL; ?>" alt="<?php echo $title; ?>"></a>
									<?php } ?>
								</div><!--media-left-->
								<div class="product-body">
									<h5 class="media-heading">
										post : <a href="<?php echo get_permalink($post->ID);?>"><?php echo $title; ?></a>
									</h5>
									<h5 class="media-heading">Colour: Black</h5>
									<h5 class="media-heading">Price R399</h5>
									<h5 class="media-heading">Description:</h5>
									<p class="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									
									<p style="display:none;">
										<span class="published">
											<i class="fa fa-check-circle"></i>
											<?php classieraPStatusTrns($classieraPstatus); ?>
										</span>
										<span>
											<i class="fa fa-eye"></i>
											<?php echo classiera_get_post_views($post->ID); ?>
										</span>
										<span>
											<i class="fa fa-clock-o"></i>
											<?php echo $postDate; ?>
										</span>
										<span>
											<i class="removeMargin fa fa-hashtag"></i>
											<?php esc_html_e( 'ID', 'classiera' ); ?> :                                        
											<?php echo $post->ID; ?>
										</span>
									</p>
								</div><!--media-body-->
								<?php
								//print_r($post);
								//echo '$user_id=>'.$user_id.'<br/>';
								//echo '$post->post_author=>'.$post->post_author;
								/*if($user_id==$post->post_author)
								{?>
								<div class="media-right"  style="display:none;">
									<a class="thickbox btn btn-primary sharp btn-style-one btn-sm" href="#TB_inline?height=150&amp;width=400&amp;inlineId=examplePopup<?php echo $post->ID; ?>" style="width:150px;"><i class="<?php echo $iconClass; ?> fa fa-trash-o"></i><?php esc_html_e("Remove from List", 'classiera') ?></a>
									<div class="delete-popup" id="examplePopup<?php echo $post->ID; ?>" style="display:none">
										<h4><?php esc_html_e("Are you sure you want to remove this from list?", 'classiera') ?></h4>
										<a class="btn btn-primary sharp btn-style-one btn-sm" href="<?php site_url();?>/list-items/?list_id=<?php echo $_GET['list_id'];?>&remove_id=<?php echo $post->ID;?>">
										<span class="button-inner"><?php esc_html_e("Confirm", 'classiera') ?></span>
										</a>
									</div>
								</div>
								<?php
								}*/?>
							</div><!--media border-bottom-->
						</div>
						
						
						<!-- old code -->
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						<?php
						
						 endwhile;
						}
						} ?>
						<?php									
						  if(function_exists('classiera_pagination')){
							classiera_pagination();
						  }
						?>
						<?php wp_reset_query(); ?>	
					</div><!--user-ads user-my-ads-->
					<!-- my ads -->
				</div><!--user-detail-section-->
			</div><!--col-lg-9-->
		</div><!--row-->
	</div><!-- container-->
</section>
<!-- user pages -->
<?php get_footer(); ?>