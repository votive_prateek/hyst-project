<?php

/**

 * Template name: Edit Inventory

 *

 * Learn more: http://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage classiera

 * @since classiera 1.0

 */



if ( !is_user_logged_in() ) {
	wp_redirect( home_url() ); exit;
}

if(!current_user_can('inventory_user'))
{
	if(!current_user_can('company_user'))
	{
		get_template_part('error');
		exit;
	}
}

global $redux_demo;
$featuredADS = '';
$googleFieldsOn = $redux_demo['google-lat-long'];
$classieraLatitude = $redux_demo['contact-latitude'];
$classieraLongitude = $redux_demo['contact-longitude'];
$postCurrency = $redux_demo['classierapostcurrency'];
$classieraAddress = $redux_demo['classiera_address_field_on'];
$classiera_ads_typeOn = $redux_demo['classiera_ads_type'];
$termsandcondition = $redux_demo['termsandcondition'];
$postContent = '';
$caticoncolor="";
$category_icon_code ="";
$category_icon="";
$category_icon_color="";
global $current_user;
wp_get_current_user();
$hasError = false;
$userID = $current_user->ID;
$query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>'-1') );
if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();

	if(isset($_GET['post'])) {	

		if($_GET['post'] == $post->ID)

		{
			$posts_id = $_GET['post'];
			$author = get_post_field( 'post_author', $posts_id );
			if(current_user_can('administrator') ){
				
			}else{
				if($author != $userID) {
					wp_redirect( home_url() ); exit;
				}
			}
			$current_post = $post->ID;
			$title = get_the_title();
			$content = get_the_content();
			$posttags = get_the_tags($current_post);
			if ($posttags) {
			  foreach($posttags as $tag) {
				$tags_list = $tag->name . ' '; 
			  }
			}

			$postcategory = get_the_category( $current_post );
			
			$category_id = $postcategory[0]->cat_ID;
			
			$post_category_type = get_post_meta($post->ID, 'post_category_type', true);
			$classieraPostDate = get_the_date('Y-m-d H:i:s', $posts_id);			
			$post_price = get_post_meta($post->ID, 'post_price', true);			
			$post_old_price = get_post_meta($post->ID, 'post_old_price', true);
			$classieraTagDefault = get_post_meta($post->ID, 'post_currency_tag', true);
			
			$post_phone = get_post_meta($post->ID, 'post_phone', true);
			
			$post_main_cat = get_post_meta($post->ID, 'post_perent_cat', true);
			$post_child_cat = get_post_meta($post->ID, 'post_child_cat', true);
			$post_inner_cat = get_post_meta($post->ID, 'post_inner_cat', true);
			if(empty($post_inner_cat)){
				$category_id = $post_child_cat;
			}else{
				$category_id = $post_inner_cat;
			}
			if(empty($category_id)){
				$category_id = $post_main_cat;
			}
			$post_location = get_post_meta($post->ID, 'post_location', true);
			
			$post_state = get_post_meta($post->ID, 'post_state', true);
			$post_city = get_post_meta($post->ID, 'post_city', true);

			$post_latitude = get_post_meta($post->ID, 'post_latitude', true);

			$post_longitude = get_post_meta($post->ID, 'post_longitude', true);

			$post_price_plan_id = get_post_meta($post->ID, 'post_price_plan_id', true);

			$post_address = get_post_meta($post->ID, 'post_address', true);

			$post_video = get_post_meta($post->ID, 'post_video', true);
			
			$featuredIMG = get_post_meta($post->ID, 'featured_img', true);
			
			$itemCondition = get_post_meta($post->ID, 'item-condition', true);
			
			$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);
			
			$classiera_post_type = get_post_meta($post->ID, 'classiera_post_type', true);
			$pay_per_post_product_id = get_post_meta($post->ID, 'pay_per_post_product_id', true);
			$days_to_expire = get_post_meta($post->ID, 'days_to_expire', true);
			
			$featured_post = "0";
			$post_price_plan_activation_date = get_post_meta($post->ID, 'post_price_plan_activation_date', true);
			$post_price_plan_expiration_date = get_post_meta($post->ID, 'post_price_plan_expiration_date', true);
			$todayDate = strtotime(date('d/m/Y H:i:s'));
			$expireDate = strtotime($post_price_plan_expiration_date);  
			if(!empty($post_price_plan_activation_date)) {
				if(($todayDate < $expireDate) or empty($post_price_plan_expiration_date)) {
					$featured_post = "1";
				}
			}
			if(empty($post_latitude)) {
				$post_latitude = 0;
			}			
			if(empty($post_longitude)) {
				$post_longitude = 0;
				$mapZoom = 2;
			} else {
				$mapZoom = 16;
			}	

			if ( has_post_thumbnail() ) {	

				$post_thumbnail = get_the_post_thumbnail($current_post, 'thumbnail');		

			} 

		}

	}
endwhile; endif;
wp_reset_query();
global $current_post;
$postTitleError = '';
$post_priceError = '';
$catError = '';
$featPlanMesage = '';
//
$error=array();
if(isset($_POST['submitted']) && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) {
	//echo 'i am here';exit;
	if(trim($_POST['postTitle']) === '') {		
		$postTitleError =  esc_html__( 'Please select a title.', 'classiera' );
		$hasError = true;
	} else {
		$postTitle = trim($_POST['postTitle']);
	}

	if(empty($_POST['cat'])){		
		$catError =  esc_html__( 'Please select a category.', 'classiera' );
		$hasError = true;
		$error[]='Please select a category.';
	} 
	if(isset($_POST['postTitle']) && $_POST['postTitle']=='')
			{
				$hasError = true;
				$error[]='Please enter a title.';
			}
			if(isset($_POST['postContent']) && $_POST['postContent']=='')
			{
				$hasError = true;
				$error[]='Please enter a description.';
			}
//	echo $catError;exit;
	if($hasError != true) {
	
	//echo 'i am here';exit;
		//$classieraPostType = $_POST['classiera_post_type'];
		if(is_super_admin() ){
			$postStatus = 'publish';
		}elseif(!is_super_admin()){		

			if($redux_demo['post-options-edit-on'] == 1){
				$postStatus = 'private';
			}else{
				$postStatus = 'publish';
			}
			if($classieraPostType == 'payperpost'){
				$postStatus = 'pending';
			}

		}
		
		//Check Category//
		if(trim($_POST['cat']) === '-1') {			
			$catError =  esc_html__( 'Please select a category.', 'classiera' );
			$hasError = true;
		} 
		$mCatID = $_POST['cat'];
		$classieraGetCats = classiera_get_cats_on_edit($mCatID);		
		$categoriesID = get_ancestors($mCatID, 'category', 'taxonomy');
		if(!empty($categoriesID)){
			$cats = "";
			foreach ($categoriesID as $id) {
				$cats .= $id.', ';				
			}
			$catsString = $cats.$mCatID;			
			$catsArray = explode(",", $catsString);
		}else{
			$catsArray = array($mCatID);
		}		
		$post_main_cat = $classieraGetCats['post_main_cat'];
		$post_child_cat = $classieraGetCats['post_child_cat'];
		$post_inner_cat = $classieraGetCats['post_inner_cat'];
		$post_date = $_POST['classiera_post_date'];		
		$post_information = array(
			'ID' => $current_post,
			'post_title' => esc_attr(strip_tags($_POST['postTitle'])),
			'post_content' => strip_tags($_POST['postContent'], '<h1><h2><h3><strong><b><ul><ol><li><i><a><blockquote><center><embed><iframe><pre><table><tbody><tr><td><video>'),
			'post-type' => 'post',
			'post_date' => $post_date,
			'post_category' => $catsArray,
	        'tags_input'    => explode(',', $_POST['post_tags']),
			'tax_input' => array(
				'location' => $_POST['post_location'],
			),
	        'comment_status' => 'open',
	        'ping_status' => 'open',
			'post_author' => $_POST['postAuthor'],
			'post_status' => $postStatus
		);		

		$post_id = wp_insert_post($post_information);
		$googleLat = $_POST['latitude'];
		$googleLong = $_POST['longitude'];
		
		/*Check If Latitude is OFF */		
		if(empty($googleLat)){
			$latitude = $classieraLatitude;
		}else{
			$latitude = $googleLat;
		}
		/*Check If longitude is OFF */		
		if(empty($googleLong)){
			$longitude = $classieraLongitude;
		}else{
			$longitude = $googleLong;
		}

		$post_price_status = trim($_POST['post_price']);
		$old_price_status = trim($_POST['post_old_price']);
		
		$itemCondition = $_POST['item-condition'];


		global $redux_demo; 
		$free_listing_tag = $redux_demo['free_price_text'];

		if(empty($post_price_status)) {
			$post_price_content = $free_listing_tag;
		} else {
			$post_price_content = $post_price_status;
		}
		
		if(empty($old_price_status)) {
			$old_price_content = $free_listing_tag;
		} else {
			$old_price_content = $old_price_status;
		}

		$catID = $mCatID.'custom_field';		
		$custom_fields = $_POST[$catID];
		/*If We are using CSC Plugin*/
		/*Get Country Name*/
		if(isset($_POST['post_location'])){
			$postLo = $_POST['post_location'];
			$allCountry = get_posts( array( 'include' => $postLo, 'post_type' => 'countries', 'posts_per_page' => -1, 'suppress_filters' => 0, 'orderby'=>'post__in' ) );
			foreach( $allCountry as $country_post ){
				$postCounty = $country_post->post_title;
			}
		}
		if(isset($_POST['post_state'])){
			$poststate = $_POST['post_state'];
		}
		if(isset($_POST['post_state'])){
			$postCity = $_POST['post_city'];
		}
		if(isset($_POST['classiera_CF_Front_end'])){
			$classiera_CF_Front_end = $_POST['classiera_CF_Front_end'];
		}
		if(isset($_POST['classiera_sub_fields'])){
			$classiera_sub_fields = $_POST['classiera_sub_fields'];
		}
		if(isset($_POST['new_featured'])){
			$new_featured = $_POST['new_featured'];
		}		
		if(isset($new_featured) && !empty($new_featured)){
			update_post_meta($post_id, '_thumbnail_id', $new_featured);
		}		
		/*If We are using CSC Plugin*/		

		if(isset($_POST['post_category_type'])){
			update_post_meta($post_id, 'post_category_type', esc_attr( $_POST['post_category_type'] ) );
		}
		$postMultiTag = $_POST['post_currency_tag'];

		update_post_meta($post_id, 'custom_field', $custom_fields);
		update_post_meta($post_id, 'classiera_CF_Front_end', $classiera_CF_Front_end);
		update_post_meta($post_id, 'classiera_sub_fields', $classiera_sub_fields);
		update_post_meta($post_id, 'classiera_ads_type', $_POST['classiera_ads_type'], $allowed);
		
		update_post_meta($post_id, 'post_perent_cat', $post_main_cat, $allowed);
		update_post_meta($post_id, 'post_child_cat', $post_child_cat, $allowed);				
		update_post_meta($post_id, 'post_inner_cat', $post_inner_cat, $allowed);
		
		update_post_meta($post_id, 'post_currency_tag', $postMultiTag, $allowed);

		update_post_meta($post_id, 'post_price', $post_price_content, $allowed);
		
		update_post_meta($post_id, 'post_old_price', $old_price_content, $allowed);
		if(isset($_POST['post_phone'])){
		update_post_meta($post_id, 'post_phone', $_POST['post_phone'], $allowed);
		}

		update_post_meta($post_id, 'post_location', wp_kses($postCounty, $allowed));
		
		update_post_meta($post_id, 'post_state', wp_kses($poststate, $allowed));
		update_post_meta($post_id, 'post_city', wp_kses($postCity, $allowed));
		
		update_post_meta($post_id, 'post_latitude', wp_kses($latitude, $allowed));
		update_post_meta($post_id, 'post_longitude', wp_kses($longitude, $allowed));

		update_post_meta($post_id, 'post_address', wp_kses($_POST['address'], $allowed));

		update_post_meta($post_id, 'post_video', $_POST['video'], $allowed);
		
		update_post_meta($post_id, 'featured_img', $_POST['featured-image'], $allowed);
		
		if(isset($_POST['item-condition'])){
			update_post_meta($post_id, 'item-condition', $itemCondition, $allowed);
		}
		update_post_meta($post_id, 'classiera_post_type', $_POST['classiera_post_type'], $allowed);
		update_post_meta($post_id, 'pay_per_post_product_id', $_POST['pay_per_post_product_id'], $allowed);
		update_post_meta($post_id, 'days_to_expire', $_POST['days_to_expire'], $allowed);
update_post_meta($post_id, 'days_to_expire', $_POST['days_to_expire'], $allowed);
				$expiry_date1=date("Y-m-d", strtotime($_POST['expiry_date1']));
				$expiry_date2=date("Y-m-d", strtotime($_POST['expiry_date2']));
				$expiry_date3=date("Y-m-d", strtotime($_POST['expiry_date3']));
				$expiry_date4=date("Y-m-d", strtotime($_POST['expiry_date4']));
				$expiry_date5=date("Y-m-d", strtotime($_POST['expiry_date5']));
				if((isset($_POST['found_location1']) && $_POST['found_location1']!='') && (isset($_POST['location1_price']) && $_POST['location1_price']!=''))
				{
					$wpdb->query("update wp_found_hysts set expiry_date='".$expiry_date1."', qty='".$_POST['qty1']."' where id='".$_POST['found_id1']."'");
				}
				if((isset($_POST['found_location2']) && $_POST['found_location2']!='') && (isset($_POST['location2_price']) && $_POST['location2_price']!=''))
				{
					$wpdb->query("update wp_found_hysts set expiry_date='".$expiry_date2."', qty='".$_POST['qty2']."' where id='".$_POST['found_id2']."'");
				}
				if((isset($_POST['found_location3']) && $_POST['found_location3']!='') && (isset($_POST['location3_price']) && $_POST['location3_price']!=''))
				{
					$wpdb->query("update wp_found_hysts set expiry_date='".$expiry_date3."', qty='".$_POST['qty3']."' where id='".$_POST['found_id3']."'");
				}
				if((isset($_POST['found_location4']) && $_POST['found_location4']!='') && (isset($_POST['location4_price']) && $_POST['location4_price']!=''))
				{
					$wpdb->query("update wp_found_hysts set expiry_date='".$expiry_date4."', qty='".$_POST['qty4']."' where id='".$_POST['found_id4']."'");
				}
				if((isset($_POST['found_location5']) && $_POST['found_location5']!='') && (isset($_POST['location5_price']) && $_POST['location5_price']!=''))
				{
					$wpdb->query("update wp_found_hysts set expiry_date='".$expiry_date5."', qty='".$_POST['qty5']."' where id='".$_POST['found_id5']."'");
				}
		$permalink = get_permalink( $post_id );


		//If Its posting featured image//
		if(trim($_POST['classiera_post_type']) != 'classiera_regular'){
			if($_POST['classiera_post_type'] == 'payperpost'){
				//Do Nothing on Pay Per Post//
			}elseif($_POST['classiera_post_type'] == 'classiera_regular_with_plan'){
				//Regular Ads Posting with Plans//
				$classieraPlanID = trim($_POST['regular_plan_id']);
				global $wpdb;
				$current_user = wp_get_current_user();
				$userID = $current_user->ID;
				$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE id = $classieraPlanID" );
				if($result){
					$tablename = $wpdb->prefix . 'classiera_plans';
					$newRegularUsed = $info->regular_used +1;
					$update_data = array('regular_used' => $newRegularUsed);
					$where = array('id' => $classieraPlanID);
					$update_format = array('%s');
					$wpdb->update($tablename, $update_data, $where, $update_format);
				}
			}else{
				//Featured Post with Plan Ads//
				$featurePlanID = trim($_POST['classiera_post_type']);
				global $wpdb;
				$current_user = wp_get_current_user();
				$userID = $current_user->ID;
				$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE id = $featurePlanID" );
				if ($result){
					$featuredADS = 0;
					$tablename = $wpdb->prefix . 'classiera_plans';
					foreach ( $result as $info ){
						$totalAds = $info->ads;
						if (is_numeric($totalAds)){
							$totalAds = $info->ads;
							$usedAds = $info->used;
							$infoDays = $info->days;
						}								
						if($totalAds == 'unlimited'){
							$availableADS = 'unlimited';
						}else{
							$availableADS = $totalAds-$usedAds;
						}								
						if($usedAds < $totalAds && $availableADS != "0" || $totalAds == 'unlimited'){
							global $wpdb;
							$newUsed = $info->used +1;
							$update_data = array('used' => $newUsed);
							$where = array('id' => $featurePlanID);
							$update_format = array('%s');
							$wpdb->update($tablename, $update_data, $where, $update_format);
							update_post_meta($post_id, 'post_price_plan_id', $featurePlanID );

							$dateActivation = date('m/d/Y H:i:s');
							update_post_meta($post_id, 'post_price_plan_activation_date', $dateActivation );		
							
							$daysToExpire = $infoDays;
							$dateExpiration_Normal = date("m/d/Y H:i:s", strtotime("+ ".$daysToExpire." days"));
							update_post_meta($post_id, 'post_price_plan_expiration_date_normal', $dateExpiration_Normal );



							$dateExpiration = strtotime(date("m/d/Y H:i:s", strtotime("+ ".$daysToExpire." days")));
							update_post_meta($post_id, 'post_price_plan_expiration_date', $dateExpiration );
							update_post_meta($post_id, 'featured_post', "1" );
						}
					}
				}
			}
		}
//print_r($_FILES);
		if ( $_FILES ) {
			$files = $_FILES['upload_attachment'];
			foreach ($files['name'] as $key => $value) {
				if ($files['name'][$key]) {
					$file = array(
						'name'     => $files['name'][$key],
						'type'     => $files['type'][$key],
						'tmp_name' => $files['tmp_name'][$key],
						'error'    => $files['error'][$key],
						'size'     => $files['size'][$key]
					);		 

					$_FILES = array("upload_attachment" => $file);		 

					foreach ($_FILES as $file => $array) {
						$newupload = classiera_insert_attachment($file,$post_id);
					}

				}

			}

		}
		if (isset($_POST['att_remove'])) {
			foreach ($_POST['att_remove'] as $att_id){
				wp_delete_attachment($att_id);
			}
		}
		wp_redirect( $permalink ); exit;

	}

} 


get_header();  ?>
<?php 
	global $redux_demo;
	$page = get_page($post->ID);
	$current_page_id = $page->ID;
?>	
<?php while ( have_posts() ) : the_post(); ?>
<style>
.form-main-section .list-unstyled li a{padding: 8px 15px;
display: table;
text-align: left;text-align: center;
width: 100%;
height: 100%;
border-radius: 3px;moz-border-radius: 3px;webkit-border-radius: 3px;}
.list-unstyled li a:hover, .list-unstyled li a:hover i{background:#000; color:#fff !important;moz-border-radius: 3px;webkit-border-radius: 3px;}
.classieraSubReturn li{float:left; width:20%;}
</style>
<section class="user-pages section-gray-bg user-detail-section edit_section all_usertemplate">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
			<?php
						// print_r($error);
						 for($lp=0;$lp<count($error);$lp++)
						 {
						 	echo '<span style="float:left; clear:left; color:red;">'.$error[$lp].'</span>';
						 }

							?>
							<div style="clear:left;"></div>
				<div class="submit-post section-bg-white fileupload-wrapper">
				
				    <div id="myUpload">
					<?php /*<form class="form-horizontal" action="" role="form" id="primaryPostForm" method="POST" data-toggle="validator" enctype="multipart/form-data">*/?>
					
						<h4 class="text-uppercase border-bottom"></h4>
						
						<h2 class="user_name"><?php esc_html_e('EDIT YOUR HYST', 'classiera') ?></h2>
						<hr class="line1">
						<div style="clear:left;"></div>
						<div class="top-buffer3"></div>
						
						<input type="hidden" id="classiera_post_date" name="classiera_post_date" value="<?php echo $classieraPostDate; ?>">
						<div class="form-main-section classiera-`1	qpost-cat">
						
							<div class="classiera-post-main-cat" id="accordion">
							
								<div class="row">
									<div class="single_title title_link">
										<a href="javascript:void(0);" target-id="#category" parent-id="#accordion" class="accordian_toggle"><h2 class="classiera-post-inner-heading"><?php esc_html_e('Select a Category', 'classiera') ?></h2>
										<img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>" /></a>
									</div>
								</div>
								<div class="top-buffer2"></div>
								
								<div class="list_icon inner-toggle" id="category">
								    <div class="col-md-8 col-md-offset-2">
										<select id="cat" class="postform" name="cat">
											<option value="All" disabled><?php esc_html_e('Select Category..', 'classiera') ?></option>
											<?php 	
												$currCatID = $category_id;
												$args = array(
													'hierarchical' => '0',
													'hide_empty' => '0'
												);
												$categories = get_categories($args);
												foreach ($categories as $cat){
													if($cat->category_parent == 0){
														$catID = $cat->cat_ID;
														?>
														<option <?php if($currCatID == $catID){echo "selected";} ?> value="<?php echo $cat->cat_ID; ?>">
															<?php echo $cat->cat_name; ?>
														</option>
														<?php
														$args2 = array(
															'hide_empty' => '0',
															'parent' => $catID
														);
														$categories = get_categories($args2);
														foreach($categories as $cat){
															$catSubID = $cat->cat_ID;
															?>
															<option <?php if($currCatID == $cat->cat_ID){echo "selected";} ?> value="<?php echo $cat->cat_ID; ?>">-- <?php echo $cat->cat_name; ?></option>
															<?php
															$args3 = array(
																'hide_empty' => '0',
																'parent' => $catSubID
															);
															$categories = get_categories($args3);
															foreach($categories as $cat){
																?>
																<option <?php if($currCatID == $cat->cat_ID){echo "selected";} ?> value="<?php echo $cat->cat_ID; ?>">--- <?php echo $cat->cat_name; ?></option>
																<?php
															}
														}												
													}
												}
											?>
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div><!--classiera-post-cat-->
						<!--AdsDetails-->
						<div class="form-main-section post-detail">
							<h4 class="text-uppercase border-bottom hide"><?php esc_html_e('Inventory Details', 'classiera') ?> :</h4>
							<?php if($classiera_ads_typeOn == 1){?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Type of HYST', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">										
                                        <input id="sell" type="radio" value="sell" name="classiera_ads_type" <?php if($classiera_ads_type == 'sell'){echo "checked";}?>>
                                        <label for="sell"><?php esc_html_e('I want to sell', 'classiera') ?></label>
										
                                        <input id="buy" type="radio" value="buy" name="classiera_ads_type" <?php if($classiera_ads_type == 'buy'){echo "checked";}?>>
                                        <label for="buy"><?php esc_html_e('I want to buy', 'classiera') ?></label>
										
										<input type="radio" name="classiera_ads_type" value="rent" id="rent" <?php if($classiera_ads_type == 'rent'){echo "checked";}?>>
										<label for="rent"><?php esc_html_e('I want to rent', 'classiera') ?></label>
										
										<input type="radio" name="classiera_ads_type" value="hire" id="hire" <?php if($classiera_ads_type == 'hire'){echo "checked";}?>>
										<label for="hire"><?php esc_html_e('I want to hire', 'classiera') ?></label>
											
										<input id="sold" type="radio" value="sold" name="classiera_ads_type" <?php if($classiera_ads_type == 'sold'){echo "checked";}?>>
                                        <label for="sold"><?php esc_html_e('Sold', 'classiera') ?></label>
                                    </div>

                                </div>
                            </div><!--Type of Ad-->
							<?php } ?>
							
							<div style="clear:left;"></div>
							<div class="top-buffer2"></div>
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group col-sm-6">
									<label class="col-sm-3 text-left hide flip" for="title"><?php esc_html_e('HYST title', 'classiera') ?> : <span>*</span></label>
										<input id="title" data-minlength="5" name="postTitle" type="text" class="form-control form-control-md" value="<?php echo $title; ?>" placeholder="<?php esc_html_e('Ad Title Goes here', 'classiera') ?>" required>
										<div class="help-block hide"><?php esc_html_e('type minimum 5 characters', 'classiera') ?></div>
										<?php 
											$post_id = $_GET['post'];
											$author = get_post_field ('post_author', $post_id);
										?>
										<input type="hidden" id="postAuthor"  name="postAuthor" value="<?php echo $author; ?>">
								</div><!--Ad title-->
								<?php 
							$classieraPriceSecOFF = $redux_demo['classiera_sale_price_off'];
							$classieraMultiCurrency = $redux_demo['classiera_multi_currency'];
							$regularpriceon= $redux_demo['regularpriceon'];
							$postCurrency = $redux_demo['classierapostcurrency'];
							?>
							<?php if($classieraPriceSecOFF == 1){?>
							<div class="form-group col-sm-6">
                                <label class="col-sm-3 text-left hide flip"><?php esc_html_e(' price', 'classiera') ?> : </label>
                                    <div class="form-inline">
										<?php if($classieraMultiCurrency == 'multi'){?>
										<div class="col-sm-12">
                                            <div class="inner-addon right-addon input-group price__tag">
                                                <div class="input-group-addon">
                                                    <span class="currency__symbol">
														<?php echo classiera_Display_currency_sign($classieraTagDefault); ?>
													</span>
                                                </div>
                                                <i class="form-icon right-form-icon fa fa-angle-down"></i>
												<?php echo classiera_Select_currency_dropdow($classieraTagDefault); ?>
                                            </div>
                                        </div>
										<?php } ?>
                                                <div class="input-group-addon hide">
													<span class="currency__symbol">
													<?php 
													if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
														echo $postCurrency;
													}elseif($classieraMultiCurrency == 'multi'){
														echo classiera_Display_currency_sign($classieraTagDefault);
													}else{
														echo "&dollar;";
													}
													?>	
													</span>
												</div>
                                                <input type="text" name="post_price" id="post_price" value="<?php echo $post_price; ?>" class="form-control form-control-md" placeholder="<?php esc_html_e('sale price', 'classiera') ?>">
										<?php $regularpriceon= $redux_demo['regularpriceon']; ?>
										<?php if($regularpriceon == 1){?>
                                        <div class="col-sm-6" style="display:none;">
                                            <div class="input-group">
                                                <div class="input-group-addon">
													<span class="currency__symbol">
													<?php 
													if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
														echo $postCurrency;
													}elseif($classieraMultiCurrency == 'multi'){
														echo classiera_Display_currency_sign($classieraTagDefault);
													}else{
														echo "&dollar;";
													}
													?>	
													</span>
												</div>
                                                <input type="text" name="post_old_price" value="<?php echo $post_old_price; ?>" class="form-control form-control-md" placeholder="<?php esc_html_e('Regular price', 'classiera') ?>">
                                            </div>
                                        </div>
										<?php } ?>
                                    </div>
									<?php $postCurrency = $redux_demo['classierapostcurrency'];?>
									<?php if (!empty($postCurrency)){?>
                                    <div class="help-block hide"><?php esc_html_e('Currency sign is already set as', 'classiera') ?>&nbsp;<?php echo $postCurrency; ?>&nbsp;<?php esc_html_e('Please do not use currency sign in price field. Only use numbers ex: 12345', 'classiera') ?></div>
									<?php } ?>
                            </div><!--Ad Price-->
							<?php } ?>
								
								<div class="form-group col-sm-12">
									<label class="col-sm-3 text-left flip hide" for="description"><?php esc_html_e('HYST description', 'classiera') ?> : <span>*</span></label>
										<textarea name="postContent" id="description"  cols="5" rows="5" class="form-control" data-error="<?php esc_html_e('Write description', 'classiera') ?>" required><?php echo $content;?></textarea>
										<div class="help-block with-errors"></div>
								</div><!--Ad description-->
							</div>
							
							
							<div class="clearfix"></div>
							<div class="form-group">
								<div class="single_title row">
									<h2 class="classiera-post-inner-heading"><?php esc_html_e('HYST Tags', 'classiera') ?></h2>
								</div>
								    <div class="top-buffer2"></div>
                                    <div class="form-inline">
                                        <div class="col-sm-8 col-sm-offset-2">
                                                <div class="input-group-addon hide"><i class="fa fa-tags"></i></div>
                                               <?php
													echo "<input type='text' id='post_tags' placeholder='Tags' name='post_tags' value='";
													$posttags = get_the_tags($current_post);
													if ($posttags) {
													  foreach($posttags as $tag) {
														$tags_list = $tag->name . ', '; 
														echo $tags_list;
													  }
													}
													echo "' size='' maxlength='' class='form-control form-control-md'>"; 
												 ?>
                                        </div>
                                    </div>
							        <div class="clearfix"></div>
                                    <div class="help-block"><?php esc_html_e('Tags Example : ads, car, cat, business', 'classiera') ?></div>
                                    <div class="top-buffer2"></div>
                            </div><!--Ad Tags-->
							
							<div class="clearfix"></div>
							
							<?php  /* 
							$classieraPriceSecOFF = $redux_demo['classiera_sale_price_off'];
							$classieraMultiCurrency = $redux_demo['classiera_multi_currency'];
							$regularpriceon= $redux_demo['regularpriceon'];
							$postCurrency = $redux_demo['classierapostcurrency'];
							?>
							<?php if($classieraPriceSecOFF == 1){?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e(' price', 'classiera') ?> : </label>
                                <div class="col-sm-9">
                                    <div class="form-inline row">
										<?php if($classieraMultiCurrency == 'multi'){?>
										<div class="col-sm-12">
                                            <div class="inner-addon right-addon input-group price__tag">
                                                <div class="input-group-addon">
                                                    <span class="currency__symbol">
														<?php echo classiera_Display_currency_sign($classieraTagDefault); ?>
													</span>
                                                </div>
                                                <i class="form-icon right-form-icon fa fa-angle-down"></i>
												<?php echo classiera_Select_currency_dropdow($classieraTagDefault); ?>
                                            </div>
                                        </div>
										<?php } ?>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
													<span class="currency__symbol">
													<?php 
													if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
														echo $postCurrency;
													}elseif($classieraMultiCurrency == 'multi'){
														echo classiera_Display_currency_sign($classieraTagDefault);
													}else{
														echo "&dollar;";
													}
													?>	
													</span>
												</div>
                                                <input type="text" name="post_price" id="post_price" value="<?php echo $post_price; ?>" class="form-control form-control-md" placeholder="<?php esc_html_e('sale price', 'classiera') ?>">
                                            </div>
                                        </div>
										<?php $regularpriceon= $redux_demo['regularpriceon']; ?>
										<?php if($regularpriceon == 1){?>
                                        <div class="col-sm-6" style="display:none;">
                                            <div class="input-group">
                                                <div class="input-group-addon">
													<span class="currency__symbol">
													<?php 
													if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
														echo $postCurrency;
													}elseif($classieraMultiCurrency == 'multi'){
														echo classiera_Display_currency_sign($classieraTagDefault);
													}else{
														echo "&dollar;";
													}
													?>	
													</span>
												</div>
                                                <input type="text" name="post_old_price" value="<?php echo $post_old_price; ?>" class="form-control form-control-md" placeholder="<?php esc_html_e('Regular price', 'classiera') ?>">
                                            </div>
                                        </div>
										<?php } ?>
                                    </div>
									<?php $postCurrency = $redux_demo['classierapostcurrency'];?>
									<?php if (!empty($postCurrency)){?>
                                    <div class="help-block"><?php esc_html_e('Currency sign is already set as', 'classiera') ?>&nbsp;<?php echo $postCurrency; ?>&nbsp;<?php esc_html_e('Please do not use currency sign in price field. Only use numbers ex: 12345', 'classiera') ?></div>
									<?php } ?>
                                </div>
                            </div><!--Ad Price-->
							<?php } */?>
							<!--ContactPhone-->
							<?php $classieraAskingPhone = $redux_demo['phoneon'];?>
							<?php if($classieraAskingPhone == 1){?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Phone/Mobile', 'classiera') ?> :</label>
                                <div class="col-sm-9">
                                    <div class="form-inline row">
                                        <div class="col-sm-12">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                                                <input type="text" name="post_phone" id="post_phone" value="<?php echo $post_phone; ?>" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter your phone number or Mobile number', 'classiera') ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="help-block"><?php esc_html_e('Its Not required, but if you will put phone here then it will show publicly', 'classiera') ?></div>
                                </div>
                            </div>
							<?php } ?>
							<!--ContactPhone-->	
							<?php 
								$adpostCondition= $redux_demo['adpost-condition'];
								if($adpostCondition == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Item Condition', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <input id="new" type="radio" name="item-condition" value="<?php esc_html_e('new', 'classiera') ?>" name="item-condition" <?php if($itemCondition == 'new'){echo "checked";}?>>
                                        <label for="new"><?php esc_html_e('Brand New', 'classiera') ?></label>
                                        <input id="used" type="radio" name="item-condition" value="<?php esc_html_e('used', 'classiera') ?>" name="item-condition" <?php if($itemCondition == 'used'){echo "checked";}?>>
                                        <label for="used"><?php esc_html_e('Used', 'classiera') ?></label>
                                    </div>
                                </div>
                            </div><!--Item condition-->
								<?php } ?>
						</div>
						<!--AdsDetails-->
						<!--CustomDetails-->
						<div class="classieraExtraFields">
						<?php 
						$user_id = $category_id;
						$user_name = get_cat_name( $user_id );
						$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
						$wpcrown_category_custom_field_option = $tag_extra_fields[$user_id]['category_custom_fields'];
						$wpcrown_category_custom_field_type = $tag_extra_fields[$user_id]['category_custom_fields_type'];
						if(empty($wpcrown_category_custom_field_option)) {								
							$catobject = get_category($user_id,false);
							$parentcat = $catobject->category_parent;
							if(!empty($parentcat) || $parentcat != 0){
								$wpcrown_category_custom_field_option = $tag_extra_fields[$parentcat]['category_custom_fields'];
								$wpcrown_category_custom_field_type = $tag_extra_fields[$parentcat]['category_custom_fields_type'];
							}
						}
						?>
						<div class="form-main-section extra-fields wrap-content" id="cat-<?php echo $user_id; ?>" <?php if($currCatID == $user_id) { ?>style="display: block;"<?php  } else { ?>style="display: none;"<?php } ?>>
							<?php $wpcrown_custom_fields = get_post_meta($current_post, 'custom_field', true);?>
							<h4 class="text-uppercase border-bottom hide"><?php esc_html_e('Extra Fields For', 'classiera') ?>&nbsp;<?php echo $user_name;?> :</h4>
							<?php 
								for($i = 0; $i < (count($wpcrown_category_custom_field_option)); $i++){ 
									if($wpcrown_category_custom_field_type[$i][1] == 'text'){
										?>
										<div class="form-group" id="cat-<?php echo $user_id; ?>">
											<label class="col-sm-3 text-left flip"><?php echo $wpcrown_category_custom_field_option[$i][0]; ?>: <span>*</span></label>
											<div class="col-sm-6">
												<input type="hidden" class="custom_field" id="custom_field[<?php echo $i; ?>][0]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][0]" value="<?php echo $wpcrown_category_custom_field_option[$i][0] ?>" size="12">
										
												<input type="hidden" class="custom_field" id="custom_field[<?php echo $i; ?>][2]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][2]" value="<?php echo $wpcrown_category_custom_field_type[$i][1] ?>" size="12">
												
												<input type="text" placeholder="<?php if (!empty($wpcrown_category_custom_field_option[$i][0])) echo $wpcrown_category_custom_field_option[$i][0]; ?>" class="form-control form-control-md" id="custom_field[<?php echo $i; ?>][1]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][1]" value="<?php if($currCatID == $user_id) { echo $wpcrown_custom_fields[$i][1]; } ?>" size="12">
											</div>
										</div>
										<?php
									}
								}
							?>
							<?php 
							/*If Custom Fields is dropdown*/
							for ($i = 0; $i < (count($wpcrown_category_custom_field_option)); $i++) {
								if($wpcrown_category_custom_field_type[$i][1] == 'dropdown'){
							?>
								<div class="form-group" id="cat-<?php echo $user_id; ?>">
									<label class="col-sm-3 text-left hide flip"><?php echo $wpcrown_category_custom_field_option[$i][0]; ?>: <span>*</span></label>
									<div class="col-sm-6 col-md-offset-3">
										<div class="inner-addon right-addon">
											<i class="form-icon right-form-icon fa fa-angle-down"></i>
											<input type="hidden" class="custom_field" id="custom_field[<?php echo $i; ?>][0]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][0]" value="<?php echo $wpcrown_category_custom_field_option[$i][0] ?>" size="12">
											<input type="hidden" class="custom_field" id="custom_field[<?php echo $i; ?>][2]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][2]" value="<?php echo $wpcrown_category_custom_field_type[$i][1] ?>" size="12">
											
											<select class="form-control form-control-md" id="custom_field[<?php echo $i; ?>][1]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][1]">
												<?php
												echo '<option>'.$wpcrown_category_custom_field_option[$i][0].'</option>';
												$options = $wpcrown_category_custom_field_type[$i][2];
												$optionsarray = explode(',',$options);
												foreach($optionsarray as $option){
													if($wpcrown_custom_fields[$i][1] == $option){
														$selected = 'selected';
													}
													else{
														$selected = '';
													}
													echo '<option '.$selected.' value="'.$option.'">'.$option.'</option>';
												}
												?>
											</select>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							<?php } 
							}
							?>
							<?php 
							 for($i = 0; $i < (count($wpcrown_category_custom_field_option)); $i++) {
								 if($wpcrown_category_custom_field_type[$i][1] == 'checkbox'){
									if($wpcrown_custom_fields[$i][1] == 'on'){
										$checked = 'checked';
									}else{
										$checked = '';
									} 
							?>
								<div class="form-group" id="cat-<?php echo $user_id; ?>">
									<p class="featurehide featurehide<?php echo $i; ?>"><?php esc_html_e('Select Features', 'classiera') ?></p>
									<div class="col-sm-6">
										<div class="inner-addon right-addon">
											<i class="form-icon right-form-icon fa fa-angle-down"></i>
											<input type="hidden" class="custom_field" id="custom_field[<?php echo $i; ?>][0]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][0]" value="<?php echo $wpcrown_category_custom_field_option[$i][0] ?>" size="12">
											
											<input type="hidden" class="custom_field" id="custom_field[<?php echo $i; ?>][2]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][2]" value="<?php echo $wpcrown_category_custom_field_type[$i][1] ?>" size="12">
											
											
											<div class="checkbox">
												<input type="checkbox" <?php echo $checked; ?> class="custom_field custom_field_visible input-textarea newcehckbox" id="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][1]" name="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][1]">
												<label for="<?php echo $user_id; ?>custom_field[<?php echo $i; ?>][1]" class="newcehcklabel"><?php echo $wpcrown_category_custom_field_option[$i][0]; ?></label>
											</div>
										</div>
									</div>
								</div>
							<?php 
								}
							}
							?>	
						</div>
						</div>
						<!--CustomDetails-->
						<!-- add photos and media -->
						<?php /*?><div class="form-main-section media-detail">
							<?php
							//Image Count Check
							global $redux_demo;
							global $wpdb;
							$paidIMG = $redux_demo['premium-ads-limit'];
							$regularIMG = $redux_demo['regular-ads-limit'];								
							$current_user = wp_get_current_user();
							$userID = $current_user->ID;
							$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
							$totalAds = 0;
							$usedAds = 0;
							$availableADS = '';
							if(!empty($result)){
								foreach ( $result as $info ) {
									$availAds = $info->ads;
									if(is_numeric($availAds)){
										$totalAds += $info->ads;
										$usedAds += $info->used;
									}									
								}
							}
							$availableADS = $totalAds-$usedAds;
							//echo $availableADS."shabir";
							if($availableADS == "0" || empty($result)){
								$imageLimit = $regularIMG;
							}else{
								$imageLimit = $paidIMG;
							}
							?>
							<h4 class="text-uppercase border-bottom"><?php esc_html_e('Image And Video', 'classiera') ?> :</h4>
							<div class="form-group">
								<label class="col-sm-3 text-left flip"><?php esc_html_e('Photos and Video for your HYST', 'classiera') ?> :</label>
								<div class="col-sm-9">
									<div class="classiera-dropzone-heading">
                                        <i class="classiera-dropzone-heading-text fa fa-cloud-upload" aria-hidden="true"></i>
                                        <div class="classiera-dropzone-heading-text">
                                            <p><?php esc_html_e('Select files to Upload / Drag and Drop Files', 'classiera') ?></p>
                                            <p><?php esc_html_e('You can add multiple images. Ads With photo get 50% more Responses', 'classiera') ?></p>
											<p class="limitIMG"><?php esc_html_e('You can upload', 'classiera') ?>&nbsp;<?php echo $imageLimit; ?>&nbsp;<?php esc_html_e('Images maximum.', 'classiera') ?></p>
                                        </div>
                                    </div><!--classiera-dropzone-heading-->
									<div id="mydropzone" class="classiera-image-upload clearfix" data-maxfile="<?php echo $imageLimit; ?>">
										<!--PreviousImages-->
										<?php require_once get_template_directory() . '/inc/BFI_Thumb.php'; ?>
										<?php
										$imageCount = 0;
										$params = array( 'width' => 110, 'height' => 70, 'crop' => true );
										$imgargs = array(
											'post_parent' => $current_post,
											'post_status' => 'inherit',
											'post_type'   => 'attachment', 
											'post_mime_type'   => 'image', 
											'order' => 'ASC',
											'orderby' => 'menu_order ID',
										);
										$attachments = get_children($imgargs);
										if($attachments){
										foreach($attachments as $att_id => $attachment){
												$attachment_ID = $attachment->ID;
												$full_img_url = wp_get_attachment_url($attachment->ID);
												$split_pos = strpos($full_img_url, 'wp-content');
												$split_len = (strlen($full_img_url) - $split_pos);
												$abs_img_url = substr($full_img_url, $split_pos, $split_len);
												$full_info = @getimagesize(ABSPATH.$abs_img_url);
										?>
											<div id="<?php echo $attachment_ID; ?>" class="edit-post-image-block">
												<img class="edit-post-image" src="<?php echo bfi_thumb( "$full_img_url", $params ) ?>" />
												<div class="remove-edit-post-image">
													<i class="fa fa-minus-square-o"></i>
													<span class="remImage"><?php esc_html_e('Remove', 'classiera');?></span>
													<input type="hidden" name="" value="<?php echo $attachment_ID; ?>">
												</div><!--remove-edit-post-image-->
											</div>
											<?php $imageCount++;?>
										<?php }?>
										<?php }?>
										<!--PreviousImages-->
										<?php 
										$imageCounter = $imageLimit-$imageCount;
										for ($i = 0; $i < $imageCounter; $i++){
										?>
                                        <div class="classiera-image-box">
                                            <div class="classiera-upload-box">
												<input name="image-count" type="hidden" value="<?php echo $imageCount; ?>" />
                                                <input class="classiera-input-file imgInp" id="imgInp<?php echo $i; ?>" type="file" name="upload_attachment[]">												
                                                <label class="img-label" for="imgInp<?php echo $i; ?>"><i class="fa fa-plus-square-o"></i></label>
                                                <div class="classiera-image-preview">
                                                    <img class="my-image" src="">
                                                    <span class="remove-img"><i class="fa fa-times-circle"></i></span>
                                                </div>
                                            </div>
                                        </div>
										<?php } ?>										
										<input type="hidden" name="classiera_featured_img" id="classiera_featured_img" value="">
                                    </div>
									<!--Video-->
									<?php 
									$classiera_video_postads = $redux_demo['classiera_video_postads'];
									if($classiera_video_postads == 1){
									?>
                                    <div class="iframe">
                                        <div class="iframe-heading">
                                            <i class="fa fa-video-camera"></i>
                                            <span><?php esc_html_e('Put here iframe or video url.', 'classiera') ?></span>
                                        </div>
                                                        <input type="file" class="form-control" name="video" id="video-code" placeholder="<?php esc_html_e('Put here video ', 'classiera') ?>">
                                        <div class="help-block">
                                            <p><?php esc_html_e('Add video', 'classiera') ?></p>
                                        </div>
                                    </div>
									<?php } ?>
									<!--Video-->
								</div><!--col-sm-9-->
							</div>
						</div><?php */?>
						<div style="clear:left;"></div>
						<!-- add photos and media -->
						<!-- post location -->
						<?php 
						$classiera_ad_location_remove = $redux_demo['classiera_ad_location_remove'];
						if($classiera_ad_location_remove == 1){
						?>
						<div class="form-main-section post-location">
							<?php 							
							$country_posts = get_posts( array( 'post_type' => 'countries', 'posts_per_page' => -1, 'suppress_filters' => 0 ) );
							if(!empty($country_posts)){
							?>
							<div class="single_title row"> 
							      <h2><?php esc_html_e('Ad Location', 'classiera') ?></h2>
							</div>
							<?php }							
							if(!empty($country_posts)){
								?>
							<!--Select Country-->
							<div class="form-group">
                                <label class="col-sm-3 text-left hide flip"><?php esc_html_e('Select Country', 'classiera') ?>: <span>*</span></label>
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="inner-addon right-addon">
									    <div class="top-buffer2"></div>
                                        <i class="form-icon right-form-icon fa hide fa-angle-down"></i>
                                        <select name="post_location" id="post_location" class="form-control form-control-md">
                                            <option <?php if(empty($post_location)){ echo "selected"; }?> disabled value=""><?php esc_html_e('Select Country', 'classiera'); ?></option>
                                            <?php 
											foreach( $country_posts as $country_post ){
												if($post_location == $country_post->post_title){
													$getStatesbyID = $country_post->ID;
												}
												?>
												<option <?php if($post_location == $country_post->post_title){ echo "selected"; }?> value="<?php echo $country_post->ID; ?>"><?php echo $country_post->post_title; ?></option>
												<?php
											}
											?>
                                        </select>
										<div class="top-buffer2"></div>
                                    </div>
                                </div>
								<div class="clearfix"></div>
                            </div> 
							<?php } ?>
							<!--Select Country-->	
							<!--Select States-->
							<?php 
							$locationsStateOn = $redux_demo['location_states_on'];
							if($locationsStateOn == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Select State', 'classiera') ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <div class="inner-addon right-addon">
                                        <i class="form-icon right-form-icon fa fa-angle-down"></i>
										<select name="post_state" id="post_state" class="selectState form-control form-control-md" required>
											<option value="" disabled>
												<?php esc_html_e('Select State', 'classiera'); ?>
											</option>
											<?php 
											if (function_exists('classiera_get_states_by_country_id')) {
												echo classiera_get_states_by_country_id($getStatesbyID, $post_state);
											}											
											?>
										</select>
                                    </div>
                                </div>
                            </div>
							<?php } ?>
							<!--Select States-->
							<!--Select City-->
							<?php 
							$locationsCityOn= $redux_demo['location_city_on'];
							if($locationsCityOn == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Select City', 'classiera'); ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <div class="inner-addon right-addon">
                                        <i class="form-icon right-form-icon fa fa-angle-down"></i>
										<select name="post_city" id="post_city" class="selectCity form-control form-control-md" required>
											<option disabled>
												<?php esc_html_e('Select City', 'classiera'); ?>
											</option>
											<?php 
											if (function_exists('classiera_get_cities_by_state')) {
												echo classiera_get_cities_by_state($post_state, $post_city);
											}											
											?>
										</select>
                                    </div>
                                </div>
                            </div>
							<?php } ?>
							<!--Select City-->
							<div style="clear:left;"></div>
							<!--Address-->
							<?php if($classieraAddress == 1){?>
							       <input id="address" type="text" name="address" value="<?php echo $post_address; ?>" class="form-control address_search form-control-md" placeholder="<?php esc_html_e('Address or City', 'classiera') ?>" required>
							   
							<?php } ?>
							<!--Address-->
							<!--Google Value-->
							<div class="form-group">
                                <label class="col-sm-3 hide text-left flip"><?php esc_html_e('Set Latitude & Longitude', 'classiera') ?> : <span>*</span></label>
                                <div class="row">
								<?php 
									$googleFieldsOn = $redux_demo['google-lat-long']; 
									if($googleFieldsOn == 1){
								?>
                                    <div class="form-inline row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                <input type="text" name="latitude" id="latitude" value="<?php echo $post_latitude; ?>" class="form-control form-control-md" placeholder="<?php esc_html_e('Latitude', 'classiera') ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                <input type="text" name="longitude" value="<?php echo $post_longitude; ?>" id="longitude" class="form-control form-control-md" placeholder="<?php esc_html_e('Longitude', 'classiera') ?>" required>
                                            </div>
                                        </div>
                                    </div>
									<?php }else{ ?>
										<input type="hidden" id="latitude" name="latitude" value="<?php echo $post_latitude; ?>">
										<input type="hidden" id="longitude" name="longitude" value="<?php echo $post_longitude; ?>">
									<?php } ?>
                                    <div id="post-map" class="submitMAp">
                                       <?php /* <div id="map-canvas"></div>*/?>
									   <div id="newmappost" style="width: 100%; height: 500px"> </div>
										<?php /*<script type="text/javascript">
								jQuery(document).ready(function($) {
									var geocoder;
									var map;
									var marker;
									var geocoder = new google.maps.Geocoder();
									function geocodePosition(pos) {
											geocoder.geocode({
											latLng: pos
										}, function(responses) {
										if (responses && responses.length > 0) {
										  updateMarkerAddress(responses[0].formatted_address);
										} else {
										  updateMarkerAddress('Cannot determine address at this location.');
										}

									  });

									}

									function updateMarkerPosition(latLng) {
									  jQuery('#latitude').val(latLng.lat());
									  jQuery('#longitude').val(latLng.lng());
									}



									function updateMarkerAddress(str) {
									  jQuery('#address').val(str);
									}



									function initialize() {
									  var latlng = new google.maps.LatLng(<?php echo $post_latitude; ?>, <?php echo $post_longitude; ?>);
									  var mapOptions = {
										zoom: <?php echo $mapZoom; ?>,
										center: latlng
									  }

									  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
									  geocoder = new google.maps.Geocoder();
									  marker = new google.maps.Marker({
										position: latlng,
										map: map,
										draggable: true
									  });
									  // Add dragging event listeners.
									  google.maps.event.addListener(marker, 'dragstart', function() {
										updateMarkerAddress('Dragging...');
									  });									  

									  google.maps.event.addListener(marker, 'drag', function() {
										updateMarkerPosition(marker.getPosition());
									  });									  

									  google.maps.event.addListener(marker, 'dragend', function() {
										geocodePosition(marker.getPosition());
									  });
									}



									google.maps.event.addDomListener(window, 'load', initialize);
									jQuery(document).ready(function() {							         

									  initialize();									          

									  jQuery(function(){
										jQuery("#address").autocomplete({
										  //This bit uses the geocoder to fetch address values
										  source: function(request, response) {
											geocoder.geocode( {'address': request.term }, function(results, status) {
											  response(jQuery.map(results, function(item) {
												return {
												  label:  item.formatted_address,
												  value: item.formatted_address,
												  latitude: item.geometry.location.lat(),
												  longitude: item.geometry.location.lng()
												}

											  }));

											})

										  },

										  //This bit is executed upon selection of an address

										  select: function(event, ui) {
											jQuery("#latitude").val(ui.item.latitude);
											jQuery("#longitude").val(ui.item.longitude);
											var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
											marker.setPosition(location);
											map.setZoom(16);
											map.setCenter(location);
										  }

										});

									  });

									  

									  //Add listener to marker for reverse geocoding
									  google.maps.event.addListener(marker, 'drag', function() {
										geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
										  if (status == google.maps.GeocoderStatus.OK) {
											if (results[0]) {
											  jQuery('#address').val(results[0].formatted_address);
											  jQuery('#latitude').val(marker.getPosition().lat());
											  jQuery('#longitude').val(marker.getPosition().lng());
											}

										  }
										});
									  });							  

									});
								});
									</script>*/?>
									<script>
     
     
									   var map, infoWindow;
								
									  function initMap() {
										map = new google.maps.Map(document.getElementById('newmappost'), {
										  center: {lat: 0, lng: 0},
										  zoom: 13,
										  mapTypeId: 'roadmap'
										});
										
										 infoWindow = new google.maps.InfoWindow;
								
										// Try HTML5 geolocation.
										if (navigator.geolocation) {
										  navigator.geolocation.getCurrentPosition(function(position) {
											var pos = {
											  lat: position.coords.latitude,
											  lng: position.coords.longitude
											};
											document.getElementById("latitude").value =  position.coords.latitude;
											document.getElementById("longitude").value = position.coords.longitude;
											
											infoWindow.setPosition(pos);
											infoWindow.setContent('Location found.');
											infoWindow.open(map);
											map.setCenter(pos);
										  }, function() {
											handleLocationError(true, infoWindow, map.getCenter());
										  });
										} else {
										  // Browser doesn't support Geolocation
										  handleLocationError(false, infoWindow, map.getCenter());
										}
									  
								
										
										// Create the search box and link it to the UI element.
										var input = document.getElementById('address');
										var searchBox = new google.maps.places.SearchBox(input);
										map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
								
										// Bias the SearchBox results towards current map's viewport.
										map.addListener('bounds_changed', function() {
										  searchBox.setBounds(map.getBounds());
										});
								
										var markers = [];
										// Listen for the event fired when the user selects a prediction and retrieve
										// more details for that place.
										//To capture click event.
											 google.maps.event.addListener(map, 'click', function (e) {
												document.getElementById("latitude").value = e.latLng.lat();
												document.getElementById("longitude").value = e.latLng.lng();
												placeMarker(e.latLng,map);
											 });
										
										searchBox.addListener('places_changed', function() {
										  var places = searchBox.getPlaces();
								
										  if (places.length == 0) {
											return;
										  }
								
										  // Clear out the old markers.
										  markers.forEach(function(marker) {
											marker.setMap(null);
										  });
										  markers = [];
								
										  // For each place, get the icon, name and location.
										  var bounds = new google.maps.LatLngBounds();
										  places.forEach(function(place) {
											if (!place.geometry) {
											  console.log("Returned place contains no geometry");
											  return;
											}
											
								
											if (place.geometry.viewport) {
											  // Only geocodes have viewport.
											  bounds.union(place.geometry.viewport);
											} else {
											  bounds.extend(place.geometry.location);
											}
										  });
										  map.fitBounds(bounds);
										});
									  }
								
									 var marker;
									function placeMarker(location,map) {
									  if ( marker ) {
										marker.setPosition(location);
									  } else {
										marker = new google.maps.Marker({
										  position: location,
										  map: map
										});
									  }
									}
									function handleLocationError(browserHasGeolocation, infoWindow, pos) {
										infoWindow.setPosition(pos);
										infoWindow.setContent(browserHasGeolocation ?
															  'Error: The Geolocation service failed.' :
															  'Error: Your browser doesn\'t support geolocation.');
										infoWindow.open(map);
									}
								  
									</script>
                                    </div>
                                </div>
                            </div>
							<!--Google Value-->
						</div>
						<?php } ?>
						<div style="clear:left;"></div>
						<!-- post location -->
						<!--Select Ads Type-->
						<div class="form-main-section post-type">
							<h4 class="text-uppercase border-bottom hide"><?php esc_html_e('Select HYST Type', 'classiera') ?> :</h4>
							<p class="help-block"><?php esc_html_e('Select an Option to make your  featured or regular', 'classiera') ?></p>
							<div class="form-group">
							<?php
							/*Get Current Ad Type*/
							$featured_post = "0";
							$post_price_plan_activation_date = get_post_meta($current_post, 'post_price_plan_activation_date', true);
							$post_price_plan_expiration_date = get_post_meta($current_post, 'post_price_plan_expiration_date', true);
							$post_price_plan_expiration_date_noarmal = get_post_meta($current_post, 'post_price_plan_expiration_date_normal', true);
							$todayDate = strtotime(date('m/d/Y h:i:s'));
							$expireDate = $post_price_plan_expiration_date;
							if(!empty($post_price_plan_activation_date)) {
								if(($todayDate < $expireDate) or $post_price_plan_expiration_date == 0) {
									$featured_post = "1";
								}
							}
							/*Get Current Ad Type*/
							?>
							<div style="clear:left;"></div>
							<?php if($featured_post == "1") { ?>
							<div class="col-sm-4 col-md-3 col-lg-3 active-post-type">
								<h3 class="text-uppercase">
									<?php esc_html_e('Featured:', 'classiera') ?>
								</h3>
								<div class="radio">
									<input type="radio" id="feature-post" name="feature-post" value="featured" class="form-checkbox" checked><?php esc_html_e('Expiry:', 'classiera') ?> <?php if($post_price_plan_expiration_date_noarmal == 0) { ?> <?php esc_html_e( 'Never', 'classiera' ); ?> <?php } else { echo $post_price_plan_expiration_date_noarmal; } ?>
								</div>
							</div>
							<?php }else{ ?>
							<?php 							
								$regular_ads = $redux_demo['regular-ads'];
								$classieraRegularAdsDays = $redux_demo['ad_expiry'];
								$current_user = wp_get_current_user();
								$userID = $current_user->ID;
								$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
								$totalAds = '';
								$usedAds = '';
								$availableADS = '';
								$planCount = 0;
									if(!empty($result)){
										foreach ( $result as $info ) {											
											$totalAds = $info->ads;
											$usedAds = $info->used;											
											$name = $info->plan_name;
											if($totalAds == 'unlimited'){
												$name = esc_html__( 'Unlimited for Admin Only', 'classiera' );
												$availableADS = 'unlimited';
											}else{
												$availableADS = $totalAds-$usedAds;
											}
											if($availableADS != 0 || $totalAds == 'unlimited'){
											?>
												<div class="col-sm-4 col-md-3 col-lg-3">
													<div class="post-type-box">
														<h3 class="text-uppercase">
															<?php echo $name; ?>
														</h3>
														<p><?php esc_html_e('Total HYST Available', 'classiera') ?> : <?php echo $availableADS; ?></p>
														<p><?php esc_html_e('Used HYST with this Plan', 'classiera') ?> : <?php echo $usedAds; ?></p>
														<div class="radio">
															<input id="featured<?php echo $planCount; ?>" type="radio" name="classiera_post_type" value="<?php echo $info->id; ?>">
															<label for="featured<?php echo $planCount; ?>"><?php esc_html_e('Select', 'classiera') ?></label>
														</div>
													</div>
												</div>
											<?php
											}
											$planCount++;
										}
									}
							?>
								<?php if($regular_ads == 1 ){?>
									<div class="col-md-6 col-md-offset-3 active-post-type">
										<div class="post-type-box">
											<h3 class="text-uppercase"><?php esc_html_e('Regular', 'classiera') ?></h3>
											<p><?php esc_html_e('For', 'classiera') ?>&nbsp;<?php echo $classieraRegularAdsDays; ?>&nbsp;<?php esc_html_e('days', 'classiera') ?></p>
											<div class="radio">
												<input id="regular" type="radio" name="classiera_post_type" value="classiera_regular" checked>
												<label for="regular"><?php esc_html_e('Select', 'classiera') ?></label>
											</div>
											<input type="hidden" name="regular-ads-enable" value=""  >
										</div>
									</div>
								<?php } ?>
									<!--Pay Per Post Per Category Base-->
								<div class="col-sm-4 col-md-3 col-lg-3 classieraPayPerPost">
									<div class="post-type-box">
										<h3 class="text-uppercase">
											<?php esc_html_e('Featured HYST', 'classiera') ?>
											<p class="classieraPPP"></p>
											<input id="payperpost" type="radio" name="classiera_post_type" value="payperpost">
											<label for="payperpost">
											<?php esc_html_e('select', 'classiera') ?>
											</label>
										</h3>
									</div>
								</div>
								<!--Pay Per Post Per Category Base-->
							<?php } ?>
							</div>
						</div>
						<!--Select Ads Type-->
						
						<div class="clearfix"></div>
						<div class="top-buffer2"></div>
						   <div class="single_title row">
								<h2>location</h2>
						</div>
						<div class="clearfix"></div>
						<?php
						global $wpdb;
						//echo "select * from wp_found_hysts where hyst_id='".$post->ID."'";
						$sel_found = $wpdb->get_results("select * from wp_found_hysts where hyst_id='".$_GET['post']."'");
						if(count($sel_found)>0)
						{
							$count=1;
							//echo '=>';
							//print_r($sel_found);
							foreach($sel_found as $key=>$val)
							{?><div style="clear:left;"></div>
								<div class="form-group" style="margin-top:20px;">
									<label class="col-sm-3 text-left hide flip"><?php esc_html_e('Found Location '.$count, 'classiera'); ?>: <span>*</span></label>
									<div class="col-md-8 col-md-offset-2">
										<div class="inner-addon right-addon">
											    <i class="form-icon right-form-icon hide fa"></i>
											    <?php //echo date("m/d/Y", strtotime($sel_found[$key]->expiry_date));?>
											<div class="form-group col-sm-6">	
												<input type="text" name="found_location<?php echo $count;?>" id="found_location<?php echo $count;?>" class="form-control form-control-md" value="<?php echo $sel_found[$key]->found_location;?>" readonly>
											</div>
											<div class="form-group col-sm-6">
												<input type="text" value="<?php echo $sel_found[$key]->found_price;?>" name="location<?php echo $count;?>_price" readonly id="location<?php echo $count;?>_price" class="form-control form-control-md">
											</div>
											<div class="form-group col-sm-6">
												<input size="16" type="text" id="expiry_date<?php echo $count;?>"  name="expiry_date<?php echo $count;?>" class="form-control form-control-md" value="<?php echo date("m/d/Y", strtotime($sel_found[$key]->expiry_date));?>">
											</div>
											<div class="form-group col-sm-6">
												<input type="text" value="<?php echo $sel_found[$key]->qty;?>" name="qty<?php echo $count;?>" id="qty<?php echo $count;?>" placeholder="Quantity" class="form-control form-control-md">
											</div>
												<input type="hidden" name="found_id<?php echo $count;?>" id="found_id<?php echo $count;?>" value="<?php echo $sel_found[$key]->id;?>">
											<div class="top-buffer1"></div>
											<hr class="line2">
											<div class="top-buffer1"></div>
											
										</div>
									</div>
								</div>
								 <script>
									jQuery(document).ready(function($) {
										//jQuery("#expire_date").datepicker();
										jQuery("#expiry_date<?php echo $count;?>").datepicker({
										 minDate: new Date(<?php echo date("Y, m, d");?>)
										});
									});									    
								</script><?php
								$count++;
							}
						}
						?>
						
							
						</div>
						
						
    </script>	
						<?php 
						$featured_plans = $redux_demo['featured_plans'];
						if(!empty($featured_plans)){
							if($featuredADS == "0" || empty($result)){
						?>
                                <div class="help-block terms-use">
                                    <?php esc_html_e('Currently you have no active plan for featured HYST. You must purchase a', 'classiera') ?> <strong><a href="<?php echo $featured_plans; ?>" target="_blank"><?php esc_html_e('Featured Pricing Plan', 'classiera') ?></a></strong> <?php esc_html_e('to be able to publish a Featured Ad.', 'classiera') ?>
                                </div>
						<?php }} ?>
                                <div class="help-block terms-use">
                                    <?php esc_html_e('By clicking "Update HYST", you agree to our', 'classiera') ?> <a href="<?php echo $termsandcondition; ?>"><?php esc_html_e('Terms of Use', 'classiera') ?></a> <?php esc_html_e('and acknowledge that you are the rightful owner of this item', 'classiera') ?>
                                </div>
						<div style="clear:left;"></div>
						<div id="mydropzone" class="classiera-image-upload clearfix" data-maxfile="<?php echo $imageLimit; ?>">
										<!--PreviousImages-->
										<?php require_once get_template_directory() . '/inc/BFI_Thumb.php'; ?>
										<?php
										$imageCount = 0;
										$params = array( 'width' => 110, 'height' => 70, 'crop' => true );
										$imgargs = array(
											'post_parent' => $current_post,
											'post_status' => 'inherit',
											'post_type'   => 'attachment', 
											'post_mime_type'   => 'image', 
											'order' => 'ASC',
											'orderby' => 'menu_order ID',
										);
										$attachments = get_children($imgargs);
										if($attachments){
										foreach($attachments as $att_id => $attachment){
												$attachment_ID = $attachment->ID;
												$full_img_url = wp_get_attachment_url($attachment->ID);
												$split_pos = strpos($full_img_url, 'wp-content');
												$split_len = (strlen($full_img_url) - $split_pos);
												$abs_img_url = substr($full_img_url, $split_pos, $split_len);
												$full_info = @getimagesize(ABSPATH.$abs_img_url);
										?>
											<div id="<?php echo $attachment_ID; ?>" class="edit-post-image-block">
												<img class="edit-post-image" src="<?php echo bfi_thumb( "$full_img_url", $params ) ?>" />
												<?php /*<div class="remove-edit-post-image">
													<i class="fa fa-minus-square-o"></i>
													<span class="remImage"><?php esc_html_e('Remove', 'classiera');?></span>
													<input type="hidden" name="" value="<?php echo $attachment_ID; ?>">
												</div><!--remove-edit-post-image-->*/?>
											</div>
											<?php $imageCount++;?>
										<?php }?>
										<?php }?></div>
							<style>
					 #jquery-script-menu {
					position: fixed;
					height: 90px;
					width: 100%;
					top: 0;
					left: 0;
					border-top: 5px solid #316594;
					background: #fff;
					-moz-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					-webkit-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					z-index: 999999;
					padding: 10px 0;
					-webkit-box-sizing:content-box;
					-moz-box-sizing:content-box;
					box-sizing:content-box;
					}
					
					.jquery-script-center {
					width: 960px;
					margin: 0 auto;
					}
					.jquery-script-center ul {
					width: 212px;
					float:left;
					line-height:45px;
					margin:0;
					padding:0;
					list-style:none;
					}
					.jquery-script-center a {
						text-decoration:none;
					}
					.jquery-script-ads {
					width: 728px;
					height:90px;
					float:right;
					}
					.jquery-script-clear {
					clear:both;
					height:0;
					}
					table tr{background:#ffffff !important;}
					 </style>
					 <?php //echo 'line 60'?>
					<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/bootstrap.min.css">
					<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
					 <?php //echo 'line 63';?>
					<link href="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/dist/bootstrap-FileUpload.css" rel="stylesheet" type="text/css">
					<div>
						
						<?php /*<div class="fileupload-wrapper"><div id="myUpload"></div></div>*/?>
					  </div>
					 
					 <?php /* <script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/jquery-1.12.4.min.js" ></script>*/?>
					  <script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/bootstrap.min.js"></script>
					<script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/dist/bootstrap-FileUpload_editinventory.js"></script>
					<script>
					  jQuery("#myUpload").bootstrapFileUpload({
					  url: "<?php echo site_url();?>/edit-inventory/?post=<?php echo $_GET['post'];?>"
					});
					
					jQuery("#title").change(function() {
					  jQuery("#hdn_title").val(jQuery("#title").val());
					});
					jQuery("#description").change(function() {
					  jQuery("#hdn_description").val(jQuery("#description").val());
					});
					jQuery("#post_tags").change(function() {
					  jQuery("#hdn_post_tags").val(jQuery("#post_tags").val());
					});
					jQuery("#post_price").change(function() {
					  jQuery("#hdn_post_price").val(jQuery("#post_price").val());
					});
					jQuery("#address").change(function() {
					  jQuery("#hdn_address").val(jQuery("#address").val());
					});
					jQuery("#latitude").change(function() {
					  jQuery("#hdn_latitude").val(jQuery("#latitude").val());
					});
					jQuery("#longitude").change(function() {
					  jQuery("#hdn_longitude").val(jQuery("#longitude").val());
					});
					</script>
					<script>
					function submit_form()
					{
						jQuery("#hdn_classiera-main-cat-field").val(jQuery("#classiera-main-cat-field").val());
						jQuery("#hdn_classiera-sub-cat-field").val(jQuery("#classiera-sub-cat-field").val());
						jQuery("#hdn_classiera_third_cat").val(jQuery("#classiera_third_cat").val());
						jQuery("#hdn_regular-ads-enable").val(jQuery("#regular-ads-enable").val());
						jQuery("#hdn_regular_plan_id").val(jQuery("#regular_plan_id").val());
						jQuery("#hdn_submitted").val(jQuery("#submitted").val());
						jQuery("#hdn_post_nonce_field").val(jQuery("#post_nonce_field").val());
						jQuery("#hdn_title").val(jQuery("#title").val());
						jQuery("#hdn_post_tags").val(jQuery("#post_tags").val());
						jQuery("#hdn_address").val(jQuery("#address").val());
						jQuery("#hdn_found_location1").val(jQuery("#found_location1").val());
						jQuery("#hdn_found_location2").val(jQuery("#found_location2").val());
						jQuery("#hdn_found_location3").val(jQuery("#found_location3").val());
						jQuery("#hdn_found_location4").val(jQuery("#found_location4").val());
						jQuery("#hdn_found_location5").val(jQuery("#found_location5").val());
						jQuery("#hdn_location1_price").val(jQuery("#location1_price").val());
						jQuery("#hdn_location2_price").val(jQuery("#location2_price").val());
						jQuery("#hdn_location3_price").val(jQuery("#location3_price").val());
						jQuery("#hdn_location4_price").val(jQuery("#location4_price").val());
						jQuery("#hdn_location5_price").val(jQuery("#location5_price").val());
						 jQuery("#hdn_description").val(jQuery("#description").val());
						 jQuery("#hdn_post_price").val(jQuery("#post_price").val());
						jQuery("#hdn_expiry_date1").val(jQuery("#expiry_date1").val());
						jQuery("#hdn_expiry_date2").val(jQuery("#expiry_date2").val());
						jQuery("#hdn_expiry_date3").val(jQuery("#expiry_date3").val());
						jQuery("#hdn_expiry_date4").val(jQuery("#expiry_date4").val());
						jQuery("#hdn_expiry_date5").val(jQuery("#expiry_date5").val());
						
						jQuery("#hdn_qty1").val(jQuery("#qty1").val());
						jQuery("#hdn_qty2").val(jQuery("#qty2").val());
						jQuery("#hdn_qty3").val(jQuery("#qty3").val());
						jQuery("#hdn_qty4").val(jQuery("#qty4").val());
						jQuery("#hdn_qty5").val(jQuery("#qty5").val());
						
						jQuery("#hdn_found_id1").val(jQuery("#found_id1").val());
						jQuery("#hdn_found_id2").val(jQuery("#found_id2").val());
						jQuery("#hdn_found_id3").val(jQuery("#found_id3").val());
						jQuery("#hdn_found_id4").val(jQuery("#found_id4").val());
						jQuery("#hdn_found_id5").val(jQuery("#found_id5").val());
						
						 jQuery("#hdn_cat").val(jQuery("#cat").val());
						
						
						
						  jQuery("#hdn_latitude").val(jQuery("#latitude").val());
						  jQuery("#hdn_longitude").val(jQuery("#longitude").val());
						jQuery("#frm_save_hyst").submit();
						
					}
					</script>
						
					<?php /*</form>*/?>
					<div class="clearfix"></div>
					<div class="top-buffer2"></div>
					    <div class="form-main-section">
                            <div class="col-sm-4 col-sm-offset-4">
							   <hr class="line1">
					           <div class="top-buffer1"></div>
								<input type="hidden" class="regular_plan_id" name="regular_plan_id" value="">
								<?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>
								<input type="hidden" name="submitted" id="submitted" value="true">
                                <button class="post-submit btn btn-primary sharp theme-btn btn-md btn-style-one btn-block" type="submit" name="op" value="<?php esc_html_e('Update Ad', 'classiera') ?>" onclick="submit_form();"><?php esc_html_e('Update HYST', 'classiera') ?></button>
					            <div class="top-buffer1"></div>
								<hr class="line1">
                            </div>
                        </div>
					<div class="clearfix"></div>
					</div>
				</div><!--submit-post-->
			</div><!--col-lg-9-->
		</div><!--row-->
	</div><!--container-->
</section><!--user-pages-->
<?php endwhile; ?>

<?php get_footer(); ?>