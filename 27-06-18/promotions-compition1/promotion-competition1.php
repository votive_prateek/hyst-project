<?php
/*
Plugin Name: Promotion and Compitition1

*/

// Register settings using the Settings API 
 
// Modify capability
/**
 * Register a custom menu page.
 */
 
 
function wpdocs_register_competition_menu_page1(){
    add_menu_page( 
        __( 'Competition1', 'textdomain' ),
        'Competition1',
        'manage_options',
        'Competition1',
        'competition_menu_page1',
        '',
        7
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_competition_menu_page1' );
function get_extension2($file) {
 $extension = end(explode(".", $file));
 return $extension ? $extension : false;
}
/**
 * Display a custom menu page
 */
function competition_menu_page1(){
	$error=array();
	global $wpdb;


wp_enqueue_script( 'jquery-ui-datepicker' );

?>
<style type="text/css">
#selectCatCheck {
    width: 0;
    height: 0;
    opacity: 0;
    position: absolute;
}
</style><?php
?><link href="<?php echo get_template_directory_uri() . '/css/classiera.css';?>" type="text/css"/></link>
<?php

		wp_enqueue_script('bootstrap-dropdownhover', get_template_directory_uri() . '/js/bootstrap-dropdownhover.js', 'jquery', '', true);	
	wp_enqueue_script('validator.min', get_template_directory_uri() . '/js/validator.min.js', 'jquery', '', true);
	wp_enqueue_script('owl.carousel.min', get_template_directory_uri() . '/js/owl.carousel.min.js', 'jquery', '', true);	
	wp_enqueue_script('jquery.matchHeight', get_template_directory_uri() . '/js/jquery.matchHeight.js', 'jquery', '', true);
	wp_enqueue_script('infinitescroll', get_template_directory_uri() . '/js/infinitescroll.js', 'jquery', '', true);
	wp_enqueue_script('masonry.pkgd.min', get_template_directory_uri() . '/js/masonry.pkgd.min.js', 'jquery', '', true);
	wp_enqueue_script('select2.min', get_template_directory_uri() . '/js/select2.min.js', 'jquery', '', true);
	wp_enqueue_script('classiera', get_template_directory_uri() . '/js/classiera.js', 'jquery', '', true);
	
	wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', 'jquery', '', true);	
	
	
	
	if(isset($_POST['btn_submit']) && $_POST['btn_submit']!='')
	{
	//print_r($_POST);exit;
		$classieraMainCat = $_POST['classiera-main-cat-field'];
				$classieraChildCat = $_POST['classiera-sub-cat-field'];
				$classieraThirdCat = $_POST['classiera_third_cat'];
		$post_information = array(
			'post_title' => esc_attr(strip_tags($_POST['name'])),			
			'post_content' => strip_tags($_POST['description'], '<h1><h2><h3><strong><b><ul><ol><li><i><a><blockquote><center><embed><iframe><pre><table><tbody><tr><td><video><br>'),
			'post-type' => 'post',
			'post_category' => array($classieraMainCat, $classieraChildCat, $classieraThirdCat),
			'tags_input'    => explode(',', $_POST['competition_tags']),
			'tax_input' => array(
			'location' => $_POST['address'],
			),
			'comment_status' => 'open',
			'ping_status' => 'open',
			'post_status' => 'publish'
		);

		$post_id = wp_insert_post($post_information);
		$classiera_CF_Front_end = $_POST['classiera_CF_Front_end'];
		$classiera_sub_fields = $_POST['classiera_sub_fields'];
		update_post_meta($post_id, 'is_competition', '1');
		update_post_meta($post_id, 'classiera_CF_Front_end', $classiera_CF_Front_end);
		update_post_meta($post_id, 'classiera_sub_fields', $classiera_sub_fields);

		update_post_meta($post_id, 'compitition_type', $_POST['compitition_type']);
		
		update_post_meta($post_id, 'post_perent_cat', $classieraMainCat, $allowed);
		update_post_meta($post_id, 'post_child_cat', $classieraChildCat, $allowed);				
		update_post_meta($post_id, 'post_inner_cat', $classieraThirdCat, $allowed);
		update_post_meta($post_id, 'is_inventory', '0', $allowed);
		update_post_meta($post_id, 'start_date', $_POST['start_date'], $allowed);
		update_post_meta($post_id, 'end_date', $_POST['end_date'], $allowed);
		
		
		update_post_meta($post_id, 'post_location', wp_kses($_POST['address'], $allowed));
				
		update_post_meta($post_id, 'post_state', wp_kses($poststate, $allowed));
		update_post_meta($post_id, 'post_city', wp_kses($postCity, $allowed));

		update_post_meta($post_id, 'post_latitude', wp_kses($_POST['latitude'], $allowed));

		update_post_meta($post_id, 'post_longitude', wp_kses($_POST['longitude'], $allowed));

		update_post_meta($post_id, 'post_address', wp_kses($_POST['address'], $allowed));
		
		update_post_meta($post_id, 'entry_limit', wp_kses($_POST['entry_limit'], $allowed));
		if(isset($_POST['is_entry_fields']) && $_POST['is_entry_fields']==1)
		{
			$e_textbox=1;
			$e_textarea=1;
			$e_number=1;
			$e_email=1;
			$e_image_upload=1;
			$e_multi_image_upload=1;
			$e_video_upload=1;
			$e_multi_video_upload=1;
			$e_image_video_upload=1;
			$e_file_upload=1;
			$e_datepicker=1;
			$e_checkbox=1;
			$e_radio=1;
			$e_dropdown=1;
			$e_map_location=1;
			update_post_meta($post_id, 'is_entry_fields', wp_kses($_POST['is_entry_fields'], $allowed));
			for($abc=1;$abc<=$_POST['entry_field'];$abc++)
			{
				if(isset($_POST['entry_title_textbox'.$abc]) && $_POST['entry_title_textbox'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_textbox_'.$e_textbox, wp_kses($_POST['entry_title_textbox'.$abc], $allowed));
					if(isset($_POST['entry_description_textbox'.$abc]) && $_POST['entry_description_textbox'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_textbox_'.$e_textbox, wp_kses($_POST['entry_description_textbox'.$abc], $allowed));
						$e_textbox++;
					}
				}
				
				
				if(isset($_POST['entry_title_textarea'.$abc]) && $_POST['entry_title_textarea'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_textarea_'.$e_textarea, wp_kses($_POST['entry_title_textarea'.$abc], $allowed));
					if(isset($_POST['entry_description_textarea'.$abc]) && $_POST['entry_description_textarea'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_textarea_'.$e_textarea, wp_kses($_POST['entry_description_textarea'.$abc], $allowed));
					}
					$e_textarea++;
				}
				
				
				if(isset($_POST['entry_title_number'.$abc]) && $_POST['entry_title_number'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_number_'.$e_number, wp_kses($_POST['entry_title_number'.$abc], $allowed));
					if(isset($_POST['entry_description_number'.$abc]) && $_POST['entry_description_number'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_number_'.$e_number, wp_kses($_POST['entry_description_number'.$abc], $allowed));
					}
					$e_number++;
				}
				
				
				if(isset($_POST['entry_title_email'.$abc]) && $_POST['entry_title_email'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_email_'.$e_email, wp_kses($_POST['entry_title_email'.$abc], $allowed));
					if(isset($_POST['entry_description_email'.$abc]) && $_POST['entry_description_email'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_email_'.$e_email, wp_kses($_POST['entry_description_email'.$abc], $allowed));
					}
					$e_email++;
				}
				
				
			
				if(isset($_POST['entry_title_image_upload'.$abc]) && $_POST['entry_title_image_upload'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_image_upload_'.$e_image_upload, wp_kses($_POST['entry_title_image_upload'.$abc], $allowed));
					if(isset($_POST['entry_description_image_upload'.$abc]) && $_POST['entry_description_image_upload'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_image_upload_'.$e_image_upload, wp_kses($_POST['entry_description_image_upload'.$abc], $allowed));
					}
					$e_image_upload++;
				}
				
				
				if(isset($_POST['entry_title_multi_image_upload'.$abc]) && $_POST['entry_title_multi_image_upload'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_multi_image_upload_'.$e_multi_image_upload, wp_kses($_POST['entry_title_multi_image_upload'.$abc], $allowed));
					if(isset($_POST['entry_description_multi_image_upload'.$abc]) && $_POST['entry_description_multi_image_upload'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_multi_image_upload_'.$e_multi_image_upload, wp_kses($_POST['entry_description_multi_image_upload'.$abc], $allowed));
					}
					$e_multi_image_upload++;
				}
				
			
				if(isset($_POST['entry_title_video_upload'.$abc]) && $_POST['entry_title_video_upload'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_video_upload_'.$e_video_upload, wp_kses($_POST['entry_title_video_upload'.$abc], $allowed));
					if(isset($_POST['entry_description_video_upload'.$abc]) && $_POST['entry_description_video_upload'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_video_upload_'.$e_video_upload, wp_kses($_POST['entry_description_video_upload'.$abc], $allowed));
					}
					$e_video_upload++;
				}
				
				
				if(isset($_POST['entry_title_multi_video_upload'.$abc]) && $_POST['entry_title_multi_video_upload'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_multi_video_upload_'.$e_multi_video_upload, wp_kses($_POST['entry_title_multi_video_upload'.$abc], $allowed));
					if(isset($_POST['entry_description_multi_video_upload'.$abc]) && $_POST['entry_description_multi_video_upload'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_multi_video_upload_'.$e_multi_video_upload, wp_kses($_POST['entry_description_multi_video_upload'.$abc], $allowed));
					}
					$e_multi_video_upload++;
				}
				
			
				if(isset($_POST['entry_title_image_video_upload'.$abc]) && $_POST['entry_title_image_video_upload'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_image_video_upload_'.$e_image_video_upload, wp_kses($_POST['entry_title_image_video_upload'.$abc], $allowed));
					if(isset($_POST['entry_description_image_video_upload'.$abc]) && $_POST['entry_description_image_video_upload'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_image_video_upload_'.$e_image_video_upload, wp_kses($_POST['entry_description_image_video_upload'.$abc], $allowed));
					}
					$e_image_video_upload++;
				}
				
			
				if(isset($_POST['entry_title_map_location'.$abc]) && $_POST['entry_title_map_location'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_map_location_'.$e_map_location, wp_kses($_POST['entry_title_map_location'.$abc], $allowed));
					if(isset($_POST['entry_description_map_location'.$abc]) && $_POST['entry_description_map_location'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_map_location_'.$e_map_location, wp_kses($_POST['entry_description_map_location'.$abc], $allowed));
					}
					$e_map_location++;
				}
				
				
				if(isset($_POST['entry_title_checkbox'.$abc]) && $_POST['entry_title_checkbox'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_checkbox_'.$e_checkbox, wp_kses($_POST['entry_title_checkbox'.$abc], $allowed));
					if(isset($_POST['entry_description_checkbox'.$abc]) && $_POST['entry_description_checkbox'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_checkbox_'.$e_checkbox, wp_kses($_POST['entry_description_checkbox'.$abc], $allowed));
						update_post_meta($post_id, 'entry_checkbox_options_'.$e_checkbox, wp_kses($_POST['entry_checkbox_options'.$abc], $allowed));
					}
					$e_checkbox++;
				}
				
				
				if(isset($_POST['entry_title_radio'.$abc]) && $_POST['entry_title_radio'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_radio_'.$e_radio, wp_kses($_POST['entry_title_radio'.$abc], $allowed));
					if(isset($_POST['entry_description_radio'.$abc]) && $_POST['entry_description_radio'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_radio_'.$e_radio, wp_kses($_POST['entry_description_radio'.$abc], $allowed));
						update_post_meta($post_id, 'entry_radio_options_'.$e_radio, wp_kses($_POST['entry_radio_options'.$abc], $allowed));
					}
					$e_radio++;
				}
				
				
				if(isset($_POST['entry_title_date_picker'.$abc]) && $_POST['entry_title_date_picker'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_date_picker_'.$e_datepicker, wp_kses($_POST['entry_title_date_picker'.$abc], $allowed));
					if(isset($_POST['entry_description_date_picker'.$abc]) && $_POST['entry_description_date_picker'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_date_picker_'.$e_datepicker, wp_kses($_POST['entry_description_date_picker'.$abc], $allowed));
					}
					$e_datepicker++;
				}
				
			
				if(isset($_POST['entry_title_dropdown'.$abc]) && $_POST['entry_title_dropdown'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_dropdown_'.$e_dropdown, wp_kses($_POST['entry_title_dropdown'.$abc], $allowed));
					if(isset($_POST['entry_description_dropdown'.$abc]) && $_POST['entry_description_dropdown'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_dropdown_'.$e_dropdown, wp_kses($_POST['entry_description_dropdown'.$abc], $allowed));
					}
					update_post_meta($post_id, 'entry_dropdown_options_'.$e_dropdown, wp_kses($_POST['entry_dropdown_options'.$abc], $allowed));
					$e_dropdown++;
				}
				
			
				if(isset($_POST['entry_title_file_upload'.$abc]) && $_POST['entry_title_file_upload'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_file_upload_'.$e_file_upload, wp_kses($_POST['entry_title_file_upload'.$abc], $allowed));
					if(isset($_POST['entry_description_file_upload'.$abc]) && $_POST['entry_description_file_upload'.$abc]!='')
					{
						update_post_meta($post_id, 'entry_description_file_upload_'.$e_file_upload, wp_kses($_POST['entry_description_file_upload'.$abc], $allowed));
					}
					$e_file_upload++;
				}
				
			
				if(isset($_POST['entry_title_recaptcha'.$abc]) && $_POST['entry_title_recaptcha'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_title_recaptcha', wp_kses($_POST['entry_title_recaptcha'.$abc], $allowed));
				}
				if(isset($_POST['entry_description_recaptcha'.$abc]) && $_POST['entry_description_recaptcha'.$abc]!='')
				{
					update_post_meta($post_id, 'entry_description_recaptcha', wp_kses($_POST['entry_description_recaptcha'.$abc], $allowed));
				}
			
			
		
			
			
			}
			update_post_meta($post_id, 'e_total_textbox', wp_kses($e_textbox-1, $allowed));
			update_post_meta($post_id, 'e_total_textarea', wp_kses($e_textarea-1, $allowed));
			update_post_meta($post_id, 'e_total_number', wp_kses($e_number-1, $allowed));
			update_post_meta($post_id, 'e_total_email', wp_kses($e_email-1, $allowed));
			update_post_meta($post_id, 'e_total_image_upload', wp_kses($e_image_upload-1, $allowed));
			update_post_meta($post_id, 'e_total_multi_image_upload', wp_kses($e_multi_image_upload-1, $allowed));
			update_post_meta($post_id, 'e_total_video_upload', wp_kses($e_video_upload-1, $allowed));
			update_post_meta($post_id, 'e_total_multi_video_upload', wp_kses($e_multi_video_upload-1, $allowed));
			update_post_meta($post_id, 'e_total_image_video_upload', wp_kses($e_image_video_upload-1, $allowed));
			update_post_meta($post_id, 'e_total_file_upload', wp_kses($e_file_upload-1, $allowed));
			update_post_meta($post_id, 'e_total_date_pocker', wp_kses($e_datepicker-1, $allowed));
			update_post_meta($post_id, 'e_total_checkbox', wp_kses($e_checkbox-1, $allowed));
			update_post_meta($post_id, 'e_total_radio', wp_kses($e_radio-1, $allowed));
			update_post_meta($post_id, 'e_total_dropdown', wp_kses($e_dropdown-1, $allowed));
			update_post_meta($post_id, 'e_total_map_location', wp_kses($e_map_location-1, $allowed));
			
			
		}
		update_post_meta($post_id, 'total_stage', wp_kses($_POST['total_stage'], $allowed));
	if(isset($_POST['total_stage']) && $_POST['total_stage']>=1)
	{
		
		for($lp=1;$lp<=$_POST['total_stage'];$lp++)
		{
			
			$textboxcount=1;
			$textareacount=1;
			$numbercount=1;
			$emailcount=1;
			$imageuploadcount=1;
			$multiimageuploadcount=1;
			$videouploadcount=1;
			$multivideouploadcount=1;
			$imagevideouploadcount=1;
			$maplocationcount=1;
			$checkboxcount=1;
			$radiocount=1;
			$datepickercount=1;
			$dropdowncount=1;
			$fileuploadcount=1;
			$recaptchacount=1;
					//stage_field
			update_post_meta($post_id, 'stage_order_'.$lp, wp_kses($_POST['stage_order_'.$lp], $allowed));
			update_post_meta($post_id, 'stage_name_'.$lp, wp_kses($_POST['stage_name_'.$lp], $allowed));
			update_post_meta($post_id, 'stage_description_'.$lp, wp_kses($_POST['stage_description_'.$lp], $allowed));
			update_post_meta($post_id, 'stage_start_date_'.$lp, wp_kses($_POST['stage_start_date_'.$lp], $allowed));
			update_post_meta($post_id, 'stage_end_date_'.$lp, wp_kses($_POST['stage_end_date_'.$lp], $allowed));
			
			for($abc=1;$abc<=$_POST['stage_field'];$abc++)
			{
				if(isset($_POST['stage_title_textbox_'.$lp.'_'.$abc]) && $_POST['stage_title_textbox_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_textbox_'.$lp.'_'.$textboxcount, wp_kses($_POST['stage_title_textbox_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_textbox_'.$lp.'_'.$textboxcount, wp_kses($_POST['stage_description_textbox_'.$lp.'_'.$abc], $allowed));
					$textboxcount++;
				}
				if(isset($_POST['stage_title_textarea_'.$lp.'_'.$abc]) && $_POST['stage_title_textarea_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_textarea_'.$lp.'_'.$textareacount, wp_kses($_POST['stage_title_textarea_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_textarea_'.$lp.'_'.$textareacount, wp_kses($_POST['stage_description_textarea_'.$lp.'_'.$abc], $allowed));
					$textareacount++;
				}
				if(isset($_POST['stage_title_number_'.$lp.'_'.$abc]) && $_POST['stage_title_number_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_number_'.$lp.'_'.$numbercount, wp_kses($_POST['stage_title_number_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_number_'.$lp.'_'.$numbercount, wp_kses($_POST['stage_description_number_'.$lp.'_'.$abc], $allowed));
					$numbercount++;
				}
				if(isset($_POST['stage_title_email_'.$lp.'_'.$abc]) && $_POST['stage_title_email_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_email_'.$lp.'_'.$emailcount, wp_kses($_POST['stage_title_email_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_email_'.$lp.'_'.$emailcount, wp_kses($_POST['stage_description_email_'.$lp.'_'.$abc], $allowed));
					$emailcount++;
				}
				if(isset($_POST['stage_title_image_upload_'.$lp.'_'.$abc]) && $_POST['stage_title_image_upload_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_image_upload_'.$lp.'_'.$imageuploadcount, wp_kses($_POST['stage_title_image_upload_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_image_upload_'.$lp.'_'.$imageuploadcount, wp_kses($_POST['stage_description_image_upload_'.$lp.'_'.$abc], $allowed));
					$imageuploadcount++;
				}
				if(isset($_POST['stage_title_multi_image_upload_'.$lp.'_'.$abc]) && $_POST['stage_title_multi_image_upload_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_multi_image_upload_'.$lp.'_'.$multiimageuploadcount, wp_kses($_POST['stage_title_multi_image_upload_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_multi_image_upload_'.$lp.'_'.$multiimageuploadcount, wp_kses($_POST['stage_description_multi_image_upload_'.$lp.'_'.$abc], $allowed));
					$multiimageuploadcount++;
				}
				
				if(isset($_POST['stage_title_video_upload_'.$lp.'_'.$abc]) && $_POST['stage_title_video_upload_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_video_upload_'.$lp.'_'.$videouploadcount, wp_kses($_POST['stage_title_video_upload_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_video_upload_'.$lp.'_'.$videouploadcount, wp_kses($_POST['stage_description_video_upload_'.$lp.'_'.$abc], $allowed));
					$videouploadcount++;
				}
				if(isset($_POST['stage_title_multi_video_upload_'.$lp.'_'.$abc]) && $_POST['stage_title_multi_video_upload_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_multi_video_upload_'.$lp.'_'.$multivideouploadcount, wp_kses($_POST['stage_title_multi_video_upload_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_multi_video_upload_'.$lp.'_'.$multivideouploadcount, wp_kses($_POST['stage_description_multi_video_upload_'.$lp.'_'.$abc], $allowed));
					$multivideouploadcount++;
				}
				
				if(isset($_POST['stage_title_image_video_upload_'.$lp.'_'.$abc]) && $_POST['stage_title_image_video_upload_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_image_video_upload_'.$lp.'_'.$imagevideouploadcount, wp_kses($_POST['stage_title_image_video_upload_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_image_video_upload_'.$lp.'_'.$imagevideouploadcount, wp_kses($_POST['stage_description_image_video_upload_'.$lp.'_'.$abc], $allowed));
					$imagevideouploadcount++;
				}
				
				if(isset($_POST['stage_title_location_'.$lp.'_'.$abc]) && $_POST['stage_title_location_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_map_location_'.$lp.'_'.$maplocationcount, wp_kses($_POST['stage_title_location_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_map_location_'.$lp.'_'.$maplocationcount, wp_kses($_POST['stage_description_location_'.$lp.'_'.$abc], $allowed));
					$maplocationcount++;
				}
				if(isset($_POST['stage_title_checkbox_'.$lp.'_'.$abc]) && $_POST['stage_title_checkbox_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_checkbox_'.$lp.'_'.$checkboxcount, wp_kses($_POST['stage_title_checkbox_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_checkbox_'.$lp.'_'.$checkboxcount, wp_kses($_POST['stage_description_checkbox_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_checkbox_options_'.$lp.'_'.$checkboxcount, wp_kses($_POST['stage_checkbox_options_'.$lp.'_'.$abc], $allowed));
					$checkboxcount++;
				}
				
				if(isset($_POST['stage_title_radio_'.$lp.'_'.$abc]) && $_POST['stage_title_radio_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_radio_'.$lp.'_'.$radiocount, wp_kses($_POST['stage_title_radio_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_radio_'.$lp.'_'.$radiocount, wp_kses($_POST['stage_description_radio_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_radio_options_'.$lp.'_'.$radiocount, wp_kses($_POST['stage_radio_options_'.$lp.'_'.$abc], $allowed));
					$radiocount++;
				}
				
				/*if(isset($_POST['stage_title_radio_'.$lp.'_'.$abc]) && $_POST['stage_title_radio_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_radio_'.$lp.'_'.$radiocount, wp_kses($_POST['stage_title_radio_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_radio_'.$lp.'_'.$radiocount, wp_kses($_POST['stage_description_radio_'.$lp.'_'.$abc], $allowed));
					$radiocount++;
				}*/
				
				if(isset($_POST['stage_title_date_picker_'.$lp.'_'.$abc]) && $_POST['stage_title_date_picker_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_date_picker_'.$lp.'_'.$datepickercount, wp_kses($_POST['stage_title_date_picker_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_date_picker_'.$lp.'_'.$datepickercount, wp_kses($_POST['stage_description_date_picker_'.$lp.'_'.$abc], $allowed));
					$datepickercount++;
				}
				
				if(isset($_POST['stage_title_dropdown_'.$lp.'_'.$abc]) && $_POST['stage_title_dropdown_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_dropdown_'.$lp.'_'.$dropdowncount, wp_kses($_POST['stage_title_dropdown_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_dropdown_'.$lp.'_'.$dropdowncount, wp_kses($_POST['stage_description_dropdown_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_dropdown_options_'.$lp.'_'.$dropdowncount, wp_kses($_POST['stage_dropdown_options_'.$lp.'_'.$abc], $allowed));
					$dropdowncount++;
				}
				
				if(isset($_POST['stage_title_file_upload_'.$lp.'_'.$abc]) && $_POST['stage_title_file_upload_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_file_upload_'.$lp.'_'.$fileuploadcount, wp_kses($_POST['stage_title_file_upload_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_file_upload_'.$lp.'_'.$fileuploadcount, wp_kses($_POST['stage_description_file_upload_'.$lp.'_'.$abc], $allowed));
					$fileuploadcount++;
				}
				if(isset($_POST['stage_title_recaptcha_'.$lp.'_'.$abc]) && $_POST['stage_title_recaptcha_'.$lp.'_'.$abc]!='')
				{
					update_post_meta($post_id, 'stage_title_recaptcha_'.$lp.'_'.$recaptchacount, wp_kses($_POST['stage_title_recaptcha_'.$lp.'_'.$abc], $allowed));
					update_post_meta($post_id, 'stage_description_recaptcha_'.$lp.'_'.$recaptchacount, wp_kses($_POST['stage_description_recaptcha_'.$lp.'_'.$abc], $allowed));
					$recaptchacount++;
				}
				
			}
				 
			/*update_post_meta($post_id, 'stage_title_textbox_'.$lp, wp_kses($_POST['stage_title_textbox_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_textbox_'.$lp, wp_kses($_POST['stage_description_textbox_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_textarea_'.$lp, wp_kses($_POST['stage_title_textarea_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_textarea_'.$lp, wp_kses($_POST['stage_description_textarea_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_number_'.$lp, wp_kses($_POST['stage_title_number_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_number_'.$lp, wp_kses($_POST['stage_description_number_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_email_'.$lp, wp_kses($_POST['stage_title_email_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_email_'.$lp, wp_kses($_POST['stage_description_email_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_image_upload_'.$lp, wp_kses($_POST['stage_title_image_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_image_upload_'.$lp, wp_kses($_POST['stage_description_image_upload_'.$lp], $allowed));
		
		
		
		update_post_meta($post_id, 'stage_title_multi_image_upload_'.$lp, wp_kses($_POST['stage_title_multi_image_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_multi_image_upload_'.$lp, wp_kses($_POST['stage_description_multi_image_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_video_upload_'.$lp, wp_kses($_POST['stage_title_video_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_video_upload_'.$lp, wp_kses($_POST['stage_description_video_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_multi_video_upload_'.$lp, wp_kses($_POST['stage_title_multi_video_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_multi_video_upload_'.$lp, wp_kses($_POST['stage_description_multi_video_upload_'.$lp], $allowed));
		
		update_post_meta($post_id, 'stage_title_image_video_upload_'.$lp, wp_kses($_POST['stage_title_image_video_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_image_video_upload_'.$lp, wp_kses($_POST['stage_description_image_video_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_map_location_'.$lp, wp_kses($_POST['stage_title_map_location_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_map_location', wp_kses($_POST['stage_description_map_location_'.$lp], $allowed));
		
		update_post_meta($post_id, 'stage_title_checkbox_'.$lp, wp_kses($_POST['stage_title_checkbox_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_checkbox_'.$lp, wp_kses($_POST['stage_description_checkbox_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_radio_'.$lp, wp_kses($_POST['stage_title_radio_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_radio_'.$lp, wp_kses($_POST['stage_description_radio_'.$lp], $allowed));
		
		
		update_post_meta($post_id, 'stage_title_date_picker_'.$lp, wp_kses($_POST['stage_title_date_picker_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_date_picker_'.$lp, wp_kses($_POST['stage_description_date_picker_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_dropdown_'.$lp, wp_kses($_POST['stage_title_dropdown_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_dropdown_'.$lp, wp_kses($_POST['stage_description_dropdown_'.$lp], $allowed));
		
		update_post_meta($post_id, 'stage_title_file_upload_'.$lp, wp_kses($_POST['stage_title_file_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_file_upload_'.$lp, wp_kses($_POST['stage_description_file_upload_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_title_recaptcha_'.$lp, wp_kses($_POST['stage_title_recaptcha_'.$lp], $allowed));
		update_post_meta($post_id, 'stage_description_recaptcha_'.$lp, wp_kses($_POST['stage_description_recaptcha_'.$lp], $allowed));
		*/
		 $mainfilename = $_FILES['stage_main_image_'.$lp]['name'];
				  $extmain= get_extension2($mainfilename);
				  $mainnewfilename=time().'_main_'.$lp.".".$extmain;
				  
			move_uploaded_file($_FILES['stage_main_image_'.$lp]['tmp_name'][$i],dirname(__FILE__).'/upload/'.$mainnewfilename);
			$countfiles = count($_FILES['stage_additional_images_videos_'.$lp]['name']);
 
 				// Looping all files
				$cnt=1;
				 for($i=0;$i<$countfiles;$i++){
				   $filename = $_FILES['stage_additional_images_videos_'.$lp]['name'][$i];
				  $ext= get_extension2($filename);
				  $newfilename=time().'_'.$lp.".".$ext;
				   // Upload file
				   $cnt++;
				   move_uploaded_file($_FILES['stage_additional_images_videos_'.$lp]['tmp_name'][$i],dirname(__FILE__).'/upload/'.$newfilename);
					$wpdb->query("insert into wp_competition_stage_file(stage_id, file_name) values('".$stage_id."', '".$newfilename."');");
					
					update_post_meta($post_id, 'stage_additional_images_videos_'.$lp, wp_kses($newfilename, $allowed));
				 }
			
		}
		
		
	}
		
	
				
		
		if ( isset($_FILES['main_image']) ) {
			$count = 0;
			$files = $_FILES['main_image'];
			foreach ($files['name'] as $key => $value) {				
				if ($files['name'][$key]) {
					$file = array(
						'name'     => $files['name'][$key],
						'type'     => $files['type'][$key],
						'tmp_name' => $files['tmp_name'][$key],
						'error'    => $files['error'][$key],
						'size'     => $files['size'][$key]
					);
					$_FILES = array("upload_attachment" => $file);
					
					foreach ($_FILES as $file => $array){								
						
							$attachment_id = classiera_insert_attachment($file,$post_id);
							set_post_thumbnail( $post_id, $attachment_id );
														
						$count++;
					}
					
				}						
		}/*Foreach*/
		
		
	}
	
	if ( isset($_FILES['additional_images_videos']) ) {
					$count = 0;
					$files = $_FILES['additional_images_videos'];
					foreach ($files['name'] as $key => $value) {				
						if ($files['name'][$key]) {
							$file = array(
								'name'     => $files['name'][$key],
								'type'     => $files['type'][$key],
								'tmp_name' => $files['tmp_name'][$key],
								'error'    => $files['error'][$key],
								'size'     => $files['size'][$key]
							);
							$_FILES = array("additional_images_videos" => $file);
							
							foreach ($_FILES as $file => $array){								
								
									$attachment_id = classiera_insert_attachment($file,$post_id);
																
								$count++;
							}
							
						}						
					}/*Foreach*/
				}
	if(!empty($_FILES['terms_condition_file']['name'])) {
         
        // Setup the array of supported file types. In this case, it's just PDF.
        $supported_types = array('application/pdf');
         
        // Get the file type of the upload
        $arr_file_type = wp_check_filetype(basename($_FILES['terms_condition_file']['name']));
        $uploaded_type = $arr_file_type['type'];
         
        // Check if the type is supported. If not, throw an error.
        if(in_array($uploaded_type, $supported_types)) {
 
            // Use the WordPress API to upload the file
            $upload = wp_upload_bits($_FILES['terms_condition_file']['name'], null, file_get_contents($_FILES['terms_condition_file']['tmp_name']));
     
            if(isset($upload['error']) && $upload['error'] != 0) {
              //  wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
            } else {
                add_post_meta($post_id, 'wp_custom_attachment', $upload);
                update_post_meta($post_id, 'wp_custom_attachment', $upload);     
            } // end if/else
 
        } else {
           // wp_die("The file type that you've uploaded is not a PDF.");
        } // end if/else
         
    } // end if
		
	}
	
	
	echo '<br/>';
	
	
	
		if(!empty($error))
		{
			for($lp=0;$lp<count($error);$lp++)
			{
				echo '<span style="color:red;">'.$error[$lp].'</span>';
			}
		}  
		else if(isset($_GET['msg']) && $_GET['msg']=='1')
		{
			echo '<span style="color:green;">Competition inserted successfully</span>';
		}
		else if(isset($_GET['msg']) && $_GET['msg']=='2')
		{
			echo '<span style="color:green;">Competition updated successfully</span>';
		}
		else if(isset($_GET['msg']) && $_GET['msg']=='3')
		{
			echo '<span style="color:green;">Competition removed successfully</span>';
		}
		else if(isset($_GET['msg']) && $_GET['msg']=='4')
		{
			echo '<span style="color:green;">Competition File removed successfully</span>';
		}
		$competition_name='';
	if((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') && (isset($_REQUEST['edit_id']) && $_REQUEST['edit_id']!='' && is_numeric($_REQUEST['edit_id'])))
	{
		$sel= $wpdb->get_results( "SELECT * FROM wp_competition WHERE id='".$_REQUEST['edit_id']."'" );
		$name=$sel[0]->name;
		$description=$sel[0]->description;
		$start_date=date("m/d/Y h:i:s", strtotime($sel[0]->start_date));
		$end_date=date("m/d/Y h:i:s", strtotime($sel[0]->end_date));
	}
	   ?>
	  <h2> Competition/Promotion Management</h2>
	  <form name="frm_competition" action="" method="post" id="frm_competition" enctype="multipart/form-data">
	  
	  <div class="form-main-section classiera-post-cat">
							<div class="classiera-post-main-cat">
								<h4 class="classiera-post-inner-heading">
									<?php esc_html_e('Select a Category', 'classiera') ?> :
								</h4>

								<ul class="list-unstyled list-inline">
									<?php 
									$categories = get_terms('category', array(
											'hide_empty' => 0,
											'parent' => 0,
											'order'=> 'ASC'
										)	
									);
									foreach ($categories as $category){
										//print_r($category);
										$tag = $category->term_id;
										$classieraCatFields = get_option(MY_CATEGORY_FIELDS);
										if (isset($classieraCatFields[$tag])){
											$classieraCatIconCode = $classieraCatFields[$tag]['category_icon_code'];
											$classieraCatIcoIMG = $classieraCatFields[$tag]['your_image_url'];
											$classieraCatIconClr = $classieraCatFields[$tag]['category_icon_color'];
										}
										if(empty($classieraCatIconClr)){
											$iconColor = $primaryColor;
										}else{
											$iconColor = $classieraCatIconClr;
										}
										$category_icon = stripslashes($classieraCatIconCode);
										?>
										<li class="match-height">
											<a href="#" id="<?php echo $tag; ?>" class="border">
												<?php 
												if($classieraIconsStyle == 'icon'){
													?>
													<i class="<?php echo $category_icon; ?>" style="color:<?php echo $iconColor; ?>;"></i>
													<?php
												}elseif($classieraIconsStyle == 'img'){
													?>
													<img src="<?php echo $classieraCatIcoIMG; ?>" alt="<?php echo get_cat_name( $catName ); ?>">
													<?php
												}
												?>
												<span><?php echo get_cat_name( $tag ); ?></span>
											</a>
										</li>
										<?php
									}
									?>
								</ul><!--list-unstyled-->
								<input class="classiera-main-cat-field" id="classiera-main-cat-field" name="classiera-main-cat-field" type="hidden" value="">
							</div><!--classiera-post-main-cat-->
							<div class="classiera-post-sub-cat">
								<h4 class="classiera-post-inner-heading">
									<?php esc_html_e('Select a Sub Category', 'classiera') ?> :
								</h4>
								<ul class="list-unstyled classieraSubReturn">
								</ul>
								<input class="classiera-sub-cat-field" name="classiera-sub-cat-field" id="classiera-sub-cat-field" type="hidden" value="">
							</div><!--classiera-post-sub-cat-->
							<!--ThirdLevel-->
							<div class="classiera_third_level_cat">
								<h4 class="classiera-post-inner-heading">
									<?php esc_html_e('Select a Sub Category', 'classiera') ?> :
								</h4>
								<ul class="list-unstyled classieraSubthird">
								</ul>
								<input class="classiera_third_cat" name="classiera_third_cat" id="classiera_third_cat" type="hidden" value="">
								
							</div>
							<!--ThirdLevel-->
						</div>
						<div style="clear:left;"></div>
						<div class="form-main-section post-detail">
							<h4 class="text-uppercase border-bottom"><?php esc_html_e('Inventory Details', 'classiera') ?> :</h4>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Selected Category', 'classiera') ?> : </label>
                                <div class="col-sm-9">
                                    <p class="form-control-static"></p>
									<input type="text" id="selectCatCheck" value="" data-error="<?php esc_html_e('Please select a category.', 'classiera') ?>" required >
									<div class="help-block with-errors selectCatDisplay"></div>
                                </div>
                            </div><!--Selected Category-->
						</div>
						
		  <table>
		  <tr>
				<td>Type</td>
				<td>
					<select id="compitition_type" name="compitition_type">
						<option id="1">Promotion</option>
						<option id="2">Competition</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Title</td>
				<td><input type="text" name="name" value="<?php echo $name;?>" id="competetion"></td>
			</tr>
			<tr>
				<td>Description</td>
				<td>
					<textarea id="description" name="description"><?php echo $description;?></textarea>
				</td>
			</tr>
			<tr>
				<td>Start Date</td>
				<td>
					<input size="16" type="text" value="<?php echo $start_date;?>" readonly id="start_date" name="start_date" <?php ?> class="form-control form-control-md" placeholder="Start Date">


				</td>
			</tr>
			<tr>
				<td>End Date</td>
				<td>
					<input size="16" type="text" value="<?php echo $end_date;?>" readonly id="end_date" name="end_date" class="form-control form-control-md" placeholder="End Date">
					
				</td>
			</tr>
			<?php
			if((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') && (isset($_REQUEST['edit_id']) && $_REQUEST['edit_id']!='' && is_numeric($_REQUEST['edit_id'])))
			{
				//echo "select * from wp_competition_file where competition_id='".$_REQUEST['edit_id']."'";
				$images=$wpdb->get_results("select * from wp_competition_file where competition_id='".$_REQUEST['edit_id']."'");
				foreach($images as $key=>$val)
				{?>
					<tr>
						<td colspan="2">
							<img src="<?php echo plugins_url();?>/promotions-competitions/upload/<?php echo $images[$key]->file_name;?>" width="100"/>&nbsp;<a href="admin.php?page=Competition&action=remove_file&remove_id=<?php echo $images[$key]->id;?>">Remove</a>
						</td>
					</tr>
					<?php
				}
			}
			
			?>
			
			<script>
									/*	jQuery(document).ready(function($) {
											//jQuery("#expire_date").datepicker();
											jQuery("#start_date").datepicker({
											 minDate: new Date(<?php echo date("d/m/Y");?>)
											});
											$('#start_time').({
											   timeFormat:'hh:mm'
											});
										});	
										jQuery(document).ready(function($) {
											//jQuery("#expire_date").datepicker();
											jQuery("#end_date").datepicker({
											 minDate: new Date(<?php echo date("d/m/Y");?>)
											});
											$('#end_time').datepicker({
											   timeFormat:'hh:mm'
											});
										});		*/
										
										/*jQuery(document).ready(function($){
											jQuery('#start_date').datepicker({
											   minDate: new Date(<?php echo date("d/m/Y");?>)
											});
										});*/
					    				/*jQuery('#start_date').datepicker({
											dateFormat : 'DD, d MM, yy'
										});*/
										jQuery(document).ready(function(){
											jQuery('#start_date').datepicker({
												minDate: new Date(<?php echo date("d/m/Y");?>)
												//timeFormat:'hh:mm'
											});
											jQuery('#end_date').datepicker({
												minDate: new Date(<?php echo date("d/m/Y");?>)
												//timeFormat:'hh:mm'
											});
										});
									</script>
			<tr>
				<td>Upload Main Image</td>
				<td>
					<input type="file" name="main_image[]" id="main_image" />
				</td>
			</tr>
			<tr>
				<td>Additional Images/Videos</td>
				<td>
					<input type="file" name="additional_images_videos[]" id="additional_images_videos[]" multiple/>
				</td>
			</tr>
			<tr>
				<td>Competition Tags</td>
				<td>
					<input type="text" name="competition_tags" id="competition_tags" multiple/>
				</td>
			</tr>
			<tr>
				<td>Location</td>
				<td>
					<input style="width:560px;" id="address" type="text" name="address" value="" class="form-control form-control-md" placeholder="<?php esc_html_e('Address or City', 'classiera') ?>" required>
					<div class="form-group">
							
                                <div class="col-sm-9">
								
										<input type="hidden" id="latitude" name="latitude">
										<input type="hidden" id="longitude" name="longitude">
									
                                    <div id="post-map" class="submitMAp">
                                        <?php /*<div id="map-canvas"></div>*/?>
										<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPh0D0NhIV-fKCHmoHrcJk1K0vRUfiwiI&libraries=places&callback=initMap" async defer></script>
										<div id="newmappost" style="width: 700px; height: 500px"> </div>
										<script>
     
     
									   var map, infoWindow;
								
									  function initMap() {
										map = new google.maps.Map(document.getElementById('newmappost'), {
										  center: {lat: 0, lng: 0},
										  zoom: 13,
										  mapTypeId: 'roadmap'
										});
										
										 infoWindow = new google.maps.InfoWindow;
								
										// Try HTML5 geolocation.
										if (navigator.geolocation) {
										  navigator.geolocation.getCurrentPosition(function(position) {
											var pos = {
											  lat: position.coords.latitude,
											  lng: position.coords.longitude
											};
											document.getElementById("latitude").value =  position.coords.latitude;
											document.getElementById("longitude").value = position.coords.longitude;
											
											infoWindow.setPosition(pos);
											infoWindow.setContent('Location found.');
											infoWindow.open(map);
											map.setCenter(pos);
										  }, function() {
											handleLocationError(true, infoWindow, map.getCenter());
										  });
										} else {
										  // Browser doesn't support Geolocation
										  handleLocationError(false, infoWindow, map.getCenter());
										}
									  
								
										
										// Create the search box and link it to the UI element.
										var input = document.getElementById('address');
										var searchBox = new google.maps.places.SearchBox(input);
										map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
								
										// Bias the SearchBox results towards current map's viewport.
										map.addListener('bounds_changed', function() {
										  searchBox.setBounds(map.getBounds());
										});
								
										var markers = [];
										// Listen for the event fired when the user selects a prediction and retrieve
										// more details for that place.
										//To capture click event.
											 google.maps.event.addListener(map, 'click', function (e) {
												document.getElementById("latitude").value = e.latLng.lat();
												document.getElementById("longitude").value = e.latLng.lng();
												placeMarker(e.latLng,map);
											 });
										
										searchBox.addListener('places_changed', function() {
										  var places = searchBox.getPlaces();
								
										  if (places.length == 0) {
											return;
										  }
								
										  // Clear out the old markers.
										  markers.forEach(function(marker) {
											marker.setMap(null);
										  });
										  markers = [];
								
										  // For each place, get the icon, name and location.
										  var bounds = new google.maps.LatLngBounds();
										  places.forEach(function(place) {
											if (!place.geometry) {
											  console.log("Returned place contains no geometry");
											  return;
											}
											
								
											if (place.geometry.viewport) {
											  // Only geocodes have viewport.
											  bounds.union(place.geometry.viewport);
											} else {
											  bounds.extend(place.geometry.location);
											}
										  });
										  map.fitBounds(bounds);
										});
									  }
								
									 var marker;
									function placeMarker(location,map) {
									  if ( marker ) {
										marker.setPosition(location);
									  } else {
										marker = new google.maps.Marker({
										  position: location,
										  map: map
										});
									  }
									}
									function handleLocationError(browserHasGeolocation, infoWindow, pos) {
										infoWindow.setPosition(pos);
										infoWindow.setContent(browserHasGeolocation ?
															  'Error: The Geolocation service failed.' :
															  'Error: Your browser doesn\'t support geolocation.');
										infoWindow.open(map);
									}
								  
									</script>
                                    </div>
								
                                </div>
                            </div>
				</td>
			</tr>
			 <tr>
			 	<td>Entry Limit</td>
				<td><select id="entry_limit" name="entry_limit"><option value="1">Single</option><option value="2">Multi</option></select></td>
			 </tr>
			  <tr>
			 	<td>Tearms & Condition</td>
				<td><input type="file" name="terms_condition_file" id="terms_condition_file" /></td>
			 </tr>
			 <?php /*<tr>
			 	<td>Entry Requirement</td>
				<td></td>
			 </tr>*/?>
			 <script type="text/javascript" language="javascript">
					var var_textbox=0;
					var var_textarea=0;
					var var_number=0;
					var var_email=0;
					var var_image_upload=0;
					var var_multi_image_upload=0;
					var var_video_upload=0;
					var var_multi_video_upload=0;
					var var_image_video_upload=0;
					var var_location=0;
					var var_checkbox=0;
					var var_radio=0;
					var var_date_picker=0;
					var var_dropdown=0;
					var var_file_upload=0;
					var var_recaptcha=0;
					var entry_field=1;
					
					var stage_field=1;
					function entry_dropdown_change(th, val) {
					jQuery("#entry_field").val(entry_field);
						//var end = this.value;
						//alert(th.value);
							//jQuery("#field_options"+entry_field).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_title_textbox'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_title_options'+entry_field+'"></textarea></td></tr></table></td>');<br />
							//alert(entry_field);
							//
							//alert(val);
							/*if(th.val=='dropdown' || th.val=='radio' || th.val=='checkbox')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_title_textbox'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_title_options'+entry_field+'"></textarea></td></tr></table></td>');
							}
							else
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_title_textbox'+entry_field+'" ></textarea></td></tr></table></td>');
							}*/
							//alert(th);
							if(th=='textbox')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_textbox'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='textarea')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textarea'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_textarea'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='number')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_number'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_number'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='email')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_email'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_email'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_image_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_image_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_image_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_multi_image_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_multi_image_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='video_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_video_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_video_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_video_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_multi_video_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_multi_video_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_video_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_image_video_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_image_video_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='location')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_location'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_location'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='checkbox')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_checkbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_checkbox'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_checkbox_options'+entry_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='radio')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_radio'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_radio'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_radio_options'+entry_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='date_picker')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_date_picker'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_date_picker'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='dropdown')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_dropdown'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_dropdown'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_dropdown_options'+entry_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='file_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_file_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_file_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='recaptcha')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_recaptcha'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_recaptcha'+entry_field+'" ></textarea></td></tr></table></td>');
							}
						
					}
					function entry_dropdown_change_1(th, val) {
					jQuery("#entry_field").val(entry_field);
						//var end = this.value;
						//alert(th.value);
						
						/*if(th.val=='dropdown' || th.val=='radio' || th.val=='checkbox')
						{
							jQuery("#field_options1").html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_title_textbox'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_title_options'+entry_field+'"></textarea></td></tr></table></td>');
						}
						else
						{
							jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_title_textbox'+entry_field+'" ></textarea></td></tr></table></td>');
							
						}*/
						//alert(th.val);
						//val=1;
						//var th=jQuery("#entry_field_1").val();
						if(th=='textbox')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_textbox'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='textarea')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_textarea'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_textarea'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='number')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_number'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_number'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='email')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_email'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_email'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_image_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_image_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_image_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_multi_image_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_multi_image_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='video_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_video_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_video_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_video_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_multi_video_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_multi_video_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}else if(th=='image_video_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_image_video_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_image_video_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='location')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_location'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_location'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='checkbox')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_checkbox'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_checkbox'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_checkbox_options'+entry_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='radio')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_radio'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_radio'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_radio_options'+entry_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='date_picker')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_date_picker'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_date_picker'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='dropdown')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_dropdown'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_dropdown'+entry_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_dropdown_options'+entry_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='file_upload')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_file_upload'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_file_upload'+entry_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='recaptcha')
							{
								jQuery("#field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="entry_title_recaptcha'+entry_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="entry_description_recaptcha'+entry_field+'" ></textarea></td></tr></table></td>');
							}
					}
					function add_more_entry_fields(th)
					{
						entry_field++;
						jQuery("#entry_field").val(entry_field);
						//jQuery("#add_more_entry_fields").append('<td colspan="2"><table style="width:100%; float:left;"><tr><td>Select Field</td><td><select name="entry_field'+entry_field+'" id="entry_field'+entry_field+'" onchange="entry_dropdown_change(this);"><option value="textbox">Textbox</option><option value="textarea">Textarea</option><option value="number">Number</option><option value="email">Email</option><option value="image_upload">Image Upload</option><option value="multi_image_upload">Multi Image Upload</option><option value="video_upload">Video Upload</option><option value="multi_video_upload">Multi Video Upload</option><option value="image_video_upload">Image/Video Upload</option><option value="location">Location</option><option value="checkbox">Checkbox</option><option value="radio">radio</option><option value="date_picker">Date Picker</option><option value="dropdown">Dropdown</option><option value="file_upload">File Upload</option><option value="recaptcha">ReCaptcha</option></select></td></tr></table></td>');
						//jQuery("#add_more_entry_fields").append('<tr id="field_options'+entry_field+'"></tr>');
					//	jQuery("#add_more_entry_fields").append('<table><tr><td>Select Field</td><td><select name="entry_field'+entry_field+'" id="entry_field'+entry_field+'" onchange="entry_dropdown_change(this);"><option value="textbox">Textbox</option><option value="textarea">Textarea</option><option value="number">Number</option><option value="email">Email</option><option value="image_upload">Image Upload</option><option value="multi_image_upload">Multi Image Upload</option><option value="video_upload">Video Upload</option><option value="multi_video_upload">Multi Video Upload</option><option value="image_video_upload">Image/Video Upload</option><option value="location">Location</option><option value="checkbox">Checkbox</option><option value="radio">radio</option><option value="date_picker">Date Picker</option><option value="dropdown">Dropdown</option><option value="file_upload">File Upload</option><option value="recaptcha">ReCaptcha</option></select></td></tr><tr id="field_options'+entry_field+'"></tr></table>');
					//	jQuery("#add_more_entry_fields").append('');
						//jQuery("#add_more_entry_fields").append('<tr><td colspan="2"><table id="field_table1"><tr><td>Select Field</td><td><select name="entry_field_1" id="entry_field_1" onchange="entry_dropdown_change_1(this, 'field_table1');"><option value="textbox">Textbox</option><option value="textarea">Textarea</option><option value="number">Number</option><option value="email">Email</option><option value="image_upload">Image Upload</option><option value="multi_image_upload">Multi Image Upload</option><option value="video_upload">Video Upload</option><option value="multi_video_upload">Multi Video Upload</option><option value="image_video_upload">Image/Video Upload</option><option value="location">Location</option><option value="checkbox">Checkbox</option><option value="radio">radio</option><option value="date_picker">Date Picker</option><option value="dropdown">Dropdown</option><option value="file_upload">File Upload</option><option value="recaptcha">ReCaptcha</option></select></td></tr>');
							//jQuery("#add_more_entry_fields").append('<tr id="field_options1"></tr></table></td></tr>');
							
							jQuery("#add_more_entry_fields").append('<table><tr><td>Select Field</td><td><select name="entry_field'+entry_field+'" id="entry_field'+entry_field+'" onchange="entry_dropdown_change(this.value, '+entry_field+');"><option value="textbox">Textbox</option><option value="textarea">Textarea</option><option value="number">Number</option><option value="email">Email</option><option value="image_upload">Image Upload</option><option value="multi_image_upload">Multi Image Upload</option><option value="video_upload">Video Upload</option><option value="multi_video_upload">Multi Video Upload</option><option value="image_video_upload">Image/Video Upload</option><option value="location">Location</option><option value="checkbox">Checkbox</option><option value="radio">radio</option><option value="date_picker">Date Picker</option><option value="dropdown">Dropdown</option><option value="file_upload">File Upload</option><option value="recaptcha">ReCaptcha</option></select></td></tr><tr id="field_options'+entry_field+'"></tr></table>');
							
					}
					function add_more_entry_fields_1()
					{
						entry_field++;
						jQuery("#entry_field").val(entry_field);
						jQuery("#add_more_entry_fields").append('<td colspan="2"><table><tr><td>Select Field</td><td><select name="entry_field'+entry_field+'" id="entry_field'+entry_field+'" onchange="entry_dropdown_change(this);"><option value="textbox">Textbox</option><option value="textarea">Textarea</option><option value="number">Number</option><option value="email">Email</option><option value="image_upload">Image Upload</option><option value="multi_image_upload">Multi Image Upload</option><option value="video_upload">Video Upload</option><option value="multi_video_upload">Multi Video Upload</option><option value="image_video_upload">Image/Video Upload</option><option value="location">Location</option><option value="checkbox">Checkbox</option><option value="radio">radio</option><option value="date_picker">Date Picker</option><option value="dropdown">Dropdown</option><option value="file_upload">File Upload</option><option value="recaptcha">ReCaptcha</option></select></td></tr></table></td><tr id="field_options'+entry_field+'"></tr>');
					}
					
					
					var stagesfields=new Array();
					
					function stage_dropdown_change(th, val, stag_no) {
					jQuery("#stage_field").val(stage_field);
					//alert(th);
					//alert(val);
					//var stag_no=jQuery("#total_stage").val();
						if(th=='textbox')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_textbox_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_textbox_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='textarea')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_textarea_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_textarea_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='number')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_number_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_number_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='email')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_email_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_email_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_image_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_image_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_image_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_multi_image_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_multi_image_upload'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='video_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_video_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_video_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_video_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_multi_video_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_multi_video_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_video_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_image_video_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_image_video_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='location')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_location_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_location_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='checkbox')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_checkbox_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_checkbox_'+stag_no+'_'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_checkbox_options_'+stag_no+'_'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='radio')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_radio_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_radio_'+stag_no+'_'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_radio_options_'+stag_no+'_'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='date_picker')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_date_picker_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_date_picker_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='dropdown')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_dropdown_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_dropdown_'+stag_no+'_'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_dropdown_options_'+stag_no+'_'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='file_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_file_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_file_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='recaptcha')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_recaptcha_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_recaptcha_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
					}
					function stage_dropdown_change_1(th, val, stag_no) {
					jQuery("#stage_field").val(stage_field);
					//var stag_no=jQuery("#total_stage").val();
						/*if(th=='textbox')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_textbox'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_textbox'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='textarea')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_textarea'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_textarea'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='number')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_number'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_number'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='email')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_email'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_email'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_upload')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_image_upload'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_image_upload'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_image_upload')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_multi_image_upload'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_multi_image_upload'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='video_upload')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_video_upload'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_video_upload'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_video_upload')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_multi_video_upload'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_multi_video_upload'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_video_upload')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_image_video_upload'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_image_video_upload'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='location')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_location'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_location'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='checkbox')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_checkbox'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_checkbox'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_checkbox_options'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='radio')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_radio'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_radio'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_radio_options'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='date_picker')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_date_picker'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_date_picker'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='dropdown')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_dropdown'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_dropdown'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_dropdown_options'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='file_upload')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_file_upload'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_file_upload'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='recaptcha')
							{
								jQuery("#stage_field_options"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_recaptcha'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_title_recaptcha'+stage_field+'" ></textarea></td></tr></table></td>');
							}*/
							if(th=='textbox')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_textbox_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_textbox_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='textarea')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_textarea_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_textarea_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='number')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_number_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_number_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='email')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_email_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_email_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_image_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_image_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_image_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_multi_image_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_multi_image_upload'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='video_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_video_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_video_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='multi_video_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_multi_video_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_multi_video_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='image_video_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_image_video_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_image_video_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='location')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_location_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_location_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='checkbox')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_checkbox_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_checkbox_'+stag_no+'_'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_checkbox_options_'+stag_no+'_'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='radio')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_radio_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_radio_'+stag_no+'_'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_radio_options_'+stag_no+'_'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='date_picker')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_date_picker_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_date_picker_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='dropdown')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_dropdown_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_dropdown_'+stag_no+'_'+stage_field+'" ></textarea></td></tr><tr><td>Options</td><td><textarea name="stage_dropdown_options_'+stag_no+'_'+stage_field+'"></textarea></td></tr></table></td>');
							}
							else if(th=='file_upload')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_file_upload_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_file_upload_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
							else if(th=='recaptcha')
							{
								jQuery("#stage_field_options_"+stag_no+"_"+val).html('<td colspan="2"><table><tr><td>Title</td><td><input type="textbox" name="stage_title_recaptcha_'+stag_no+'_'+stage_field+'" value=""></td></tr><tr><td>Description</td><td><textarea name="stage_description_recaptcha_'+stag_no+'_'+stage_field+'" ></textarea></td></tr></table></td>');
							}
					}
					function add_more_stage_fields(stag_no)
					{
						stage_field++;
						jQuery("#stage_field").val(stage_field);
							//var stag_no=jQuery("#total_stage").val();
							jQuery("#add_more_stage_fields"+stag_no).append('<table><tr><td>Select Field</td><td><select name="stage_field_'+stag_no+'_'+stage_field+'" id="stage_field_'+stag_no+'_'+stage_field+'" onchange="stage_dropdown_change(this.value, '+stage_field+', '+stag_no+');"><option value="textbox">Textbox</option><option value="textarea">Textarea</option><option value="number">Number</option><option value="email">Email</option><option value="image_upload">Image Upload</option><option value="multi_image_upload">Multi Image Upload</option><option value="video_upload">Video Upload</option><option value="multi_video_upload">Multi Video Upload</option><option value="image_video_upload">Image/Video Upload</option><option value="location">Location</option><option value="checkbox">Checkbox</option><option value="radio">radio</option><option value="date_picker">Date Picker</option><option value="dropdown">Dropdown</option><option value="file_upload">File Upload</option><option value="recaptcha">ReCaptcha</option></select></td></tr><tr id="stage_field_options_'+stag_no+'_'+stage_field+'"></tr></table>');
							
					}
					function add_more_stage_fields_1(stag_no)
					{
						stage_field++;
						jQuery("#stage_field").val(stage_field);
						//var stag_no=jQuery("#total_stage").val();
					//	alert("here");
					//	jQuery("#add_more_stage_fields").append('<td colspan="2"><table><tr><td>Select Field</td><td><select name="stage_field_'+stag_no+'_'+stage_field+'" id="stage_field'+stage_field+'" onchange="stage_dropdown_change(this);"><option value="textbox">Textbox</option><option value="textarea">Textarea</option><option value="number">Number</option><option value="email">Email</option><option value="image_upload">Image Upload</option><option value="multi_image_upload">Multi Image Upload</option><option value="video_upload">Video Upload</option><option value="multi_video_upload">Multi Video Upload</option><option value="image_video_upload">Image/Video Upload</option><option value="location">Location</option><option value="checkbox">Checkbox</option><option value="radio">radio</option><option value="date_picker">Date Picker</option><option value="dropdown">Dropdown</option><option value="file_upload">File Upload</option><option value="recaptcha">ReCaptcha</option></select></td></tr></table></td><tr id="stage_field_options'+stage_field+'"></tr>');
					jQuery("#add_more_stage_fields"+stag_no).append('<table><tr><td>Select Field</td><td><select name="stage_field_'+stag_no+'_'+stage_field+'" id="stage_field_'+stag_no+'_'+stage_field+'" onchange="stage_dropdown_change(this.value, '+stage_field+', '+stag_no+');"><option value="textbox">Textbox</option><option value="textarea">Textarea</option><option value="number">Number</option><option value="email">Email</option><option value="image_upload">Image Upload</option><option value="multi_image_upload">Multi Image Upload</option><option value="video_upload">Video Upload</option><option value="multi_video_upload">Multi Video Upload</option><option value="image_video_upload">Image/Video Upload</option><option value="location">Location</option><option value="checkbox">Checkbox</option><option value="radio">radio</option><option value="date_picker">Date Picker</option><option value="dropdown">Dropdown</option><option value="file_upload">File Upload</option><option value="recaptcha">ReCaptcha</option></select></td></tr><tr id="stage_field_options_'+stag_no+'_'+stage_field+'"></tr></table>');
					}
				</script>
			 <tr><td>Add fields for entry requirement
			 </td><td><input type="checkbox" name="is_entry_fields" id="is_entry_fields" value="1" /></td></tr>
			 <tr>
			 	<td colspan="2">
				<table id="field_table1">
				<tr>
			 	<td>Select Field</td>
			 	<td>
				
				<select name="entry_field_1" id="entry_field_1" onchange="entry_dropdown_change_1(this.value, '1');">
					<option value="textbox">Textbox</option>
					<option value="textarea">Textarea</option>
					<option value="number">Number</option>
					<option value="email">Email</option>
					<option value="image_upload">Image Upload</option>
					<option value="multi_image_upload">Multi Image Upload</option>
					<option value="video_upload">Video Upload</option>
					<option value="multi_video_upload">Multi Video Upload</option>
					<option value="image_video_upload">Image/Video Upload</option>
					<option value="location">Location</option>
					<option value="checkbox">Checkbox</option>
					<option value="radio">radio</option>
					<option value="date_picker">Date Picker</option>
					<option value="dropdown">Dropdown</option>
					<option value="file_upload">File Upload</option>
					<option value="recaptcha">ReCaptcha</option>
				</select></td>
				</tr>
				<tr id="field_options1">
					
				</tr>
				</table>
				</td>
			 </tr>
			 <?php
			 /*static code*/
			 /*
			 ?>
			 <tr>
			 	<td colspan="2">
				<table id="field_table1">
				<tr>
			 	<td>Select Field</td>
			 	<td>
				
				<select name="entry_field_1" id="entry_field_1" onchange="entry_dropdown_change_1(this, 'field_table1');">
					<option value="textbox">Textbox</option>
					<option value="textarea">Textarea</option>
					<option value="number">Number</option>
					<option value="email">Email</option>
					<option value="image_upload">Image Upload</option>
					<option value="multi_image_upload">Multi Image Upload</option>
					<option value="video_upload">Video Upload</option>
					<option value="multi_video_upload">Multi Video Upload</option>
					<option value="image_video_upload">Image/Video Upload</option>
					<option value="location">Location</option>
					<option value="checkbox">Checkbox</option>
					<option value="radio">radio</option>
					<option value="date_picker">Date Picker</option>
					<option value="dropdown">Dropdown</option>
					<option value="file_upload">File Upload</option>
					<option value="recaptcha">ReCaptcha</option>
				</select></td>
				</tr>
				<tr id="field_options1">
					<td colspan="2"><table><tbody><tr><td>Title</td><td><input name="entry_title_textbox1" value="" type="textbox"></td></tr><tr><td>Description</td><td><textarea name="entry_title_textbox1"></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_title_options1"></textarea></td></tr></tbody></table></td>
				</tr>
				</table>
				</td>
			 </tr>
			 <tr>
			 	<td colspan="2">
				<table id="field_table1">
				<tr>
			 	<td>Select Field</td>
			 	<td>
				
				<select name="entry_field_1" id="entry_field_1" onchange="entry_dropdown_change_1(this, 'field_table1');">
					<option value="textbox">Textbox</option>
					<option value="textarea">Textarea</option>
					<option value="number">Number</option>
					<option value="email">Email</option>
					<option value="image_upload">Image Upload</option>
					<option value="multi_image_upload">Multi Image Upload</option>
					<option value="video_upload">Video Upload</option>
					<option value="multi_video_upload">Multi Video Upload</option>
					<option value="image_video_upload">Image/Video Upload</option>
					<option value="location">Location</option>
					<option value="checkbox">Checkbox</option>
					<option value="radio">radio</option>
					<option value="date_picker">Date Picker</option>
					<option value="dropdown">Dropdown</option>
					<option value="file_upload">File Upload</option>
					<option value="recaptcha">ReCaptcha</option>
				</select></td>
				</tr>
				<tr id="field_options1">
					<td colspan="2"><table><tbody><tr><td>Title</td><td><input name="entry_title_textbox1" value="" type="textbox"></td></tr><tr><td>Description</td><td><textarea name="entry_title_textbox1"></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_title_options1"></textarea></td></tr></tbody></table></td>
				</tr>
				</table>
				</td>
			 </tr>
			 <tr>
			 	<td colspan="2">
				<table id="field_table1">
				<tr>
			 	<td>Select Field</td>
			 	<td>
				
				<select name="entry_field_1" id="entry_field_1" onchange="entry_dropdown_change_1(this, 'field_table1');">
					<option value="textbox">Textbox</option>
					<option value="textarea">Textarea</option>
					<option value="number">Number</option>
					<option value="email">Email</option>
					<option value="image_upload">Image Upload</option>
					<option value="multi_image_upload">Multi Image Upload</option>
					<option value="video_upload">Video Upload</option>
					<option value="multi_video_upload">Multi Video Upload</option>
					<option value="image_video_upload">Image/Video Upload</option>
					<option value="location">Location</option>
					<option value="checkbox">Checkbox</option>
					<option value="radio">radio</option>
					<option value="date_picker">Date Picker</option>
					<option value="dropdown">Dropdown</option>
					<option value="file_upload">File Upload</option>
					<option value="recaptcha">ReCaptcha</option>
				</select></td>
				</tr>
				<tr id="field_options1">
					<td colspan="2"><table><tbody><tr><td>Title</td><td><input name="entry_title_textbox1" value="" type="textbox"></td></tr><tr><td>Description</td><td><textarea name="entry_title_textbox1"></textarea></td></tr><tr><td>Options</td><td><textarea name="entry_title_options1"></textarea></td></tr></tbody></table></td>
				</tr>
				</table>
				</td>
			 </tr>
			 <?php
			 */
			 /*static code*/
			 ?>
			 <tr>
			 	<td colspan="2" id="add_more_entry_fields"></td>
			</tr>
			<tr><td></td><td><a href="javascript:void(0);" onclick="add_more_entry_fields();">Add More Entry Fields</a></td></tr>
			<tr>
			<tr>
				<td>Do you want to add Competition Stages?</td>
				<td><input type="checkbox" name="is_stages" id="is_stages" value="1" onclick="add_stages_fields();"/></td>
			</tr>
			<tr>
				<td colspan="2" id="stages_fields">
				</td>
			</tr>
			
				<td colspan="2">
					<?php
					if((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') && (isset($_REQUEST['edit_id']) && $_REQUEST['edit_id']!='' && is_numeric($_REQUEST['edit_id'])))
					{
						
						?>
							<input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>">
							<input type="hidden" name="edit_id" value="<?php echo $_REQUEST['edit_id'];?>">
						<?php
					}
					else
					{?>
							<input type="hidden" name="action" value="insert"><?php
					}
					?>
					<input type="hidden" name="total_stage" id="total_stage" value="0" />
					
					<input type="hidden" name="entry_field" id="entry_field" value="" />
					<input type="hidden" name="stage_field" id="stage_field" value="" />
					<input type="submit" name="btn_submit" value="Save">
					<script>
					function add_stages_fields()
					{
						if(jQuery("#total_stage").val()==0)
						{
							jQuery("#total_stage").val(Number(jQuery("#total_stage").val())+1);
							var stage_number=jQuery("#total_stage").val();
							jQuery.ajax({
										type: "POST",
										url: "<?php site_url();?>/ajax_stages_fields.php",
										data: "stage_number="+stage_number+"&stage_field="+stage_field,
										success: function(rdata) {
											// success function is called when data came back
											// for example: get your content and display it on your site
											jQuery("#stages_fields").html(rdata);
										}
							});
						}
						
						
					}
					function addmorestage()
					{
						jQuery("#total_stage").val(Number(jQuery("#total_stage").val())+1);
							var stage_number=jQuery("#total_stage").val();
							
							
							/*for(var i=0; i<stagesfields[stage_number].length; i++){
							   var name = stagesfields[stage_number][i];
							   if(name == stage_field){
								status = 'Exist';
								stagesfields[stage_number][]=Number(stagesfields[stage_number][i])+1;
								break;
							   }
							  }
							  if(stagesfields[stage_number].length>=1)
								{
									 if(status!='Exist')
									  {
										stagesfields[stage_number][]
									  }
								}*/
							 
							
							/*else
							{
								stage_number[stage_number][]=1;	
							}*/
							jQuery.ajax({
										type: "POST",
										url: "<?php site_url();?>/ajax_stages_fields.php",
										data: "stage_number="+stage_number+"&stage_field="+stage_field,
										success: function(rdata) {
											// success function is called when data came back
											// for example: get your content and display it on your site
											jQuery("#stages_fields").append(rdata);
										}
							});
					}
						
						
					
					</script>
				</td>
			</tr>
		  </table>
	  
	  </form>
</div></div></div>
	   <?php 
	   
	  
   }
   
   //print_r($results);

?>