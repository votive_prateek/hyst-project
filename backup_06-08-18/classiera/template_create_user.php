<?php
/**
 * Template name: Template Create User
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Classiera
 * @since Classiera
 */
if ( !is_user_logged_in() ) {
	global $redux_demo; 
	$login = $redux_demo['login'];
	wp_redirect( $login ); exit;
}
if(!current_user_can('inventory_user'))
{
	get_template_part('error');
	exit;
}
get_header();
?>
<section class="user-pages section-gray-bg edit_pages all_usertemplate">
	<div class="container-fluid">
        <div class="row">
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
				<div class="user-detail-section section-bg-white">
				    <div class="row">
						<div class="user-contact-details single_title" style="margin-bottom:0px;">
							<h1 class="user_name"><?php esc_html_e("Create company user", 'classiera') ?></h1>
						</div>
					</div>	
					<div class="clearfix"></div>
					<p class="user-detail-section-heading poppins-lite pad0 margin0">
						 <img src="<?=get_template_directory_uri().'/../classiera-child/images/quate.png' ?>" width="60" height="60"><br>
						basic info								
					</p>
					<div class="clearfix"></div>
					<div class="user-detail-section section-bg-white company_register_form top-buffer1" style="padding:10px 15px;">
						<?php
						 echo do_shortcode('[dm_registration_form]'); ?>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<?php
get_footer();
?>