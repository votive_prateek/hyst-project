<div class="wrap">
		<div id="lbg_logo">
			<h2>Overview</h2>
		</div>
		<div class="postbox-container" style="width:100%">
			<div class="postbox">
				<h3 style="padding:7px 10px;">LBG - Magic Slider</h3>
				<div class="inside">		
				<p>This plugin will allow you to administrate an advanced slider withd layers, animate using the CSS3 transitions.</p>
				<p>You have available the following sections:</p> 
				<ul class="lbg_list-1">
					<li><a href="?page=magic_slider_Manage_Sliders">Manage Sliders</a></li>
					<li><a href="?page=magic_slider_Add_New">Add New</a> (Slider)</li>
		          <li><a href="?page=magic_slider_Help">Help</a></li> 
				</ul>
			  </div>
			</div>	
		</div>
	</div>