<?php

/**

 * Template name: Edit Inventory

 *

 * Learn more: http://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage classiera

 * @since classiera 1.0

 */
if ( !is_user_logged_in() ) {
	global $redux_demo; 
	$login = $redux_demo['login'];
	wp_redirect( $login ); exit;
}
if(!current_user_can('inventory_user'))
{
	if(!current_user_can('company_user'))
	{
		get_template_part('error');
		exit;
	}
}
$postTitleError = '';
$post_priceError = '';
$catError = '';
$featPlanMesage = '';
$postContent = '';
$hasError ='';
$allowed ='';
$caticoncolor="";
$classieraCatIconCode ="";
$category_icon="";
$category_icon_color="";

global $current_user;
wp_get_current_user();
$hasError = false;
$userID = $current_user->ID;
$query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>'-1') );
if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();

	if(isset($_GET['post'])) {	

		if($_GET['post'] == $post->ID)

		{
			$posts_id = $_GET['post'];
			$author = get_post_field( 'post_author', $posts_id );
			if(current_user_can('administrator') ){
				
			}else{
				if($author != $userID) {
					wp_redirect( home_url() ); exit;
				}
			}
			$current_post = $post->ID;
			$title = get_the_title();
			$content = get_the_content();
			$posttags = get_the_tags($current_post);
			if ($posttags) {
			  foreach($posttags as $tag) {
				$tags_list = $tag->name . ' '; 
			  }
			}

			$postcategory = get_the_category( $current_post );
			
			$category_id = $postcategory[0]->cat_ID;
			
			$post_category_type = get_post_meta($post->ID, 'post_category_type', true);
			$classieraPostDate = get_the_date('Y-m-d H:i:s', $posts_id);			
			$post_price = get_post_meta($post->ID, 'post_price', true);			
			$post_old_price = get_post_meta($post->ID, 'post_old_price', true);
			$classieraTagDefault = get_post_meta($post->ID, 'post_currency_tag', true);
			
			$post_phone = get_post_meta($post->ID, 'post_phone', true);
			
			$post_main_cat = get_post_meta($post->ID, 'post_perent_cat', true);
			$post_child_cat = get_post_meta($post->ID, 'post_child_cat', true);
			$post_inner_cat = get_post_meta($post->ID, 'post_inner_cat', true);
			if(empty($post_inner_cat)){
				$category_id = $post_child_cat;
			}else{
				$category_id = $post_inner_cat;
			}
			if(empty($category_id)){
				$category_id = $post_main_cat;
			}
			$post_location = get_post_meta($post->ID, 'post_location', true);
			
			$post_state = get_post_meta($post->ID, 'post_state', true);
			$post_city = get_post_meta($post->ID, 'post_city', true);

			$post_latitude = get_post_meta($post->ID, 'post_latitude', true);

			$post_longitude = get_post_meta($post->ID, 'post_longitude', true);

			$post_price_plan_id = get_post_meta($post->ID, 'post_price_plan_id', true);

			$post_address = get_post_meta($post->ID, 'post_address', true);

			$post_video = get_post_meta($post->ID, 'post_video', true);
			
			$featuredIMG = get_post_meta($post->ID, 'featured_img', true);
			
			$itemCondition = get_post_meta($post->ID, 'item-condition', true);
			
			$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);
			
			$classiera_post_type = get_post_meta($post->ID, 'classiera_post_type', true);
			$pay_per_post_product_id = get_post_meta($post->ID, 'pay_per_post_product_id', true);
			$days_to_expire = get_post_meta($post->ID, 'days_to_expire', true);
			
			$featured_post = "0";
			$post_price_plan_activation_date = get_post_meta($post->ID, 'post_price_plan_activation_date', true);
			$post_price_plan_expiration_date = get_post_meta($post->ID, 'post_price_plan_expiration_date', true);
			$todayDate = strtotime(date('d/m/Y H:i:s'));
			$expireDate = strtotime($post_price_plan_expiration_date);  
			if(!empty($post_price_plan_activation_date)) {
				if(($todayDate < $expireDate) or empty($post_price_plan_expiration_date)) {
					$featured_post = "1";
				}
			}
			if(empty($post_latitude)) {
				$post_latitude = 0;
			}			
			if(empty($post_longitude)) {
				$post_longitude = 0;
				$mapZoom = 2;
			} else {
				$mapZoom = 16;
			}	

			if ( has_post_thumbnail() ) {	

				$post_thumbnail = get_the_post_thumbnail($current_post, 'thumbnail');		

			} 

		}

	}
endwhile; endif;
wp_reset_query();

global $redux_demo;
$featuredADS = 0;
$primaryColor = $redux_demo['color-primary'];
$googleFieldsOn = $redux_demo['google-lat-long'];
$classieraLatitude = $redux_demo['contact-latitude'];
$classieraLongitude = $redux_demo['contact-longitude'];
$classieraAddress = $redux_demo['classiera_address_field_on'];
$postCurrency = $redux_demo['classierapostcurrency'];
$classieraIconsStyle = $redux_demo['classiera_cat_icon_img'];
$termsandcondition = $redux_demo['termsandcondition'];
$classieraProfileURL = $redux_demo['profile'];
$classiera_ads_typeOn = $redux_demo['classiera_ads_type'];

$error=array();
if(isset($_POST['updatepost'])){

//print_r($_POST);exit;
	if(trim($_POST['postTitle']) != '' && $_POST['classiera-main-cat-field'] != ''){		
		if(isset($_POST['submitted']) && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) {
			
			if(empty($_POST['postTitle'])){
				$postTitleError =  esc_html__( 'Please enter a title.', 'classiera' );
				$hasError = true;
			}else{
				$postTitle = trim($_POST['postTitle']);
			}
			if(empty($_POST['classiera-main-cat-field'])){
				$catError = esc_html__( 'Please select a category', 'classiera' );
				$hasError = true;
				$error[]='Please select a category.';
			}
			if(isset($_POST['postTitle']) && $_POST['postTitle']=='')
			{
				$hasError = true;
				$error[]='Please enter a title.';
			}
			if(isset($_POST['postContent']) && $_POST['postContent']=='')
			{
				$hasError = true;
				$error[]='Please enter a description.';
			}
			if(isset($_POST['found_location1']) && $_POST['found_location1']=='')
			{
				$hasError = true;
				$error[]='Please enter a Found Location 1.';
			}
			if(isset($_POST['location1_price']) && $_POST['location1_price']=='')
			{
				$hasError = true;
				$error[]='Please enter a Location 1 Price.';
			}
			//print_r($_POST);exit;
			//Image Count check//
			$userIMGCount = $_POST['image-count'];
			$files = $_FILES['upload_attachment'];
			$count = $files['name'];
			$filenumber = count($count);			
			if($filenumber > $userIMGCount){
				$imageError = esc_html__( 'You selected Images Count is exceeded', 'classiera' );
				$hasError = true;
			}
			//Image Count check//

			if($hasError != true && !empty($_POST['classiera_post_type']) || isset($_POST['regular-ads-enable'])) {
			
			
				$classieraPostType = $_POST['classiera_post_type'];
				//Set Post Status//
				if(is_super_admin() ){
					$postStatus = 'publish';
				}elseif(!is_super_admin()){
					if($redux_demo['post-options-on'] == 1){
						$postStatus = 'private';
					}else{
						$postStatus = 'publish';
					}
					if($classieraPostType == 'payperpost'){
						$postStatus = 'pending';
					}
				}
				//Set Post Status//
				//Check Category//
				$classieraMainCat = $_POST['classiera-main-cat-field'];
				$classieraChildCat = $_POST['classiera-sub-cat-field'];
				$classieraThirdCat = $_POST['classiera_third_cat'];
				if(empty($classieraThirdCat)){
					$classieraCategory = $classieraChildCat;
				}else{
					$classieraCategory = $classieraThirdCat;
				}
				if(empty($classieraCategory)){
					$classieraCategory = $classieraMainCat;
				}
				//Check Category//
				//Setup Post Data//
				$post_information = array(
					'ID'           => $current_post,
					'post_title' => esc_attr(strip_tags($_POST['postTitle'])),			
					'post_content' => strip_tags($_POST['postContent'], '<h1><h2><h3><strong><b><ul><ol><li><i><a><blockquote><center><embed><iframe><pre><table><tbody><tr><td><video><br>'),
					'post-type' => 'post',
					'post_category' => array($classieraMainCat, $classieraChildCat, $classieraThirdCat),
					'tags_input'    => explode(',', $_POST['post_tags']),
					'tax_input' => array(
					'location' => $_POST['post_location'],
					),
					'comment_status' => 'open',
					'ping_status' => 'open',
					'post_status' => $postStatus
				);

				wp_update_post($post_information);
				$post_id = $current_post;
				
				//Setup Price//
				$postMultiTag = $_POST['post_currency_tag'];
				$post_price = trim($_POST['post_price']);
				$post_old_price = trim($_POST['post_old_price']);
				
				$found_latlong1=explode("#",$_POST['found_latlong1']);
				$found_latitude1=$found_latlong1[0];
				$found_longitude1=$found_latlong1[1];
				
				/*Check If Latitude is OFF */
				//$googleLat = $_POST['latitude'];
				$googleLat = $found_latitude1;
				if(empty($googleLat)){
					$latitude = $classieraLatitude;
				}else{
					$latitude = $googleLat;
				}
				/*Check If longitude is OFF */
				//$googleLong = $_POST['longitude'];
				$googleLong = $found_longitude1;
				if(empty($googleLong)){
					$longitude = $classieraLongitude;
				}else{
					$longitude = $googleLong;
				}
				
				$featuredIMG = $_POST['classiera_featured_img'];
				$itemCondition = $_POST['item-condition'];		
				$catID = $classieraCategory.'custom_field';		
				$custom_fields = $_POST[$catID];
				/*If We are using CSC Plugin*/
				
				/*Get Country Name*/
				if(isset($_POST['post_location'])){
					$postLo = $_POST['post_location'];
					$allCountry = get_posts( array( 'include' => $postLo, 'post_type' => 'countries', 'posts_per_page' => -1, 'suppress_filters' => 0, 'orderby'=>'post__in' ) );
					foreach( $allCountry as $country_post ){
						$postCounty = $country_post->post_title;
					}
				}				
				$poststate = $_POST['post_state'];
				$postCity = $_POST['post_city'];
				$classiera_CF_Front_end = $_POST['classiera_CF_Front_end'];
				$classiera_sub_fields = $_POST['classiera_sub_fields'];
				
				/*If We are using CSC Plugin*/
				if(isset($_POST['post_category_type'])){
					update_post_meta($post_id, 'post_category_type', esc_attr( $_POST['post_category_type'] ) );
				}				
				update_post_meta($post_id, 'custom_field', $custom_fields);
				update_post_meta($post_id, 'classiera_CF_Front_end', $classiera_CF_Front_end);
				update_post_meta($post_id, 'classiera_sub_fields', $classiera_sub_fields);

				update_post_meta($post_id, 'post_currency_tag', $postMultiTag, $allowed);
				update_post_meta($post_id, 'post_price', $post_price, $allowed);
				update_post_meta($post_id, 'post_old_price', $post_old_price, $allowed);
				
				update_post_meta($post_id, 'post_perent_cat', $classieraMainCat, $allowed);
				update_post_meta($post_id, 'post_child_cat', $classieraChildCat, $allowed);				
				update_post_meta($post_id, 'post_inner_cat', $classieraThirdCat, $allowed);
				update_post_meta($post_id, 'is_inventory', '1', $allowed);
				
				update_post_meta( $post_id, 'featured_post', '1' );
				
				update_post_meta($post_id, 'post_phone', $_POST['post_phone'], $allowed);
				
				update_post_meta($post_id, 'classiera_ads_type', $_POST['classiera_ads_type'], $allowed);
				if(isset($_POST['seller'])){
					update_post_meta($post_id, 'seller', $_POST['seller'], $allowed);
				}

				//update_post_meta($post_id, 'post_location', wp_kses($postCounty, $allowed));
				//update_post_meta($post_id, 'post_location', $_POST['my_location'], $allowed);
				//update_post_meta($post_id, 'post_location', wp_kses($_POST['address'], $allowed));
				update_post_meta($post_id, 'post_location', wp_kses($_POST['found_location1'], $allowed));
				
				update_post_meta($post_id, 'post_state', wp_kses($poststate, $allowed));
				update_post_meta($post_id, 'post_city', wp_kses($postCity, $allowed));

				update_post_meta($post_id, 'post_latitude', wp_kses($found_latitude1, $allowed));

				update_post_meta($post_id, 'post_longitude', wp_kses($found_longitude1, $allowed));

				update_post_meta($post_id, 'post_address', wp_kses($_POST['found_location1'], $allowed));

				update_post_meta($post_id, 'post_video', $_POST['video'], $allowed);
				update_post_meta($post_id, 'featured_img', $featuredIMG, $allowed);
				update_post_meta($post_id, 'is_found', '1');
				if(isset($_POST['item-condition'])){
					update_post_meta($post_id, 'item-condition', $itemCondition, $allowed);
				}
				update_post_meta($post_id, 'classiera_post_type', $_POST['classiera_post_type'], $allowed);
				update_post_meta($post_id, 'pay_per_post_product_id', $_POST['pay_per_post_product_id'], $allowed);
				update_post_meta($post_id, 'days_to_expire', $_POST['days_to_expire'], $allowed);

				$resuls = $wpdb->get_results("SELECT `id` FROM `wp_found_hysts` WHERE `hyst_id` = '".$post_id."'");

					$i=1;
					foreach ($resuls as $resul) {
						//echo $resul->id;
						//echo "<br>";
						$found = $_POST['found_id'.$i];
						$found_location = $_POST['found_location'.$i];
						$found_latlong=explode("#",$_POST['found_latlong'.$i]);
						$found_latitude=$found_latlong[0];
						$found_longitude=$found_latlong[1];
						$location_price = $_POST['location'.$i.'_price'];
						$expiry_date = date("Y-m-d", strtotime($_POST['expiry_date'.$i]));
						$qty = $_POST['qty'.$i];

						if ($found==$resul->id) {
							$wpdb->query("UPDATE `wp_found_hysts` SET `found_price` = '".$location_price."', `found_location` = '".$found_location."', `found_latitude` = '".$found_latitude."', `found_longitude` = '".$found_longitude."', `expiry_date` = '".$expiry_date."', `qty` = '".$qty."' WHERE `wp_found_hysts`.`id` = ".$resul->id);
						}

						$i++;
					}

					for ($j=$i; $j < 10; $j++) {

						$found_locationj = $_POST['found_location'.$j];
						$found_latlongj=explode("#",$_POST['found_latlong'.$j]);
						$found_latitudej=$found_latlongj[0];
						$found_longitudej=$found_latlongj[1];
						$location_pricej = $_POST['location'.$j.'_price'];
						$expiry_datej = date("Y-m-d", strtotime($_POST['expiry_date'.$j]));
						$qtyj = $_POST['qty'.$j];

						if((isset($found_locationj) && $found_locationj!='') && (isset($location_pricej) && $location_pricej!='')) {
							$wpdb->query("INSERT INTO `wp_found_hysts` (`id`, `hyst_id`, `hyst_owner_id`, `found_user_id`, `found_price`, `found_location`, `confirmed_by_owner`, `confirmed_by_user3`, `confirmed_by_user3_userid`, `confirmed_by_user4`, `confirmed_by_user4_userid`, `found_latitude`, `found_longitude`, `is_found_by_business_employee`, `expiry_date`, `qty`, `is_expire_notified`, `is_url`) VALUES (NULL, '".$post_id."', '".get_current_user_id()."', '".get_current_user_id()."', '".$location_pricej."', '".$found_locationj."', '1', '1', '".get_current_user_id()."', '1', '".get_current_user_id()."', '".$found_latitudej."', '".$found_longitudej."', '1', '".$expiry_datej."', '".$qtyj."', '0', '0')");
						}
					}

				if($classieraPostType == 'payperpost'){
					$permalink = $classieraProfileURL;
				}else{
					$permalink = get_permalink( $post_id );
				}
				//If Its posting featured image//
				if(trim($_POST['classiera_post_type']) != 'classiera_regular'){
					if($_POST['classiera_post_type'] == 'payperpost'){
						//Do Nothing on Pay Per Post//
					}elseif($_POST['classiera_post_type'] == 'classiera_regular_with_plan'){
						//Regular Ads Posting with Plans//
						$classieraPlanID = trim($_POST['regular_plan_id']);
						global $wpdb;
						$current_user = wp_get_current_user();
						$userID = $current_user->ID;
						$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE id = $classieraPlanID" );
						if($result){
							$tablename = $wpdb->prefix . 'classiera_plans';
							foreach ( $result as $info ){
								$newRegularUsed = $info->regular_used +1;
								$update_data = array('regular_used' => $newRegularUsed);
								$where = array('id' => $classieraPlanID);
								$update_format = array('%s');
								$wpdb->update($tablename, $update_data, $where, $update_format);
							}
						}
					}else{
						//Featured Post with Plan Ads//
						$featurePlanID = trim($_POST['classiera_post_type']);
						global $wpdb;
						$current_user = wp_get_current_user();
						$userID = $current_user->ID;
						$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE id = $featurePlanID" );
						if ($result){
							$featuredADS = 0;
							$tablename = $wpdb->prefix . 'classiera_plans';
							foreach ( $result as $info ){
								$totalAds = $info->ads;
								if (is_numeric($totalAds)){
									$totalAds = $info->ads;
									$usedAds = $info->used;
									$infoDays = $info->days;
								}								
								if($totalAds == 'unlimited'){
									$availableADS = 'unlimited';
								}else{
									$availableADS = $totalAds-$usedAds;
								}								
								if($usedAds < $totalAds && $availableADS != "0" || $totalAds == 'unlimited'){
									global $wpdb;
									$newUsed = $info->used +1;
									$update_data = array('used' => $newUsed);
									$where = array('id' => $featurePlanID);
									$update_format = array('%s');
									$wpdb->update($tablename, $update_data, $where, $update_format);
									update_post_meta($post_id, 'post_price_plan_id', $featurePlanID );

									$dateActivation = date('m/d/Y H:i:s');
									update_post_meta($post_id, 'post_price_plan_activation_date', $dateActivation );		
									
									$daysToExpire = $infoDays;
									$dateExpiration_Normal = date("m/d/Y H:i:s", strtotime("+ ".$daysToExpire." days"));
									update_post_meta($post_id, 'post_price_plan_expiration_date_normal', $dateExpiration_Normal );



									$dateExpiration = strtotime(date("m/d/Y H:i:s", strtotime("+ ".$daysToExpire." days")));
									update_post_meta($post_id, 'post_price_plan_expiration_date', $dateExpiration );
									update_post_meta($post_id, 'featured_post', "1" );
								}
							}
						}
					}
				}
				//If Its posting featured image//
				if ( isset($_FILES['uploadattachment']) ) {
					$count = 0;
					$files = $_FILES['uploadattachment'];
					foreach ($files['name'] as $key => $value) {				
						if ($files['name'][$key]) {
							$file = array(
								'name'     => $files['name'][$key],
								'type'     => $files['type'][$key],
								'tmp_name' => $files['tmp_name'][$key],
								'error'    => $files['error'][$key],
								'size'     => $files['size'][$key]
							);
							$_FILES = array("upload_attachment" => $file);
							
							foreach ($_FILES as $file => $array){								
								$featuredimg = $_POST['classiera_featured_img'];
								if($count == $featuredimg){
									$attachment_id = classiera_insert_attachment($file,$post_id);
									set_post_thumbnail( $post_id, $attachment_id );
								}else{
									$attachment_id = classiera_insert_attachment($file,$post_id);
								}								
								$count++;
							}
							
						}						
					}/*Foreach*/
				}					
				wp_redirect($permalink); exit();
			}
		}
	}else{
		if(trim($_POST['postTitle']) === '') {
			$postTitleError = esc_html__( 'Please enter a title.', 'classiera' );	
			$hasError = true;
		}
		if($_POST['classiera-main-cat-field'] === '-1') {
			$catError = esc_html__( 'Please select a category.', 'classiera' );
			$hasError = true;
			$error[]='Please select category.';
		} 
		if(isset($_POST['postTitle']) && $_POST['postTitle']=='')
		{
			$hasError = true;
			$error[]='Please enter a title.';
		}
		if(isset($_POST['postContent']) && $_POST['postContent']=='')
		{
			$hasError = true;
			$error[]='Please enter a description.';
		}
		if(isset($_POST['found_location1']) && $_POST['found_location1']=='')
		{
			$hasError = true;
			$error[]='Please enter a Found Location 1.';
		}
		if(isset($_POST['location1_price']) && $_POST['location1_price']=='')
		{
			$hasError = true;
			$error[]='Please enter a Location 1 Price.';
		}
	}

}
get_header();  ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php 
	$page = get_page($post->ID);
	$current_page_id = $page->ID;
	$featuredUsed = '';
	$featuredAds = '';
	$regularUsed = '';
	$regularAds = '';
?>
<style>
.form-main-section .list-unstyled li a{padding: 8px 15px;
display: table;
text-align: left;text-align: center;
width: 100%;
height: 100%;
border-radius: 3px;moz-border-radius: 3px;webkit-border-radius: 3px;}
.list-unstyled li a:hover, .list-unstyled li a:hover i{background:#000; color:#fff !important;moz-border-radius: 3px;webkit-border-radius: 3px;}
.classieraSubReturn li{float:left; width:20%;}
</style>
<section id="edit_post" class="user-pages section-gray-bg user-detail-section edit_section all_usertemplate">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
				<?php 
				global $redux_demo;
				global $wpdb;
				$current_user = wp_get_current_user();
				$userID = $current_user->ID;			
				$featured_plans = $redux_demo['featured_plans'];
				$classieraRegularAdsOn = $redux_demo['regular-ads'];
				$postLimitOn = $redux_demo['regular-ads-posting-limit'];
				$regularCount = $redux_demo['regular-ads-user-limit'];
				$cUserCheck = current_user_can( 'administrator' );
				$role = $current_user->roles;
				$currentRole = $role[0];
				$classieraAllowPosts = false;
				$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
				foreach ($result as $info){											
					$featuredAdscheck = $info->ads;											
					if (is_numeric($featuredAdscheck)){
						$featuredAds += $info->ads;
						$featuredUsed += $info->used;
					}
					$regularAdscheck = $info->regular_ads;
					if (is_numeric($regularAdscheck)){
						$regularAds += $info->regular_ads;
						$regularUsed += $info->regular_used;
					}
				}
				if (is_numeric($featuredAds) && is_numeric($featuredUsed)){
					$featuredAvailable = $featuredAds-$featuredUsed;
				}
				if (is_numeric($regularAds) && is_numeric($regularUsed)){
					$regularAvailable = $regularAds-$regularUsed;
				}
				
				$curUserargs = array(					
					'author' => $user_ID,
					'post_status' => array('publish', 'pending', 'private', 'trash')    
				);
				$countPosts = count(get_posts($curUserargs));
				if($currentRole == "administrator"){
					$classieraAllowPosts = true;
				}else{
					if($postLimitOn == true){
						if($regularAvailable == 0 && $featuredAvailable == 0 && $countPosts >= $regularCount){
							$classieraAllowPosts = false;
						}else{
							$classieraAllowPosts = true;
						}
					}else{
						$classieraAllowPosts = true;
					}
				}
				//echo $postLimitOn.' Limit<br />';
				//echo $regularAvailable.' regularAvailable<br />';
				//echo $featuredAvailable.' featuredAvailable<br />';
				//echo $countPosts.' countPosts<br />';
				//echo $regularCount.' regularCount<br />';
				//echo $classieraAllowPosts.' classieraAllowPosts<br />';
				if($classieraAllowPosts == false){
					?>
					<div class="alert alert-warning" role="alert">
					  <strong><?php esc_html_e('Hello.', 'classiera') ?></strong><?php esc_html_e('You HYST Posts limit are exceeded, Please Purchase a Plan for posting More Ads.', 'classiera') ?>&nbsp;&nbsp;<a class="btn btn-primary btn-sm" href="<?php echo $featured_plans; ?>"><?php esc_html_e('Purchase Plan', 'classiera') ?></a>
					</div>
					<?php
				}elseif($classieraAllowPosts == true){
				?>
				<div class="submit-post section-bg-white">
				<div class="fileupload-wrapper"><div id="myUpload">
					<form class="form-horizontal" action="" role="form" id="primaryPostForm" method="POST" data-toggle="validator" enctype="multipart/form-data">
						<hr class="line1">
						<h2 class="user_name two-lines"><?php
						
						$user = wp_get_current_user();
							$roles = $user->roles;
							
							if(in_array('inventory_user',$roles) || in_array('company_user',$roles))
							{
								 esc_html_e(' EDIT INVENTORY', 'classiera');
							}
							else
							{
								 esc_html_e(' EDIT HYST', 'classiera');
							}
						 ?></h2>
						 <hr class="line1">
						  <?php
						// print_r($error);
						 for($lp=0;$lp<count($error);$lp++)
						 {
						 	echo '<span style="float:left; clear:left; color:red;">'.$error[$lp].'</span>';
						 }

							?>
						<div style="clear:left;"></div>
						<div class="top-buffer3"></div>
						<!--Category-->
						<div class="form-main-section classiera-post-cat">
							<div class="classiera-post-main-cat" id="accordion">
							    <div class="row">
								    <div class="single_title title_link">
										<a href="javascript:void(0);" target-id="#category" parent-id="#accordion" class="accordian_toggle"><h2 class="classiera-post-inner-heading"><?php esc_html_e('Select a Category', 'classiera') ?></h2>
										<img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>" /></a>
									</div>
								</div>
								<div class="top-buffer2"></div>
								<style>
								/*.mycustselectcat{background:#000;}*/
								#category .active{background:#000; color:#fff;}
								#category .active a span{color:#fff !important;}
								#subcategory li{width:14% !important;}
								#subcategory .active{background:#000; color:#fff;}
								#subcategory .active a{color:#fff !important;}
								</style>
								<ul class="list-unstyled list-inline list_icon inner-toggle" id="category">
									<?php 
									$categories = get_terms('category', array(
											'hide_empty' => 0,
											'parent' => 0,
											'order'=> 'ASC'
										)	
									);
									$count=0;
									foreach ($categories as $category){
									$count++;
										//print_r($category);
										$tag = $category->term_id;
										$classieraCatFields = get_option(MY_CATEGORY_FIELDS);
										if (isset($classieraCatFields[$tag])){
											$classieraCatIconCode = $classieraCatFields[$tag]['category_icon_code'];
											$classieraCatIcoIMG = $classieraCatFields[$tag]['your_image_url'];
											$classieraCatIconClr = $classieraCatFields[$tag]['category_icon_color'];
										}
										if(empty($classieraCatIconClr)){
											$iconColor = $primaryColor;
										}else{
											$iconColor = $classieraCatIconClr;
										}
										$category_icon = stripslashes($classieraCatIconCode);
										?>
										<li class="match-height maincatli <?php echo ($post_main_cat==$tag)?'active':''; ?>"" id="maincat_div_<?php echo $count;?>" onclick="return changemaincatbg('<?php echo $count;?>', '<?php echo $tag;?>');">
											<a href="#" id="<?php echo $tag; ?>" class="border">
												<?php 
												if($classieraIconsStyle == 'icon' && $category_icon!=''){
													?>
													<i class="<?php echo $category_icon; ?>" style="color:<?php echo $iconColor; ?>;"></i>
													<?php
												}elseif($classieraIconsStyle == 'img'){
													?>
													<img src="<?php echo $classieraCatIcoIMG; ?>" alt="<?php echo get_cat_name( $catName ); ?>">
													<?php
												}
												?>
												<span><?php echo get_cat_name( $tag ); ?></span>
											</a>
										</li>
										<?php
									}
									?>
								</ul><!--list-unstyled-->
								<script type="text/javascript" language="javascript">
								function changemaincatbg(cnt, catid)
								{
									var totcount='<?php echo $count;?>';
									jQuery("#maincat_div_"+cnt).addClass("mycustselectcat");
									jQuery("#classiera-main-cat-field").val(catid);
									for(var lp=1;lp<=totcount;lp++)
									{
										if(lp!=cnt)
											jQuery("#maincat_div_"+cnt).removeClass("mycustselectcat");
									}
								}

								jQuery(document).ready(function($){
									var hascls = $("#category li.maincatli.active").hasClass('active');
							    	if (hascls) {
							    	jQuery("#category").show();		
							        var mainCatId = jQuery("#category li.maincatli.active a").attr('id');
									var data = {
										'action': 'classieraGetSubCatOnClick',
										'mainCat': mainCatId,
									};
									jQuery.post(ajaxurl, data, function(response){
										jQuery('.classieraSubReturn').html(response);
										if(response){
											jQuery('.classiera-post-sub-cat').show();
											var post_child_cat = '<?php echo $post_child_cat ?>';
											jQuery('.classiera-post-sub-cat #subcategory li#subcatli_'+post_child_cat).addClass('active');
										}			
									});
							    	}
								});
								</script>
								<input class="classiera-main-cat-field" id="classiera-main-cat-field" name="classiera-main-cat-field" type="hidden" value="<?php echo $post_main_cat; ?>">
							</div><!--classiera-post-main-cat-->
							<script type="text/javascript" language="javascript">
							/*$('#subcategory li').click(function() {
							alert("i am here");
								$('#subcategory li.active').removeClass('active');
								$(this).addClass('active');
							});*/
							/*jQuery( document ).ready(function() {
								//console.log( "ready!" );
									jQuery('#subcategory li').click(function() {
									alert("abc");
									}
								});
							});
							jQuery( document ).ready(function() {
								jQuery('#subcategory li a').on('click', function(){
									alert("ac");
									jQuery(this).parent().addClass('active').siblings().removeClass('active');
								});
							});*/
							
							function changesubcatbg(id)
							{
								
									/*jQuery("#maincat_div_"+cnt).addClass("mycustselectcat");
									for(var lp=1;lp<=totcount;lp++)
									{
										if(lp!=cnt)
											jQuery("#maincat_div_"+cnt).removeClass("mycustselectcat");
									}*/
									jQuery('#subcategory li').removeClass('active');
									jQuery("#subcatli_"+id).addClass("active");
							}
							</script>
							
							
							<div class="classiera-post-sub-cat show" id="accordion1">
							
							    <div class="row">
									<div class="single_title title_link">
										<a href="javascript:void(0);" target-id="#subcategory" parent-id="#accordion1" class="accordian_toggle"><h2 class="classiera-post-inner-heading"><?php esc_html_e('Select a Sub Category', 'classiera') ?></h2>
										<img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>" /></a>
									</div>
								</div>
								<div class="top-buffer2"></div>
								
							
								<ul class="list-unstyled classieraSubReturn list_icon list_icon1 inner-toggle show" id="subcategory"></ul>
								
								<input class="classiera-sub-cat-field" name="classiera-sub-cat-field" id="classiera-sub-cat-field" type="hidden" value="<?php echo $post_child_cat; ?>">
							</div><!--classiera-post-sub-cat-->
							
							
							<!--ThirdLevel-->
							<div class="classiera_third_level_cat hide">
								<h4 class="classiera-post-inner-heading">
									<?php esc_html_e('Select a Sub Category', 'classiera') ?> :
								</h4>
								<ul class="list-unstyled classieraSubthird">
								</ul>
								<input class="classiera_third_cat" name="classiera_third_cat" id="classiera_third_cat" type="hidden" value="">
								
							</div>
							<!--ThirdLevel-->
						</div>
						
						
						<!--Category-->
						<div style="clear:left;"></div>
						<div class="top-buffer2"></div>
						
						
						<div class="form-main-section post-detail">
							<hr class="line1">
							<h2 class="user_name two-lines"><?php esc_html_e('Inventory Details', 'classiera') ?></h2>
							<hr class="line1">
							
							<div style="clear:left;"></div>
						    <div class="top-buffer2"></div>
							
							<div class="form-group hide">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Selected Category', 'classiera') ?> : </label>
                                <div class="col-sm-9">
                                    <p class="form-control-static"></p>
									<input type="text" id="selectCatCheck" value="" data-error="<?php esc_html_e('Please select a category.', 'classiera') ?>" required >
									<div class="help-block with-errors selectCatDisplay"></div>
                                </div>
                            </div><!--Selected Category-->
							<?php if($classiera_ads_typeOn == 1){?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Type of HYST', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <input id="sell" value="sell" type="radio" name="classiera_ads_type" checked>
                                        <label for="sell"><?php esc_html_e('I want to sell', 'classiera') ?></label>
										
                                        <input id="buy" value="buy" type="radio" name="classiera_ads_type">
                                        <label for="buy"><?php esc_html_e('I want to buy', 'classiera') ?></label>
										
										<input type="radio" name="classiera_ads_type" value="rent" id="rent">
										<label for="rent"><?php esc_html_e('I want to rent', 'classiera') ?></label>
										
										<input type="radio" name="classiera_ads_type" value="hire" id="hire">
										<label for="hire"><?php esc_html_e('I want to hire', 'classiera') ?></label>
                                    </div>
                                </div>
                            </div><!--Type of Ad-->
							<?php } ?>
							<div style="clear:left;"></div>
							
							
							
							<div class="col-md-10 col-md-offset-1">
								<div class="form-group col-sm-6">
									<label class="col-sm-3 text-left flip hide" for="title"><?php esc_html_e(' title', 'classiera') ?> : <span>*</span></label>
										<input id="title" data-minlength="5" name="postTitle" type="text" class="form-control form-control-md" placeholder="<?php esc_html_e(' title', 'classiera') ?>" value="<?php echo $title; ?>" required>
										<div class="help-block hide"><?php esc_html_e('type minimum 5 characters', 'classiera') ?></div>
								</div><!--Ad title-->
								<div class="form-group height-50 col-sm-6">
									<label class="col-sm-3 text-left flip hide" for="description"><?php esc_html_e(' description', 'classiera') ?> : <span>*</span></label>
										<textarea name="postContent" placeholder="description" rows="2" id="description" class="form-control" data-error="<?php esc_html_e('Write description', 'classiera') ?>" required><?php echo $content;?></textarea>
										<div class="help-block with-errors"></div>
								</div><!--Ad description-->
								<!--Ad Tags-->
								<div class="form-group col-sm-6">
									<label class="col-sm-3 text-left hide flip"><?php esc_html_e('HYST Tags', 'classiera') ?> : </label>
										<div class="form-inline">
											<img class="input-img" src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>" height="30px" width="30px">
										   <?php
												echo "<input type='text' id='post_tags' placeholder='Tags' name='post_tags' value='";
												$posttags = get_the_tags($current_post);
												if ($posttags) {
												  foreach($posttags as $tag) {
													$tags_list = $tag->name . ', '; 
													echo $tags_list;
												  }
												}
												echo "' size='' maxlength='' class='form-control form-control-md'>"; 
											 ?>
										</div>
										<div class="help-block hide"><?php esc_html_e('Tags Example : ads, car, cat, business', 'classiera') ?></div>
									
								</div>
								<?php 
								$classieraPriceSecOFF = $redux_demo['classiera_sale_price_off'];
								$classieraMultiCurrency = $redux_demo['classiera_multi_currency'];
								$regularpriceon= $redux_demo['regularpriceon'];
								$postCurrency = $redux_demo['classierapostcurrency'];
								$classieraTagDefault = $redux_demo['classiera_multi_currency_default'];
								?>
								<?php if($classieraPriceSecOFF == 1){?>
								<div class="form-group col-sm-6">
									<label class="col-sm-3 text-left hide flip"><?php esc_html_e(' price', 'classiera') ?> : </label>
										<div class="form-inline">
											<?php if($classieraMultiCurrency == 'multi'){?>
											<div class="col-sm-12">
												<div class="inner-addon right-addon input-group price__tag">
													<div class="input-group-addon">
														<span class="currency__symbol">
															<?php echo classiera_Display_currency_sign($classieraTagDefault); ?>
														</span>
													</div>
													<i class="form-icon right-form-icon fa fa-angle-down"></i>
													<?php echo classiera_Select_currency_dropdow($classieraTagDefault); ?>
												</div>
											</div>
											<?php } ?>
											
											
								
											<div class="input-group-addon hide">
												<span class="currency__symbol">
												<?php 
												if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
													echo $postCurrency;
												}elseif($classieraMultiCurrency == 'multi'){
													echo classiera_Display_currency_sign($classieraTagDefault);
												}else{
													echo "&dollar;";
												}
												?>	
												</span>
											</div>
											<input type="text" name="post_price" id="post_price" class="form-control form-control-md" placeholder="<?php esc_html_e(' price', 'classiera') ?>" value="<?php echo $post_price; ?>">


													
											<?php if($regularpriceon == 1){?>
											<div class="col-sm-6" style="display:none;">
												<div class="input-group">
													<div class="input-group-addon">
														<span class="currency__symbol">
														<?php 
														if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){
															echo $postCurrency;
														}elseif($classieraMultiCurrency == 'multi'){
															echo classiera_Display_currency_sign($classieraTagDefault);
														}else{
															echo "&dollar;";
														}
														?>	
														</span>
													</div>
													<input type="text" name="post_old_price" class="form-control form-control-md" placeholder="<?php esc_html_e('REMOVE', 'classiera') ?>">
												</div>
											</div>
											<?php } ?>	
										<?php if (!empty($postCurrency) && $classieraMultiCurrency == 'single'){?>
										<div class="help-block hide"><?php esc_html_e('Currency sign is already set as', 'classiera') ?>&nbsp;<?php echo $postCurrency; ?>&nbsp;<?php esc_html_e('Please do not use currency sign in price field. Only use numbers ex: 12345', 'classiera') ?></div>
										<?php } ?>
									</div>
								</div><!--Ad Price-->
								<?php } ?>
							</div>
							<!--ContactPhone-->
							<?php $classieraAskingPhone = $redux_demo['phoneon'];?>
							<?php if($classieraAskingPhone == 1){?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Phone/Mobile', 'classiera') ?> :</label>
                                <div class="col-sm-9">
                                    <div class="form-inline row">
                                        <div class="col-sm-12">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                                                <input type="text" name="post_phone" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter your phone number or Mobile number', 'classiera') ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="help-block"><?php esc_html_e('Its Not required, but if you will put phone here then it will show publicly', 'classiera') ?></div>
                                </div>
                            </div>
							<?php } ?>
							<!--ContactPhone-->							
							<?php 
								$adpostCondition= $redux_demo['adpost-condition'];
								if($adpostCondition == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Item Condition', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <input id="new" type="radio" name="item-condition" value="<?php esc_html_e('new', 'classiera') ?>" name="item-condition" checked>
                                        <label for="new"><?php esc_html_e('Brand New', 'classiera') ?></label>
                                        <input id="used" type="radio" name="item-condition" value="<?php esc_html_e('used', 'classiera') ?>" name="item-condition">
                                        <label for="used"><?php esc_html_e('Used', 'classiera') ?></label>
                                    </div>
                                </div>
                            </div><!--Item condition-->
								<?php } ?>
						</div><!---form-main-section post-detail-->
						<!-- extra fields -->
						<div class="classieraExtraFields" style="display:none;"></div>
						<!-- extra fields -->
						<!-- add photos and media -->
						<?php								
							/*Image Count Check*/
							global $redux_demo;
							global $wpdb;
							$paidIMG = $redux_demo['premium-ads-limit'];
							$regularIMG = $redux_demo['regular-ads-limit'];								
							$current_user = wp_get_current_user();
							$userID = $current_user->ID;
							$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
							$totalAds = 0;
							$usedAds = 0;
							$availableADS = '';
							if(!empty($result)){
								foreach ( $result as $info ){
									$availAds = $info->ads;
									if (is_numeric($availAds)) {
										$totalAds += $info->ads;
										$usedAds += $info->used;
									}
								}
							}
							$availableADS = $totalAds-$usedAds;							
							if($availableADS == "0" || empty($result)){
								$imageLimit = $regularIMG;
							}else{
								$imageLimit = $paidIMG;
							}
							if($currentRole == "administrator"){
								$imageLimit = $paidIMG;
							}
						if($imageLimit != 0){
						/*?>
						<div class="form-main-section media-detail">
							
                            <h4 class="text-uppercase border-bottom"><?php esc_html_e('Image And Video', 'classiera') ?> :</h4>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Photos and Video for your HYST', 'classiera') ?> :</label>
                                <div class="col-sm-9">
                                    <div class="classiera-dropzone-heading">
                                        <i class="classiera-dropzone-heading-text fa fa-cloud-upload" aria-hidden="true"></i>
                                        <div class="classiera-dropzone-heading-text">
                                            <p><?php esc_html_e('Select files to Upload', 'classiera') ?></p>
                                            <p><?php esc_html_e('You can add multiple images. HYST With photo get 50% more Responses', 'classiera') ?></p>
											<p class="limitIMG"><?php esc_html_e('', 'classiera') ?>&nbsp;&nbsp;<?php esc_html_e('', 'classiera') ?></p>
                                        </div>
                                    </div>
                                    <!-- HTML heavily inspired by http://blueimp.github.io/jQuery-File-Upload/ -->
                                    <div id="mydropzone" class="classiera-image-upload clearfix" data-maxfile="<?php echo $imageLimit; ?>">
										<!--Imageloop-->
										<?php 
										for ($i = 0; $i <8 ; $i++){
										?>
                                        <div class="classiera-image-box">
                                            <div class="classiera-upload-box">
												<input name="image-count" type="hidden" value="<?php echo $imageLimit; ?>" />
                                                <input class="classiera-input-file imgInp" id="imgInp<?php echo $i; ?>" type="file" name="upload_attachment[]">												
                                                <label class="img-label" for="imgInp<?php echo $i; ?>"><i class="fa fa-plus-square-o"></i></label>
                                                <div class="classiera-image-preview">
                                                    <img class="my-image" src=""/>
                                                    <span class="remove-img"><i class="fa fa-times-circle"></i></span>
                                                </div>
                                            </div>
                                        </div>
										<?php } ?>
										<input type="hidden" name="classiera_featured_img" id="classiera_featured_img" value="0">
										<!--Imageloop-->
                                    </div>
									
									<?php 
									$classiera_video_postads = $redux_demo['classiera_video_postads'];
									if($classiera_video_postads == 1){
									?>
                                    <div class="iframe">
                                        <div class="iframe-heading">
                                            <i class="fa fa-video-camera"></i>
                                            <span><?php esc_html_e('Put here iframe or video url.', 'classiera') ?></span>
                                        </div>
                                        <input type="file" class="form-control" name="video" id="video-code" placeholder="<?php esc_html_e('Put here video ', 'classiera') ?>">
                                        <div class="help-block">
                                            <p><?php esc_html_e('Add  video ', 'classiera') ?></p>
                                        </div>
                                    </div>
									<?php } ?>
                                </div>
                            </div>
                        </div>
						
						*/?>
						
						<?php } ?>
						
						
					<?php	/*<style>
							.image-upload > input {
							  display:none;
							  
							}
						</style>
						<div class="image-upload" style="margin-bottom:20px;">
						  <label for="file-input">
							<img src="<?php echo site_url();?>/wp-content/uploads/2018/04/file-upload-icon.png" style="width:100px;height:auto;" style="pointer-events: none"/>
						  </label>
						<script type="text/javascript" language="javascript">
							function makeFileList() {
							  var input = document.getElementById("file-input");
							  var ul = document.getElementById("fileList");
							  while (ul.hasChildNodes()) {
								ul.removeChild(ul.firstChild);
							  }
							  for (var i = 0; i < input.files.length; i++) {
								var li = document.createElement("li");
								li.innerHTML = input.files[i].name;
								ul.appendChild(li);
							  }
							  if(!ul.hasChildNodes()) {
								var li = document.createElement("li");
								li.innerHTML = 'No Files Selected';
								ul.appendChild(li);
							  }
							}
						</script>
						  <input id="file-input" name="upload_attachment[]" type="file"  onChange="makeFileList();" multiple/>
						  
						  <p style="margin-left:50px;">
							  <strong>Files You Selected:</strong>
							</p>
							
							<ul id="fileList" style="margin-left:50px;">
							  <li>No Files Selected</li>
							</ul>
						</div>
						*/?>

						<!-- add photos and media -->
						
						<div class="clearfix"></div>
						<div class="top-bufer2"></div>
						<!-- post location -->
						<?php
						$classiera_ad_location_remove = $redux_demo['classiera_ad_location_remove'];
						if($classiera_ad_location_remove == 1){
						?>
						<div class="form-main-section post-location" style="margin-bottom:15px !important;">							
							<div class="row">
							    <div class="single_title">
									<h2 class="classiera-post-inner-heading"><?php esc_html_e(' location', 'classiera') ?></h2>
								</div>
							</div>
							<div class="top-bufer2"></div>							
							<?php 
							$args = array(
								'post_type' => 'countries',
								'posts_per_page'   => -1,
								'orderby'          => 'title',
								'order'            => 'ASC',
								'post_status'      => 'publish',
								'suppress_filters' => false 
							);
							$country_posts = get_posts($args);
							if(!empty($country_posts)){
							?>
							<!--Select Country-->
							<div class="form-group" style="display:none;">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Select Country', 'classiera') ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <div class="inner-addon right-addon">
                                        <i class="form-icon right-form-icon fa fa-angle-down"></i>
                                        <select name="post_location" id="post_location" class="form-control form-control-md">
                                            <option value="-1" selected disabled><?php esc_html_e('Select Country', 'classiera'); ?></option>
                                            <?php 
											foreach( $country_posts as $country_post ){
												?>
												<option value="<?php echo $country_post->ID; ?>"><?php echo $country_post->post_title; ?></option>
												<?php
											}
											?>
                                        </select>
                                    </div>
                                </div>
                            </div>
							<?php } ?>
							<!--Select Country-->	
							<!--Select States-->
							<?php 
							$locationsStateOn = $redux_demo['location_states_on'];
							if($locationsStateOn == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Select State', 'classiera') ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <div class="inner-addon right-addon">
                                        <i class="form-icon right-form-icon fa fa-angle-down"></i>
										<select name="post_state" id="post_state" class="selectState form-control form-control-md" >
											<option value=""><?php esc_html_e('Select State', 'classiera'); ?></option>
										</select>
                                    </div>
                                </div>
                            </div>
							<?php } ?>
							<!--Select States-->
							<!--Select City-->
							<?php 
							$locationsCityOn= $redux_demo['location_city_on'];
							if($locationsCityOn == 1){
							?>
							<div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Select City', 'classiera'); ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <div class="inner-addon right-addon">
                                        <i class="form-icon right-form-icon fa fa-angle-down"></i>
										<select name="post_city" id="post_city" class="selectCity form-control form-control-md" >
											<option value=""><?php esc_html_e('Select City', 'classiera'); ?></option>
										</select>
                                    </div>
                                </div>
                            </div>
							<?php } ?>
							<!--Select City-->
							<!--Address-->
							<?php if($classieraAddress == 1){?>
							<div class="form-group">
                                <?php /*<label class="col-sm-3 text-left flip"><?php esc_html_e('Address', 'classiera'); ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                        <a id="getLocation" href="#" name="address" class="form-icon form-icon-size-small" title="<?php esc_html_e('Click here to get your own location', 'classiera'); ?>">
                               <i class="fa fa-crosshairs" style="
    position:  relative;
   left: 580px;
    top: 33px;
"></i>*/?>
                           <!-- </a> -->
                            <?php /*<input type="text" id="getCity" name="<?php echo $classieraLocationName; ?>" class="form-control form-control-sm sharp-edge" placeholder="<?php esc_html_e('location', 'classiera'); ?>">*/?>
							
							<?php /*<input type="text" id="getCity" name="my_location" class="form-control form-control-sm sharp-edge" placeholder="<?php esc_html_e('location', 'classiera'); ?>">*/?>
							<?php /* <input style="width:560px;" id="address" type="text" name="address" value="" class="form-control form-control-md" placeholder="<?php esc_html_e('Address or City', 'classiera') ?>" required>*/?>
										
                                    <!--</div>-->
                            </div>
							<?php } ?>
							<!--Address-->
							<!--Google Value-->
							<div class="form-group">
								<?php 
									$googleFieldsOn = $redux_demo['google-lat-long']; 
									if($googleFieldsOn == 1){
								?>
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Set Latitude & Longitude', 'classiera') ?> : <span>*</span></label>
									<?php } ?>
                                <div class="col-sm-9">
								<?php 
									$googleFieldsOn = $redux_demo['google-lat-long']; 
									if($googleFieldsOn == 1){
								?>
                                    <div class="form-inline row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                <input type="text" name="latitude" id="latitude" class="form-control form-control-md" placeholder="<?php esc_html_e('Latitude', 'classiera') ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                <input type="text" name="longitude" id="longitude" class="form-control form-control-md" placeholder="<?php esc_html_e('Longitude', 'classiera') ?>" required>
                                            </div>
                                        </div>
                                    </div>
									<?php }else{ ?>
										<input type="hidden" id="latitude" name="latitude">
										<input type="hidden" id="longitude" name="longitude">
									<?php } ?>
									<?php 
									
								$googleMapadPost = $redux_demo['google-map-adpost']; 
								if($googleMapadPost == 1){/*
								
								?>
                                    <div id="post-map" class="submitMAp">
                                       
										<div id="newmappost" style="width: 700px; height: 500px"> </div>
										<script>
     
     
									   var map, infoWindow;
								
									  function initMap() {
									  initAutocomplete();
										map = new google.maps.Map(document.getElementById('newmappost'), {
										  center: {lat: 0, lng: 0},
										  zoom: 13,
										  mapTypeId: 'roadmap'
										});
										
										 infoWindow = new google.maps.InfoWindow;
								
										// Try HTML5 geolocation.
										if (navigator.geolocation) {
										  navigator.geolocation.getCurrentPosition(function(position) {
											var pos = {
											  lat: position.coords.latitude,
											  lng: position.coords.longitude
											};
											document.getElementById("latitude").value =  position.coords.latitude;
											document.getElementById("longitude").value = position.coords.longitude;
											
											infoWindow.setPosition(pos);
											infoWindow.setContent('Location found.');
											infoWindow.open(map);
											map.setCenter(pos);
										  }, function() {
											handleLocationError(true, infoWindow, map.getCenter());
										  });
										} else {
										  // Browser doesn't support Geolocation
										  handleLocationError(false, infoWindow, map.getCenter());
										}
									  
								
										
										// Create the search box and link it to the UI element.
										var input = document.getElementById('address');
										var searchBox = new google.maps.places.SearchBox(input);
										map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
								
										// Bias the SearchBox results towards current map's viewport.
										map.addListener('bounds_changed', function() {
										  searchBox.setBounds(map.getBounds());
										});
								
										var markers = [];
										// Listen for the event fired when the user selects a prediction and retrieve
										// more details for that place.
										//To capture click event.
											 google.maps.event.addListener(map, 'click', function (e) {
												document.getElementById("latitude").value = e.latLng.lat();
												document.getElementById("longitude").value = e.latLng.lng();
												placeMarker(e.latLng,map);
											 });
										
										searchBox.addListener('places_changed', function() {
										  var places = searchBox.getPlaces();
								
										  if (places.length == 0) {
											return;
										  }
								
										  // Clear out the old markers.
										  markers.forEach(function(marker) {
											marker.setMap(null);
										  });
										  markers = [];
								
										  // For each place, get the icon, name and location.
										  var bounds = new google.maps.LatLngBounds();
										  places.forEach(function(place) {
											if (!place.geometry) {
											  console.log("Returned place contains no geometry");
											  return;
											}
											
								
											if (place.geometry.viewport) {
											  // Only geocodes have viewport.
											  bounds.union(place.geometry.viewport);
											} else {
											  bounds.extend(place.geometry.location);
											}
										  });
										  map.fitBounds(bounds);
										});
									  }
								
									 var marker;
									function placeMarker(location,map) {
									  if ( marker ) {
										marker.setPosition(location);
									  } else {
										marker = new google.maps.Marker({
										  position: location,
										  map: map
										});
									  }
									}
									function handleLocationError(browserHasGeolocation, infoWindow, pos) {
										infoWindow.setPosition(pos);
										infoWindow.setContent(browserHasGeolocation ?
															  'Error: The Geolocation service failed.' :
															  'Error: Your browser doesn\'t support geolocation.');
										infoWindow.open(map);
									}
								  
									</script>
									
									
                                    </div>
								<?php */} ?>
                                </div>
								
								
                            </div>
							<div style="clear:left;"></div>
							   <?php
							   function wpse_enqueue_datepicker() {
									// Load the datepicker script (pre-registered in WordPress).
									wp_enqueue_script( 'jquery-ui-datepicker' );
								
									// You need styling for the datepicker. For simplicity I've linked to Google's hosted jQuery UI CSS.
									wp_register_style( 'jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
									wp_enqueue_style( 'jquery-ui' );  
								}
								add_action( 'wp_enqueue_scripts', 'wpse_enqueue_datepicker' );
							   ?>   
							  
							<!--Google Value-->
							<div style="clear:left;"></div>
							
							    <div class="col-md-10 col-md-offset-1">
							    	<?php
										global $wpdb;
										$sel_found = $wpdb->get_results("select * from wp_found_hysts where hyst_id='".$_GET['post']."'");
										//echo "<pre>";print_r($sel_found);
										if(count($sel_found)>0)
										{
											$count=1;
											foreach($sel_found as $key=>$val)
											{ ?>
									<div class="form-group">
										<label class="col-sm-3 text-left hide flip"><?php esc_html_e('Found Location '.$count, 'classiera'); ?>: <span>*</span></label>
											<div class="inner-addon right-addon">
												<i class="form-icon right-form-icon hide fa"></i>
												<div class="form-group col-sm-6">
												     <input onFocus="geolocate<?php echo $count;?>()" onChange="getlatlong<?php echo $count;?>(this.value, 0);" type="text" name="found_location<?php echo $count;?>" id="found_location<?php echo $count;?>" class="form-control form-control-md" placeholder="location <?php echo $count;?>" value="<?php echo $sel_found[$key]->found_location;?>" autocomplete="on">
												</div>
											    <div class="form-group col-sm-6">
     												<input type="text" value="<?php echo $sel_found[$key]->found_price;?>" name="location<?php echo $count;?>_price" id="location<?php echo $count;?>_price" placeholder="price" class="form-control form-control-md">
												</div>
												<div class="form-group col-sm-6">
												     <input size="16" type="text" value="<?php echo date("m/d/Y", strtotime($sel_found[$key]->expiry_date));?>" readonly id="expiry_date<?php echo $count;?>" name="expiry_date<?php echo $count;?>" class="form-control form-control-md" placeholder="Expire Date">
												</div>
												<div class="form-group col-sm-6">
												     <input type="text" value="<?php echo $sel_found[$key]->qty;?>" name="qty<?php echo $count;?>" id="qty<?php echo $count;?>" placeholder="Quantity" class="form-control form-control-md">
												</div>
												<input type="hidden" name="found_id<?php echo $count;?>" id="found_id<?php echo $count;?>" value="<?php echo $sel_found[$key]->id;?>">
												<input type="hidden" name="found_latlong<?php echo $count;?>" id="found_latlong<?php echo $count;?>" value="<?php echo $sel_found[$key]->found_latitude."#".$sel_found[$key]->found_longitude;?>" />
											</div>
											<div class="clearfix"></div>
											<hr class="line1"/>
									</div>  
								   	<script>
										jQuery(document).ready(function($) {
											//jQuery("#expire_date").datepicker();
											jQuery("#expiry_date<?php echo $count;?>").datepicker({
											 minDate: new Date(<?php echo date("Y, m, d");?>)
											});
										});									    
									</script>
									<div style="clear:left;"></div>
									<?php
								$count++;
							}
						}
						?>
									<div id="more_location_cust_div">
								<?php
								for($lp=$count;$lp<=10;$lp++)
								{?>
									<div class="form-group" id="disp_location_div_<?php echo $lp;?>" style="display:none;">
									<label class="col-sm-3 hide text-left flip"><?php esc_html_e('Found Location '.$lp, 'classiera'); ?>:</label>
										<div class="inner-addon right-addon">
											<i class="form-icon right-form-icon hide fa"></i>
											<div class="form-group col-sm-6">
												<input type="text" onFocus="geolocate<?php echo $lp;?>()" onChange="getlatlong<?php echo $lp;?>(this.value, 0);" value="" name="found_location<?php echo $lp;?>" id="found_location<?php echo $lp;?>" class="form-control form-control-md" placeholder="location <?php echo $lp;?>"/>
											</div>	
											<div class="form-group col-sm-6">
												<input type="text" value="" class="form-control form-control-md" name="location<?php echo $lp;?>_price" id="location<?php echo $lp;?>_price" placeholder="price">
											</div> 
											<div class="form-group col-sm-6">											
												<input size="16" type="text" value="" readonly id="expiry_date<?php echo $lp;?>" name="expiry_date<?php echo $lp;?>" class="form-control form-control-md" placeholder="Expire Date">
											</div>
											<div class="form-group col-sm-6">											
												<input type="text" value="" name="qty<?php echo $lp;?>" id="qty<?php echo $lp;?>" placeholder="Quantity" class="form-control form-control-md">
												<input type="hidden" name="found_latlong<?php echo $lp;?>" id="found_latlong<?php echo $lp;?>" value="" />
										    </div>
										</div>
									</div>
									  <script>
										jQuery(document).ready(function($) {
											//jQuery("#expire_date").datepicker();
											jQuery("#expiry_date<?php echo $lp;?>").datepicker({
											 minDate: new Date(<?php echo date("Y, m, d");?>)
											});
										});									    
									</script>
									<?php
									}?>
								</div>
								</div>
								<?php /*<div style="clear:left;"></div>
								<div class="form-group" style="margin-top:20px;">
									<label class="col-sm-3 text-left flip"><?php esc_html_e('Expire date', 'classiera'); ?>: <span>*</span></label>
									<div class="col-sm-6">
										<div class="inner-addon right-addon">
											<input size="16" type="text" value="" readonly id="expire_date5" name="expire_date5" class="form-control form-control-md">
										<span class="add-on"><i class="icon-th"></i></span>
										</div>
									</div>
										
									</div>   */?>  
							   
								
								
								
								<script type="text/javascript" language="javascript">
								var nextdisploc='<?php echo $count; ?>';
								//alert(nextdisploc);
								function add_location_func()
								{
									jQuery("#disp_location_div_"+nextdisploc).show();
									nextdisploc++;
								}
								</script>
								<div class="form-group col-md-6 col-md-offset-3">
						            <hr class="line1 visible-xs"/>
									<div class="form-group text-center mt-15">
										<button type="button" class="update_btn sharp btn-sm btn-style-one bg-black" onclick="add_location_func()"><img src="<?=get_template_directory_uri().'/../classiera-child/images/webcam.png'?>" height="30px" width="30px"/>&nbsp;<span>add location</span></button>
									</div>	
								</div>
								
							 </div>
						     <hr class="line1"/>
							 <div class="top-buffer2"></div>
						</div>
						
						<script> var placeSearch, autocomplete1;
						initAutocomplete();
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete1 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location1')),
            {types: ['geocode']});
			
		 autocomplete2 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location2')),
            {types: ['geocode']});
		 autocomplete3 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location3')),
            {types: ['geocode']});
			 autocomplete4 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location4')),
            {types: ['geocode']});
			 autocomplete5 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location5')),
            {types: ['geocode']});
			 autocomplete6 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location6')),
            {types: ['geocode']});
			 autocomplete7 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location7')),
            {types: ['geocode']});
			 autocomplete8 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location8')),
            {types: ['geocode']});
			 autocomplete9 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location9')),
            {types: ['geocode']});
			 autocomplete10 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('found_location10')),
            {types: ['geocode']});
		

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        //autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete1.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
	  
	  
	  function initialize() {
        var input = document.getElementById('found_location1');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            //document.getElementById('city2').value = place.name;
            document.getElementById('found_latitude1').value = place.geometry.location.lat();
            document.getElementById('found_longitude1').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
           // alert(place.address_components[0].long_name);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize); 
	
	
      function geolocate1() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			
			//jQuery("#found_latitude1").val(position.coords.latitude);
			//jQuery("#found_longitude1").val(position.coords.longitude);
											
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete1.setBounds(circle.getBounds());
          });
        }
      }
	  
	   function getlatlong1(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong1($("#found_location1").val(), 1);
						}
						else
						{
							
								if(data=='')
								{
									 getlatlong1($("#found_location1").val(), 1);
								}
								else
								{
								 $("#found_latlong1").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   
	   function getlatlong2(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong2($("#found_location2").val(), 1);
						}
						else
						{
							if(data=='')
								{
									 getlatlong2($("#found_location2").val(), 1);
								}
								else
								{
							
								 $("#found_latlong2").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   
	   function getlatlong3(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong3($("#found_location3").val(), 1);
						}
						else
						{
							
							if(data=='')
								{
									 getlatlong3($("#found_location3").val(), 1);
								}
								else
								{
								 $("#found_latlong3").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   
	   function getlatlong4(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong4($("#found_location4").val(), 1);
						}
						else
						{
							if(data=='')
								{
									 getlatlong4($("#found_location4").val(), 1);
								}
								else
								{
							 
								 $("#found_latlong4").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   
	   function getlatlong5(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong5($("#found_location5").val(), 1);
						}
						else
						{
							if(data=='')
								{
									 getlatlong5($("#found_location5").val(), 1);
								}
								else
								{
							 
								 $("#found_latlong5").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   
	   function getlatlong6(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong6($("#found_location6").val(), 1);
						}
						else
						{
							if(data=='')
								{
									 getlatlong6($("#found_location6").val(), 1);
								}
								else
								{
							 
								 $("#found_latlong6").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   function getlatlong7(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong7($("#found_location7").val(), 1);
						}
						else
						{
							if(data=='')
								{
									 getlatlong7($("#found_location7").val(), 1);
								}
								else
								{
							 
								 $("#found_latlong7").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   
	   function getlatlong8(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong8($("#found_location8").val(), 1);
						}
						else
						{
							if(data=='')
								{
									 getlatlong8($("#found_location8").val(), 1);
								}
								else
								{
							 
								 $("#found_latlong8").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   function getlatlong9(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong9($("#found_location9").val(), 1);
						}
						else
						{
							if(data=='')
								{
									 getlatlong9($("#found_location9").val(), 1);
								}
								else
								{
							 
								 $("#found_latlong9").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	   function getlatlong10(val, flg) {
			//alert(val);
			//alert(jQuery("#found_location1").val())
				$.ajax({
					url: '<?php echo site_url()?>/get_latlong_from_address.php',
					
					type: 'POST',
					// This is query string i.e. country_id=123
					data: {address : val},
					success: function(data) {
						if(flg==0)
						{
							 getlatlong10($("#found_location10").val(), 1);
						}
						else
						{
							if(data=='')
								{
									 getlatlong10($("#found_location10").val(), 1);
								}
								else
								{
							 
								 $("#found_latlong10").val(data);
								}
						}
					
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
			
		
	   }
	    function geolocate2() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude2").val(position.coords.latitude);
			jQuery("#found_longitude2").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete2.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate3() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude3").val(position.coords.latitude);
			jQuery("#found_longitude3").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete3.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate4() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude4").val(position.coords.latitude);
			jQuery("#found_longitude4").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete4.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate5() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude5").val(position.coords.latitude);
			jQuery("#found_longitude5").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete5.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate6() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude6").val(position.coords.latitude);
			jQuery("#found_longitude6").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete6.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate7() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude7").val(position.coords.latitude);
			jQuery("#found_longitude7").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete7.setBounds(circle.getBounds());
          });
        }
      }
	  function geolocate8() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude8").val(position.coords.latitude);
			jQuery("#found_longitude8").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete8.setBounds(circle.getBounds());
          });
        }
      }
	   function geolocate9() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude9").val(position.coords.latitude);
			jQuery("#found_longitude9").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete9.setBounds(circle.getBounds());
          });
        }
      }
	  function geolocate10() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			jQuery("#found_latitude10").val(position.coords.latitude);
			jQuery("#found_longitude10").val(position.coords.longitude);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete10.setBounds(circle.getBounds());
          });
        }
      }
    </script>	
						<?php } ?>
						
						<style>
					 #jquery-script-menu {
					position: fixed;
					height: 90px;
					width: 100%;
					top: 0;
					left: 0;
					border-top: 5px solid #316594;
					background: #fff;
					-moz-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					-webkit-box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					box-shadow: 0 2px 3px 0px rgba(0, 0, 0, 0.16);
					z-index: 999999;
					padding: 10px 0;
					-webkit-box-sizing:content-box;
					-moz-box-sizing:content-box;
					box-sizing:content-box;
					}
					
					.jquery-script-center {
					width: 960px;
					margin: 0 auto;
					}
					.jquery-script-center ul {
					width: 212px;
					float:left;
					line-height:45px;
					margin:0;
					padding:0;
					list-style:none;
					}
					.jquery-script-center a {
						text-decoration:none;
					}
					.jquery-script-ads {
					width: 728px;
					height:90px;
					float:right;
					}
					.jquery-script-clear {
					clear:both;
					height:0;
					}
					table tr{background:#ffffff !important;}
					 </style>
					 <?php //echo 'line 60'?>
					<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/bootstrap.min.css">
					<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
					 <?php //echo 'line 63';?>
					<link href="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/dist/bootstrap-FileUpload.css" rel="stylesheet" type="text/css">
					<div>
						
						<?php /*<div class="fileupload-wrapper"><div id="myUpload"></div></div>*/?>
					  </div>
					 
					 <?php /* <script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/jquery-1.12.4.min.js" ></script>*/?>
					  <script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/bootstrap.min.js"></script>
					<script src="<?php echo get_template_directory_uri();?>/File-Upload-Plugin-Bootstrap-jQuery/dist/bootstrap-FileUpload.js"></script>
					<script>
					  jQuery("#myUpload").bootstrapFileUpload({
					  url: "<?php echo site_url();?>/create-inventory/"
					});
					
					jQuery("#title").change(function() {
					  jQuery("#hdn_title").val(jQuery("#title").val());
					});
					jQuery("#description").change(function() {
					  jQuery("#hdn_description").val(jQuery("#description").val());
					});
					jQuery("#post_tags").change(function() {
					  jQuery("#hdn_post_tags").val(jQuery("#post_tags").val());
					});
					jQuery("#post_price").change(function() {
					  jQuery("#hdn_post_price").val(jQuery("#post_price").val());
					});
					jQuery("#address").change(function() {
					  jQuery("#hdn_address").val(jQuery("#address").val());
					});
					jQuery("#latitude").change(function() {
					  jQuery("#hdn_latitude").val(jQuery("#latitude").val());
					});
					jQuery("#longitude").change(function() {
					  jQuery("#hdn_longitude").val(jQuery("#longitude").val());
					});
					jQuery("#found_location1").change(function() {
					 jQuery("#hdn_found_location1").val(jQuery("#found_location1").val());
					})
					jQuery("#found_location2").change(function() {
					 jQuery("#hdn_found_location2").val(jQuery("#found_location2").val());
					})
					jQuery("#found_location3").change(function() {
					 jQuery("#hdn_found_location3").val(jQuery("#found_location3").val());
					})
					jQuery("#found_location4").change(function() {
					 jQuery("#hdn_found_location4").val(jQuery("#found_location4").val());
					})
					jQuery("#found_location5").change(function() {
					 jQuery("#hdn_found_location5").val(jQuery("#found_location5").val());
					});
					
					
					jQuery("#found_location6").change(function() {
					 jQuery("#hdn_found_location6").val(jQuery("#found_location6").val());
					})
					jQuery("#found_location7").change(function() {
					 jQuery("#hdn_found_location7").val(jQuery("#found_location7").val());
					})
					jQuery("#found_location8").change(function() {
					 jQuery("#hdn_found_location8").val(jQuery("#found_location8").val());
					})
					jQuery("#found_location9").change(function() {
					 jQuery("#hdn_found_location9").val(jQuery("#found_location9").val());
					})
					jQuery("#found_location10").change(function() {
					 jQuery("#hdn_found_location10").val(jQuery("#found_location10").val());
					});
					
					
					
					jQuery("#hdn_location1_price").change(function() {
					 jQuery("#hdn_location1_price").val(jQuery("#location1_price").val());
					});
					jQuery("#hdn_location2_price").change(function() {
					 jQuery("#hdn_location2_price").val(jQuery("#location2_price").val());
					});
					jQuery("#hdn_location3_price").change(function() {
					 jQuery("#hdn_location3_price").val(jQuery("#location3_price").val());
					});
					jQuery("#hdn_location4_price").change(function() {
					 jQuery("#hdn_location4_price").val(jQuery("#location4_price").val());
					});
					jQuery("#hdn_location5_price").change(function() {
					 jQuery("#hdn_location5_price").val(jQuery("#location5_price").val());
					});
					
					jQuery("#hdn_location6_price").change(function() {
					 jQuery("#hdn_location6_price").val(jQuery("#location6_price").val());
					});
					jQuery("#hdn_location7_price").change(function() {
					 jQuery("#hdn_location7_price").val(jQuery("#location7_price").val());
					});
					jQuery("#hdn_location8_price").change(function() {
					 jQuery("#hdn_location8_price").val(jQuery("#location8_price").val());
					});
					jQuery("#hdn_location9_price").change(function() {
					 jQuery("#hdn_location9_price").val(jQuery("#location9_price").val());
					});
					jQuery("#hdn_location10_price").change(function() {
					 jQuery("#hdn_location10_price").val(jQuery("#location10_price").val());
					});
					
					jQuery("#found_latitude1").change(function() {
					 jQuery("#hdn_latitude1").val(jQuery("#found_latitude1").val());
					});
					jQuery("#found_longitude1").change(function() {
					 jQuery("#hdn_longitude1").val(jQuery("#found_longitude1").val());
					});
					
					jQuery("#found_latitude2").change(function() {
					 jQuery("#hdn_latitude2").val(jQuery("#found_latitude2").val());
					});
					jQuery("#found_longitude2").change(function() {
					 jQuery("#hdn_longitude2").val(jQuery("#found_longitude2").val());
					});
					
					jQuery("#found_latitude3").change(function() {
					 jQuery("#hdn_latitude3").val(jQuery("#found_latitude3").val());
					});
					jQuery("#found_longitude3").change(function() {
					 jQuery("#hdn_longitude3").val(jQuery("#found_longitude3").val());
					});
					
					jQuery("#found_latitude4").change(function() {
					 jQuery("#hdn_latitude4").val(jQuery("#found_latitude4").val());
					});
					jQuery("#found_longitude4").change(function() {
					 jQuery("#hdn_longitude4").val(jQuery("#found_longitude4").val());
					});
					
					jQuery("#found_latitude5").change(function() {
					 jQuery("#hdn_latitude5").val(jQuery("#found_latitude5").val());
					});
					jQuery("#found_longitude5").change(function() {
					 jQuery("#hdn_longitude5").val(jQuery("#found_longitude5").val());
					});
					
					
					
					jQuery("#found_latitude6").change(function() {
					 jQuery("#hdn_latitude6").val(jQuery("#found_latitude6").val());
					});
					jQuery("#found_longitude6").change(function() {
					 jQuery("#hdn_longitude6").val(jQuery("#found_longitude6").val());
					});
					
					jQuery("#found_latitude7").change(function() {
					 jQuery("#hdn_latitude7").val(jQuery("#found_latitude7").val());
					});
					jQuery("#found_longitude7").change(function() {
					 jQuery("#hdn_longitude7").val(jQuery("#found_longitude7").val());
					});
					
					jQuery("#found_latitude8").change(function() {
					 jQuery("#hdn_latitude8").val(jQuery("#found_latitude8").val());
					});
					jQuery("#found_longitude8").change(function() {
					 jQuery("#hdn_longitude8").val(jQuery("#found_longitude8").val());
					});
					
					jQuery("#found_latitude9").change(function() {
					 jQuery("#hdn_latitude9").val(jQuery("#found_latitude9").val());
					});
					jQuery("#found_longitude9").change(function() {
					 jQuery("#hdn_longitude9").val(jQuery("#found_longitude9").val());
					});
					
					jQuery("#found_latitude10").change(function() {
					 jQuery("#hdn_latitude10").val(jQuery("#found_latitude10").val());
					});
					jQuery("#found_longitude10").change(function() {
					 jQuery("#hdn_longitude10").val(jQuery("#found_longitude10").val());
					});
					
					
					
					
					
					
					
					
					
					
						
						
					</script>
						
						
						<!-- post location -->
						<!-- seller information without login-->
						<?php if( !is_user_logged_in()){?>
						<div class="form-main-section seller">
                            <h4 class="text-uppercase border-bottom"><?php esc_html_e('Seller Information', 'classiera') ?> :</h4>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Are', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <input id="individual" type="radio" name="seller" checked>
                                        <label for="individual"><?php esc_html_e('Individual', 'classiera') ?></label>
                                        <input id="dealer" type="radio" name="seller">
                                        <label for="dealer"><?php esc_html_e('Dealer', 'classiera') ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Name', 'classiera') ?>: <span>*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" name="user_name" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter Your Name', 'classiera') ?>">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Email', 'classiera') ?> : <span>*</span></label>
                                <div class="col-sm-6">
                                    <input type="email" name="user_email" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter your email', 'classiera') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 text-left flip"><?php esc_html_e('Your Phone or Mobile No', 'classiera') ?> :<span>*</span></label>
                                <div class="col-sm-6">
                                    <input type="tel" name="user_phone" class="form-control form-control-md" placeholder="<?php esc_html_e('Enter your Mobile or Phone number', 'classiera') ?>">
                                </div>
                            </div>
                        </div>
						<?php }?>
						<!-- seller information without login -->
						<!--Select Ads Type-->
						<?php 
						$totalAds = '';
						$usedAds = '';
						$availableADS = '';
						$planCount = 0;						
						$regular_ads = $redux_demo['regular-ads'];
						$classieraRegularAdsDays = $redux_demo['ad_expiry'];
						$current_user = wp_get_current_user();
						$userID = $current_user->ID;
						$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
						?>
						<div class="form-main-section post-type" style="display:none;">
                            <h4 class="text-uppercase border-bottom"><?php esc_html_e('Select HYST Post Type', 'classiera') ?> :</h4>
                            <p class="help-block"><?php esc_html_e('Select an Option to make your  featured or regular', 'classiera') ?></p>
                            <div class="form-group">
							<!--Regular Ad with plans-->
							<?php							
							if($postLimitOn == true && $countPosts >= $regularCount && $currentRole != "administrator"){
								if(!empty($result)){
									$count = 0;
									foreach( $result as $info ){
										$totalRegularAds = $info->regular_ads;
										$usedRegularAds = $info->regular_used;
										$regularID = $info->id;
										$availableRegularADS = $totalRegularAds-$usedRegularAds;
										$planName = $info->plan_name;
										if($availableRegularADS != 0){
											?>
											<div class="col-sm-4 col-md-3 col-lg-3">
												<div class="post-type-box">
													<h3 class="text-uppercase"><?php esc_html_e('Regular with ', 'classiera') ?><?php echo $planName; ?></h3>
													<p>
													<?php esc_html_e('Available Regular ads ', 'classiera') ?> :
													<?php echo $availableRegularADS; ?>
													</p>
													<p>
													<?php esc_html_e('Used Regular ads', 'classiera') ?> : 
													<?php echo $usedRegularAds; ?>
													</p>
													<div class="radio">
														<input id="regularPlan<?php echo $regularID; ?>" class="classieraGetID" type="radio" name="classiera_post_type" value="classiera_regular_with_plan" data-regular-id="<?php echo $info->id; ?>">
														<label for="regularPlan<?php echo $regularID; ?>"><?php esc_html_e('Select', 'classiera') ?></label>
													</div><!--radio-->
												</div><!--post-type-box-->
											</div><!--col-sm-4-->
											<?php
										}
									}
								}
							}else{
								if($regular_ads == 1){
								?>
								<!--Regular Ad-->
								<div class="col-sm-4 col-md-3 col-lg-3 active-post-type">
                                    <div class="post-type-box">
                                        <h3 class="text-uppercase"><?php esc_html_e('Regular', 'classiera') ?></h3>
                                        <p><?php esc_html_e('For', 'classiera') ?>&nbsp;<?php echo $classieraRegularAdsDays; ?>&nbsp;<?php esc_html_e('days', 'classiera') ?></p>
                                        <div class="radio">
                                            <input id="regular" type="radio" name="classiera_post_type" value="classiera_regular" checked>
                                            <label for="regular"><?php esc_html_e('Select', 'classiera') ?></label>
                                        </div>
										<input type="hidden" name="regular-ads-enable" id="regular-ads-enable" value=""  >
                                    </div>
                                </div>
								<!--Regular Ad-->
								<?php
								}
							}
							?>
							<!--Regular Ad with plans-->
							<?php
								if(!empty($result)){
									foreach ( $result as $info ) {
										//print_r($info);
										$premiumID = $info->id;
										$name = $info->plan_name;
										$totalAds = $info->ads;
										$usedAds = $info->used;
										if($totalAds == 'unlimited'){
											$name = esc_html__( 'Unlimited for Admin Only', 'classiera' );
											$availableADS = 'unlimited';
										}else{
											$availableADS = $totalAds-$usedAds;
										}
										
										if($availableADS != 0 || $totalAds == 'unlimited'){
										?>
											<div class="col-sm-4 col-md-3 col-lg-3">
												<div class="post-type-box">
													<h3 class="text-uppercase">
														<?php echo $name; ?>
													</h3>
													<p><?php esc_html_e('Total HYST Available', 'classiera') ?> : <?php echo $availableADS; ?></p>
													<p><?php esc_html_e('Used HYST with this Plan', 'classiera') ?> : <?php echo $usedAds; ?></p>
													<div class="radio">
														<input id="featured<?php echo $premiumID; ?>" type="radio" name="classiera_post_type" value="<?php echo $info->id; ?>">
														<label for="featured<?php echo $premiumID; ?>"><?php esc_html_e('Select', 'classiera') ?></label>
													</div>
												</div>
											</div>
										<?php
										}										
									}
								}
							?>	
								<!--Pay Per Post Per Category Base-->
								<div class="col-sm-4 col-md-3 col-lg-3 classieraPayPerPost">
									<div class="post-type-box">
										<h3 class="text-uppercase">
											<?php esc_html_e('Featured HYST', 'classiera') ?>
										</h3>	
										<p class="classieraPPP"></p>
										<div class="radio">
											<input id="payperpost" type="radio" name="classiera_post_type" value="payperpost">
											<label for="payperpost">
											<?php esc_html_e('select', 'classiera') ?>
											</label>
										</div>										
									</div>
								</div>
								<!--Pay Per Post Per Category Base-->
                            </div>
                        </div>
						<!--Select Ads Type-->
						<?php 
						$featured_plans = $redux_demo['featured_plans'];
						if(!empty($featured_plans)){
							if($featuredADS == "0" || empty($result)){
						?>
						<div class="row">
                            <div class="col-sm-9">
                                <div class="help-block terms-use">
                                    <?php esc_html_e('Currently you have no active plan for featured HYST. You must purchase a', 'classiera') ?> <strong><a href="<?php echo $featured_plans; ?>" target="_blank"><?php esc_html_e('Featured Pricing Plan', 'classiera') ?></a></strong> <?php esc_html_e('to be able to publish a Featured Ad.', 'classiera') ?>
                                </div>
                            </div>
                        </div>
						<?php }} ?>

						<div id="" class="mydropzone classiera-image-upload clearfix" data-maxfile="<?php echo $imageLimit; ?>">
							<!--PreviousImages-->
							<?php require_once get_template_directory() . '/inc/BFI_Thumb.php'; ?>
							<?php
							$imageCount = 0;
							$params = array( 'width' => 110, 'height' => 70, 'crop' => true );
							$imgargs = array(
								'post_parent' => $current_post,
								'post_status' => 'inherit',
								'post_type'   => 'attachment', 
								'post_mime_type'   => 'image', 
								'order' => 'ASC',
								'orderby' => 'menu_order ID',
							);
							$attachments = get_children($imgargs);
							//echo "<pre>";print_r($attachments);
							if($attachments){
							foreach($attachments as $att_id => $attachment){
									$attachment_ID = $attachment->ID;
									$full_img_url = wp_get_attachment_url($attachment->ID);
									$split_pos = strpos($full_img_url, 'wp-content');
									$split_len = (strlen($full_img_url) - $split_pos);
									$abs_img_url = substr($full_img_url, $split_pos, $split_len);
									$full_info = @getimagesize(ABSPATH.$abs_img_url);

									$path_parts = pathinfo($full_img_url);
							?>
								<div id="<?php echo $attachment_ID; ?>" class="edit-post-image-block custom-edit-sec">
									<div class="remove-edit-post-image fileupload-previewrow thumb">
										<!-- <span class="removeImage" attids="<?php echo $attachment_ID; ?>"><?php esc_html_e('Remove', 'classiera');?></span> -->
										<div class="col-lg-1"><img class="edit-post-image" src="<?php echo bfi_thumb( "$full_img_url", $params ) ?>" />
										</div>
										<div class="col-lg-4"><?php echo $path_parts['basename']; ?>
											
										</div>
										<div class="col-lg-5"><div class="progress-bar"></div></div>
										<div class="col-lg-2">
											<button type="button" class="removeImage btn btn-danger fileupload-remove" attids="<?php echo $attachment_ID; ?>" value="<?php echo $path_parts['filename']; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/remove_file.png' ?>" height="40px" width="40px"/>
												<span>Remove File</span>
											</button>
										</div>
										<!-- <input type="hidden" name="attids" class="attids" value="<?php echo $attachment_ID; ?>"> -->
									</div><!--remove-edit-post-image-->
								</div>
								<?php $imageCount++;?>
							<?php }?>
							<?php }?>
						</div>

						<div class="btn btn-success fileupload-add newuploads"><input name="uploadattachment[]" type="file" id="files" multiple="multiple"><img src="<?php echo site_url(); ?>/wp-content/themes/classiera-child/images/file.png" height="40px" width="40px">&nbsp;Add Files…</div>

						<script type="text/javascript">
							  if (window.File && window.FileList && window.FileReader) {
							    jQuery("#files").on("change", function(e) {
							      var files = e.target.files,
							        filesLength = files.length;
							      for (var i = 0; i < filesLength; i++) {
							        var f = files[i];
							        var fileReader = new FileReader();
							        fileReader.onload = (function(e) {
							          var file = e.target;
							          jQuery("<div class=\"edit-post-image-block custom-edit-sec onedit-imageview\"><div class=\"remove-edit-post-image fileupload-previewrow thumb\">" + "<div class=\"col-lg-1\"><img class=\"edit-post-image imageThumb\" src=\"" + e.target.result + "\" title=\"" + f.name + "\"/></div>" 
							          	+ "<div class=\"col-lg-4\">"+ f.name +"</div>"
							          	+ "<div class=\"col-lg-5\"><div class=\"progress-bar\"></div></div>"
							          	+ "<div class=\"col-lg-2\"><a class=\"btn btn-danger fileupload-remove\" href=\"javascript:void(0)\"><img src=\"<?=get_template_directory_uri().'/../classiera-child/images/remove_file.png' ?>\" height=\"40px\" width=\"40px\"/>" 
							          	+ "<span class=\"remove\">Remove File</span></a></div>" 
							          	+ "</div></div>").insertAfter(".mydropzone");
							          jQuery(".remove").click(function(){
							            jQuery(this).parent().parent().parent().parent(".custom-edit-sec").remove();
							          });
							          
							          // Old code here
							          /*$("<img></img>", {
							            class: "imageThumb",
							            src: e.target.result,
							            title: file.name + " | Click to remove"
							          }).insertAfter("#files").click(function(){$(this).remove();});*/
							          
							        });
							        fileReader.readAsDataURL(f);
							        //alert(f.name);
							      }
							    });
							  } else {
							    alert("Your browser doesn't support to File API")
							  }
						</script>
						
						<div class="form-main-section">
						    
							<div class="col-md-6 col-md-offset-3">
							    <div class="top-buffer2"></div>
							    <?php /*?><div class="form-group">
								     <input name="location_search" id="location_search" class="form-control form-control-md" placeholder=" search for your location" type="text">
							    </div><?php */?>
							</div>
							
							<div class="col-sm-12 buttons">
							        <input type="hidden" class="regular_plan_id" id="regular_plan_id" name="regular_plan_id" value="">
									<?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>
									<input type="hidden" name="submitted" id="submitted" value="true">
									<button class="post-submit" type="submit" name="updatepost" value="<?php esc_html_e('Update Ad', 'classiera') ?>" style="width:20%; margin:auto;"><img src="<?=get_template_directory_uri().'/../classiera-child/images/send.png' ?>" height="40px" width="40px"/><?php esc_html_e('Update HYST', 'classiera') ?></button>	
							</div>
                        
						
						</div>
						
						
						<div class="clearfix"></div>
						<div class="top-buffer1"></div>
						<hr class="line1"/>
						<div class="row">
                            <div class="col-sm-12">
                                <div class="help-block terms-use">
                                    <?php esc_html_e('By clicking "Publish ", you agree to our', 'classiera') ?> <a href="<?php echo $termsandcondition; ?>"><?php esc_html_e('Terms of Use', 'classiera') ?></a> <?php esc_html_e('', 'classiera') ?>
                                </div>
                            </div>
                        </div>
						<div class="clearfix"></div>
					</form>
					<script>
					function submit_form()
					{
						jQuery("#hdn_classiera-main-cat-field").val(jQuery("#classiera-main-cat-field").val());
						jQuery("#hdn_classiera-sub-cat-field").val(jQuery("#classiera-sub-cat-field").val());
						jQuery("#hdn_classiera_third_cat").val(jQuery("#classiera_third_cat").val());
						jQuery("#hdn_regular-ads-enable").val(jQuery("#regular-ads-enable").val());
						jQuery("#hdn_regular_plan_id").val(jQuery("#regular_plan_id").val());
						jQuery("#hdn_submitted").val(jQuery("#submitted").val());
						jQuery("#hdn_post_nonce_field").val(jQuery("#post_nonce_field").val());
						jQuery("#hdn_title").val(jQuery("#title").val());
						jQuery("#hdn_post_tags").val(jQuery("#post_tags").val());
						jQuery("#hdn_address").val(jQuery("#address").val());
						jQuery("#hdn_found_location1").val(jQuery("#found_location1").val());
						jQuery("#hdn_found_location2").val(jQuery("#found_location2").val());
						jQuery("#hdn_found_location3").val(jQuery("#found_location3").val());
						jQuery("#hdn_found_location4").val(jQuery("#found_location4").val());
						jQuery("#hdn_found_location5").val(jQuery("#found_location5").val());
						jQuery("#hdn_found_location6").val(jQuery("#found_location6").val());
						jQuery("#hdn_found_location7").val(jQuery("#found_location7").val());
						jQuery("#hdn_found_location8").val(jQuery("#found_location8").val());
						jQuery("#hdn_found_location9").val(jQuery("#found_location9").val());
						jQuery("#hdn_found_location10").val(jQuery("#found_location10").val());
						
						jQuery("#hdn_location1_price").val(jQuery("#location1_price").val());
						jQuery("#hdn_location2_price").val(jQuery("#location2_price").val());
						jQuery("#hdn_location3_price").val(jQuery("#location3_price").val());
						jQuery("#hdn_location4_price").val(jQuery("#location4_price").val());
						jQuery("#hdn_location5_price").val(jQuery("#location5_price").val());
						jQuery("#hdn_location6_price").val(jQuery("#location6_price").val());
						jQuery("#hdn_location7_price").val(jQuery("#location7_price").val());
						jQuery("#hdn_location8_price").val(jQuery("#location8_price").val());
						jQuery("#hdn_location9_price").val(jQuery("#location9_price").val());
						jQuery("#hdn_location10_price").val(jQuery("#location10_price").val());
						
						jQuery("#hdn_expiry_date1").val(jQuery("#expiry_date1").val());
						jQuery("#hdn_expiry_date2").val(jQuery("#expiry_date2").val());
						jQuery("#hdn_expiry_date3").val(jQuery("#expiry_date3").val());
						jQuery("#hdn_expiry_date4").val(jQuery("#expiry_date4").val());
						jQuery("#hdn_expiry_date5").val(jQuery("#expiry_date5").val());
						jQuery("#hdn_expiry_date6").val(jQuery("#expiry_date6").val());
						jQuery("#hdn_expiry_date7").val(jQuery("#expiry_date7").val());
						jQuery("#hdn_expiry_date8").val(jQuery("#expiry_date8").val());
						jQuery("#hdn_expiry_date9").val(jQuery("#expiry_date9").val());
						jQuery("#hdn_expiry_date10").val(jQuery("#expiry_date10").val());
						
						jQuery("#hdn_qty1").val(jQuery("#qty1").val());
						jQuery("#hdn_qty2").val(jQuery("#qty2").val());
						jQuery("#hdn_qty3").val(jQuery("#qty3").val());
						jQuery("#hdn_qty4").val(jQuery("#qty4").val());
						jQuery("#hdn_qty5").val(jQuery("#qty5").val());
						
						jQuery("#hdn_qty6").val(jQuery("#qty6").val());
						jQuery("#hdn_qty7").val(jQuery("#qty7").val());
						jQuery("#hdn_qty8").val(jQuery("#qty8").val());
						jQuery("#hdn_qty9").val(jQuery("#qty9").val());
						jQuery("#hdn_qty10").val(jQuery("#qty10").val());
						
						
						jQuery("#hdn_found_latlong1").val(jQuery("#found_latlong1").val());
						jQuery("#hdn_found_latlong2").val(jQuery("#found_latlong2").val());
						jQuery("#hdn_found_latlong3").val(jQuery("#found_latlong3").val());
						jQuery("#hdn_found_latlong4").val(jQuery("#found_latlong4").val());
						jQuery("#hdn_found_latlong5").val(jQuery("#found_latlong5").val());
						
						jQuery("#hdn_found_latlong6").val(jQuery("#found_latlong6").val());
						jQuery("#hdn_found_latlong7").val(jQuery("#found_latlong7").val());
						jQuery("#hdn_found_latlong8").val(jQuery("#found_latlong8").val());
						jQuery("#hdn_found_latlong9").val(jQuery("#found_latlong9").val());
						jQuery("#hdn_found_latlong10").val(jQuery("#found_latlong10").val());
						
						
						
						
						
						  jQuery("#hdn_latitude").val(jQuery("#latitude").val());
						  jQuery("#hdn_longitude").val(jQuery("#longitude").val());
						jQuery("#frm_save_hyst").submit();
						
					}
					</script>
					</div></div>
				</div><!--submit-post-->
				<?php } ?>
			</div><!--col-lg-9 col-md-8 user-content-heigh-->
		</div><!--row-->
	</div><!--container-->
</section><!--user-pages-->
<?php endwhile; ?>
<?php get_footer(); ?>
<script>

jQuery('.removeImage').click(function() {
var idd=jQuery(this).parents(".custom-edit-sec");
var currrentpost = '<?php echo $current_post ?>';
var attids = jQuery(this).attr('attids');

jQuery.ajax({
    type        : 'POST',
    url         : '<?php echo admin_url('admin-ajax.php'); ?>',
    data        : { action : 'custom_delete_post_attachment', postID: currrentpost, att_ids: attids },
    success     : function(response) {
    	    idd.remove();
        }
    });  
});
</script>