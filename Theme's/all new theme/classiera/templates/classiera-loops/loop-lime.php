<?php 
	global $redux_demo;
	$classieraIconsStyle = $redux_demo['classiera_cat_icon_img'];
	$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
	$classieraAdsView = $redux_demo['home-ads-view'];
	$classieraItemClass = "item-grid";
	if($classieraAdsView == 'list'){
		$classieraItemClass = "item-list";
	}
	$category_icon_code = "";
	$category_icon_color = "";
	$catIcon = "";
	global $post;
	$category = get_the_category();
	$catID = $category[0]->cat_ID;
	if ($category[0]->category_parent == 0) {
		$tag = $category[0]->cat_ID;
		$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
		if (isset($tag_extra_fields[$tag])) {
			$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
			$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
			$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
		}
	}elseif(isset($category[1]->category_parent) && $category[1]->category_parent == 0){
		$tag = $category[0]->category_parent;
		$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
		if (isset($tag_extra_fields[$tag])) {
			$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
			$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
			$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
		}
	}else{
		$tag = $category[0]->category_parent;
		$tag_extra_fields = get_option(MY_CATEGORY_FIELDS);
		if (isset($tag_extra_fields[$tag])) {
			$category_icon_code = $tag_extra_fields[$tag]['category_icon_code'];
			$category_icon_color = $tag_extra_fields[$tag]['category_icon_color'];
			$classieraCatIcoIMG = $tag_extra_fields[$tag]['your_image_url'];
		}
	}
	if(!empty($category_icon_code)) {
		$category_icon = stripslashes($category_icon_code);
	}							
	$post_price = get_post_meta($post->ID, 'post_price', true);
	$theTitle = get_the_title();
	$postCatgory = get_the_category( $post->ID );							
	$categoryLink = get_category_link($catID);
	$classiera_ads_type = get_post_meta($post->ID, 'classiera_ads_type', true);
	$post_currency_tag = get_post_meta($post->ID, 'post_currency_tag', true);
	
	$post_location = get_post_meta($post->ID, 'post_location', true);
	$post_state = get_post_meta($post->ID, 'post_state', true);
	$post_city = get_post_meta($post->ID, 'post_city', true);	
	$featured_post = get_post_meta($post->ID, 'featured_post', true);
	
	$is_competition= get_post_meta($post->ID, 'is_competition', true);
	
	$classieraPostAuthor = $post->post_author;
	$classieraAuthorEmail = get_the_author_meta('user_email', $classieraPostAuthor);
?>
<div class="col-lg-6 col-md-6 col-sm-6 match-height item <?php echo $classieraItemClass; ?>">
	<div class="classiera-box-div classiera-box-div-v1">
		<figure class="clearfix">
		<?php
		if($featured_post=='1')
		{?>
		    <div class="feature_image">
			
		         <img src="<?=get_template_directory_uri() . '/../classiera-child/images/featured.png' ?>"/>
			</div>	
			
			<div class="xs-feature_img visible-xs">
				 <img src="<?=get_template_directory_uri().'/../classiera-child/images/mob-nav-1.png' ?>" height="50px" width="50px" />
				 <div class="clearfix"></div>
				 <span class="label label-large label-grey arrowed-in-right arrowed-in">featured</span>
			</div>
			
		<?php
		}?> 
			<div class="premium-img">
			<?php 
			$classieraFeaturedPost = get_post_meta($post->ID, 'featured_post', true);
			?>
			<div style="display:none;">
			<?php
					if($classieraFeaturedPost == 1){
						?>
						<div class="featured-tag">
							<span class="left-corner"></span>
							<span class="right-corner"></span>
							<div class="featured">
								<p><?php esc_html_e( 'Featured', 'classiera' ); ?></p>
							</div>
						</div>
						<?php 
					}
					else if($is_competition==1)
					{?>
						<div class="featured-tag">
							<span class="left-corner"></span>
							<span class="right-corner"></span>
							<div class="featured">
								<p style="font-size:13px;">
								<?php
								$competition_type= get_post_meta($post->ID, 'compitition_type', true);
								//echo '==>';
								//print_r($competition_type);
								if($competition_type==1)
								{
									echo 'Promotion';
								}
								else if($competition_type==2)
								{
									echo 'Competition';
								}
								 ?></p>
							</div>
						</div>
						<?php
					
					} ?>
			</div>
			
		
			<?php
			if( has_post_thumbnail()){
				$imageurl = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'classiera-370');
				$thumb_id = get_post_thumbnail_id($post->ID);
				?>
				<a href="<?php the_permalink(); ?>" class="outline active"><img class="img-responsive" src="<?php echo $imageurl[0]; ?>" alt="<?php echo $theTitle; ?>"></a>
				<?php
			}else{
				?>
				<a href="<?php the_permalink(); ?>" class="outline active"><img class="img-responsive" src="<?php echo get_template_directory_uri() . '/images/nothumb.png' ?>" alt="No Thumb"/></a>
				<?php
			}
			?>
				<span class="hover-posts">
					<a href="<?php the_permalink(); ?>" class="outline active"><?php esc_html_e( 'view HYST', 'classiera' ); ?></a>
				</span>
				<?php if(!empty($classiera_ads_type)){?>
				<span class="classiera-buy-sel">
					<?php classiera_buy_sell($classiera_ads_type); ?>
				</span>
				<?php } ?>
				
				
				
			</div><!--premium-img-->
			
			
			<figcaption class="pad0 text-left">
			    <div class="product-body pad15">
					<h3 class="media-heading poppins-lite"><a href="<?php the_permalink(); ?>" class="poppins-lite" style="font-weight: 500;"><?php echo $theTitle; ?></a></h5>
					
					<a href="<?php the_permalink(); ?>"><h5 class="price media-heading">
					<?php if(!empty($post_price)){?>
					     Price :
						<span class="classiera-price-tag" style="background-color:<?php echo $category_icon_color; ?>; color:<?php echo $category_icon_color; ?>;">
								<?php 
								if(is_numeric($post_price)){
									echo classiera_post_price_display($post_currency_tag, $post_price);
								}else{ 
									echo $post_price; 
								}
								?>
						</span>
					<?php } ?>	
					</h5></a>
					
					<a href="<?php the_permalink(); ?>" ><h5 class="media-heading"><?php esc_html_e( 'Category', 'classiera' ); ?> : 
						<?php /*<a href="<?php echo $categoryLink; ?>"><?php echo $postCatgory[0]->name; ?></a>*/?>
						<?php echo $postCatgory[0]->name; ?>
					</h5></a>
					<a href="<?php the_permalink(); ?>" >
					<span class="category-icon-box" style=" background:<?php echo $category_icon_color; ?>; color:<?php echo $category_icon_color; ?>;display:none;">
						<?php 
						if($classieraIconsStyle == 'icon'){
							?>
							<i class="<?php echo $category_icon_code;?>"></i>
							<?php
						}elseif($classieraIconsStyle == 'img'){
							?>
							<img src="<?php echo $classieraCatIcoIMG; ?>" alt="<?php echo $postCatgory[0]->name; ?>">
							<?php
						}
						?>
					</span>
					</a>
					<div class="top-buffer1"></div>
					<p class="justify pad0 black" style="min-height:100px;">
						<?php echo substr(get_the_excerpt(), 0,260); ?><a href="<?php the_permalink(); ?>" class="expend_icon"><img src="<?=get_template_directory_uri().'/../classiera-child/images/down_arrow.png' ?>"/></a>
					</p>
					<div class="post-tags">
						<span><i class="fa fa-tags"></i>
						<?php esc_html_e('Tags', 'classiera'); ?>&nbsp; :
						</span>
						<?php the_tags('','',''); ?>
					</div><!--post-tags-->
					<div class="top-buffer1">
					<h3 class="user_details"><?php /*<img src="<?=get_template_directory_uri().'/../classiera-child/images/user.png' ?>"/> user : jane doe*/?>
					<?php
					$author_id=$post->post_author; 
					$profileIMGID = get_user_meta($author_id, "classify_author_avatar_url", true);
									$profileIMG = classiera_get_profile_img($profileIMGID);
									$authorName = get_the_author_meta('display_name', $author_id);
									$author_verified = get_the_author_meta('author_verified', $author_id);
									//$authoremail=the_author_meta( 'email' , $author_id );
									if ( is_user_logged_in() ) {
									$is_approve_prof_pic=get_user_meta($author_id,'profile_pic_approve');
									//echo '$is_approve_prof_pic:'.$is_approve_prof_pic;
									if($is_approve_prof_pic[0]=='1')
									{
										if(empty($profileIMG)){
										$profileIMG = classiera_get_avatar_url ( get_the_author_meta('user_email', $author_id), $size = '50' );
										?>
									<a href="<?php echo get_author_posts_url( $author_id, get_the_author_meta( 'user_nicename' ) ); ?>"><img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="float:left;"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;"> user: <?php echo $authorName;?></span>	</a>
									<?php
									
									}else{ ?>
									<a href="<?php echo get_author_posts_url($author_id, get_the_author_meta( 'user_nicename' ) ); ?>"><img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="float:left;"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;">  user: <?php echo $authorName;?></span></a>	
									<?php }
									}
									else
									{
										?><a href="<?php echo get_author_posts_url($author_id, get_the_author_meta( 'user_nicename' ) ); ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/user.png' ?>"/><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;">  user: <?php echo $authorName;?></span></a><?php
									}
									}
									else
									{
										$is_approve_prof_pic=get_user_meta($author_id,'profile_pic_approve');
									if($is_approve_prof_pic[0]=='1')
									{
										if(empty($profileIMG)){
										$profileIMG = classiera_get_avatar_url ( get_the_author_meta('user_email', $author_id), $size = '50' );
										?>
									<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="float:left;"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;"> user: <?php echo $authorName;?></span>
									<?php
									}else{ ?>
									<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="float:left;"><span style="font-family: !Poppins-ExtraLight important;
font-size: 16px !important;
color: #fff !important;">  user: <?php echo $authorName;?></span>	
									<?php }
									}
									else
									{?><img src="<?=get_template_directory_uri().'/../classiera-child/images/user.png' ?>"/>  user: <?php echo $authorName;?><?php
									}
									} ?></h3>
									<div style="clear:left;"></div>
					<div class="top-buffer1">
					
				</div>	
			</figcaption>
		</figure>
	</div><!--classiera-box-div-->
</div><!--col-lg-4-->