<?php
/*
Plugin Name: Interest Management

*/

// Register settings using the Settings API 
 
// Modify capability
/**
 * Register a custom menu page.
 */
function wpdocs_register_my_custom_menu_page(){
    add_menu_page( 
        __( 'Interest', 'textdomain' ),
        'Interest',
        'manage_options',
        'Interest',
        'my_custom_menu_page',
        '',
        6
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
 
/**
 * Display a custom menu page
 */
function my_custom_menu_page(){
	$error='';
	global $wpdb;
	if((isset($_GET['action']) && $_GET['action']=='remove') && (isset($_GET['remove_id']) && $_GET['remove_id']!='' && is_numeric($_GET['remove_id'])))
	{
		$wpdb->query("DELETE FROM wp_interest WHERE id='".$_GET['remove_id']."'");
		header("location: admin.php?page=Interest&msg=3");
	}
 	if(isset($_POST['btn_submit']) && $_POST['btn_submit']=='Save')
	{
		if(isset($_POST['interest']) && $_POST['interest']=='')
		{
			$error='Please insert interest name';
		}
		if($error=='')
		{
			if(isset($_POST['action']) && $_POST['action']=='insert')
			{
				$wpdb->query("INSERT INTO wp_interest (`name`) VALUES ('".$_POST['interest']."')");
				header("location: admin.php?page=Interest&msg=1");
			}
			else if(isset($_POST['action']) && $_POST['action']=='edit')
			{
				$wpdb->query("UPDATE wp_interest SET `name`='".$_POST['interest']."' WHERE id='".$_POST['edit_id']."'"  );
				header("location: admin.php?page=Interest&msg=2");
			}
			
		}
	}
	echo '<br/>';
	if($error!='')
	{
		echo '<span style="color:red;">'.$error.'</span>';
	}  
	else if(isset($_GET['msg']) && $_GET['msg']=='1')
	{
		echo '<span style="color:green;">Interest inserted successfully</span>';
	}
	else if(isset($_GET['msg']) && $_GET['msg']=='2')
	{
		echo '<span style="color:green;">Interest updated successfully</span>';
	}
	else if(isset($_GET['msg']) && $_GET['msg']=='3')
	{
		echo '<span style="color:green;">Interest removed successfully</span>';
	}
	$interest_name='';
	if((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') && (isset($_REQUEST['edit_id']) && $_REQUEST['edit_id']!='' && is_numeric($_REQUEST['edit_id'])))
	{
		$sel= $wpdb->get_results( "SELECT * FROM wp_interest WHERE id='".$_REQUEST['edit_id']."'" );
		$interest_name=$sel[0]->name;
	}
   ?>
  <h2> Interest Management</h2>
  <form name="frm_interest" action="" method="post" id="frm_interest">
	 <?php /* <table>
		<tr>
			<td>Interest Name</td>
			<td><input type="text" name="interest" value="<?php echo $interest_name;?>" id="interest"></td>
		</tr>
		<tr>
			<td colspan="2">
				<?php
				if((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') && (isset($_REQUEST['edit_id']) && $_REQUEST['edit_id']!='' && is_numeric($_REQUEST['edit_id'])))
				{
					
					?>
						<input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>">
						<input type="hidden" name="edit_id" value="<?php echo $_REQUEST['edit_id'];?>">
					<?php
				}
				else
				{?>
						<input type="hidden" name="action" value="insert"><?php
				}
				?>
				<input type="submit" name="btn_submit" value="Save">
			</td>
		</tr>
	  </table>
  */?>
  <div id="titlediv">
	<div id="titlewrap">
			<?php /*<label class="" id="title-prompt-text" for="title">Enter interest here</label>*/?>
		
	<input name="interest" size="30" value="<?php echo $interest_name;?>" id="interest" spellcheck="true" autocomplete="off" type="text" style="width:40%; padding:15px;">
	</div>
	<div class="inside">
		<div id="edit-slug-box" class="hide-if-no-js">
			</div>
	</div>
	<?php
				if((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') && (isset($_REQUEST['edit_id']) && $_REQUEST['edit_id']!='' && is_numeric($_REQUEST['edit_id'])))
				{
					
					?>
						<input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>">
						<input type="hidden" name="edit_id" value="<?php echo $_REQUEST['edit_id'];?>">
					<?php
				}
				else
				{?>
						<input type="hidden" name="action" value="insert"><?php
				}
				?>
	<input type="submit" name="btn_submit" value="Save" class="button button-primary button-large">
	</div>
  </form>
   <?php 
   
   $results = $wpdb->get_results( "SELECT * FROM wp_interest order by id desc" );
   if(count($results)>0)
   {
   //	$count = 0;
   	?>
		<style>
		/*.list{border:2px solid #fff; margin-top:20px;}
		table tr{   
		    display: table-row;
    vertical-align: inherit;
    border-collapse: separate;
    border-spacing: 2px;
    border-color: grey;}
		table th{background:#fff; vertical-align:middle; padding:5px 5px; text-align:left;}
		.odd{background:#eee;border:1px solid #fff;}
		.even{background:#fff;border:1px solid #eee;}
		table td{padding:2px 5px;}*/
		</style>
		 <link href="<?php echo plugins_url();?>/graphical_analysis/plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />

        <!-- App css -->
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/responsive.css" rel="stylesheet" type="text/css" />
		 <div class="row" style="margin-top:20px;">
		<div class="col-lg-6">
			<div class="card-box">
				<h4 class="m-t-0 header-title"><b>LIST TO APPROVE</b></h4>
				<div class="table-responsive">
		<table class="table table-hover m-0 mails table-actions-bar">
			<tr><th width="100">Id</th><th width="200">Name</th><th>Action</th></tr>
			<?php
			foreach($results as $key=>$val)
			{?>
				<tr class="<?php echo ++$count%2 ? "odd" : "even";?>">
					<td><h5 class="m-b-0 m-t-0"><?php echo $val->id;?></h5></td>
					<td><h5 class="m-b-0 m-t-0"><?php echo $val->name;?></h5></td></td>
					<td><a href="admin.php?page=Interest&action=edit&edit_id=<?php echo $val->id;?>" class="table-action-btn"><i class="mdi mdi-pencil"></i></a><a href="admin.php?page=Interest&action=remove&remove_id=<?php echo $val->id;?>" class="table-action-btn"><i class="mdi mdi-delete"></i></a></td>
				</tr>
			<?php
			}?>
		</table>
		</div>
		</div>
		</div>
		</div>
	<?php
   }
   
   //print_r($results);
}
?>