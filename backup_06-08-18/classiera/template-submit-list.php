<?php
/**
 * Template name: Submit Lists
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage classiera
 * @since classiera 1.0
 */

if ( !is_user_logged_in() ) {
	global $redux_demo; 
	$login = $redux_demo['login'];
	wp_redirect( $login ); exit;
}
global $wpdb;
$error=array();
$flg=0;
//print_r($_POST);
if(isset($_POST['btn_save']) && $_POST['btn_save']!='')
{
	if((isset($_POST['action']) && $_POST['action']=='edit') && (isset($_POST['edit_id']) && $_POST['edit_id']!=''))
	{
		if(isset($_POST['list_title']) && $_POST['list_title']=='')
		{
			$error[]='Please eneter list title';
			$flg=1;
		}
		//print_r($error);
		if($flg==0)
		{
			if(isset($_POST['is_public']) && $_POST['is_public']=='1')
				$wpdb->query("update wp_user_lists set list_title='".$_POST['list_title']."', is_public='".$_POST['is_public']."' where id='".$_POST['edit_id']."'");
			else
				$wpdb->query("update wp_user_lists set list_title='".$_POST['list_title']."', is_public='0' where id='".$_POST['edit_id']."'");
		}
	}
	else
	{
		if(isset($_POST['list_title']) && $_POST['list_title']=='')
		{
			$error[]='Please eneter list title';
			$flg=1;
		}
		//print_r($error);
		if($flg==0)
		{
			if(isset($_POST['is_public']) && $_POST['is_public']=='1')
				$wpdb->query("insert into wp_user_lists(user_id, list_title, is_public) values('".$_POST['user_id']."', '".$_POST['list_title']."', '".$_POST['is_public']."')");
			else
				$wpdb->query("insert into wp_user_lists(user_id, list_title, is_public) values('".$_POST['user_id']."', '".$_POST['list_title']."', '0')");
		}
	}
}
if((isset($_GET['action']) && $_GET['action']=='remove') && (isset($_GET['rm_id']) && $_GET['rm_id']!='' && is_numeric($_GET['rm_id'])))
{
	$wpdb->query("delete from wp_user_lists where id='".$_GET['rm_id']."'");
}
$postTitleError = '';
$post_priceError = '';
$catError = '';
$featPlanMesage = '';
$postContent = '';
$hasError ='';
$allowed ='';
$caticoncolor="";
$classieraCatIconCode ="";
$category_icon="";
$category_icon_color="";
global $redux_demo;
$featuredADS = 0;
$primaryColor = $redux_demo['color-primary'];
$googleFieldsOn = $redux_demo['google-lat-long'];
$classieraLatitude = $redux_demo['contact-latitude'];
$classieraLongitude = $redux_demo['contact-longitude'];
$classieraAddress = $redux_demo['classiera_address_field_on'];
$postCurrency = $redux_demo['classierapostcurrency'];
$classieraIconsStyle = $redux_demo['classiera_cat_icon_img'];
$termsandcondition = $redux_demo['termsandcondition'];
$classieraProfileURL = $redux_demo['profile'];
$classiera_ads_typeOn = $redux_demo['classiera_ads_type'];
//print_r($_POST);
//echo '<br/>';
//echo date("Y-m-d", strtotime($_POST['expiry_date1']));exit;

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php 
	$page = get_page($post->ID);
	$current_page_id = $page->ID;
	$featuredUsed = '';
	$featuredAds = '';
	$regularUsed = '';
	$regularAds = '';
?>
<style>
.form-main-section .list-unstyled li a{padding: 8px 15px;
display: table;
text-align: left;text-align: center;
width: 100%;
height: 100%;
border-radius: 3px;moz-border-radius: 3px;webkit-border-radius: 3px;}
.list-unstyled li a:hover, .list-unstyled li a:hover i{background:#000; color:#fff !important;moz-border-radius: 3px;webkit-border-radius: 3px;}
.classieraSubReturn li{float:left; width:20%;}
</style>
<section class="user-pages section-gray-bg all_usertemplate">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12">
			   <div class="row">
				<?php get_template_part( 'templates/profile/userabout' );?>
			   </div>
			</div><!--col-lg-3-->
			
			<div class="clearfix">
			</div>
			
			
			<div class="col-lg-12 col-md-12 user-content-height">
				<?php 
				global $redux_demo;
				global $wpdb;
				$current_user = wp_get_current_user();
				$userID = $current_user->ID;			
				$featured_plans = $redux_demo['featured_plans'];
				$classieraRegularAdsOn = $redux_demo['regular-ads'];
				$postLimitOn = $redux_demo['regular-ads-posting-limit'];
				$regularCount = $redux_demo['regular-ads-user-limit'];
				$cUserCheck = current_user_can( 'administrator' );
				$role = $current_user->roles;
				$currentRole = $role[0];
				$classieraAllowPosts = false;
				$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}classiera_plans WHERE user_id = $userID ORDER BY id DESC" );
				foreach ($result as $info){											
					$featuredAdscheck = $info->ads;											
					if (is_numeric($featuredAdscheck)){
						$featuredAds += $info->ads;
						$featuredUsed += $info->used;
					}
					$regularAdscheck = $info->regular_ads;
					if (is_numeric($regularAdscheck)){
						$regularAds += $info->regular_ads;
						$regularUsed += $info->regular_used;
					}
				}
				if (is_numeric($featuredAds) && is_numeric($featuredUsed)){
					$featuredAvailable = $featuredAds-$featuredUsed;
				}
				if (is_numeric($regularAds) && is_numeric($regularUsed)){
					$regularAvailable = $regularAds-$regularUsed;
				}
				
				$curUserargs = array(					
					'author' => $user_ID,
					'post_status' => array('publish', 'pending', 'draft', 'private', 'trash')    
				);
				$countPosts = count(get_posts($curUserargs));
				if($currentRole == "administrator"){
					$classieraAllowPosts = true;
				}else{
					if($postLimitOn == true){
						if($regularAvailable == 0 && $featuredAvailable == 0 && $countPosts >= $regularCount){
							$classieraAllowPosts = false;
						}else{
							$classieraAllowPosts = true;
						}
					}else{
						$classieraAllowPosts = true;
					}
				}
				
				// You can set $user_id to any users, but this gets the current users ID.

    $classieraAuthorEmail = $current_user->user_email;
	$classieraAuthorIMG = get_user_meta($userID, "classify_author_avatar_url", true);
	$classieraAuthorIMG = classiera_get_profile_img($classieraAuthorIMG);
	if(empty($classieraAuthorIMG)){
		$classieraAuthorIMG = classiera_get_avatar_url ($classieraAuthorEmail, $size = '150' );
	}
				
				//echo $postLimitOn.' Limit<br />';
				//echo $regularAvailable.' regularAvailable<br />';
				//echo $featuredAvailable.' featuredAvailable<br />';
				//echo $countPosts.' countPosts<br />';
				//echo $regularCount.' regularCount<br />';
				//echo $classieraAllowPosts.' classieraAllowPosts<br />';
				if($classieraAllowPosts == false){
					?>
					<div class="alert alert-warning" role="alert">
					  <strong><?php esc_html_e('Hello.', 'classiera') ?></strong><?php esc_html_e('You HYST Posts limit are exceeded, Please Purchase a Plan for posting More Ads.', 'classiera') ?>&nbsp;&nbsp;<a class="btn btn-primary btn-sm" href="<?php echo $featured_plans; ?>"><?php esc_html_e('Purchase Plan', 'classiera') ?></a>
					</div>
					<?php
				}elseif($classieraAllowPosts == true){
				$author_id=$userID; 
					$profileIMGID = get_user_meta($author_id, "classify_author_avatar_url", true);
									$profileIMG = classiera_get_profile_img($profileIMGID);
									$authorName = get_the_author_meta('display_name', $author_id);
									$author_verified = get_the_author_meta('author_verified', $author_id);
				?>
				<div class="submit-post section-bg-white manage_list">
				
				       <!--author-info-->
						<div class="author-info">
							<div class="media">
								<?php /*<img class="media-object" src="https://secure.gravatar.com/avatar/b70aa8f893e46deb2adfb69f90dd57d0?s=150&d=mm&r=g"/>*/
								if(empty($profileIMG)){
										$profileIMG = classiera_get_avatar_url ( get_the_author_meta('user_email', $author_id), $size = '150' );?>
										
									<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="max-width:120px; max-height:120px;">
									<?php
									}else{ ?>
									<img class="media-object img-circle author-avatar" src="<?php echo $profileIMG; ?>" alt="<?php echo $authorName; ?>" style="max-width:120px; max-height:120px;">
									<?php } ?>
								<div class="media-body">
								    <hr class="line1"/>
									<h2 class="user_name"><?php echo $authorName;?> : manage lists</h2>
								    <hr class="line1"/>
									<div class="join_dis">
									     <div class="top-buffer2"></div>
									         <?php
													//echo $flg.'<br/>';
													//print_r($error);
													if($error!='')
													{
														for($lp=0;$lp<count($error);$lp++)
														{
															echo '<span style="color:red;">'.$error[$lp].'</span>';
														}
													}
													if((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') && (isset($_REQUEST['edit_id']) && $_REQUEST['edit_id']!=''))
													{
														$get_list=$wpdb->get_results("select * from wp_user_lists where id='".$_REQUEST['edit_id']."'");
														//echo '=>';
														//print_r($get_list);
														//echo $get_list[0]->list_title;
														//echo '<=';?>
													<p>list name</p>
													<form name="frm_list" id="frm_list" method="post" action="">
													        <div class="single_description">
																<div class="inner-addon flex">
																	<input type="text" placeholder="enter text to give your list a name" class="form-control form-control-sm" id="list_title" name="list_title" value="<?php echo $get_list[0]->list_title;?>">
																	<button type="submit" name="btn_save" class="btn btn-primary btn-sm btn-style-one btn-block" value="save">save</button>
																	
																	<input type="hidden" name="edit_id" value="<?php echo $_REQUEST['edit_id'];?>" />
																	<input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
																	<input type="hidden" name="user_id" value="<?php echo $userID;?>" />
																</div>
																<?php
																$is_public=$get_list[0]->is_public;
																$checked='';
																if($is_public==1)
																{
																	$checked='checked="checked"';
																}
																?>
																<?php /*Is Public?<input type="checkbox" name="is_public" style="position: absolute !important;	z-index: 1;	visibility: visible; opacity: 1;" value="1" <?php echo $checked;?>/>*/?>
																<div class="checkbox">	
																	<label class="custom-checkbox-lbl">
																		<input type="checkbox" name="is_public" id="is_public" style="position: absolute !important;	z-index: 1;	visibility: visible; opacity: 1;" value="1" <?php echo $checked;?>/>
																		<i class="fa fa-3x icon-checkbox custom-checkbox-icon"></i>
																	</label>
																	<p><label for="is_public"><?php esc_html_e( 'Is Public?', 'classiera' ); ?></label></p>
																</div>
															</div>
													</form>
													<?php
													}
													else
													{
													?>
													<p>list name</p>
													<form name="frm_list" id="frm_list" method="post" action="">
													        <div class="single_description">
																<div class="inner-addon flex">
																	<input type="text" placeholder="enter text to give your list a name" class="form-control form-control-sm" id="list_title" name="list_title">
																	<button type="submit" name="btn_save" class="btn btn-primary btn-sm btn-style-one btn-block" value="save">save</button>
																	
																	
																	<input type="hidden" name="user_id" value="<?php echo $userID;?>" />
																</div>
																
																<div class="checkbox">	
																	<label class="custom-checkbox-lbl">
																		<input type="checkbox" name="is_public" id="is_public" style="position: absolute !important;	z-index: 1;	visibility: visible; opacity: 1;" value="1" />
																		<i class="fa fa-3x icon-checkbox custom-checkbox-icon"></i>
																	</label>
																	<p><label for="is_public"><?php esc_html_e( 'Is Public?', 'classiera' ); ?></label></p>
																</div>	
																
																
																
																
															</div>
													</form>
													<?php
													}?>
										 <div class="top-buffer2"></div>
									</div>
								    <hr class="line2"/>
									<div class="top-buffer2"></div>
									<div class="join_dis">
									        <p>Your Lists</p>
											
											<div class="list_table">
												<?php
												$sel_lists=$wpdb->get_results("select * from wp_user_lists where user_id='".$userID."'");
												if(count($sel_lists)>0)
												{?>
													<table class="table" cellpadding="0" cellspacing="0">
														<tr>
															<th width="300">Id</th>
															<th width="300">Name</th>
															<th width="200" class="text-right">Action</th>
														</tr>
														<?php
														foreach($sel_lists as $key=>$val)
														{?>
															<tr>
																<td width="300" class="text-left"><?php echo $sel_lists[$key]->id;?></td>
																<td width="300" class="text-left"><?php echo $sel_lists[$key]->list_title;?></td>
																<?php /*<td width="200" class="text-right"><a href="<?php echo site_url();?>/list-items/?list_id=<?php echo $sel_lists[$key]->id;?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/eye.png' ?>"/> VIEW</a>&nbsp;<a href="<?php echo site_url();?>/manage-list/?edit_id=<?php echo $sel_lists[$key]->id;?>&action=edit">Edit</a></td>*/?>
																<td width="200" class="text-right"><a href="<?php echo site_url();?>/list-items/?list_id=<?php echo $sel_lists[$key]->id;?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/eye.png' ?>"/></a>&nbsp;<a href="<?php echo site_url();?>/manage-list/?edit_id=<?php echo $sel_lists[$key]->id;?>&action=edit"><img src="<?=get_template_directory_uri().'/../classiera-child/images/setting36x36-1.png' ?>"/></a>&nbsp;<a href="<?php echo site_url();?>/manage-list/?rm_id=<?php echo $sel_lists[$key]->id;?>&action=remove" onclick="return remove_confirm();"><img src="<?=get_template_directory_uri().'/../classiera-child/images/del-icon-36x36.png' ?>"/></a></td>
															</tr>
															<?php
														}
														?>
													</table>
													<script>
														function remove_confirm()
														{
															var r = confirm("Are you sure you want to remove?");
															if (r == true) {
																return true;
															} else {
																return false;
															}
														} 
													</script>
													<?php
												}
												else
												{
													echo '<h5>You havent created list yet</h5>';
												}	
											
												?>
											</div>
									</div>
									<div class="top-buffer2"></div>
									<?php /*<hr class="line2"/>
									<div class="join_dis">
									    <ul class="list-unstyled text-center contact_icon">
											<li>
												<a href="mailto:kevin@simplecloud.co.za"><img src="https://hyst2.temp.co.za/wp-content/themes/classiera/../classiera-child/images/setting.png"> list settings</a>
											</li>
							            </ul>
									</div>							
									<hr class="line2"/>*/?>
									<div class="top-buffer2"></div>
								</div><!--media-body-->
							</div><!--media-->
						</div><!--author-info-->
				
				
				
				
				
				</div>
				</div><!--submit-post-->
				<?php } ?>
			</div><!--col-lg-9 col-md-8 user-content-heigh-->
		</div><!--row-->
	</div><!--container-->
</section><!--user-pages-->
<?php endwhile; ?>

<?php get_footer(); ?>