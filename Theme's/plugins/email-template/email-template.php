<?php
/*
Plugin Name: Email Template

*/

// Register settings using the Settings API 
 

function wpdocs_register_my_custom_menu_page2(){
    add_menu_page( 
        __( 'Email Template', 'textdomain' ),
        'Email Template',
        'manage_options',
        'emailtemplate',
        'my_custom_menu_page2',
        '',
        6
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page2' );
 
/**
 * Display a custom menu page
 */
function my_custom_menu_page2(){
global $wpdb;
$error_insert=array();
$error_edit=array();
if(isset($_POST['action']) && $_POST['action']=='insert')
{
	if(isset($_POST['email_title']) && $_POST['email_title']=='')
	{
		$error_insert[]='Please insert title.';
	}
	if(isset($_POST['email_content']) && $_POST['email_content']=='')
	{
		$error_insert[]='Please insert content.';
	}
	if(isset($_POST['status']) && $_POST['status']=='')
	{
		$error_insert[]='Please select status.';
	}
	if(empty($error_insert))
	{
		$wpdb->query("insert into wp_email_template(email_title, email_content, `status`) values('".$_POST['email_title']."','".$_POST['email_content']."','".$_POST['status']."')");
		wp_redirect('admin.php?page=emailtemplate&msg=insert');
		exit;
	}
}

if((isset($_POST['action']) && $_POST['action']=='edit') && (isset($_POST['edit_id']) && $_POST['edit_id']!=''))
{
	if(isset($_POST['email_title']) && $_POST['email_title']=='')
	{
		$error_edit[]='Please insert title.';
	}
	if(isset($_POST['email_content']) && $_POST['email_content']=='')
	{
		$error_edit[]='Please insert content.';
	}
	if(isset($_POST['status']) && $_POST['status']=='')
	{
		$error_edit[]='Please select status.';
	}
	if(empty($error_edit))
	{
		$wpdb->query("update wp_email_template set email_title='".$_POST['email_title']."', email_content='".$_POST['email_content']."', `status`='".$_POST['status']."' where id='".$_POST['edit_id']."'");
		wp_redirect('admin.php?page=emailtemplate&msg=edit');
		exit;
	}
}


if(isset($_REQUEST['action']) && $_REQUEST['action']=='insert')
{
?>
	<h2>Email Template</h2>
	<?php
	if(!empty($error_insert))
	{
		for($lp=0;$lp<count($error_insert);$lp++)
		{
			echo '<span style="color:red;">'.$error_insert[$lp].'</span><br/>';
		}
	}
	?>
	
	<form name="frm_email_template" id="frm_email_template" method="post" action="">
	<table class="table table-hover m-0 mails table-actions-bar" width="80%">
		<tr>
			<td>Title</td>
			<td>
			<div id="titlediv">
	<div id="titlewrap">
			<input type="text" name="email_title" id="email_title" value="" style="width:100%; padding:15px;">
			</div>
			</div></td>
		</tr>
		<tr>
			<td>Content</td>
			<td>
			<?php wp_editor('', 'email_content'); ?></td>
		</tr>
		<tr>
			<td>Status</td>
			<td><input type="radio" name="status" value="1" checked="checked">&nbsp;Active<br/>
			<input type="radio" name="status" value="0">Inactive</td>
		</tr>
		<tr>
			<td><input type="hidden" name="action" value="insert"></td>
			<td><input type="submit" name="butnsubmit" value="Save" class="button button-primary button-large"></td>
		</tr>
	</table>
	</form>
<?php
}
else if((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') && (isset($_REQUEST['edit_id']) && $_REQUEST['edit_id']!=''))
{
	$select_template=$wpdb->get_results("select * from wp_email_template where id='".$_REQUEST['edit_id']."'");
	
	if((isset($_POST['action']) && $_POST['action']=='edit') && (isset($_POST['edit_id']) && $_POST['edit_id']!=''))
	{
		$email_title=$_POST['email_title'];
		$email_content=$_POST['email_content'];
		$status=$_POST['status'];
	}
	else
	{
		$email_title=$select_template[0]->email_title;
		$email_content=$select_template[0]->email_content;
		$status=$select_template[0]->status;
	}?>
	<h2>Email Template</h2>
	<?php
	if(!empty($error_edit))
	{
		for($lp=0;$lp<count($error_edit);$lp++)
		{
			echo '<span style="color:red;">'.$error_edit[$lp].'</span><br/>';
		}
	}
	?>
	<form name="frm_email_template" id="frm_email_template" method="post" action="">
	<table style="width:80%">
		<tr>
			<td>Title</td>
			<td><div id="titlediv">
	<div id="titlewrap"><input type="text" name="email_title" id="email_title" value="<?php echo $email_title;?>"  style="width:100%; padding:15px;"></div></div></td>
		</tr>
		<tr>
			<td>Content</td>
			<td><?php wp_editor($email_content, 'email_content'); ?></td>
		</tr>
		<tr>
			<td>Status</td>
			<td>
			<?php
			if($status==1)
			{
				$checked1='checked="checked"';
				$checked0='';
			}
			else
			{
				$checked0='checked="checked"';
				$checked1='';
			}
			?>
			<input type="radio" name="status" value="1" <?php echo $checked1;?>>&nbsp;Active<br/>
			<input type="radio" name="status" value="0" <?php echo $checked0;?>>Inactive</td>
		</tr>
		<tr>
			<td><input type="hidden" name="action" value="edit">
			<input type="hidden" name="edit_id" value="<?php echo $_REQUEST['edit_id'];?>"></td>
			<td><input type="submit" name="butnsubmit" value="Save" class="button button-primary button-large"></td>
		</tr>
	</table>
	</form>
<?php

}
else
{?>
 <link href="<?php echo plugins_url();?>/graphical_analysis/plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />

        <!-- App css -->
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo plugins_url();?>/graphical_analysis/assets/css/responsive.css" rel="stylesheet" type="text/css" />
<h1>Template Listing</h1>
	 <div class="row" style="margin-top:20px;">
		<div class="col-lg-6">
			<div class="card-box">
				<h4 class="m-t-0 header-title"><b>LIST TEMPLATE</b></h4>
				<div class="table-responsive"><?php	

//echo '<h2>List Template</h2>';
	if(isset($_GET['msg']) && $_GET['msg']=='insert')
	{
		echo '<span style="color:green;">Email Template inserted successfully</span><br/>';
	}
	else if(isset($_GET['msg']) && $_GET['msg']=='edit')
	{
		echo '<span style="color:green;">Email Template updated successfully</span><br/>';
	}
	//echo '<a href="admin.php?page=emailtemplate&action=insert">Create New Template</a>';
	$select_template=$wpdb->get_results("select * from wp_email_template");?>
	
	<table class="table table-hover m-0 mails table-actions-bar">
		<tr>
			<th width="100" style="text-align:left;">ID</th>
			<th width="200" style="text-align:left;">Title</th>
			<th width="100" style="text-align:left;">Status</th>
			<th width="100" style="text-align:left;">Edit</th>
		</tr>
		
		<?php
		foreach($select_template as $key=>$val)
		{?>
			<tr><td><h5 class="m-b-0 m-t-0"><?php echo $val->id;?></h5></td>
			<td><h5 class="m-b-0 m-t-0"><?php echo $val->email_title;?></h5></td>
			<td><h5 class="m-b-0 m-t-0"><?php if($val->status==1){echo 'Active';}else{echo 'Inactive';};?></h5></td>
			<td><a href="admin.php?page=emailtemplate&action=edit&edit_id=<?php echo $val->id;?>" class="table-action-btn"><i class="mdi mdi-pencil"></i></a></td></tr>
			<?php
		}?>
		</table>
		</div>
		</div>
		</div>
		</div>
<?php
}

}


?>