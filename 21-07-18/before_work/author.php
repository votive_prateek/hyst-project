<?php
/**
 * Template name: Profile Page
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage classiera
 * @since classiera
 */

global $user_ID;
$author = get_user_by( 'slug', get_query_var( 'author_name' ) ); $user_ID = $author->ID;
$user_info = get_userdata($user_ID);
get_header(); 
//echo "<pre>";print_r($user_info);die();

global $redux_demo;
$currentUser_ID = '';
global $current_user; wp_get_current_user(); $currentUser_ID == $current_user->ID;
$contact_email = get_the_author_meta( 'user_email', $currentUser_ID );
$classieraContactEmailError = $redux_demo['contact-email-error'];
$classieraContactNameError = $redux_demo['contact-name-error'];
$classieraConMsgError = $redux_demo['contact-message-error'];
$classieraContactThankyou = $redux_demo['contact-thankyou-message'];
$classieraAuthorStyle = $redux_demo['classiera_author_page_style'];
$classieraCurrencyTag = $redux_demo['classierapostcurrency'];
$classieraIconsStyle = $redux_demo['classiera_cat_icon_img'];
$classieraAuthorInfo = $redux_demo['classiera_author_contact_info'];
$classieraAdsView = $redux_demo['home-ads-view'];
$classieraItemClass = "item-grid";
if($classieraAdsView == 'list'){
	$classieraItemClass = "item-list";
}
$classieraOnlineCheck = classiera_user_last_online($user_ID);
$UserRegistered = $current_user->user_registered;
$dateFormat = get_option( 'date_format' );
$classieraRegDate = date_i18n($dateFormat,  strtotime($UserRegistered));

$first_name_visible = get_the_author_meta('first_name_visible', $user_info->ID );
$last_name_visible = get_the_author_meta('last_name_visible', $user_info->ID );
$job_visible = get_the_author_meta('job_visible', $user_info->ID );
$location_visible = get_the_author_meta('location_visible', $user_info->ID );
$description_visible = get_the_author_meta('description_visible', $user_info->ID );
$my_interest_visible = get_the_author_meta('my_interest_visible', $user_info->ID );
$phone_visible = get_the_author_meta('phone_visible', $user_info->ID );
$phone2_visible = get_the_author_meta('phone2_visible', $user_info->ID );
$website_visible = get_the_author_meta('website_visible', $user_info->ID );
$email_visible = get_the_author_meta('email_visible', $user_info->ID );
$facebook_visible = get_the_author_meta('facebook_visible', $user_info->ID );
$twitter_visible = get_the_author_meta('twitter_visible', $user_info->ID );
$googleplus_visible = get_the_author_meta('googleplus_visible', $user_info->ID );
$instagram_visible = get_the_author_meta('instagram_visible', $user_info->ID );
$pinterest_visible = get_the_author_meta('pinterest_visible', $user_info->ID );
$linkedin_visible = get_the_author_meta('linkedin_visible', $user_info->ID );
$vimeo_visible = get_the_author_meta('vimeo_visible', $user_info->ID );
$youtube_visible = get_the_author_meta('youtube_visible', $user_info->ID );

global $nameError;
global $emailError;
global $commentError;
global $subjectError;
global $humanTestError;
$caticoncolor="";
$category_icon_code ="";
$category_icon="";
$category_icon_color="";

//If the form is submitted
if(isset($_POST['submitted'])) {
	
		//Check to make sure that the name field is not empty
		if(trim($_POST['contactName']) === '') {
			$nameError = $classieraContactNameError;
			$hasError = true;
		} elseif(trim($_POST['contactName']) === 'Name*') {
			$nameError = $classieraContactNameError;
			$hasError = true;
		}	else {
			$name = trim($_POST['contactName']);
		}

		//Check to make sure that the subject field is not empty
		if(trim($_POST['subject']) === '') {
			$subjectError = $classiera_contact_subject_error;
			$hasError = true;
		} elseif(trim($_POST['subject']) === 'Subject*') {
			$subjectError = $classiera_contact_subject_error;
			$hasError = true;
		}	else {
			$subject = trim($_POST['subject']);
		}
		
		//Check to make sure sure that a valid email address is submitted
		if(trim($_POST['email']) === '')  {
			$emailError = $classieraContactEmailError;
			$hasError = true;
		} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
			$emailError = $classieraContactEmailError;
			$hasError = true;
		} else {
			$email = trim($_POST['email']);
		}
			
		//Check to make sure comments were entered	
		if(trim($_POST['comments']) === '') {
			$commentError = $classieraConMsgError;
			$hasError = true;
		} else {
			if(function_exists('stripslashes')) {
				$comments = stripslashes(trim($_POST['comments']));
			} else {
				$comments = trim($_POST['comments']);
			}
		}

		//Check to make sure that the human test field is not empty
		if(trim($_POST['humanTest']) != '8') {
			$humanTestError = "Not Human :(";
			$hasError = true;
		} else {

		}
			
		//If there is no error, send the email
		if(!isset($hasError)) {

			$emailTo = $contact_email;
			$subject = $subject;	
			$body = "Name: $name \n\nEmail: $email \n\nMessage: $comments";
			$headers = 'From <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;
			
			wp_mail($emailTo, $subject, $body, $headers);

			$emailSent = true;

	}
}

?>
<section class="author-box text-center user_section other_user_page">
	<div class="container border author-box-bg">
		<div class="row">
			<div class="col-lg-12 p15">
			    <div class="bg-gray">
				<div class="row no-gutter removeMargin author-first-row">
				    <div class="details_section">
						<div class="col-lg-12 col-sm-12">
							<div class="author-info">
								<div class="media">
										<?php 
										$classieraAuthorIMGURL = get_user_meta($user_ID, "classify_author_avatar_url", true);
										$classieraAuthorIMGURL = classiera_get_profile_img($classieraAuthorIMGURL);
										$author_verified = get_the_author_meta('author_verified', $user_ID);
										if(empty($classieraAuthorIMGURL)){
											$author_id = get_the_author_meta('user_email', $user_ID);
											$classieraAuthorIMGURL = classiera_get_avatar_url ($author_id, $size = '150' );
										}
									$is_approve_prof_pic=get_user_meta($user_ID,'profile_pic_approve');
									if($is_approve_prof_pic[0]=='1')
									{
										?>
										<img class="media-object center-block" src="<?php echo $classieraAuthorIMGURL; ?>" alt="<?php echo get_the_author_meta('display_name', $user_ID ); ?>">
									<?php
									}
									else
									{?>
										<img class="media-object center-block" src="<?=get_template_directory_uri().'/../classiera-child/images/user-icon.png' ?>" alt="<?php echo get_the_author_meta('display_name', $user_ID ); ?>"><?php
									}?>
									<div class="media-body">
										<hr class="line1"/>
										
										<h2 class="user_name">hello: <?php echo the_author_posts_link(); ?> <?php echo classiera_author_verified($user_ID);?></h2>
										<hr class="line1"/>
										<div class="join_dis">
											 <div class="top-buffer2"></div>
											 <p><?php esc_html_e('joined', 'classiera') ?>&nbsp;<?php echo $classieraRegDate;?></p>
											 <?php if($classieraOnlineCheck == false){?>
											 <p><?php esc_html_e('last log on 11th September 2018', 'classiera') ?></p>
											 <?php } ?>
											 <div class="top-buffer2"></div>
										</div>
										<!-- <hr class="line2"/> -->
																						<?php /* if($classieraOnlineCheck == false){?>
													<span class="offline"><i class="fa fa-circle"></i><?php esc_html_e('last log on 11th September 2018', 'classiera') ?></span>
													<?php }else{ ?>
													<span><i class="fa fa-circle"></i><?php esc_html_e('Online', 'classiera') ?></span>
													<?php } */?>
													
													
													
									</div><!--media-body-->
								</div><!--media-->
							</div><!--author-info-->
						</div><!--col-lg-12-->
					</div>
				</div><!--row-->
				<div class="row no-gutter removeMargin author-second-row">
				   <div class="details_section">
						<?php if ($description_visible=='publically' || $current_user->ID==$author->ID) { ?>
						 <div class="col-lg-12">
						   <div class="top-buffer2"></div>
							<!-- about me -->
							<div class="about-me text-center">
								<h4 class="border0 user-title"><?php esc_html_e("about me:", 'classiera') ?></h4>
								<p style="text-align:center;"><?php $author_desc = get_the_author_meta('description', $user_ID); echo $author_desc; ?></p>
								<div class="top-buffer2"></div>
							</div>
						</div><!--col-lg-12-->
						<?php } ?>
						
						<?php if ($my_interest_visible=='publically' || $current_user->ID==$author->ID) { ?>
						<div class="col-lg-12">
							<!-- about me -->
							<div class="interests about-me hide text-center">
								<div class="top-buffer2"></div>
								<hr class="line2"/>
								<div class="top-buffer2"></div>
								<h4 class="border0 user-title"><?php esc_html_e("interests", 'classiera') ?></h4>
								<p><?php  $interest=get_user_meta($user_ID, 'interests'); //echo $interest[0];
									$interest_exp=explode(",",$interest[0]);
									for($lp=0;$lp<count($interest_exp);$lp++)
									{
									if($lp==0)
									echo $interest_exp[$lp];
									else
									echo ', '.$interest_exp[$lp];
								}?></p>
								<div class="top-buffer2"></div>
							</div>
						</div><!--col-lg-12-->
						<?php } ?>
						
						
						<?php if($phone_visible=='publically' || $phone2_visible=='publically' || $website_visible=='publically' || $email_visible=='publically' || $current_user->ID==$author->ID) { ?>
						<div class="col-lg-12">
							<?php if($classieraAuthorInfo == 1) {
								$userPhone = get_the_author_meta('phone', $user_ID);
								//$userPhone2 = get_the_author_meta('phone2', $user_ID);
								$userWebsite = get_the_author_meta('user_url', $user_ID);
								$userMobile = get_the_author_meta('phone2', $user_ID);
								$userEmail = get_the_author_meta('user_email', $user_ID);
								if(!empty($userPhone) || !empty($userWebsite) || !empty($userMobile) || !empty($userEmail)) { ?>
									<div class="top-buffer2"></div>
									<hr class="line2"/>
									<div class="top-buffer2"></div>
									<div class="author-contact-details contact_icon">
										<h4 class="border0 user-title text-center">
											<?php esc_html_e('Contact Details', 'classiera') ?>
										</h4>
										<?php /*<div class="contact-detail-col">
										<?php if(!empty($userPhone)){?>
										<span>
											<i class="fa fa-phone-square"></i>
											<a href="tel:<?php echo $userPhone; ?>"><?php echo $userPhone; ?></a>
										</span>
										<?php } ?>
										</div><!--contact-detail-col-->*/?>
										<div class="contact-detail-row ">
											<?php if(!empty($userPhone) && $phone_visible=='publically' || $current_user->ID==$author->ID) { ?>
												<div class="contact-detail-col">
														<span>
															<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-3.png'?>"/>
															<a href="tel:<?php echo $userPhone; ?>"><?php echo $userPhone; ?></a>
														</span>
												</div><!--contact-detail-col-->
											<?php } ?>
											<?php if(!empty($userMobile) && $phone2_visible=='publically' || $current_user->ID==$author->ID) { ?>
												<div class="contact-detail-col">
													<span>
														<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-4.png'?>"/>
														<a href="tel:<?php echo $userMobile; ?>"><?php echo $userMobile; ?></a>
													</span>
												</div><!--contact-detail-col-->
											<?php } ?>
											<?php if(!empty($userWebsite) && ($website_visible=='publically' || $current_user->ID==$author->ID)) { ?>
												<div class="contact-detail-col">
													<span>
														<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-1.png'?>"/>
														<a href="<?php echo $userWebsite; ?>"><?php echo $userWebsite; ?></a>
													</span>
												</div><!--contact-detail-col-->
											<?php } ?>
											<?php if(!empty($userEmail) && $email_visible=='publically' || $current_user->ID==$author->ID) { ?>
												<div class="contact-detail-col">
													<span>
														<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-2.png'?>"/>
														<a href="mailto:<?php echo $userEmail; ?>"><?php echo $userEmail; ?></a>
													</span>
												</div><!--contact-detail-col-->
											<?php } ?>
										</div><!--contact-detail-row-->
									</div><!--col-lg-5-->
							<?php
								}
							}?>
						</div><!--row no-gutter removeMargin author-second-row-->
						<?php } ?>
					
					
						<div class="col-lg-12 col-sm-12">
							<?php if($classieraAuthorInfo == 1){
							
								$userFB = $user_info->facebook;
								$userTW = $user_info->twitter;
								$userGoogle = $user_info->googleplus;
								$userPin = $user_info->pinterest;
								$userLin = $user_info->linkedin;
								$userInsta = $user_info->instagram;
								$userVimeo = $user_info->vimeo;
								$userYouTube = $user_info->youtube;
							
							if(!empty($userFB) || !empty($userTW) || !empty($userGoogle) || !empty($userInsta) || !empty($userPin) || !empty($userLin) || !empty($userVimeo) || !empty($userYouTube))
							{?>
							<div class="top-buffer2"></div>
						   <hr class="line3"/>
							<div class="top-buffer2"></div>
							<div class="author-social">
								<h4 class="border0 user-title">
									<?php esc_html_e('our social media links', 'classiera') ?>
								</h4>
								<div class="author-social-icons">
									<?php /*?><ul class="list-unstyled list-inline">
										<?php 
										$userFacebook = $user_info->facebook;
										$usertwitter = $user_info->twitter;
										$usergoogleplus = $user_info->googleplus;
										$userpinterest = $user_info->pinterest; 
										$userlinkedin = $user_info->linkedin;
										$userEmail = $user_info->user_email; 
										$userInsta = $user_info->instagram;	
										?>
										<?php if(!empty($userFacebook)){?>
										<li>
											<a href="<?php echo $userFacebook; ?>">
												<img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/fb.png'?>"/>
											</a>
										</li>
										<?php } ?>
										<?php if(!empty($usertwitter)){?>
										<li>
											<a href="<?php echo $usertwitter; ?>">
												<img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/tw.png'?>"/>
											</a>
										</li>
										<?php } ?>
										<?php if(!empty($usergoogleplus)){?>
										<li>
											<a href="<?php echo $usergoogleplus; ?>">
												<img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/gplus.png'?>"/>
											</a>
										</li>
										<?php } ?>
										<?php if(!empty($userpinterest)){?>
										<li>
											<a href="<?php echo $userpinterest; ?>">
												<img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/pin.png'?>"/>
											</a>
										</li>
										<?php } ?>
										<?php if(!empty($userlinkedin)){?>
										<li>
											<a href="<?php echo $userlinkedin; ?>">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
										<?php } ?>
										<?php if(!empty($userInsta)){?>
										<li>
											<a href="<?php echo $userInsta; ?>">
												<img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/insta.png'?>"/>
											</a>
										</li>
										<?php } ?>
										<?php if(!empty($userEmail)){?>
										<li>
											<a href="mailto:<?php echo get_the_author_meta('user_email', $user_ID); ?>">
												<img src="<?=get_template_directory_uri().'/../classiera-child/images/con-2.png'?>"/>
											</a>
										</li>
										<?php } ?>									
									</ul><?php */?>
									<ul class="list-unstyled list-inline">
							
							<?php
							
							 if(!empty($userFB) && $facebook_visible=='publically' || $current_user->ID==$author->ID){?>
								<li>
									<a href="<?php echo $userFB; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/fb.png'?>"/></a>
								</li>
							<?php } ?>
							<?php if(!empty($userTW) && $twitter_visible=='publically' || $current_user->ID==$author->ID){?>
								<li>
									<a href="<?php echo $userTW; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/tw.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userGoogle) && $googleplus_visible=='publically' || $current_user->ID==$author->ID){?>	
								<li>
									<a href="<?php echo $userGoogle; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/gplus.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userInsta) && $instagram_visible=='publically' || $current_user->ID==$author->ID){?>	
								<li>
									<a href="<?php echo $userInsta; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/insta.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userPin) && $pinterest_visible=='publically' || $current_user->ID==$author->ID){?>	
								<li>
									<a href="<?php echo $userPin; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/pin.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userLin) && $linkedin_visible=='publically' || $current_user->ID==$author->ID){?>	
								<li>
									<a href="<?php echo $userLin; ?>" class="linkin_icon"><img src="<?=get_template_directory_uri().'/../classiera-child/images/linkin.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userVimeo) && $vimeo_visible=='publically' || $current_user->ID==$author->ID){?>	
								<li>
									<a href="<?php echo $userVimeo; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/red.png'?>"/></a>
								</li>
							<?php } ?>	
							<?php if(!empty($userYouTube) && $youtube_visible=='publically' || $current_user->ID==$author->ID){?>	
								<li>
									<a href="<?php echo $userYouTube; ?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/social_icon/wl.png'?>"/></a>
								</li>
							<?php } ?>	
							</ul><!--list-unstyled-->
								</div><!--author-social-icons-->
							</div><!--author-social-->
							<?php
							} }else{ ?>
								&nbsp;
							<?php } ?>
						</div><!--col-lg-12 col-sm-12-->

						<div class="clearfix"></div>
						<div class="top-buffer2"></div>
						<hr class="line3"/>
						<div class="clearfix"></div>
					</div>	 
						 
						 
					
                 <?php
					$branch_count=get_user_meta($user_ID, "branch_count");
					if(count($branch_count) > 0 && ($location_visible=='publically' || $current_user->ID==$author->ID)){
				 ?>
					<div class="branch-section"> 
					    <div class="top-buffer2"></div>
						<div class="single_title">
							<h2 class="user-detail-section-heading">our branches</h2>
						</div>	 
						
						<div class="top-buffer2"></div>
						<div class="clearfix"></div>
						<div class="col-md-10 col-md-offset-1">
							<?php 	
									for($lp=1;$lp<=$branch_count[0];$lp++)
									{?>
										<div class="col-sm-6">
											 <div class="our_branch">
												<a href="#">
													<p><img src="<?=get_template_directory_uri().'/../classiera-child/images/branch_icon.png' ?>" height="85px" width="85px"/></p>
													   <?php $branch_location=get_user_meta($user_ID, "branch_location".$lp); ?>											   
													 <p><span><?php echo $branch_location[0];?></span></p>
												</a>
											</div>
										</div>
										<?php
									}
								
								?>
						</div>
					</div>
					<?php } ?>
					 
					<div class="clearfix"></div>
					<div class="top-buffer2"></div>	
                   
					
					<?php if($classieraAuthorStyle == 'fullwidth'){?>
										<section class="inner-page-content">
											<section class="classiera-advertisement advertisement-v1">
												<div class="tab-divs section-light-bg">
													<div class="view-head pad0 single_title">
														<div class="container">
															<div class="row">
																<div class="col-lg-12 col-sm-12 col-xs-12">
																	<div class="total-post">
																		<h2><?php
																		if(count_user_posts($user_ID)>1)
																		{ 
																		esc_html_e( 'Total hysts', 'classiera' );
																		}
																		else
																		{
																			esc_html_e( 'Total hysts', 'classiera' );
																		} ?>: 
																			<span>
																			<?php echo count_user_posts($user_ID);?>&nbsp;
																			<?php //esc_html_e( 'Hysts Posted', 'classiera' ); ?>
																			</span>
																		</h2>
																	</div><!--total-post-->
																</div><!--col-lg-6 col-sm-6 col-xs-6-->
																
																
																<div class="col-lg-6 col-sm-6 col-xs-6 hide">
																	<div class="view-as text-right flip">
																		<span><?php esc_html_e( 'View As', 'classiera' ); ?>:</span>
																		<a id="grid" class="grid btn btn-sm sharp outline <?php if($classieraAdsView == 'grid'){ echo "active"; }?>" href="#"><i class="fa fa-th"></i></a>
																		<a id="list" class="list btn btn-sm sharp outline <?php if($classieraAdsView == 'list'){ echo "active"; }?>" href="#"><i class="fa fa-bars"></i></a>
																	</div><!--view-as text-right flip-->
																</div><!--col-lg-6 col-sm-6 col-xs-6-->
															</div><!--row-->
															
														</div><!--container-->
													</div><!--view-head-->
													
													<div class="top-buffer2"></div>
													
													<div class="tab-content">
														<div role="tabpanel" class="tab-pane fade in active" id="all">
															<div class="authors-view">
																<?php 
																	global $paged, $wp_query, $wp;
																	$args = wp_parse_args($wp->matched_query);
																	if ( !empty ( $args['paged'] ) && 0 == $paged ){
																		$wp_query->set('paged', $args['paged']);
																		$paged = $args['paged'];
																	}
																	$cat_id = get_cat_ID(single_cat_title('', false));
																	$temp = $wp_query;
																	$wp_query= null;
																	$wp_query = new WP_Query();
																	$wp_query->query('post_type=post&posts_per_page=12&paged='.$paged.'&cat='.$cat_id.'&author='.$user_ID);
																while ($wp_query->have_posts()) : $wp_query->the_post();
																	get_template_part( 'templates/classiera-loops/loop-lime');
																endwhile; 
																?>
																<?php
																if( function_exists('classiera_pagination') ){
																	classiera_pagination();
																  }
																?>
																
																
																
																
																<?php
																	$sel_lists=$wpdb->get_results("select * from wp_user_lists where user_id='".$user_ID."'");
																	if(count($sel_lists)>0)
																	{?>
																        <div class="clearfix"></div>
																        <div class="single_title">
																		    <h2>Lists</h2>
																		</div>
																        <div class="top-buffer2"></div>
																		<div class="list_table">
																				<table class="table" cellpadding="0" cellspacing="0">
																					<tr>
																						<th width="300">Id</th>
																						<th width="300">Name</th>
																						<th width="200">Action</th>
																					</tr>
																					<?php
																					foreach($sel_lists as $key=>$val)
																					{?>
																						<tr>
																							<td width="300" class="text-left"><?php echo $sel_lists[$key]->id;?></td>
																							<td width="300" class="text-left"><?php echo $sel_lists[$key]->list_title;?></td>
																							<td width="200" class="text-center"><a href="<?php echo site_url();?>/list-items/?list_id=<?php echo $sel_lists[$key]->id;?>"><img src="<?=get_template_directory_uri().'/../classiera-child/images/eye.png' ?>"/> VIEW</a></td>
																						</tr>
																						<?php
																					}
																					?>
																				</table>
																		</div>
																		<?php
																	}
																	else
																	{
																		//echo '<h5>You havent created list yet</h5>';
																	}	
																?>
															</div><!--container-->
															
														</div><!--tabpanel-->
														
														
																<?php wp_reset_query(); ?>
													</div><!--tab-content-->
												</div><!--tab-divs section-light-bg-->
											</section><!--classiera-advertisement advertisement-v1-->
										</section><!--inner-page-content-->
										<?php }elseif($classieraAuthorStyle == 'sidebar'){?>
										<section class="inner-page-content border-bottom top-pad-50">
											<div class="">
												<div class="row">
													<div class="col-md-8 col-lg-9">
														<section class="classiera-advertisement advertisement-v1">
															<div class="tab-divs section-light-bg">
																<div class="view-head">
																	<div class="">
																		<div class="row">
																			<div class="col-lg-6 col-sm-6 col-xs-6">
																				<div class="total-post">
																					<p><?php esc_html_e( 'Total Hysts', 'classiera' ); ?>: 
																						<span>
																						<?php echo count_user_posts($user_ID);?>&nbsp;
																						<?php esc_html_e( 'Hysts Posted', 'classiera' ); ?>
																						</span>
																					</p>
																				</div><!--total-post-->
																			</div><!--col-lg-6 col-sm-6 col-xs-6-->
																			<div class="col-lg-6 col-sm-6 col-xs-6">
																				<div class="view-as text-right flip">
																					<span><?php esc_html_e( 'View As', 'classiera' ); ?>:</span>
																					<a id="grid" class="grid btn btn-sm sharp outline <?php if($classieraAdsView == 'grid'){ echo "active"; }?>" href="#"><i class="fa fa-th"></i></a>
																					<a id="list" class="list btn btn-sm sharp outline <?php if($classieraAdsView == 'list'){ echo "active"; }?>" href="#"><i class="fa fa-bars"></i></a>
																				</div><!--view-as text-right flip-->
																			</div><!--col-lg-6 col-sm-6 col-xs-6-->
																		</div><!--row-->
																	</div><!--container-->
																	
																	
																</div><!--view-head-->
																<div class="tab-content">
																	<div role="tabpanel" class="tab-pane fade in active" id="all">
																		<div class="container">
																			<div class="row">
																			<?php 
																			global $paged, $wp_query, $wp;
																			$args = wp_parse_args($wp->matched_query);
																			if ( !empty ( $args['paged'] ) && 0 == $paged ){
																				$wp_query->set('paged', $args['paged']);
																				$paged = $args['paged'];
																			}
																			$cat_id = get_cat_ID(single_cat_title('', false));
																			$temp = $wp_query;
																			$wp_query= null;
																			$wp_query = new WP_Query();
																			$wp_query->query('post_type=post&posts_per_page=12&paged='.$paged.'&cat='.$cat_id.'&author='.$user_ID);
																			while ($wp_query->have_posts()) : $wp_query->the_post();
																				get_template_part( 'templates/classiera-loops/loop-lime');
																			endwhile; 
																			?>
																			</div><!--row-->
																		</div><!--container-->
																	</div><!--tabpanel-->
																</div><!--tab-content-->
															</div><!--tab-divs section-light-bg-->
														</section><!--classiera-advertisement advertisement-v1-->
														<?php
														  if ( function_exists('classiera_pagination') ){
															classiera_pagination();
														  }
														?>
														<?php wp_reset_query(); ?>
													</div><!--col-md-8 col-lg-9-->
													<!--Sidebar-->
													<div class="col-md-4 col-lg-3">
														<aside class="sidebar">
															<div class="row">
																<?php get_sidebar('pages'); ?>
															</div>
														</aside>
													</div>
													<!--Sidebar-->
												</div><!--row-->
											</div><!--container-->
										</section>
										<?php } ?>
										<!-- Company Section Start-->
				</div>
			</div><!--col-lg-12-->
		</div><!--row-->
	</div><!--container border author-box-bg-->
</section><!--author-box-->
<?php 
	global $redux_demo; 
	$classieraCompany = $redux_demo['partners-on'];
	$classieraPartnersStyle = $redux_demo['classiera_partners_style'];
	if($classieraCompany == 1){
		if($classieraPartnersStyle == 1){
			get_template_part('templates/members/memberv1');
		}elseif($classieraPartnersStyle == 2){
			get_template_part('templates/members/memberv2');
		}elseif($classieraPartnersStyle == 3){
			get_template_part('templates/members/memberv3');
		}elseif($classieraPartnersStyle == 4){
			get_template_part('templates/members/memberv4');
		}elseif($classieraPartnersStyle == 5){
			get_template_part('templates/members/memberv5');
		}elseif($classieraPartnersStyle == 6){
			get_template_part('templates/members/memberv6');
		}
	}
?>
<!-- Company Section End-->	
<?php get_footer(); ?>