<?php 
	global $redux_demo;
	$classieraDisplayName = '';
	$templateProfile = '';
	$templateAllAds = '';
	$templateEditPost = '';
	$templateSubmitAd = '';
	$templateFollow = '';
	$templatePlans = '';
	$templateFavourite = '';
	$current_user = wp_get_current_user();
	$user_ID = $current_user->ID;
	$classieraAuthorEmail = $current_user->user_email;
	$classieraDisplayName = $current_user->display_name;
	if(empty($classieraDisplayName)){
		$classieraDisplayName = $current_user->user_nicename;
	}
	if(empty($classieraDisplayName)){
		$classieraDisplayName = $current_user->user_login;
	}
	$classieraAuthorIMG = get_user_meta($user_ID, "classify_author_avatar_url", true);
	$classieraAuthorIMG = classiera_get_profile_img($classieraAuthorIMG);
	if(empty($classieraAuthorIMG)){
		$classieraAuthorIMG = classiera_get_avatar_url ($classieraAuthorEmail, $size = '150' );
	}	
	$classieraOnlineCheck = classiera_user_last_online($user_ID);
	$UserRegistered = $current_user->user_registered;
	$dateFormat = get_option( 'date_format' );
	$classieraRegDate = date_i18n($dateFormat,  strtotime($UserRegistered));
	$classieraProfile = $redux_demo['profile'];
	$classieraAllAds = $redux_demo['all-ads'];
	$classieraEditProfile = $redux_demo['edit'];
	$classieraPostAds = $redux_demo['new_post'];
	$classieraInbox = $redux_demo['classiera_inbox_page_url'];
	$classieraFollowerPage = $redux_demo['classiera_user_follow'];
	$classieraUserPlansPage = $redux_demo['classiera_single_user_plans'];
	$classieraUserFavourite = $redux_demo['all-favourite'];
	$classiera_bid_system = $redux_demo['classiera_bid_system'];
	if (function_exists('icl_object_id')){ 		
		$templateProfile = 'template-profile.php';
		$templateAllAds = 'template-user-all-ads.php';
		$templateEditProfile = 'template-edit-profile.php';
		$templateSubmitAd = 'template-submit-ads.php';
		$templateFollow = 'template-follow.php';
		$templatePlans = 'template-user-plans.php';
		$templateFavourite = 'template-favorite.php';
		$templateMessage = 'template-message.php';
		
		$classieraProfile = classiera_get_template_url($templateProfile);
		$classieraAllAds = classiera_get_template_url($templateAllAds);
		$classieraEditProfile = classiera_get_template_url($templateEditProfile);
		$classieraPostAds = classiera_get_template_url($templateSubmitAd);
		$classieraFollowerPage = classiera_get_template_url($templateFollow);
		$classieraUserPlansPage = classiera_get_template_url($templatePlans);
		$classieraUserFavourite = classiera_get_template_url($templateFavourite);
		$classieraInbox = classiera_get_template_url($templateMessage);
	}
?>

<aside id="sideBarAffix" class="section-bg-white">
    <div class="bg-gray">
		<div class="author-info border-bottom pad0">
			<div class="media text-center">
			     
				<div class="top-buffer1"></div>
				<?php /*<img class="media-object" src="<?php echo $classieraAuthorIMG; ?>" alt="<?php echo $classieraDisplayName;  ?>">*/?>
				<?php
				$is_approve_prof_pic=get_user_meta($user_ID,'profile_pic_approve');
				if($is_approve_prof_pic[0]=='1')
				{?>
				<img class="media-object" src="<?php echo $classieraAuthorIMG; ?>" alt="<?php echo $classieraDisplayName;  ?>">
				<?php
				}
				else
				{
				?><img src="<?=get_template_directory_uri().'/../classiera-child/images/user-icon.png' ?>"  alt="<?php echo $classieraDisplayName;  ?>"/><?php
				}?>
			    <div class="top-buffer1"></div>
				<div class="media-body">
				    <hr class="line1">
					<h5 class="user_name">
					<?php echo $classieraDisplayName; ?>
						<?php echo classiera_author_verified($user_ID);?>
					</h5>
					<hr class="line1"/>
			        <div class="top-buffer1"></div>
					
					<p><?php esc_html_e('Member Since', 'classiera') ?>&nbsp;<?php echo $classieraRegDate;?></p>
					<?php if($classieraOnlineCheck == false){?>
					<span class="offline"><i class="fa fa-circle"></i><?php esc_html_e('Offline', 'classiera') ?></span>
					<?php }else{ ?>
					<span><i class="fa fa-circle"></i><?php esc_html_e('Online', 'classiera') ?></span>
					<?php } ?>
					
			        <div class="top-buffer1"></div>
					<hr class="line1 temp"/>
			        <div class="top-buffer1"></div>
					
					<!--<p class="sidebar_title"><?php //esc_html_e("about me", 'classiera') ?></p>
					<p class="justify"><?php //$user_id = $current_user->ID; $author_desc = get_the_author_meta('description', $user_id); echo $author_desc; ?></p> -->
					 
					
				</div><!--media-body-->
			</div><!--media-->
		</div><!--author-info-->
		<!--<hr class="line2"/>-->
		<div class="top-buffer1"></div>
	<div class="temp_class">	
		<ul class="user-page-list list-unstyled margin0 pad0">
			
			<li class="<?php if(is_page_template( 'template-user-all-ads.php' )){echo "active";}?>">
				<a href="<?php echo $classieraAllAds; ?>">
					<span><img src="<?=get_template_directory_uri().'/../classiera-child/images/nav-1.png'?>" height="50px" width="50px"/><?php
					
					$user = wp_get_current_user();
					$roles = $user->roles;
					if(in_array('inventory_user',$roles))
					{
						esc_html_e("My INVENTORY ", 'classiera');
					}
					else if(in_array('company_user',$roles))
					{
						esc_html_e("My INVENTORY ", 'classiera');
					}
					else
					{
						esc_html_e("My HYSTS ", 'classiera');
					}  ?></span>
					<span class="in-count pull-right flip hide"><?php echo count_user_posts($user_ID);?></span>
				</a>
			</li><!--My Ads-->
			<li class="<?php if(is_page_template( 'template-favorite.php' )){echo "active";}?>">
				<a href="<?php echo $classieraUserFavourite; ?>">
					<span><img src="<?=get_template_directory_uri().'/../classiera-child/images/nav-2.png'?>" height="50px" width="50px"/><?php esc_html_e("Favorite HYSTS ", 'classiera') ?></span>
					<span class="in-count pull-right flip hide">
						<?php 
							global $current_user;
							wp_get_current_user();
							$user_id = $current_user->ID;
							$myarray = classiera_authors_all_favorite($user_id);
							if(!empty($myarray)){
								$args = array(
								   'post_type' => 'post',
								   'post__in'      => $myarray
								);
							$wp_query = new WP_Query( $args );
							$current = -1;
							$current2 = 0;
							while ($wp_query->have_posts()) : $wp_query->the_post(); $current++; $current2++; 													
							endwhile;
							echo $current2;
							wp_reset_query();
							}else{
								echo "0";
							}
						?>
					</span>
				</a>
			</li><!--Watch later Ads-->
		
			<li class="<?php if(is_page_template( 'template-edit-profile.php' )){echo "active";}?>">
				<?php
				if(in_array('inventory_user',$roles))
				{?>
					<a href="<?php echo site_url().'/profile-settings-2/'; ?>"><?php
				}
				else if(in_array('company_user',$roles))
				{?>
					<a href="<?php echo site_url().'/profile-settings-2/'; ?>"><?php
				}
				else
				{?>
					<a href="<?php echo $classieraEditProfile; ?>"><?php
				}
				?>
				
					<span><img src="<?=get_template_directory_uri().'/../classiera-child/images/nav-3.png'?>" height="50px" width="50px"/><?php esc_html_e("Edit Profile", 'classiera') ?></span>
				</a>
			</li><!--Profile Setting-->
			<?php
			if(in_array('inventory_user',$roles))
			{?>
				<li class="">
					<a href="<?php echo site_url();?>/create-company-user/"><span><img src="<?=get_template_directory_uri().'/../classiera-child/images/add_user.png'?>" height="50px" width="50px"/>Create User</span></a>
				</li><?php
				
			}
			?>
			<?php
			if(in_array('inventory_user',$roles))
			{?>
				<li class="">
					<a href="<?php echo site_url();?>/import-inventory/"><span><img src="<?=get_template_directory_uri().'/../classiera-child/images/import.png'?>" height="50px" width="50px"/>Import Inventory</span></a>
				</li><?php
				
			}
			?>
			<?php
			if(in_array('company_user',$roles))
			{?>
				<li class="">
					<a href="<?php echo site_url();?>/import-inventory/"><span><img src="<?=get_template_directory_uri().'/../classiera-child/images/import.png'?>" height="50px" width="50px"/>Import Inventory</span></a>
				</li><?php
				
			}
			?>
			
			
			<li class=""><a href="<?php echo site_url();?>/manage-list/"><span><img src="<?=get_template_directory_uri().'/../classiera-child/images/nav-4.png'?>" height="50px" width="50px"/>Manage List</span></a></li>
			
			
			<li class="">
			         
					 
					 <?php
						if(in_array('inventory_user',$roles))
							{?>
								<a href="<?php echo site_url().'/create-inventory/'; ?>" ><span><img src="<?=get_template_directory_uri().'/../classiera-child/images/nav-5.png'?>" height="50px" width="50px"/>
							<?php esc_html_e("add NEW INVENTORY", 'classiera') ?></span>
						</a><?php
							}
							else if(in_array('company_user',$roles))
							{?>
								<a href="<?php echo site_url().'/create-inventory/'; ?>"><span><img src="<?=get_template_directory_uri().'/../classiera-child/images/nav-5.png'?>" height="50px" width="50px"/>
							<?php esc_html_e("add NEW INVENTORY", 'classiera') ?></span>
						</a><?php
							} 
							else
							{?>
								<a href="http://hyst2.temp.co.za/submit-new-hyst/"><span><img src="<?=get_template_directory_uri().'/../classiera-child/images/nav-5.png'?>" height="50px" width="50px"/>
							<?php esc_html_e("add NEW HYST", 'classiera') ?></span>
						</a><?php
							}
					?>			
			</li>
			
			
			<li>
				<a href="<?php echo wp_logout_url(get_option('siteurl')); ?>">
					<span><img src="<?=get_template_directory_uri().'/../classiera-child/images/nav-6.png'?>" height="50px" width="50px"/><?php esc_html_e("Log-out", 'classiera') ?></span>
				</a>
			</li><!--Logout-->
		</ul><!--user-page-list-->

	</div>	
</aside><!--sideBarAffix-->